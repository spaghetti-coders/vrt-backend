﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VRT.Core.HostedServices.Notifications
{
    /// <summary>
    /// Provides functions for sending email notifications.
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Sends a plain text email notification to the specified recipients.
        /// </summary>
        /// <param name="subject">The message's subject line</param>
        /// <param name="body">The message's body</param>
        /// <param name="recipients">The message's recipients</param>
        /// <exception cref="ValidationException">if any of the given parameters are invalid</exception>
        /// <exception cref="EmailServiceException">if an error occurred connecting to the SMTP server or during mail delivery</exception>
        Task SendPlainMail(string subject, string body, List<string> recipients);
        /// <summary>
        /// Sends an HTML email notification to the specified recipients.
        /// </summary>
        /// <param name="subject">The message's subject line</param>
        /// <param name="body">The message's body (HTML)</param>
        /// <param name="recipients">The message's recipients</param>
        /// <exception cref="ValidationException">if any of the given parameters are invalid</exception>
        /// <exception cref="EmailServiceException">if an error occurred connecting to the SMTP server or during mail delivery</exception>
        Task SendHtmlMail(string subject, string body, List<string> recipients);
    }
}
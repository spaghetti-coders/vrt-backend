﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.Extensions;
using VRT.Data.Entities;

namespace VRT.Core.HostedServices.Notifications
{
    /// <summary>
    /// Provides functions for sending email notifications.
    /// </summary>
    public class EmailService : IEmailService
    {
        #region Fields

        private EmailConfiguration _config;
        private ILogger<EmailService> _logger;

        #endregion


        /// <summary>
        /// Constructs a new EmailService object.
        /// </summary>
        /// <param name="config">An EmailConfiguration reference</param>
        /// <param name="logger">An ILogger reference</param>
        public EmailService(EmailConfiguration config, ILogger<EmailService> logger)
        {
            _config = config;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Sends a plain text email notification to the specified recipients.
        /// </summary>
        /// <param name="subject">The message's subject line</param>
        /// <param name="body">The message's body</param>
        /// <param name="recipients">The message's recipients</param>
        /// <exception cref="ValidationException">if any of the given parameters are invalid</exception>
        /// <exception cref="EmailServiceException">if an error occurred connecting to the SMTP server or during mail delivery</exception>
        public async Task SendPlainMail(string subject, string body, List<string> recipients)
        {
            // Validate arguments
            if (string.IsNullOrEmpty(subject))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(subject)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(body))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(body)}' cannot be null or empty.");
            if (recipients == null || recipients.Count == 0)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(recipients)}' must contain at least one valid email address.");


            // Build message
            var bb = new BodyBuilder { TextBody = body };
            var mailBody = bb.ToMessageBody();

            var message = new MimeMessage();
            message.Subject = subject;
            message.Body = mailBody;

            // Send mail 
            await SendMail(message, recipients);
        }
        /// <summary>
        /// Sends an HTML email notification to the specified recipients.
        /// </summary>
        /// <param name="subject">The message's subject line</param>
        /// <param name="body">The message's body (HTML)</param>
        /// <param name="recipients">The message's recipients</param>
        /// <exception cref="ValidationException">if any of the given parameters are invalid</exception>
        /// <exception cref="EmailServiceException">if an error occurred connecting to the SMTP server or during mail delivery</exception>
        public async Task SendHtmlMail(string subject, string body, List<string> recipients)
        {
            // Validate arguments
            if (string.IsNullOrEmpty(subject))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(subject)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(body))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(body)}' cannot be null or empty.");
            if (recipients == null || recipients.Count == 0)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(recipients)}' must contain at least one valid email address.");


            // Build message
            var bb = new BodyBuilder { HtmlBody = body };
            var mailBody = bb.ToMessageBody();

            var message = new MimeMessage();
            message.Subject = subject;
            message.Body = mailBody;

            // Send mail 
            await SendMail(message, recipients);
        }

        /// <summary>
        /// Sends the given MIME message to the given list of recipients. 
        /// Recipients will be sliced into batches of the configured size, 
        /// and notification(s) will be sent to them using the BCC field to preserve privacy.
        /// </summary>
        /// <param name="message">A MimeMessage object with subject and body set</param>
        /// <param name="recipients">A list of recipients to whom this messages will be send</param>
        /// <returns></returns>
        private async Task SendMail(MimeMessage message, List<string> recipients)
        {
            _logger.LogInformation($"Sending mail notification to {recipients.Count} recipient(s) ...");

            // Set sender address
            message.From.Add(new MailboxAddress(_config.SenderName, _config.SenderAddress));

            try
            {
                // Create new SMTP client
                using (var client = new SmtpClient())
                {
                    // Connect and authenticate
                    await client.ConnectAsync(_config.Host, _config.Port, _config.UseTLS);
                    await client.AuthenticateAsync(_config.Username, _config.Password);

                    // Slice recipients up into batches of configured size
                    foreach (var batch in recipients.Slice(_config.RecipientsBatchSize))
                    {
                        // Add recipients to message
                        foreach (var recipient in batch)
                        {
                            try
                            {
                                // use BCC field fro privacy 
                                message.Bcc.Add(MailboxAddress.Parse(recipient));
                            }
                            catch (Exception)
                            {
                                _logger.LogWarning($"The recipient address '{recipient}' is invalid and will be skipped.");
                            }
                        }

                        try
                        {
                            await client.SendAsync(message);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "An error occurred sending an email.");
                            throw new EmailServiceException(Messages.Errors.SmtpSendError.Code, Messages.Errors.SmtpSendError.Message);
                        }

                        // Clear message recipients for next batch
                        message.Bcc.Clear();
                    }

                    // Disconnect client
                    await client.DisconnectAsync(true);

                    _logger.LogInformation("Finished sending mail notificaion(s).");
                }
            }
            catch (Exception ex)
            {
                // Error establishing a connection or during authentication
                _logger.LogError(ex, "An unexpected error occurred connecting to the configured mail server.");
                throw new EmailServiceException(Messages.Errors.SmtpConnectionError.Code, Messages.Errors.SmtpConnectionError.Message);
            }
        }

        #endregion
    }
}

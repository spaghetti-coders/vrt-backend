﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VRT.Core.Models;

namespace VRT.Core.HostedServices.Notifications
{
    /// <summary>
    /// Defines the queue for the application's NotificationService/DistributionService.
    /// </summary>
    public interface INotificationQueue
    {
        /// <summary>
        /// Add a new notification to the queue.
        /// </summary>
        /// <param name="notification">The notification to add</param>
        void Add(Notification notification);
        /// <summary>
        /// Removes the next notification from the queue.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token</param>
        /// <returns>The next notification in the queue</returns>
        Task<Notification> Remove(CancellationToken cancellationToken);
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using VRT.Core.Configurations;
using VRT.Core.Models;
using WebPush;

namespace VRT.Core.HostedServices.Notifications
{
    /// <summary>
    /// A background service responsible for sending push and email notifications.
    /// </summary>
    public class DeliveryService : BackgroundService
    {
        #region Fields

        private readonly ILogger<DeliveryService> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly WebPushConfiguration _wpConfig;

        private readonly VapidDetails _vapidDetails;
        private readonly WebPushClient _wpClient;

        #endregion

        public INotificationQueue EventQueue { get; }


        /// <summary>
        /// Constructs a new DeliveryService object.
        /// </summary>
        /// <param name="notificationQueue">The service's event queue</param>
        /// <param name="wpConfig">A reference to the WebPushConfiguration</param>
        /// <param name="scopeFactory">An IServiceScopeFactory reference</param>
        /// <param name="logger">An ILogger reference</param>
        public DeliveryService(INotificationQueue notificationQueue, WebPushConfiguration wpConfig, IServiceScopeFactory scopeFactory, ILogger<DeliveryService> logger)
        {
            EventQueue = notificationQueue;
            _scopeFactory = scopeFactory;
            _wpConfig = wpConfig;
            _logger = logger;

            _vapidDetails = new VapidDetails(_wpConfig.VAPIDSubject, _wpConfig.VAPIDPublicKey, _wpConfig.VAPIDPrivateKey);
            _wpClient = new WebPushClient();

            _logger.LogInformation("Notification Delivery Service initialized.");
        }


        #region Methods 

        /// <summary>
        /// Implements the services core loop.
        /// </summary>
        /// <param name="stoppingToken">A cancellation token</param>
        /// <returns>a task to be awaited</returns>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Notification Delivery Service is starting ...");

            while (!stoppingToken.IsCancellationRequested)
            {
                // Get next event ...
                var notification = await EventQueue.Remove(stoppingToken);

                try
                {
                    // and process it 
                    if (notification.EmailRecipients != null && notification.EmailRecipients.Count > 0)
                        await SendEmailNotification(notification.EmailRecipients, notification.EmailSubject, notification.EmailBody, notification.EmailIsHtml);

                    if (notification.PushSubscriptions != null && notification.PushSubscriptions.Count > 0)
                        await SendWebPushNotification(notification.PushSubscriptions, notification.PushPayload);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "An unexpected error occurred processing a notification event.");
                }
            }

            _logger.LogInformation("Notification Delivery Service is stopping ...");
        }
        
        /// <summary>
        /// Tries to send an email notification to the specified recipients.
        /// </summary>
        /// <param name="recipients">The recipients to whom this notification will be sent</param>
        /// <param name="subject">The email's subject line</param>
        /// <param name="body">The email's body</param>
        /// <param name="isHtml">Indicatates whether the body is HTML (true) or plain text (false)</param>
        /// <returns>a taas to be awaited</returns>
        private async Task SendEmailNotification(List<string> recipients, string subject, string body, bool isHtml)
        {
            _logger.LogInformation($"Sending email notificiation(s) to {recipients.Count} recipients ...");

            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    // Retrieve email service reference
                    var emailService = scope.ServiceProvider.GetRequiredService<IEmailService>();

                    if (isHtml)
                        await emailService.SendHtmlMail(subject, body, recipients);
                    else
                        await emailService.SendPlainMail(subject, body, recipients);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An unexpected error occurred sending email notifications.");
            }

            _logger.LogInformation("Finished sending email notifications.");
        }
        /// <summary>
        /// Tries to send a push notification to all users with an active push subscription.
        /// </summary>
        /// <param name="subscriptions">The subscribers to whom to sent the notifications</param>
        /// <param name="message">The message to be sent</param>
        /// <returns>a task to be awaited</returns>
        private async Task SendWebPushNotification(List<Data.Entities.PushSubscription> subscriptions, PushPayload payload)
        {
            _logger.LogInformation($"Sending push notification(s) to {subscriptions.Count} subscribers ...");

            foreach (var subscription in subscriptions)
            {
                try
                {
                    var sub = new WebPush.PushSubscription(subscription.Endpoint, subscription.P256DH, subscription.Auth);
                    var msg = JsonSerializer.Serialize(payload);

                    await _wpClient.SendNotificationAsync(sub, msg, _vapidDetails);
                }
                catch (WebPushException ex)
                {
                    _logger.LogError(ex, $"An error occurred sending a push notification to the user with ID={subscription.UserId}.");
                }
            }

            _logger.LogInformation("Finished sending push notifications.");
        }
        
        #endregion
    }
}

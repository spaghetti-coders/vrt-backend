﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VRT.Core.Models;

namespace VRT.Core.HostedServices.Notifications
{
    /// <summary>
    /// The default implementation of the INotificationQueue interface.
    /// </summary>
    public class NotificationQueue : INotificationQueue
    {
        #region Fields

        private ConcurrentQueue<Notification> _queue = new ConcurrentQueue<Notification>();
        private SemaphoreSlim _signal = new SemaphoreSlim(0);

        #endregion

        #region Methods

        /// <summary>
        /// Add a new notification to the queue.
        /// </summary>
        /// <param name="notification">The notification to add</param>
        public void Add(Notification notificationEvent)
        {
            if (notificationEvent == null)
                throw new ArgumentException(nameof(notificationEvent));

            _queue.Enqueue(notificationEvent);
            _signal.Release();
        }
        /// <summary>
        /// Removes the next notification from the queue.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token</param>
        /// <returns>The next notification in the queue</returns>
        public async Task<Notification> Remove(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _queue.TryDequeue(out var notificationEvent);

            return notificationEvent;
        }


        #endregion
    }
}

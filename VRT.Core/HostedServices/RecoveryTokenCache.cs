﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VRT.Data;

namespace VRT.Core.HostedServices
{
    /// <summary>
    /// A hosted service for periodically removing expired recovery tokens from the database.
    /// </summary>
    public class RecoveryTokenCache : IHostedService, IDisposable
    {
        #region Fields

        private readonly ILogger<RecoveryTokenCache> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private Timer _timer;

        #endregion


        /// <summary>
        /// Constructs a new RecoveryTokenCache object.
        /// </summary>
        /// <param name="context">A VRTContext reference</param>
        /// <param name="logger">An ILogger reference</param>
        public RecoveryTokenCache(IServiceScopeFactory scopeFactory, ILogger<RecoveryTokenCache> logger)
        {
            _scopeFactory = scopeFactory;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A task to be awaited</returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting recovery token cache service ...");

            // Remove expired tokens from cache every hour
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromHours(1));

            return Task.CompletedTask;
        }
        /// <summary>
        /// Stops the service.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A task to be awaited</returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping recovery token cache service ...");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
        /// <summary>
        /// Disposes this service and any of its allocated resources from memory.
        /// </summary>
        public void Dispose()
        {
            _timer?.Dispose();

            _logger.LogInformation("Recovery token cache service stopped.");
        }

        /// <summary>
        /// Contains the service's logic.
        /// </summary>
        /// <param name="state">The service's state</param>
        private async void DoWork(object state)
        {
            try
            {
                _logger.LogInformation("Removing expired account recovery tokens ...");

                using (var scope = _scopeFactory.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<VRTContext>();

                    // Remove expired refresh tokens
                    var expiredTokens = await dbContext.RecoveryTokens
                        .Where(rt => rt.ExpiresAt < DateTime.UtcNow)
                        .ToListAsync();

                    dbContext.RecoveryTokens.RemoveRange(expiredTokens);
                    await dbContext.SaveChangesAsync();

                    _logger.LogInformation($"Removed recovery tokens: {expiredTokens.Count}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred removing expired account recovery tokens.");
            }
        }

        #endregion
    }
}

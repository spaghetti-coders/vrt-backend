﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VRT.Core.Services;

namespace VRT.Core.HostedServices
{
    /// <summary>
    /// A hosted service for periodically removing expired tokens from the auth manager.
    /// </summary>
    public class JwtTokenCache : IHostedService, IDisposable
    {
        #region Fields

        private Timer _timer;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ILogger<JwtTokenCache> _logger;

        #endregion


        /// <summary>
        /// Constructs a new JwtTokenCache object.
        /// </summary>
        /// <param name="scopeFactory">An IServiceScopeFactory reference</param>
        /// <param name="logger">An ILogger reference</param>
        public JwtTokenCache(IServiceScopeFactory scopeFactory, ILogger<JwtTokenCache> logger)
        {
            _scopeFactory = scopeFactory;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A task to be awaited</returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting token cache service ...");

            // Remove expired tokens from cache every minute
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));
            
            return Task.CompletedTask;
        }
        /// <summary>
        /// Stops the service.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A task to be awaited</returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping token cache service ...");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
        /// <summary>
        /// Disposes this service and any of its allocated resources from memory.
        /// </summary>
        public void Dispose()
        {
            _timer?.Dispose();

            _logger.LogInformation("Token cache service stopped.");
        }

        /// <summary>
        /// Contains the service's logic.
        /// </summary>
        /// <param name="state">The service's state</param>
        private void DoWork(object state)
        {
            _logger.LogInformation("Removing expired JWT refresh tokens from cache ...");

            using (var scope = _scopeFactory.CreateScope())
            {
                // Get an auth manager reference
                var authManager = scope.ServiceProvider.GetRequiredService<IJwtAuthManager>();

                // Remove expired refresh tokens
                var numRemoved = authManager.RemoveExpiredRefreshTokens(DateTime.UtcNow);

                // TODO: Once the revocation list for access tokens is in place, they need to be removed aswell
                
                _logger.LogInformation($"Removed refresh tokens: {numRemoved}");
            }

        }

        #endregion
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VRT.Core.Models;
using VRT.Core.Services;
using VRT.Data;
using VRT.Data.Entities;

namespace VRT.Core.HostedServices
{
    /// <summary>
    /// A hosted service for periodically deleting users that are marked for deletion.
    /// </summary>
    public class AccountDeletionService : IHostedService, IDisposable
    {
        #region Fields

        private Timer _timer;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ILogger<AccountDeletionService> _logger;

        #endregion


        /// <summary>
        /// Constructs a new AccountDeletionService object.
        /// </summary>
        /// <param name="scopeFactory">An IServiceScopeFactory reference</param>
        /// <param name="logger">An ILogger reference</param>
        public AccountDeletionService(IServiceScopeFactory scopeFactory, ILogger<AccountDeletionService> logger) 
        {
            _scopeFactory = scopeFactory;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A task to be awaited</returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting account deletion service ...");

            // Remove expired tokens from cache every minute
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromHours(12));

            return Task.CompletedTask;
        }
        /// <summary>
        /// Stops the service.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>A task to be awaited</returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping account deletion service ...");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
        /// <summary>
        /// Disposes this service and any of its allocated resources from memory.
        /// </summary>
        public void Dispose()
        {
            _timer?.Dispose();

            _logger.LogInformation("Account deletion service stopped.");
        }

        /// <summary>
        /// Contains the service's logic.
        /// </summary>
        /// <param name="state">The service's state</param>
        private void DoWork(object state)
        {
            _logger.LogInformation("Checking for user accounts that are due for deletion ...");

            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    // Get a reference to the database context
                    var dbContext = scope.ServiceProvider.GetRequiredService<VRTContext>();
                    

                    // Retrieve accounts that are due for deletion
                    var toBeDeleted = dbContext.Users
                        .Where(u => u.Status == Convert.ToInt32(UserStatus.PendingDeletion) && u.DeletionDate <= DateTime.UtcNow)
                        .ToList();

                    foreach (var user in toBeDeleted)
                    {
                        // Remove those accounts
                        dbContext.Users.Remove(user);
                        _logger.LogInformation($"Deleted user [{user.Username}].");
                    }

                    // Persist changes & notify users
                    if (toBeDeleted.Count > 0)
                    {
                        dbContext.SaveChanges();
                        NotifyUsers(scope, toBeDeleted);
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError("An unexpected error occurred performing the user account deletion.", ex);
            }
        }
        /// <summary>
        /// Convenience method for synchronously notifying the given users that there accounts have been deleted.
        /// </summary>
        /// <param name="scope">An IServiceScope reference</param>
        /// <param name="users">List of users to notify</param>
        private void NotifyUsers(IServiceScope scope, List<User> users)
        {
            try
            {
                // Get a reference to the notification service
                var notificationService = scope.ServiceProvider.GetRequiredService<INotificationService>();

                foreach (var user in users)
                {
                    // Skip users w/o email address
                    if (string.IsNullOrEmpty(user.Email))
                        continue;

                    try
                    {
                        // Notify the user
                        notificationService.SendAccountDeletedNotificationAsync(user)
                            .GetAwaiter()
                            .GetResult();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"An unexpected error occurred while notifying the user (ID={user.Id}).", ex);
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError("Unable to retrieve an instance of the notification service.", ex);
            }
        }

        #endregion
    }
}

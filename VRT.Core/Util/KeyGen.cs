﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Util
{

    /// <summary>
    /// Provides functions for generating keys and random alphanumeric strings.
    /// </summary>
    public static class KeyGen
    {
        /// <summary>
        /// The set of alphanumeric characters used to generate unique keys/strings with.
        /// </summary>
        internal static readonly char[] alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

        /// <summary>
        /// Generates a unique alphanumeric string of the requested length.
        /// </summary>
        /// <param name="length">The lenght (number of characters) of the generated string</param>
        /// <returns>a unique alphanumeric string of the requested length</returns>
        public static string GenerateUniqueKey(int length)
        {
            byte[] data = new byte[length * 4];

            // Generate random data
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(data);
            }

            // Build string from data
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                var rnd = BitConverter.ToUInt32(data, i * 4);
                var idx = rnd % alphabet.Length;

                sb.Append(alphabet[idx]);
            }

            // Return string
            return sb.ToString();
        }
    }
}

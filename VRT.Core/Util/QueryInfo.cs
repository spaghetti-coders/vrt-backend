﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Core.Constants;
using VRT.Core.Exceptions;

namespace VRT.Core.Util
{

    #region SortOrder

    /// <summary>
    /// Specfies the order by which a set of entries can be sorted.
    /// </summary>
    public enum SortOrder
    {
        /// <summary>
        /// Entries are sorted by the specified property in ascending order.
        /// </summary>
        Ascending,
        /// <summary>
        /// Entries are sorted by the specified property in descending order.
        /// </summary>
        Descending
    };

    #endregion

    #region QueryInfo

    /// <summary>
    /// Contains query information for database requests.
    /// </summary>
    public class QueryInfo
    {
        /// <summary>
        /// Constructs a new QueryInfo object.
        /// </summary>
        public QueryInfo() { }

        /// <summary>
        /// The 0-based index of the result page that should be returned.
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// The maximum number of results that should be included per result page.
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// A filter/search string that should be contained in the results' (searchable) properties.
        /// </summary>
        public string Filter { get; set; }
        /// <summary>
        /// The column/property by which returned results will be ordered by.
        /// </summary>
        public string SortColumn { get; set; }
        /// <summary>
        /// The order by which results will be returned (defaults to <see cref="SortOrder.Ascending"/>).
        /// </summary>
        public SortOrder SortOrder { get; set; }
    }

    #endregion

    #region QueryInfoBuilder

    /// <summary>
    /// A builder for <see cref="QueryInfo"/> objects.
    /// </summary>
    public class QueryInfoBuilder
    {
        #region Fields

        protected QueryInfo _queryInfo;
        protected int _defaultPageIndex;
        protected int _defaultPageSize;

        #endregion

        
        /// <summary>
        /// Constructs a new QueryInfoBuilder object that uses the specified default page index and page size.
        /// </summary>
        /// <param name="defaultPage">The default page index (must be >= 0)</param>
        /// <param name="defaultPageSize">The default page size (must be >= 1)</param>
        public QueryInfoBuilder(int defaultPage = 0, int defaultPageSize = 10)
        {
            _queryInfo = new QueryInfo
            {
                Page = defaultPage >= 0 ? defaultPageSize : 0,
                PageSize = defaultPageSize >= 1 ? defaultPageSize : 10,
                SortOrder = SortOrder.Ascending
            };
        }


        #region Methods

        /// <summary>
        /// Sets the QueryInfo's Page property to the specified <paramref name="value"/>, or, if that is not valid, the default page index.
        /// </summary>
        /// <param name="value">The page index value (the first page's index is 1)</param>
        /// <returns>this builder instance</returns>
        public QueryInfoBuilder SetPageIndexOrDefault(int value)
        {
            _queryInfo.Page = value >= 1 ? value - 1 : _defaultPageIndex;

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's Page property to the specified value.
        /// </summary>
        /// <param name="value">The page index value (the first page's index is 1)</param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ValidationException">if <paramref name="value"/> is lesser than 1.</exception>
        public QueryInfoBuilder SetPageIndex(int value)
        {
            if (value <= 0)
                throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, $"The specified page index of '{value}' is invalid. The value must be greater or equal to '1'.");

            _queryInfo.Page = value - 1;

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's PageSize property to the sepcified value, or, if that is not valid, the default page size.
        /// </summary>
        /// <param name="value">The page size value (must be greater than 1)</param>
        /// <returns>this builder instance</returns>
        public QueryInfoBuilder SetPageSizeOrDefault(int value)
        {
            _queryInfo.PageSize = value >= 1 ? value : _defaultPageSize;

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's PageSize property to the sepcified value, or, if that is not valid, the default page size, but not greater than <paramref name="maxValue"/>.
        /// </summary>
        /// <param name="value">The page size value (must be greater than 1)</param>
        /// <param name="maxValue">Specifies the upper limit for <paramref name="value"/></param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ArgumentException">if <paramref name="maxValue"/> is less then 1</exception>
        public QueryInfoBuilder SetPageSizeOrDefault(int value, int maxValue)
        {
            if (maxValue < 1)
                throw new ArgumentException("The argument value must be greater or equal to 1.", nameof(maxValue));

            _queryInfo.PageSize = value >= 1 ? Math.Min(value, maxValue) : _defaultPageSize;

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's PageSize property to the specified value.
        /// </summary>
        /// <param name="value">The page size value (must be greater than 1)</param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ValidationException">if <paramref name="value"/> is less than 1.</exception>
        public QueryInfoBuilder SetPageSize(int value)
        {
            if (value < 1)
                throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, $"The specified page size of '{value}' is invalid. The value must be greater or equal to '1'.");

            _queryInfo.PageSize = value;

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's PageSize property to the specified value.
        /// </summary>
        /// <param name="value">The page size value (must be greater than 1)</param>
        /// <param name="maxValue">Specifies the upper limit for <paramref name="value"/></param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ValidationException">if <paramref name="value"/> is less than 1.</exception>
        /// <exception cref="ArgumentException">if <paramref name="maxValue"/> is less then 1</exception>
        public QueryInfoBuilder SetPageSize(int value, int maxValue)
        {
            if (value < 1)
                throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, $"The specified page size of '{value}' is invalid. The value must be greater or equal to '1'.");
            if (maxValue < 1)
                throw new ArgumentException("The argument value must be greater or equal to 1.", nameof(maxValue));

            _queryInfo.PageSize = Math.Min(value, maxValue);

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's Filter property to the specified value.
        /// </summary>
        /// <param name="value">The filter/search value (may be <see langword="null"/>)</param>
        /// <param name="toLower">If <see langword="true"/>, converts <paramref name="value"/> to lower case.</param>
        /// <returns>this builder instance</returns>
        public QueryInfoBuilder SetFilter(string value, bool toLower = true)
        {
            if (!string.IsNullOrEmpty(value) && toLower)
                _queryInfo.Filter = value.ToLower();
            else
                _queryInfo.Filter = value;

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's SortColumn property to <paramref name="value"/>, or, if that is <see langword="null"/> or empty, to <paramref name="defaultValue"/>.
        /// </summary>
        /// <typeparam name="T">The type of entity that is being queried and by whose property the results are to be sorted</typeparam>
        /// <param name="value">The property name by which the reuslts are to be sorted by</param>
        /// <param name="defaultValue">The fall-back property by which results are to be sorted if <paramref name="value"/> is <see langword="null"/> or empty.</param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ArgumentNullException">if defaultValue is null or empty</exception>
        /// <exception cref="ArgumentException">if the source entity <typeparamref name="T"/> does not contain a property with the name specified in <paramref name="defaultValue"/></exception>
        /// <exception cref="ValidationException">if the source entity <typeparamref name="T"/> does not contain a property with the name specified in <paramref name="value"/></exception>
        public QueryInfoBuilder SetSortColumnOrDefault<T>(string value, string defaultValue)
        {
            if (string.IsNullOrEmpty(defaultValue))
                throw new ArgumentNullException("Parameter must not be null or empty.", nameof(defaultValue));
            
            var typeProps = typeof(T).GetProperties();


            // If value is a valid property of T then use it
            if (!string.IsNullOrEmpty(value))
            {
                // Make sure the entity object contains the specified property value
                if (!typeProps.Any(prop => prop.Name == value))
                    throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, $"Parameter value '{value}' is not a valid property of type '{typeof(T).FullName}'.");

                _queryInfo.SortColumn = value;
            }
            else
            {
                // ... otherwise fall-back to defaultValue

                // Make sure the entity object contains the property that was specified in the defaultValue
                if (!typeProps.Any(prop => prop.Name == defaultValue))
                    throw new ArgumentException($"Parameter value '{defaultValue}' is not a valid property of type '{typeof(T).FullName}'.", nameof(defaultValue));

                _queryInfo.SortColumn = defaultValue;
            }

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's SortColumn property to the specified value.
        /// </summary>
        /// <typeparam name="T">The type of entity that is being queried and by whose property the results are to be sorted</typeparam>
        /// <param name="value">The property name by which the reuslts are to be sorted by</param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ValidationException">if the source entity <typeparamref name="T"/> does not contain a property with the name specified in <paramref name="value"/></exception>
        public QueryInfoBuilder SetSortColumn<T>(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, "Parameter must not be null or empty.");

            var typeProps = typeof(T).GetProperties();

            // Make sure the entity object contains the specified property value
            if (!typeProps.Any(prop => prop.Name == value))
                throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, $"Parameter value '{value}' is not a valid property of type '{typeof(T).FullName}'.");

            _queryInfo.SortColumn = value;

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's SortOrder property to the specified <paramref name="value"/>, or, if that is <see langword="null"/> or empty, to <paramref name="defaultValue"/>.
        /// </summary>
        /// <param name="value">The sort order in which results are to be ordered in. Accepted values: 'asc', 'ascending', 'desc', 'descending'</param>
        /// <param name="defaultValue">The default sort order to use when <paramref name="value"/> is <see langword="null"/> or empty.</param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ValidationException">if <paramref name="value"/> is not a valid sort order.</exception>
        public QueryInfoBuilder SetSortOrderOrDefault(string value, SortOrder defaultValue)
        {
            var validValues = new string[] { "asc", "ascending", "desc", "descending" };

            if (!string.IsNullOrEmpty(value))
            {
                if (validValues.Take(2).Any(v => v == value))
                    _queryInfo.SortOrder = SortOrder.Ascending;
                else if (validValues.Skip(2).Any(v => v == value))
                    _queryInfo.SortOrder = SortOrder.Descending;
                else
                    throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, $"'{value}' is not a valid sort order");
            }
            else
            {
                _queryInfo.SortOrder = defaultValue;
            }

            return this;
        }
        /// <summary>
        /// Sets the QueryInfo's SortOrder property to the specified value.
        /// </summary>
        /// <param name="value">The sort order in which results are to be ordered in. Accepted values: 'asc', 'ascending', 'desc', 'descending'</param>
        /// <returns>this builder instance</returns>
        /// <exception cref="ValidationException">if <paramref name="value"/> is not a valid sort order.</exception>
        public QueryInfoBuilder SetSortOrder(string value)
        {
            var validValues = new string[] { "asc", "ascending", "desc", "descending" };

            if (validValues.Take(2).Any(v => v == value))
                _queryInfo.SortOrder = SortOrder.Ascending;
            else if (validValues.Skip(2).Any(v => v == value))
                _queryInfo.SortOrder = SortOrder.Descending;
            else
                throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, $"The specified sort order '{value}' is not valid. Accpted values are: 'asc', 'desc'.");

            return this;
        }
        /// <summary>
        /// Returns a new QueryInfo object that uses the values specified through this builder.
        /// </summary>
        /// <returns>a new QueryInfo object that uses the values specified through this builder.</returns>
        /// <exception cref="ValidationException">if any of the required properties have not been specified</exception>
        public QueryInfo Build()
        {
            if (string.IsNullOrEmpty(_queryInfo.SortColumn))
                throw new ValidationException(Messages.Errors.QueryIsInvalid.Code, "The sort column must be specified.");
            
            return _queryInfo;
        }

        #endregion
    }

    #endregion
}

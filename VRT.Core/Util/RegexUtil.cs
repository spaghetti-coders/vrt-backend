﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VRT.Core.Util
{
    /// <summary>
    /// A collection of regular expression related utility functions.
    /// </summary>
    public static class RegexUtil
    {
        /// <summary>
        /// Validates if the given string is a valid hex code in the format '#ffa500'.
        /// </summary>
        /// <param name="hexCode">a hex-string starting with a '#'-sign, e.g., 'ffa500'</param>
        /// <returns>true if the given string is a valid hex-code</returns>
        public static bool IsValidHexCode(string hexCode)
        {
            if (string.IsNullOrWhiteSpace(hexCode))
                return false;

            try
            {
                return Regex.IsMatch(hexCode, @"#[a-f0-9]{6}", RegexOptions.IgnoreCase);
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Validates the given email address.
        /// </summary>
        /// <param name="email">The email address to validate</param>
        /// <returns>Return <see langword="true"/> if the given email address is not null and considered valid, <see langword="false"/> if not.</returns>
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email) || HasNonASCIIChars(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                    RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        /// Convenience function for testing whether the given string contains non-ASCII characters.
        /// </summary>
        /// <param name="str">The string to test</param>
        /// <returns>Returns <see langword="true"/> if the given string contains non-ASCII caracters, <see langword="false"/> otherwise.</returns>
        private static bool HasNonASCIIChars(string str)
        {
            return (System.Text.Encoding.UTF8.GetByteCount(str) != str.Length);
        }
    }
}

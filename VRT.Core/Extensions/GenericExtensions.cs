﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Extensions
{
    /// <summary>
    /// Provides generic extension methods.
    /// </summary>
    public static class GenericExtensions
    {
        /// <summary>
        /// Slices the list into multiple list of the specified size.
        /// </summary>
        /// <typeparam name="T">The type of elements</typeparam>
        /// <param name="source">The source list</param>
        /// <param name="size">The maximum number of elements per sliced list</param>
        /// <returns>A list of lists with a maximum of <paramref name="size"/> elements</returns>
        public static List<List<T>> Slice<T>(this List<T> source, int size)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / size)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}

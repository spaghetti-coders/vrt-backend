﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using VRT.Data.Entities;

namespace VRT.Core.Extensions
{
    /// <summary>
    /// A collection of extension methods regarding the <see cref="User"/> entity.
    /// </summary>
    public static class UserExtensions
    {
        /// <summary>
        /// Returns this ICollection of users with their passwords removed.
        /// </summary>
        /// <param name="users">Collection of users objects</param>
        /// <returns>The ICollection of user objects, stripped of their passwords.</returns>
        public static ICollection<User> WithoutPasswords(this ICollection<User> users)
        {
            if (users == null)
                return null;

            return users.Select(u => u.WithoutPassword()).ToList();
        }
        /// <summary>
        /// Returns this IEnumeration of users with their passwords removed.
        /// </summary>
        /// <param name="users">Enumeration of users objects</param>
        /// <returns>The IEnumeration of user objects, stripped of their passwords.</returns>
        public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users) 
        {
            if (users == null)
                return null;

            return users.Select(u => u.WithoutPassword());
        }
        /// <summary>
        /// Returns this user object without the password.
        /// </summary>
        /// <param name="user">The user object whose password to remove</param>
        /// <returns>The user object stripped of its password value.</returns>
        public static User WithoutPassword(this User user)
        {
            if (user == null)
                return null;

            user.Password = null;
            
            return user;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Configurations
{
    /// <summary>
    /// The Email configuration model.
    /// </summary>
    public class EmailConfiguration
    {
        /// <summary>
        /// The hostname of the email server.
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// The port the email server is listening on.
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// Boolean indicating whether or not to use TLS/SSL.
        /// </summary>
        public bool UseTLS { get; set; }
        /// <summary>
        /// The username for authenticating to the server.
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// The user's password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// The sender name to use when sending notifications.
        /// </summary>
        public string SenderName { get; set; }
        /// <summary>
        /// The sender address to use when sending notifications.
        /// </summary>
        public string SenderAddress { get; set; }
        /// <summary>
        /// Specifies how many recipients will be put in a single mail message.
        /// </summary>
        public int RecipientsBatchSize { get; set; }
    }
}

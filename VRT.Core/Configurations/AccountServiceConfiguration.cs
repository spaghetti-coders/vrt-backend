﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Configurations
{
    /// <summary>
    /// The BCrypt configuration model.
    /// </summary>
    public class AccountServiceConfiguration
    {
        /// <summary>
        /// The work factor to be used by the BCrypt algorithm.  
        /// </summary>
        [Required]
        public int BCryptWorkFactor { get; set; }
        /// <summary>
        /// A BCrypt hash that is used for validating passwords of non-application users to prevent side-channels.
        /// </summary>
        [Required]
        [StringLength(60)]
        public string BCryptDefaultHash { get; set; }
        /// <summary>
        /// The number of minutes a account recovery token stays valid.  
        /// </summary>
        [Required]
        public int RecoveryTokenLifetime { get; set; }
    }
}

﻿namespace VRT.Core.Configurations
{
    /// <summary>
    /// This class specifies the settings used for configuring the JWT token authentication used throughout the application.
    /// </summary>
    public class JwtConfiguration
    {
        /// <summary>
        /// The secret used as a seed for token generation.
        /// </summary>
        public string Secret { get; set; }
        /// <summary>
        /// The issuer set in generated tokens.
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// The audience set in generated tokens.
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// The lifetime of an access token in minutes.
        /// </summary>
        public int AccessTokenLifetime { get; set; }
        /// <summary>
        /// The lifetime of a refresh token in minutes.
        /// </summary>
        public int RefreshTokenLifetime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Configurations
{
    /// <summary>
    /// This class specifies the configuration properties 
    /// </summary>
    public class WebPushConfiguration
    {
        /// <summary>
        /// The application server's contact URI as 'https:' or 'mailto:'.
        /// </summary>
        public string VAPIDSubject { get; set; }
        /// <summary>
        /// The application server's public key.
        /// </summary>
        public string VAPIDPublicKey { get; set; }
        /// <summary>
        /// The application server's private key.
        /// </summary>
        public string VAPIDPrivateKey { get; set; }
    }
}

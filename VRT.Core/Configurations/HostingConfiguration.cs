﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Configurations
{
    /// <summary>
    /// The hosting configuration model.
    /// </summary>
    public class HostingConfiguration
    {
        /// <summary>
        /// The URL where the application is being hosted, e.g., 'https://vrt.example.com'
        /// </summary>
        public string AppUrl { get; set; }
        /// <summary>
        /// The push icon's URI (relative to the to application's root URL)
        /// </summary>
        public string PushIconUri { get; set; }
        /// <summary>
        /// The application's default locale used to format locale specific information (e.g., dates).
        /// </summary>
        public string DefaultLocale { get; set; }
        /// <summary>
        /// The retention period for personal data in days.
        /// </summary>
        public int DataRetentionPeriod { get; set; }
        /// <summary>
        /// Enables/Disables the delivery of push and email notifications for the entire application.
        /// </summary>
        public bool EnableNotifications { get; set; }
    }
}

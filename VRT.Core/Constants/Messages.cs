﻿namespace VRT.Core.Constants
{
    /// <summary>
    /// This class defines the various error/warning messages used in the application.
    /// </summary>
    public static class Messages
    {
        /// <summary>
        /// Defines the error messages of the application.
        /// </summary>
        public static class Errors
        {
            #region General Error Message
            
            /// <summary>
            /// A message returned when some input failed validation.
            /// </summary>
            public static readonly ErrorMessage InputIsInvalid = new ErrorMessage { Code = "E001", Message = "The provided inputs are invalid." };
            /// <summary>
            /// A message returned when a requested object was not found.
            /// </summary>
            public static readonly ErrorMessage EntityNotFound = new ErrorMessage { Code = "E002", Message = "The requested entity doesn't exist." };
            /// <summary>
            /// A message returned when a requested object was not found.
            /// </summary>
            public static readonly ErrorMessage ReferenceNotFound = new ErrorMessage { Code = "E003", Message = "The referenced entity doesn't exist." };
            /// <summary>
            /// A message returned when a requested object was not found.
            /// </summary>
            public static readonly ErrorMessage DuplicateEntityValue = new ErrorMessage { Code = "E004", Message = "A database record with that value already exists." };
            /// <summary>
            /// A message returned when some of the provided query parameters are not valid.
            /// </summary>
            public static readonly ErrorMessage QueryIsInvalid = new ErrorMessage { Code = "E005", Message = "The provided query parameters are invalid." };
            /// <summary>
            /// A message returned when some of the provided path parameters are not valid.
            /// </summary>
            public static readonly ErrorMessage PathIsInvalid = new ErrorMessage { Code = "E006", Message = "The provided path parameter(s) are invalid." };
            /// <summary>
            /// A message returned to indicate that the requester of resource doesn't have the necessary permissions (i.e. user role) to access it.
            /// </summary>
            public static readonly ErrorMessage AccessDenied = new ErrorMessage { Code = "E007", Message = "You don't have the necessary permissions to access the requested resource." };
            /// <summary>
            /// A message returned when a requested object was not found.
            /// </summary>
            public static readonly ErrorMessage UnexpectedError = new ErrorMessage { Code = "E009", Message = "An unexpected error occurred. Please contact an administrator." };

            #endregion

            #region Account Error Messages

            /// <summary>
            /// A message returned when a user is trying to register with a username that already exists in the database.
            /// </summary>
            public static readonly ErrorMessage DuplicateUsername = new ErrorMessage { Code = "E101", Message = "A user with that username already exists." };
            /// <summary>
            /// A message returned when a users tries to authenticate using invalid credentials.
            /// </summary>
            public static readonly ErrorMessage AuthenticationFailed = new ErrorMessage { Code = "E102", Message = "The specified username or password is incorrect." };
            /// <summary>
            /// A message returned when a user tries to perform an action but their username no longer exists, or was set to inactive.
            /// </summary>
            public static readonly ErrorMessage AccountDisabledOrDeleted = new ErrorMessage { Code = "E103", Message = "The account was disabled or deleted." };
            /// <summary>
            /// A message returned when a user tries to perform an action but their username no longer exists, or was set to inactive.
            /// </summary>
            public static readonly ErrorMessage RefreshTokenInvalidOrExpired = new ErrorMessage { Code = "E104", Message = "The refresh token is invalid or has expired." };
            /// <summary>
            /// A message returned when a user tries to recover their password with an invalid recovery token.
            /// </summary>
            public static readonly ErrorMessage RecoveryTokenInvalidOrExpired = new ErrorMessage { Code = "E105", Message = "The account recovery token is invalid or has expired." };
            /// <summary>
            /// A message returned when a user is trying to register with a display name that already exists in the database.
            /// </summary>
            public static readonly ErrorMessage DuplicateDisplayName = new ErrorMessage { Code = "E106", Message = "A user with that DisplayName already exists." };
            /// <summary>
            /// A message returned when a user is trying to register with an email address that already exists in the database.
            /// </summary>
            public static readonly ErrorMessage DuplicateEmailAddress = new ErrorMessage { Code = "E107", Message = "The email address is already being used." };

            #endregion

            #region Event Error Messages

            /// <summary>
            /// A message returned when a user tries to recover their password with an invalid recovery token.
            /// </summary>
            public static readonly ErrorMessage EventAlreadyStarted = new ErrorMessage { Code = "E201", Message = "Cannot update Event that has already started." };
            /// <summary>
            /// The message returned when trying to log an event that has not yet started.
            /// </summary>
            public static readonly ErrorMessage EventIsInRetention = new ErrorMessage { Code = "E202", Message = "The event cannot be deleted while being in retention." };

            #endregion

            #region Registration Error Messages

            /// <summary>
            /// A message returned when a user tries to unregister from an event that has already started / concluded.
            /// </summary>
            public static readonly ErrorMessage UnRegisterFromStartedEvent = new ErrorMessage { Code = "E301", Message = "Cannot un/register from an event that has already started." };
            /// <summary>
            /// A message returned when a user tries to register to an event they're already registered to.
            /// </summary>
            public static readonly ErrorMessage AlreadyRegistered = new ErrorMessage { Code = "E302", Message = "The user is already registered." };
            /// <summary>
            /// A message returned when an admin tries to add a user to an event that has not yet started, or that was not open for registration.
            /// </summary>
            public static readonly ErrorMessage InvalidEventForAdminRegistration = new ErrorMessage { Code = "E303", Message = "Cannot add user to an event that has not started/was not open for registrations." };

            #endregion

            #region Email Service Error Messages

            /// <summary>
            /// A message returned when an error occurred connecting/authenticating to the configured SMTP server.
            /// </summary>
            public static readonly ErrorMessage SmtpConnectionError = new ErrorMessage { Code = "E401", Message = "An error occurred connecting to the SMTP server. See log for more details." };
            /// <summary>
            /// A message returned when an error occurred connecting/authenticating to the configured SMTP server.
            /// </summary>
            public static readonly ErrorMessage SmtpSendError = new ErrorMessage { Code = "E402", Message = "An error occurred sending an email. See log for more details." };

            #endregion

            #region Notification Service Error Messages

            /// <summary>
            /// A message returned when an error occurred connecting/authenticating to the configured SMTP server.
            /// </summary>
            public static readonly ErrorMessage FailedToLoadMailTemplate = new ErrorMessage { Code = "E501", Message = "An error occurred loading the specified mail template." };

            #endregion
            
            #region Venue Error Messages
            
            /// <summary>
            /// A message returned when a user is trying to register with a username that already exists in the database.
            /// </summary>
            public static readonly ErrorMessage DuplicateVenueName = new ErrorMessage { Code = "E601", Message = "A venue with that name already exists." };
            /// <summary>
            /// A message returned when a venue was tried to be deleted, while still having past events in retention.
            /// </summary>
            public static readonly ErrorMessage VenueHasEventsInRetention = new ErrorMessage { Code = "E602", Message = "The venue could not be deleted because there are still events in retention." };
            
            #endregion
        }
    }

    /// <summary>
    /// A simple error/warning message model.
    /// </summary>
    public class ErrorMessage
    {
        /// <summary>
        /// The message's code; e.g., 'E012'
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// A descriptive error text.
        /// </summary>
        public string Message { get; set; }
    }
}

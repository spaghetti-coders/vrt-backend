﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Constants
{
    /// <summary>
    /// The class defines the application's user roles.
    /// </summary>
    public class Roles
    {
        /// <summary>
        /// The application's administrator role.
        /// </summary>
        public const string Admin = "admin";
        /// <summary>
        /// The application's manager role.
        /// </summary>
        public const string Manager = "manager";
        /// <summary>
        /// The role designates a user that is either a manager or admin.
        /// </summary>
        public const string ManagerOrAdmin = "manager,admin";
        /// <summary>
        /// The role designates a user that is either a manager or admin.
        /// </summary>
        public const string AllUsers = "user,manager,admin";
        /// <summary>
        /// The application's user role.
        /// </summary>
        public const string User = "user";


        /// <summary>
        /// Tests whether the given role string is valid.
        /// </summary>
        /// <param name="role">The role string to test</param>
        /// <returns><see langword="true"/> if the stringt containts a valid role value, <see langword="false"/> if not</returns>
        public static bool IsValidRole(string role)
        {
            if (string.IsNullOrEmpty(role))
                return false;

            return role == Admin || role == Manager || role == User;
        }
    }
}

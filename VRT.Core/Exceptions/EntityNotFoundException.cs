﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Exceptions
{
    /// <summary>
    /// Exception thrown when a requested entity does not exist in the underlying datastore.
    /// </summary>
    public class EntityNotFoundException : VRTException
    {
        /// <summary>
        /// Constructs a new EntityNotFoundException.
        /// </summary>
        /// <param name="code">An error code uniquely identifying the problem</param>
        /// <param name="message">The message describing the cause for this exception to be thrown</param>
        public EntityNotFoundException(string code, string message) : base(code, message) { }
        /// <summary>
        /// Constructs a new EntityNotFoundException.
        /// </summary>
        /// <param name="code">An error code uniquely identifying the problem</param>
        /// <param name="message">The message describing the cause for this exception to be thrown</param>
        /// <param name="cause">The exception that caused this exception to be thrown</param>
        public EntityNotFoundException(string code, string message, Exception cause) : base(code, message, cause) { }
    }
}

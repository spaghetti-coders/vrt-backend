﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Exceptions
{
    /// <summary>
    /// This exception is used by IAccountService instances.
    /// </summary>
    public class AccountServiceException : VRTException
    {
        /// <summary>
        /// Constructs a new AccountServiceException object.
        /// </summary>
        /// <param name="code">An error code uniquely identifying the problem</param>
        /// <param name="message">The message describing the cause for this exception to be thrown</param>
        public AccountServiceException(string code, string message) : base(code, message) { }
        /// <summary>
        /// Constructs a new AccountServiceException object.
        /// </summary>
        /// <param name="code">An error code uniquely identifying the problem</param>
        /// <param name="message">The message describing the cause for this exception to be thrown</param>
        /// <param name="cause">The exception that caused this exception to be thrown</param>
        public AccountServiceException(string code, string message, Exception cause) : base(code, message, cause) { }
    }
}

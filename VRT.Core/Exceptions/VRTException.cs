﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Exceptions
{
    /// <summary>
    /// The base exception class used within the VRT application.
    /// </summary>
    public abstract class VRTException : Exception
    {
        /// <summary>
        /// Constructs a new VRTExcpetion object.
        /// </summary>
        /// <param name="code">An error code uniquely identifying the problem</param>
        /// <param name="message">The message describing the problem</param>
        public VRTException(string code, string message) : base(message) { Code = code; }
        /// <summary>
        /// Constructs a new VRTExcpetion object.
        /// </summary>
        /// <param name="code">An error code uniquely identifying the problem</param>
        /// <param name="message">The message describing the problem</param>
        /// <param name="cause">The exception that caused this exception to be thrown</param>
        public VRTException(string code, string message, Exception cause) : base(message, cause) { Code = code; }


        #region Properties

        /// <summary>
        /// An unique error code identifying the problem.
        /// </summary>
        public string Code { get; set; }

        #endregion
    }
}

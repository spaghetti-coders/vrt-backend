﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VRT.Core.Models
{
    /// <summary>
    /// The enumeration is used to describe the various states that a user account can be in.
    /// </summary>
    public enum UserStatus
    {
        /// <summary>
        /// The user has registered and is waiting to be activated.
        /// </summary>
        PendingRegistration = 0,
        /// <summary>
        /// The user's account is set to active.
        /// </summary>
        Active = 1,
        /// <summary>
        /// The user's account is set to inactive.
        /// </summary>
        Inactive = 2,
        /// <summary>
        /// The user has requested the deletion of their account.
        /// </summary>
        PendingDeletion = 3,
        /// <summary>
        /// A pseudo-status used for filtering. Accounts are not filtered by any specific status.
        /// </summary>
        Any = 4
    }
}

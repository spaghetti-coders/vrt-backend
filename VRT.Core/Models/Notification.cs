﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Core.Models
{
    /// <summary>
    /// A notification that is queued and handled by the DeliveryService.
    /// </summary>
    public class Notification
    {
        #region Push Messages

        /// <summary>
        /// The push subscribers to whom this notification should be sent.
        /// </summary>
        public List<PushSubscription> PushSubscriptions { get; set; }
        /// <summary>
        /// The push message that should be sent to subscribed users.
        /// </summary>
        public PushPayload PushPayload { get; set; }

        #endregion

        #region Email Notifications

        /// <summary>
        /// The email recipients to whom this notification should be sent.
        /// </summary>
        public List<string> EmailRecipients { get; set; }
        /// <summary>
        /// The email message's subject.
        /// </summary>
        public string EmailSubject { get; set; }
        /// <summary>
        /// The email message's body.
        /// </summary>
        public string EmailBody { get; set; }
        /// <summary>
        /// Indicates whether the <see cref="EmailBody"/> contains HTML (<see langword="true"/>) or plain text (<see langword="false"/>).
        /// </summary>
        public bool EmailIsHtml { get; set; }

        #endregion
    }
    /// <summary>
    /// Represents a push payload.
    /// </summary>
    public class PushPayload
    {
        /// <summary>
        /// Constructs a new PushPayload object.
        /// </summary>
        /// <param name="title">A notification title</param>
        /// <param name="body">A notification body</param>
        public PushPayload(string title, string body)
        {
            Notification = new PushNotification
            {
                Title = title,
                Body = body
            };
        }
        /// <summary>
        /// Constructs a new PushPayload object.
        /// </summary>
        /// <param name="title">The notification's title</param>
        /// <param name="body">The notification's message</param>
        /// <param name="icon">The notification's icon</param>
        public PushPayload(string title, string body, string icon) : this(title, body)
        {
            Notification.Icon = icon;
        }

        /// <summary>
        /// The payload's notification.
        /// </summary>
        [JsonPropertyName("notification")]
        public PushNotification Notification { get; set; }
        
        /// <summary>
        /// Adds an action to the push notification.
        /// </summary>
        /// <param name="action"></param>
        public void AddAction(string action, string title, string icon)
        {
            if (string.IsNullOrEmpty(action) || string.IsNullOrEmpty(title) || string.IsNullOrEmpty(icon))
                return;
            
            Notification.Actions.Add(new PushAction() 
            { 
                Action = action,
                Title = title,
                Icon = icon
            });
        }
    }
    /// <summary>
    /// Represents the notification model of a push payload.
    /// </summary>
    public class PushNotification
    {
        /// <summary>
        /// Constructs a new PushNotification object.
        /// </summary>
        public PushNotification()
        {
            Actions = new List<PushAction>();
            Data = new Dictionary<string, object>();
        }

        /// <summary>
        /// The message's title.
        /// </summary>
        [JsonPropertyName("title")]
        public string Title { get; set; }
        /// <summary>
        /// The message's body.
        /// </summary>
        [JsonPropertyName("body")]
        public string Body { get; set; }
        /// <summary>
        /// The message's icon.
        /// </summary>
        [JsonPropertyName("icon")]
        public string Icon { get; set; }
        /// <summary>
        /// The notification's actions. 
        /// </summary>
        [JsonPropertyName("actions")]
        public List<PushAction> Actions { get; set; }
        /// <summary>
        /// The message's data section
        /// </summary>
        [JsonPropertyName("data")]
        public Dictionary<string, Object> Data { get; set; }
    }
    /// <summary>
    /// Represents an action on a push notification.
    /// </summary>
    public class PushAction
    {
        /// <summary>
        /// The action's name/ID.
        /// </summary>
        [JsonPropertyName("action")]
        public string Action { get; set; }
        /// <summary>
        /// The title to display for this action.
        /// </summary>
        [JsonPropertyName("title")]
        public string Title { get; set; }
        /// <summary>
        /// The icon to display for this action.
        /// </summary>
        [JsonPropertyName("icon")]
        public string Icon { get; set; }
    }
}

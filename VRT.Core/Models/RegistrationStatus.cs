﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Core.Models
{
    /// <summary>
    /// Represents the different statutes users' registrations to an event can have.
    /// </summary>
    public enum RegistrationStatus
    {
        /// <summary>
        /// The user is not registered yet.
        /// </summary>
        NotRegistered = 0,
        /// <summary>
        /// The user is registered to the event, and on the list of active participants.
        /// </summary>
        Registered = 1,
        /// <summary>
        /// The user is registered to the event, and on the waiting list.
        /// </summary>
        OnWaitingList = 2
    }
}

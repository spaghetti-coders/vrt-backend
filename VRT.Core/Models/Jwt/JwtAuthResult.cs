﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Core.Models.Jwt
{
    /// <summary>
    /// The class represents the result of a successful JWT authentication.
    /// </summary>
    public class JwtAuthResult
    {
        /// <summary>
        /// The access token issued to the user.
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// The refresh token issued with this access token.
        /// </summary>
        public RefreshToken RefreshToken{ get; set; }
    }
}

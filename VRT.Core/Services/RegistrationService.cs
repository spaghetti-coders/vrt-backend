﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.Models;
using VRT.Data;
using VRT.Data.Entities;
using VRT.Data.Extensions;

namespace VRT.Core.Services
{
    /// <summary>
    /// Provides methods for managing event registrations.
    /// </summary>
    public class RegistrationService : IRegistrationService
    {
        #region Fields

        private readonly VRTContext _context;
        private readonly INotificationService _notificationService;
        private readonly ILogger<RegistrationService> _logger;

        #endregion


        /// <summary>
        /// Constructs a new RegistrationService object.
        /// </summary>
        /// <param name="context">A VRTContext reference</param>
        /// <param name="notificationService">An INotificationService reference</param>
        /// <param name="logger">An ILogger reference</param>
        public RegistrationService(VRTContext context, INotificationService notificationService, ILogger<RegistrationService> logger)
        {
            _notificationService = notificationService;
            _context = context;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Returns the number of registrations for the specified user, that are matching the given filter
        /// and registration status.
        /// </summary>
        /// <param name="userId">ID of user whose registrations are to be retrieved</param>
        /// <param name="status">The status of registrations to return</param>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>The number of registrations for the specified user</returns>
        /// <exception cref="EntityNotFoundException">If the user with the given ID doesn't exist</exception>
        public async Task<int> CountRegistrationsByUserAsync(int userId, string filter = null,
            RegistrationStatus? status = null)
        {
            // Check if user exists
            if ((await _context.Users.CountAsync(u => u.Id == userId)) == 0)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The user with the specified ID '{userId}' doesn't exist.");

            var query = _context.Registrations
                .Where(r => r.UserId == userId);

            if (status.HasValue)
                query = query.Where(r => r.Status == Convert.ToInt32(status));

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                query = query.Where(r => r.User.Username.ToLower().Contains(filter) || r.User.DisplayName.ToLower().Contains(filter) || 
                                         r.Event.Name.ToLower().Contains(filter));
            }

            return await query.CountAsync();
        }
        /// <summary>
        /// Returns a list of event registrations for the specified user, that are matching the given filter and registration status.
        /// </summary>
        /// <param name="userId">ID of user whose registrations to retrieve</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="status">The status of registrations to return</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>A list of event registrations for the specified user</returns>
        /// <exception cref="EntityNotFoundException">If the user with the given ID deosn't exist</exception>
        public async Task<IEnumerable<Registration>> FindRegistrationsByUserAsync(int userId, int page = 1, int pageSize = 10, string filter = null,
            RegistrationStatus? status = null, string sortColumn = "Registerd", string sortOrder = "desc")
        {
            // Check if user exists
            if ((await _context.Users.CountAsync(u => u.Id == userId)) == 0)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The user with the specified ID '{userId}' doesn't exist.");
            
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Registered" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";

            var query = _context.Registrations
                .Include(r => r.User)
                .Include(r => r.Event)
                .Where(r => r.UserId == userId);
            
            if (status.HasValue)
                query = query.Where(r => r.Status == Convert.ToInt32(status));

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                query = query.Where(r => r.User.Username.ToLower().Contains(filter) || r.User.DisplayName.ToLower().Contains(filter) || 
                                         r.Event.Name.ToLower().Contains(filter));
            }
            
            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Return paged results
            return await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
        /// <summary>
        /// Returns the number of registrations for the specified event, that are matching the given filter string and status.
        /// </summary>
        /// <param name="eventId">ID of the event whose registration to retrieve</param>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <param name="status">The status of registrations to return (may be null)</param>
        /// <returns>Returns the number of registrations that are matching the given filter string.</returns>
        /// <exception cref="EntityNotFoundException">If the event with the specified ID doesn't exist</exception>
        public async Task<int> CountRegistrationsByEventAsync(int eventId, string filter = null, RegistrationStatus? status = null)
        {
            // Check if event exists
            if ((await _context.Events.CountAsync(e => e.Id == eventId)) == 0)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The event with the specified ID '{eventId}' doesn't exist.");

            var query = _context.Registrations
                .Where(r => r.EventId == eventId);
            
            if (status.HasValue)
                query = query.Where(r => r.Status == Convert.ToInt32(status));
            
            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                query = query.Where(r => r.User.Username.ToLower().Contains(filter) || r.User.DisplayName.ToLower().Contains(filter) || 
                                         r.Event.Name.ToLower().Contains(filter));
            }

            return await query.CountAsync();
        }
        /// <summary>
        /// Returns a list of registrations for the specified event and registration status that are matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="eventId">ID of the event whose registration to retrieve</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="status">The status of registrations to return (may be null)</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of registrations that are matching the given filter (if any) from the underlying datastore</returns>
        /// <exception cref="EntityNotFoundException">If the event with the specified ID doesn't exist</exception>
        public async Task<IEnumerable<Registration>> FindRegistrationsByEventAsync(int eventId, int page = 1, int pageSize = 10, string filter = null, 
            RegistrationStatus? status = null, string sortColumn = "Registered", string sortOrder = "asc")
        {
            // Check if event exists
            if ((await _context.Events.CountAsync(e => e.Id == eventId)) == 0)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The event with the specified ID '{eventId}' doesn't exist.");
            
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Registered" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";

            var query = _context.Registrations
                .Include(r => r.User)
                .Include(r => r.Event)
                .Where(r => r.EventId == eventId);
            
            if (status.HasValue)
                query = query.Where(r => r.Status == Convert.ToInt32(status));

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                query = query.Where(r => r.User.Username.ToLower().Contains(filter) || r.User.DisplayName.ToLower().Contains(filter) || 
                                         r.Event.Name.ToLower().Contains(filter));
            }
            
            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Return paged results
            return await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
        /// <summary>
        /// Tries to find a registration for with the specified event and user IDs.
        /// May return <see langword="null"/>.
        /// </summary>
        /// <param name="eventId">The ID of the event</param>
        /// <param name="userId">The ID of the user</param>
        /// <returns>The registration with the specified IDs or <see langword="null"/> if no such registration exists</returns>
        public async Task<Registration> FindRegistrationAsync(int eventId, int userId)
        {
            return await _context.Registrations
                .FirstOrDefaultAsync(r => r.EventId == eventId && r.UserId == userId);
        }
        /// <summary>
        /// Creates a new registration for the specified user and event.
        /// </summary>
        /// <param name="eventId">The ID of the Event the user is registering to</param>
        /// <param name="userId">The ID of the user that is to be registered to the event</param>
        /// <returns>The newly created registration object</returns>
        /// <exception cref="EntityNotFoundException">If either the user or event with the specified IDs doesn't exit</exception>
        /// <exception cref="DuplicateValueException">If the user is already registered to that event</exception>
        public async Task<Registration> RegisterAsync(int eventId, int userId)
        {
            // Retrieve user
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The user with the specified ID '{userId}' doesn't exist.");
            
            // Retrieve event
            var evt = await _context.Events
                .Where(e => e.Id == eventId)
                .FirstOrDefaultAsync();

            if (evt == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The event with the specified ID '{eventId}' doesn't exist.");
            if (evt.Date < DateTime.UtcNow)
                throw new ValidationException(Messages.Errors.UnRegisterFromStartedEvent.Code, "The event has already started.");

            // Make sure that the user is not already registered to the event
            if (await _context.Registrations.CountAsync(r => r.EventId == eventId && r.UserId == userId) > 0)
                throw new ValidationException(Messages.Errors.AlreadyRegistered.Code, Messages.Errors.AlreadyRegistered.Message);
            
            // Get number of users that have already registered for this event
            var numRegistered = await _context.Registrations.CountAsync(r => r.EventId == eventId);

            // Create registration
            var registration = await _context.Registrations.AddAsync(new Registration
            {
                Event = evt,
                User = user,
                Registered = DateTime.UtcNow,
                Status = numRegistered >= evt.Capacity ? Convert.ToInt32(RegistrationStatus.OnWaitingList) : Convert.ToInt32(RegistrationStatus.Registered)
            });

            await _context.SaveChangesAsync();

            _logger.LogInformation($"User (ID='{userId}') registered for event (ID='{eventId}').");

            return registration.Entity;
        }
        /// <summary>
        /// Unregisters the specified user from the specified event.
        /// </summary>
        /// <param name="eventId">The ID of the event</param>
        /// <param name="userId">The ID of the user</param>
        /// <returns>The deleted Registration object</returns>
        /// <exception cref="EntityNotFoundException">if a registration for the specified event and user doesn't exist</exception>
        /// <exception cref="ValidationException">if the user tries to unregister from an event that has already started / concluded</exception>
        public async Task<Registration> UnregisterAsync(int eventId, int userId)
        {
            // Retrieve registration
            var registration = await _context.Registrations
                .Include(r => r.Event)
                .Include(r => r.User)
                .Where(r => r.EventId == eventId && r.UserId == userId)
                .FirstOrDefaultAsync();

            if (registration == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"A registration for event '{eventId}' and user '{userId}' doesn't exist.");
            if (registration.Event.Date < DateTime.UtcNow)
                throw new ValidationException(Messages.Errors.UnRegisterFromStartedEvent.Code, "The event has already started.");

            // Delete registration & persist changes
            _context.Registrations.Remove(registration);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"User (ID='{userId}') un-registered from event (ID='{eventId}').");

            // Re-calculate the event's waiting list and inform users
            await CalculateWaitingListAsync(eventId);

            return registration;
        }
        /// <summary>
        /// Adds a user's registration to an event in the past. 
        /// </summary>
        /// <param name="eventId">ID of the event</param>
        /// <param name="userId">ID of the user</param>
        /// <returns>The newly created Registration object</returns>
        /// <exception cref="EntityNotFoundException">if the event or the user does not exist</exception>
        /// <exception cref="ValidationException">if the event is not a valid target (not in the past, not open for registration, already at max participants)</exception>
        /// <exception cref="ValidationException">if the user is already registered to this event</exception>
        public async Task<Registration> AddRegistrationAsync(int eventId, int userId)
        {
            // Retrieve user
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"A user with the ID '{userId}' doesn't exist.");
            
            // Retrieve event
            var evt = await _context.Events.FirstOrDefaultAsync(e => e.Id == eventId);

            if (evt == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"An event with the ID '{eventId}' doesn't exist.");

            // Event must be in the past and open for registrations
            if (evt.Date > DateTime.UtcNow || !evt.IsOpenForRegistration)
                throw new ValidationException(Messages.Errors.InvalidEventForAdminRegistration.Code, Messages.Errors.InvalidEventForAdminRegistration.Message);

            // Retrieve event's participant numbers
            var participantsCount = await _context.Registrations.CountAsync(r => r.EventId == eventId);

            // Make sure that the event is not already at maximum capacity.
            if (participantsCount >= evt.Capacity)
                throw new ValidationException(Messages.Errors.InvalidEventForAdminRegistration.Code, $"The event is already at maximum capacity.");

            // Make sure that the user is not already registered to the event
            if (await _context.Registrations.CountAsync(r => r.EventId == eventId && r.UserId == userId) > 0)
                throw new ValidationException(Messages.Errors.AlreadyRegistered.Code, Messages.Errors.AlreadyRegistered.Message);


            // Create a new registration
            var registration = new Registration
            {
                Event = evt,
                User = user,
                Registered = DateTime.UtcNow,
                Status = (int)RegistrationStatus.Registered
            };

            // Add registration to database and persist changes
            _context.Registrations.Add(registration);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"The user (ID='{userId}') was added as participant to the event (ID='{eventId}').");

            return registration;
        }
        /// <summary>
        /// Deletes the registration with the specified event and user IDs regardless of the event's date and time.
        /// As this operation is usually performed by an administrator, the user whose registration is deleted will 
        /// be informed if the event has not yet concluded.
        /// </summary>
        /// <param name="eventId">ID of the event</param>
        /// <param name="userId">ID of the user</param>
        /// <returns>The deleted Registration object</returns>
        /// <exception cref="EntityNotFoundException">if a registration for the specified event and user doesn't exist</exception>
        public async Task<Registration> DeleteRegistrationAsync(int eventId, int userId)
        {
            // Retrieve registration
            var registration = await _context.Registrations
                .Include(r => r.Event)
                .Include(r => r.User)
                .ThenInclude(u => u.PushSubscription)
                .Where(r => r.EventId == eventId && r.UserId == userId)
                .FirstOrDefaultAsync();

            if (registration == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"A registration for event '{eventId}' and user '{userId}' doesn't exist.");

            // Delete registration & persist changes
            _context.Registrations.Remove(registration);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"The Registration for the user (ID='{userId}') was removed from event (ID='{eventId}').");

            // If the event has not yet concluded, recalculate the waiting list and notify the user
            if (registration.Event.Date > DateTime.UtcNow)
            {
                await CalculateWaitingListAsync(eventId);
                await _notificationService.SendRegistrationDeletedNotificationAsync(registration.Event, registration);
            }

            return registration;
        }
        /// <summary>
        /// Recalculates the list of active participants/waiting list for the event of the specified ID,
        /// notifying users about their registration status change (if any).
        /// </summary>
        /// <param name="eventId">ID of the event whose participant/waiting list to re-calculate</param>
        /// <returns>a task to be awaited.</returns>
        public async Task CalculateWaitingListAsync(int eventId)
        {
            _logger.LogInformation($"Re-calculating waiting list for the event with ID '{eventId}' ...");

            // TODO come up with a solution that does not involve pulling the entire database ... -.-
            // Retrieve the registrations for this event
            var registrations = await _context.Registrations
                .Include(r => r.Event)
                .Include(r => r.User)
                .ThenInclude(u => u.PushSubscription)
                .Where(r => r.EventId == eventId)
                .OrderBy(r => r.Registered)
                .ToListAsync();

            // If there's no registrations, we don't need to re-calculate
            if (registrations.Count == 0)
                return;

            // Retrieve the event's current capacity
            var capacity = registrations.FirstOrDefault()?.Event.Capacity ?? 0;

            // Determine active participants
            var activeParticipants = registrations
                .Where(r => r.Status == Convert.ToInt32(RegistrationStatus.Registered))
                .OrderBy(r => r.Registered)
                .ToList();

            // Determine users on the waiting list
            var onWaitingList = registrations
                .Where(r => r.Status == Convert.ToInt32(RegistrationStatus.OnWaitingList))
                .OrderBy(r => r.Registered)
                .ToList();


            // Calculate the number of available slots on the active participants list
            var availableSlots = capacity - activeParticipants.Count;

            // There's room for more active participants
            if (availableSlots > 0 && onWaitingList.Count > 0)
            {
                // Determine the user registrations that get upgraded from 'on waiting list' to 'active participant'
                var toUpdate = onWaitingList.Take(availableSlots > onWaitingList.Count ? onWaitingList.Count : availableSlots);

                // Update registrations
                foreach (var registration in toUpdate)
                {
                    registration.Status = Convert.ToInt32(RegistrationStatus.Registered);
                }

                // Persist changes
                await _context.SaveChangesAsync();

                // Notify users
                await _notificationService.SendMovedToParticipantsNotificationAsync(registrations.FirstOrDefault()?.Event, toUpdate.ToList());
            }
            // There's too many users on the active participants list
            else if (availableSlots < 0)
            {
                // Get the last |availableSlots| (by date of registration) active participants  
                var toUpdate = activeParticipants.TakeLast(Math.Abs(availableSlots));
                
                foreach(var registration in toUpdate)
                {
                    registration.Status = Convert.ToInt32(RegistrationStatus.OnWaitingList);
                }

                // Persist changes
                await _context.SaveChangesAsync();

                // Notify users
                await _notificationService.SendMovedToWaitingListNotificationAsync(registrations.FirstOrDefault()?.Event, toUpdate.ToList());
            }

        }
        #endregion
    }
}

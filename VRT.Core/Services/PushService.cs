﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.HostedServices;
using VRT.Core.Models;
using VRT.Data;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// The service provides functions for creating and retrieving user notifications.
    /// </summary>
    public class PushService : IPushService
    {
        #region Fields

        private readonly VRTContext _context;
        private readonly INotificationService _notificationService;
        private readonly ILogger<PushService> _logger;

        #endregion


        /// <summary>
        /// Contstructs a new NotificationService object. 
        /// </summary>
        /// <param name="context">A reference to the VRTContext</param>
        /// <param name="notificatioService">An INotificationService reference</param>
        /// <param name="logger">An ILogger reference</param>
        public PushService(VRTContext context, INotificationService notificationService, ILogger<PushService> logger)
        {
            _context = context;
            _notificationService = notificationService;
            _logger = logger;
        }


        #region Subscriptions

        /// <summary>
        /// Creates a new subscription for the specified user and returns it.
        /// </summary>
        /// <param name="userId">The ID of the user that is subscribing</param>
        /// <param name="endpoint">The user's push notification endpoint</param>
        /// <param name="pubKey">The user subscription's public key</param>
        /// <param name="secret">The user subscription's authentication secret</param>
        /// <returns>The newly created PushSubscription object</returns>
        /// <exception cref="ValidationException">if one of the specified values is null or invalid</exception>
        /// <exception cref="EntityNotFoundException">if the user with the specified ID doesn't exist</exception>
        public async Task<PushSubscription> SubscribeAsync(int userId, string endpoint, string pubKey, string secret)
        {
            if (string.IsNullOrEmpty(endpoint))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(endpoint)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(pubKey))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(pubKey)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(secret))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(secret)}' cannot be null or empty.");

            // Retrieve user from database
            var user = await _context.Users
                .Include(u => u.PushSubscription)
                .Where(u => u.Id == userId)
                .FirstOrDefaultAsync();

            if (user == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The user with ID='{userId}' doesn't exist.");

            PushSubscription subscription;

            if (user.PushSubscription == null)
            {
                // New subscription
                subscription = new PushSubscription
                {
                    UserId = user.Id,
                    Endpoint = endpoint,
                    P256DH = pubKey,
                    Auth = secret
                };

                await _context.PushSubscriptions.AddAsync(subscription);
            }
            else
            {
                // Update subscription
                user.PushSubscription.Endpoint = endpoint;
                user.PushSubscription.P256DH = pubKey;
                user.PushSubscription.Auth = secret;

                subscription = user.PushSubscription;
            }

            // Persist changes
            await _context.SaveChangesAsync();

            // Let the user now they are subscribed
            _notificationService.SendPushSubscriptionNotification(subscription);

            return subscription;
        }
        /// <summary>
        /// Deletes the user subscription to receive push notifications.
        /// </summary>
        /// <param name="userId">The ID of the user that is unsubscribing</param>
        /// <returns>The subscription that was deleted, or <see langword="null"/> if the user was not subscribed</returns>
        public async Task<PushSubscription> UnsubscribeAsync(int userId)
        {
            var subscription = await _context.PushSubscriptions.FirstOrDefaultAsync(s => s.UserId == userId);
            
            if (subscription!= null)
            {
                _context.PushSubscriptions.Remove(subscription);
                await _context.SaveChangesAsync();
            
                // Let the user now they are unsubscribed
                _notificationService.SendPushUnsubscribeNotification(subscription);
            }

            return subscription;
        }
        /// <summary>
        /// Tries to send a push notification to the specified user.
        /// </summary>
        /// <param name="userId">The user's ID</param>
        /// <param name="title">The notification's title</param>
        /// <param name="message">The message to be sent</param>
        /// <returns>a Task to be awaited</returns>
        /// <exception cref="ValidationException">if one of the specified values is null or invalid</exception>
        /// <exception cref="EntityNotFoundException">if the specified user wasn't found, or no subscription for that user exists</exception>
        public async Task SendNotificationAsync(int userId, string title, string message)
        {
            if (string.IsNullOrEmpty(message))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(message)}' cannot be null or empty.");

            var subscription = await _context.PushSubscriptions.FirstOrDefaultAsync(s => s.UserId == userId);
            if (subscription == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"A subscription for or a user with the ID {userId} doesn't exist.");

            // Send notifications
            _notificationService.SendPushNotification(subscription, title, message);
        }
        /// <summary>
        /// Sends a push notification to all active subscribers.
        /// </summary>
        /// <param name="title">The notification's title</param>
        /// <param name="message">The message to send</param>
        /// <returns>a Task to be awaited</returns>
        /// <exception cref="ValidationException">if message is null or invalid</exception>
        public async Task SendBroadcastNotificationAsync(string title, string message)
        {
            if (string.IsNullOrEmpty(message))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(message)}' cannot be null or empty.");

            var subscriptions = await _context.PushSubscriptions.ToListAsync();

            // Send notifications
            _notificationService.SendPushNotification(subscriptions, title, message);
        }

        #endregion
    }
}

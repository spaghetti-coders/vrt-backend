﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.Models;
using VRT.Core.Models.Jwt;
using VRT.Core.Util;
using VRT.Data;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// The basic implementation of the IAccountService.
    /// </summary>
    public class AccountService : IAccountService
    {
        #region Fields

        /// <summary>
        /// Matches all but the first occuring character of each word boundary in an email addresses user part (except: '_', '-', '.')
        /// </summary>
        private static readonly Regex RE_ANONYMIZE_EMAIL_ADDRESS = new Regex(@"\B[^_\-\.](?=.*@)");

        private readonly ILogger<AccountService> _logger;
        private readonly AccountServiceConfiguration _config;
        private readonly INotificationService _notificationService;
        private readonly IJwtAuthManager _authManager;
        private readonly VRTContext _context;

        #endregion


        /// <summary>
        /// Constructs a new AccountService object.
        /// </summary>
        /// <param name="context">A reference to the VRT context</param>
        /// <param name="config">An AccountServiceConfiguration</param>
        /// <param name="authManager">An IJwtAuthManager reference</param>
        /// <param name="notificationService">An INotificationService reference</param>
        /// <param name="logger">An ILogger reference</param>
        public AccountService(VRTContext context, AccountServiceConfiguration config, IJwtAuthManager authManager, INotificationService notificationService, ILogger<AccountService> logger)
        {
            _context = context;
            _config = config;
            _authManager = authManager;
            _notificationService = notificationService;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Registers a new user with the application. 
        /// </summary>
        /// <param name="username">The user's username (required)</param>
        /// <param name="displayName">The user's display name (required)</param>
        /// <param name="password">The user's password (required)</param>
        /// <param name="email">The user's email address (optional)</param>
        /// <param name="receiveNotifications">Indicates whether the user wants to receive notifications from the application</param>
        /// <returns>The new user object from the application's database</returns>
        /// <returns>The newly registered user object.</returns>
        /// <exception cref="ValidationException">if any of the given parameters are invalid.</exception>
        /// <exception cref="DuplicateValueException">if a user with the specified <paramref name="username"/>, <paramref name="displayName"/>, or <paramref name="email"/> already exists.</exception>
        public async Task<User> RegisterUserAsync(string username, string displayName, string password, string email = null, bool receiveNotifications = false)
        {
            if (string.IsNullOrEmpty(username))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(username)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(displayName))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(displayName)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(password))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(password)}' cannot be null or empty.");
            if (!string.IsNullOrEmpty(email) && !RegexUtil.IsValidEmail(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(email)}' is not a valid email address.");
            if (receiveNotifications && string.IsNullOrEmpty(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(receiveNotifications)}' can only be true in combination with a valid email address.");

            // Remove leading/trailing whitespaces in the username, and email address
            email = !string.IsNullOrEmpty(email) ? email.ToLower().Trim() : null;
            username = username.Trim();

            // Make sure the username is still available
            // NOTE: Normally we'd catch the Unique constraint violation when inserting the record, but the
            // calculation of the password hash takes so long, that testing beforehand is more feasible
            if (await _context.Users.CountAsync(u => u.Username == username) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateUsername.Code, Messages.Errors.DuplicateUsername.Message);

            // Make sure that the user's desired Display name is still available
            if (await _context.Users.CountAsync(u => u.DisplayName == displayName) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateDisplayName.Code, $"The display name '{displayName}' is already taken by another user.");

            // Make sure that the user's email address is still available
            if (!string.IsNullOrEmpty(email) && await _context.Users.CountAsync(u => u.Email == email) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateEmailAddress.Code, Messages.Errors.DuplicateEmailAddress.Message);

            // Hash password
            var passwordHash = BCrypt.Net.BCrypt.EnhancedHashPassword(password, _config.BCryptWorkFactor);

            // Create new user
            var user = new User
            {
                Username = username,
                Password = passwordHash,
                DisplayName = displayName,
                Role = Roles.User,
                Email = email,
                ReceiveNotifications = receiveNotifications,
                Created = DateTime.UtcNow,
                Status = Convert.ToInt32(UserStatus.PendingRegistration)    // User must be confirmed by an administrator
            };

            // Add user to database & persist changes
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            // Let the admins know
            await _notificationService.SendNewUserRegisteredNotificationAsync(user);
            await _notificationService.SendUserRegistrationConfirmationNotificationAsync(user);

            _logger.LogInformation($"New registration from '{user.Username}'.");

            return user;
        }
        /// <summary>
        /// Retrieves the user with the specified username from the underlying datastore.
        /// </summary>
        /// <param name="username">The username of the user to retrieve</param>
        /// <returns>The user object from the underlying datastore</returns>
        /// <exception cref="ValidationException">if the username is null or empty</exception>
        /// <exception cref="AccountServiceException">If an account with the specified username no longer exists, or the account has been set to inactive</exception>
        public async Task<User> GetUserAsync(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(username)}' cannot be null or empty.");

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);

            // User no longer exists / set to inactive
            if (user == null || user.Status != (int)UserStatus.Active)
                throw new AccountServiceException(Messages.Errors.AccountDisabledOrDeleted.Code, Messages.Errors.AccountDisabledOrDeleted.Message);

            return user;
        }
        /// <summary>
        /// Updates the user with the given username without changing their password.
        /// </summary>
        /// <param name="username">Username of user to update</param>
        /// <param name="displayName">Updated display name</param>
        /// <param name="email">updated email address</param>
        /// <param name="receiveNotifications">Indicates whether the user wants to receive notifications from the application</param>
        /// <returns>The updated user object.</returns>
        /// <exception cref="AccountServiceException">If an account with the specified username no longer exists, or the account has been set to inactive</exception>
        /// <exception cref="ValidationException">if any of the specified arguments are null or empty</exception>
        /// <exception cref="DuplicateValueException">if the specified <paramref name="displayName"/> or <paramref name="email"/> is already used by another user.</exception>
        public async Task<User> UpdateUserAsync(string username, string displayName, string email = null, bool receiveNotifications = false)
        {
            if (string.IsNullOrEmpty(username))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(username)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(displayName))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(displayName)}' cannot be null or empty.");
            if (!string.IsNullOrEmpty(email) && !RegexUtil.IsValidEmail(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(email)}' is not a valid email address.");
            if (receiveNotifications && string.IsNullOrEmpty(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(receiveNotifications)}' can only be true in combination with a valid email address.");

            // Retrieve user from database
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);

            // User no longer exists / set to inactive
            if (user == null || user.Status != (int)UserStatus.Active)
                throw new AccountServiceException(Messages.Errors.AccountDisabledOrDeleted.Code, Messages.Errors.AccountDisabledOrDeleted.Message);

            // If the display name has changed, make sure that it is not already taken by another user
            if (user.DisplayName != displayName && await _context.Users.CountAsync(u => u.DisplayName == displayName && u.Id != user.Id) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateDisplayName.Code, $"The display name '{displayName}' is already taken by another user.");

            // Make sure that the entered email address is not used by another user
            email = !string.IsNullOrEmpty(email) ? email.ToLower().Trim() : null;
            if (!string.IsNullOrEmpty(email) && await _context.Users.CountAsync(u => u.Email == email && u.Id != user.Id) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateEmailAddress.Code, "The email address is already being used by another user.");

            // Update user
            user.DisplayName = displayName;
            user.Email = email;
            user.ReceiveNotifications = receiveNotifications;

            // Persist changes
            await _context.SaveChangesAsync();

            _logger.LogInformation($"User account with ID '{user.Id}' was updated.");

            return user;
        }
        /// <summary>
        /// Updates the specified user's password.
        /// </summary>
        /// <param name="username">Username of user to update</param>
        /// <param name="password">The new password</param>
        /// <returns>a task to be awaited</returns>
        /// <exception cref = "AccountServiceException" > If an account with the specified ID no longer exists, or the account has been set to inactive</exception>
        /// <exception cref="ValidationException">if the password is null or doesn't meet the minimum length requirements</exception>
        public async Task UpdatePasswordAsync(string username, string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(password)}' cannot be null or empty.");
            if (password.Length < 8)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(password)}' must be at least 8 characters long.");

            // Retrieve user from database
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);

            // User no longer exists / set to inactive
            if (user == null || user.Status != (int)UserStatus.Active)
                throw new AccountServiceException(Messages.Errors.AccountDisabledOrDeleted.Code, Messages.Errors.AccountDisabledOrDeleted.Message);

            // Hash password
            var passwordHash = BCrypt.Net.BCrypt.EnhancedHashPassword(password, _config.BCryptWorkFactor);

            // Update user's password
            user.Password = passwordHash;

            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Tries to authenticate the user with the provided credentials.
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="password">The password</param>
        /// <returns>Returns the a JWT token pair for the authenticated user, if the authentication was successful</returns>
        /// <exception cref="ValidationException">If the supplied username or password is <see langword="null"/> or an empty string</exception>
        /// <exception cref="AccountServiceException">if the authentication failed, or the user was deleted/set to inactive</exception>
        public async Task<JwtAuthResult> AuthenticateUserAsync(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(username)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(password))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(password)}' cannot be null or empty.");

            // Retrieve user
            var user = await _context.Users
                .Include(u => u.RefreshToken)
                .Where(u => u.Username == username)
                .FirstOrDefaultAsync();

            // Verify supplied password (to prevent side-channel attacks, we will also do this when the user doesn't exist)
            var isAuthenticated = BCrypt.Net.BCrypt.EnhancedVerify(password,
                user != null ? user.Password : _config.BCryptDefaultHash);
            
            if (!isAuthenticated)
            {
                // User entered the wrong credentials, log the attempt and throw exception
                _logger.LogWarning($"Failed login attempt by user [{username}].");
                throw new AccountServiceException(Messages.Errors.AuthenticationFailed.Code, Messages.Errors.AuthenticationFailed.Message);
            }
            else if(user != null && user.Status != (int)UserStatus.Active)
            {
                // The provided credentials were correct, but the account is not set to active
                throw new AccountServiceException(Messages.Errors.AuthenticationFailed.Code, Messages.Errors.AuthenticationFailed.Message);
            }
            

            // Generate JWT token pair
            var claims = new[]
            {
                new Claim(ClaimTypes.Sid, user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Username),
                new Claim(ClaimTypes.Name, user.DisplayName),
                new Claim(ClaimTypes.Role, user.Role),
            };

            var jwtResult = await _authManager.GenerateTokensAsync(user, claims, DateTime.UtcNow);

            // Login was successful
            _logger.LogInformation($"User [{user.Username}] logged into the system.");
            return jwtResult;
        }
        /// <summary>
        /// Refreshes the JWT token pair for the user with the specified username.
        /// </summary>
        /// <param name="accessToken">The user's current access token</param>
        /// <param name="refreshToken">The user's refresh token</param>
        /// <returns>A new token pair for the user</returns>
        /// <exception cref="ValidationException">If any of the given arguments are null/empty</exception>
        /// <exception cref="AccountServiceException">If the user was disabled/deleted, or if the refresh token is unknown/expired</exception>
        public async Task<JwtAuthResult> RefreshTokenAsync(string accessToken, string refreshToken)
        {
            if (string.IsNullOrEmpty(accessToken))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(accessToken)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(refreshToken))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(refreshToken)}' cannot be null or empty.");

            // Verify that the refresh token is known
            var record = await _authManager.FindIssuedRefreshTokensAsync(refreshToken);

            if (record == null)
                throw new AccountServiceException(Messages.Errors.RefreshTokenInvalidOrExpired.Code, Messages.Errors.RefreshTokenInvalidOrExpired.Message);

            _logger.LogInformation($"User [{record.User.Username}] is trying to refresh their JWT token.");

            if (record.User.Status != (int)UserStatus.Active)
                throw new AccountServiceException(Messages.Errors.AccountDisabledOrDeleted.Code, Messages.Errors.AccountDisabledOrDeleted.Message);
            
            var jwtResult = await _authManager.RefreshAsync(refreshToken, accessToken, DateTime.UtcNow);
            _logger.LogInformation($"User [{record.User.Username}] has refreshed their JWT token.");

            return jwtResult;
        }
        /// <summary>
        /// De-authenticates (logs out) users by revoking their refresh tokens.
        /// </summary>
        /// <param name="userId">The ID of the user to de-authenticate</param>
        /// <param name="username">The username of the user to de-authenticate</param>
        /// <exception cref="ValidationException">if the specified username is null or empty</exception>
        public async Task DeauthenticateUserAsync(int userId, string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(username)}' cannot be null or empty.");

            await _authManager.RemoveRefreshTokenByUserAsync(userId);
            // TODO: The user's current access token should be added to a revocation list

            if (!string.IsNullOrEmpty(username))
                _logger.LogInformation($"User [{username}] logged out of the system.");
        }
        /// <summary>
        /// Deletes the user with the specified name.
        /// </summary>
        /// <param name="username">The user's username</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>A task that can be awaited</returns>
        /// <exception cref="ValidationException">if the given username is null or empty.</exception>
        /// <exception cref="EntityNotFoundException">if a user with the specified username doesn't exist.</exception>
        public async Task DeleteAccountAsync(string username, int retentionDays)
        {
            if (string.IsNullOrEmpty(username))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(username)}' cannot be null or empty.");

            // Retrieve user form database
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);

            if (user == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, "A user with the specified username doesn't exist.");

            // De-authenticate user
            await _authManager.RemoveRefreshTokenByUserAsync(user.Id);

            // Retrieve the last event that the user participated in
            var lastEvent = await _context.Registrations
                .Where(r => r.UserId == user.Id && r.Status == Convert.ToInt32(RegistrationStatus.Registered))
                .Where(r => r.Event.Date < DateTime.UtcNow)
                .OrderByDescending(r => r.Event.Date)
                .Select(r => r.Event)
                .FirstOrDefaultAsync();

            // Evaluate if the user can be deleted (must not have participated in any events during the last <retentionDays> days)
            if (lastEvent == null || (DateTime.UtcNow - lastEvent.Date).Days > retentionDays)
            {
                // It's safe to delete the user
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();

                _logger.LogInformation($"User [{username}] deleted their account.");

                // Inform the user that their account has been deleted
                await _notificationService.SendAccountDeletedNotificationAsync(user);
            }
            else
            {
                // Can't delete user right away, set the deletion date to the last event's date + X days
                user.DeletionDate = lastEvent.Date.AddDays(retentionDays);
                user.Status = Convert.ToInt32(UserStatus.PendingDeletion);

                // Retrieve all upcoming event that the user has registered for
                var upcomingEventRegistrations = await _context.Registrations
                    .Where(r => r.Event.Date > DateTime.UtcNow)
                    .Where(r => r.UserId == user.Id)
                    .ToListAsync();

                // Remove upcoming registrations and persist changes
                _context.Registrations.RemoveRange(upcomingEventRegistrations);
                await _context.SaveChangesAsync();

                _logger.LogInformation($"User [{username}] has requested the deletion of their account. Deletion will performed on {user.DeletionDate?.ToString("yyyy-MM-dd")}.");

                // Inform the user that the deletion of their account has been scheduled
                await _notificationService.SendAccountDeletionScheduledNotificationAync(user);
            }
        }
        /// <summary>
        /// Generates an account recovery token for the user with the specified email address and sends it to that email address.
        /// </summary>
        /// <param name="email">The user's email address</param>
        /// <returns>An account recovery token for the user.</returns>
        /// <exception cref="ValidationException">If one of the given arguments is invalid</exception>
        /// <exception cref="EntityNotFoundException">If a user with the specified email address doesn't exist</exception>
        /// <exception cref="AccountServiceException">If the account has been disabled.</exception>
        public async Task<AccountRecoveryToken> GenerateRecoveryTokenAsync(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(email)}' cannot be null or empty.");
            if (!string.IsNullOrEmpty(email) && !RegexUtil.IsValidEmail(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(email)}' is not a valid email address.");

            _logger.LogInformation($"A user [{RE_ANONYMIZE_EMAIL_ADDRESS.Replace(email, "*")}] is trying to recover their account.");

            // Find user in database
            var user = await _context.Users
                .Include(u => u.RecoveryToken)
                .Where(u => u.Email.ToLower() == email.ToLower())
                .FirstOrDefaultAsync();

            if (user == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, "A user with the email address doesn't exist.");
            if (user.Status != (int)UserStatus.Active)
                throw new ValidationException(Messages.Errors.AccountDisabledOrDeleted.Code, Messages.Errors.AccountDisabledOrDeleted.Message);

            // Generate a new recovery token and return it
            var recoveryToken = await GenerateRecoveryToken(user);

            // Send token to user
            await _notificationService.SendAccountRecoveryNotificationAsync(user, recoveryToken);

            return recoveryToken;
        }
        /// <summary>
        /// Generates an account recovery token for the user with the specified ID.
        /// </summary>
        /// <param name="userId">The ID of the user for whom to generate a new recovery token</param>
        /// <returns>An account recovery token for the user.</returns>
        /// <exception cref="EntityNotFoundException">If a user with the specified ID doesn't exist</exception>
        /// <exception cref="AccountServiceException">If the user account is set to inactive</exception>
        public async Task<AccountRecoveryToken> GenerateRecoveryTokenAsync(int userId)
        {
            // Find user in database
            var user = await _context.Users
                .Include(u => u.RecoveryToken)
                .Where(u => u.Id == userId)
                .FirstOrDefaultAsync();

            if (user == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, "The user with the specified ID doesn't exist.");
            if (user.Status != (int)UserStatus.Active)
                throw new ValidationException(Messages.Errors.AccountDisabledOrDeleted.Code, Messages.Errors.AccountDisabledOrDeleted.Message);

            _logger.LogInformation($"Account recovery for the user with ID '{userId}' was triggered.");

            // Generate a new recovery token and return it
            return await GenerateRecoveryToken(user);
        }
        /// <summary>
        /// Updates a user's password using a previously generated account recovery token.
        /// </summary>
        /// <param name="recoveryToken">A valid recovery token that identifies the user</param>
        /// <param name="password">The user's new password value</param>
        /// <returns>a Task that can be awaited</returns>
        /// <exception cref="ValidationException">if either of the given parameters is <see langword="null"/> or empty, the token is invalid or if the user account has been disabled</exception>
        public async Task ResetPasswordAsync(string recoveryToken, string password)
        {
            if (string.IsNullOrEmpty(recoveryToken))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(recoveryToken)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(password))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(password)}' cannot be null or empty.");

            _logger.LogInformation($"New attempt to reset an account password using the recovery token [{recoveryToken}].");

            // Find the specified token in the database
            var art = await _context.RecoveryTokens.FirstOrDefaultAsync(t => t.Token == recoveryToken);

            // Validate the token
            if (art == null || art.ExpiresAt < DateTime.UtcNow)
                throw new ValidationException(Messages.Errors.RecoveryTokenInvalidOrExpired.Code, Messages.Errors.RecoveryTokenInvalidOrExpired.Message);

            // Retrieve the user separately
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == art.UserId);

            // Make sure the user is still active
            if (user.Status != (int)UserStatus.Active)
                throw new ValidationException(Messages.Errors.AccountDisabledOrDeleted.Code, Messages.Errors.AccountDisabledOrDeleted.Message);

            // Update the user with the new password's hash
            user.Password = BCrypt.Net.BCrypt.EnhancedHashPassword(password, _config.BCryptWorkFactor);

            // Write changes to database
            _context.RecoveryTokens.Remove(art);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"The password for the user account with ID '{user.Id}' was successfully reset.");
        }

        /// <summary>
        /// Generates a new account recovery token for the given user (must include RecoveryToken navigation property), updates the database and returns it.
        /// </summary>
        /// <param name="user">The user for whom to generate a new AccountRecoveryToken</param>
        /// <returns>The account recovery token that was generated</returns>
        private async Task<AccountRecoveryToken> GenerateRecoveryToken(User user)
        {
            string token = null;

            // Keep generating tokens until we have one that doesn't exist yet (hopefully that won't take more than one iteration)
            do { token = KeyGen.GenerateUniqueKey(12); }
            while (await _context.RecoveryTokens.CountAsync(t => t.Token == token) > 0);

            AccountRecoveryToken recoveryToken = null;
            if (user.RecoveryToken == null)
            {
                // Create a new token
                recoveryToken = new AccountRecoveryToken
                {
                    UserId = user.Id,
                    Token = token,
                    ExpiresAt = DateTime.UtcNow.AddMinutes(_config.RecoveryTokenLifetime)
                };

                await _context.RecoveryTokens.AddAsync(recoveryToken);
            }
            else
            {
                // Update the user's existing token
                user.RecoveryToken.Token = token;
                user.RecoveryToken.ExpiresAt = DateTime.UtcNow.AddMinutes(_config.RecoveryTokenLifetime);

                recoveryToken = user.RecoveryToken;
            }

            // Persist changes
            await _context.SaveChangesAsync();

            return recoveryToken;
        }

        #endregion
    }
}

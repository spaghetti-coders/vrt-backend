﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Concurrent;
using System.Text;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;
using VRT.Core.Configurations;
using VRT.Core.Models.Jwt;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using VRT.Data.Entities;
using VRT.Data;
using Microsoft.EntityFrameworkCore;
using VRT.Core.Exceptions;
using VRT.Core.Constants;

namespace VRT.Core.Services
{
    /// <summary>
    /// The default implementation of the IJwtAuthManager interface.
    /// </summary>
    public class JwtAuthManager : IJwtAuthManager
    {
        #region Fields

        /// <summary>
        /// Returns a dictionary of all refresh tokens (per user) that are currently valid.
        /// </summary>
        private readonly ILogger<JwtAuthManager> _logger;
        private readonly JwtConfiguration _config;
        private readonly VRTContext _context;
        private readonly byte[] _secret;

        #endregion


        /// <summary>
        /// Constructs a new JwtAuthManager object.
        /// </summary>
        /// <param name="configuration">The configuration to be used by this manager instance.</param>
        /// <param name="context">An VRTContext reference</param>
        /// <param name="logger">An ILogger reference</param>
        public JwtAuthManager(JwtConfiguration configuration, VRTContext context, ILogger<JwtAuthManager> logger)
        {
            _context = context;
            _config = configuration;
            _logger = logger;

            _secret = Encoding.ASCII.GetBytes(_config.Secret);
        }


        #region Methods

        /// <summary>
        /// Generates a set of access and refresh tokens with specified claims and user.
        /// </summary>
        /// <param name="user">The user for whom the tokens will be issued</param>
        /// <param name="claims">The claims to encode in the tokens</param>
        /// <param name="now">The date and time this token will be issued at</param>
        /// <returns>A JwtAuthResult containing tokens for the user.</returns>
        public async Task<JwtAuthResult> GenerateTokensAsync(User user, Claim[] claims, DateTime now)
        {
            // Determine whether to add an audience to the token
            var shouldAddAudienceClaim = string.IsNullOrWhiteSpace(claims?.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Aud)?.Value);

            // Generate JWT security token
            var jwtToken = new JwtSecurityToken(
                _config.Issuer,
                shouldAddAudienceClaim ? _config.Audience : string.Empty,
                claims,
                expires: now.AddMinutes(_config.AccessTokenLifetime),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_secret), SecurityAlgorithms.HmacSha256Signature));

            // Generate the access token
            var accessToken = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            // Generate the refresh token
            RefreshToken refreshToken = null;
            if (user.RefreshToken == null)
            {
                refreshToken = new RefreshToken
                {
                    UserId = user.Id,
                    TokenString = GenerateRefreshTokenString(),
                    ExpiresAt = now.AddMinutes(_config.RefreshTokenLifetime)
                };

                await _context.RefreshTokens.AddAsync(refreshToken);
            }
            else
            {
                user.RefreshToken.TokenString = GenerateRefreshTokenString();
                user.RefreshToken.ExpiresAt = now.AddMinutes(_config.RefreshTokenLifetime);

                refreshToken = user.RefreshToken;
            }

            // Persist changes
            await _context.SaveChangesAsync();

            // Return token pair
            return new JwtAuthResult
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };
        }
        /// <summary>
        /// Refreshes a user's token pair.
        /// </summary>
        /// <param name="refreshToken">The user's current refresh token</param>
        /// <param name="accessToken">The user's current access token</param>
        /// <param name="now"></param>
        /// <returns>A JwtAuthResult containing a new set of tokens for the user.</returns>
        public async Task<JwtAuthResult> RefreshAsync(string refreshToken, string accessToken, DateTime now)
        {
            // Decode given access token
            var (principal, jwtToken) = DecodeJwtToken(accessToken);

            // Validate security token
            if (jwtToken == null || !jwtToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature))
                throw new SecurityTokenException("Invalid token");

            // Check if the specified refresh token exists
            var existingRefreshToken = await FindIssuedRefreshTokensAsync(refreshToken);
            if (existingRefreshToken == null)
                throw new SecurityTokenException("Invalid token");

            var username = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            if (existingRefreshToken.User.Username != username || existingRefreshToken.ExpiresAt < now)
                throw new SecurityTokenException("Invalid token");

            // All okay, generate a new pair and return it
            return await GenerateTokensAsync(existingRefreshToken.User, principal.Claims.ToArray(), now);
        }
        /// <summary>
        /// Removes all tokens that have expired compared to the given DateTime object.
        /// </summary>
        /// <param name="now">DateTime object to compare refresh tokens against</param>
        /// <returns>the number of refresh tokens that have been removed</returns>
        public int RemoveExpiredRefreshTokens(DateTime now)
        {
            // Retrieve expired tokens from database
            var tokensToRemove = _context.RefreshTokens
                .Where(rt => rt.ExpiresAt < now)
                .ToList();

            // Delete tokens from database
            _context.RefreshTokens.RemoveRange(tokensToRemove);

            // Persist changes
            return _context.SaveChanges();
        }
        /// <summary>
        /// Removes any refresh tokens that might be associated with the specified user.
        /// </summary>
        /// <param name="userId">The ID of the user whose token to discard</param>
        /// <returns>A task to be awaited</returns>
        public async Task RemoveRefreshTokenByUserAsync(int userId)
        {
            // Retrieve user's current refresh token
            var token = await _context.RefreshTokens.FirstOrDefaultAsync(rt => rt.UserId == userId);

            if (token != null)
            {
                // Remove token and persist changes
                _context.RefreshTokens.Remove(token);
                await _context.SaveChangesAsync();
            }
        }
        /// <summary>
        /// Decodes the given access token.
        /// </summary>
        /// <param name="token">The access token to decode</param>
        /// <returns>The token's claims and security token.</returns>
        public (ClaimsPrincipal, JwtSecurityToken) DecodeJwtToken(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new SecurityTokenException("Invalid token");

            // Construct the parameters that the token will be validated against
            var tvps = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _config.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(_secret),
                ValidateAudience = true,
                ValidAudience = _config.Audience,
                ValidateLifetime = false,
                ClockSkew = TimeSpan.FromMinutes(1)
            };

            // Validate the token against the parameters
            var principal = new JwtSecurityTokenHandler().ValidateToken(token, tvps, out var validatedToken);

            // Return both principal and the validated security token
            return (principal, validatedToken as JwtSecurityToken);
        }
        /// <summary>
        /// Returns the refresh token with the specified token string, or <see langword="null"/> if no such token exists.
        /// </summary>
        /// <paramref name="tokenString">The refresh token's string</paramref>
        /// <returns>the refresh token with the specified token string, or <see langword="null"/> if no such refresh token exists.</returns>
        public async Task<RefreshToken> FindIssuedRefreshTokensAsync(string tokenString)
        {
            return await _context.RefreshTokens
                .Include(rt => rt.User)
                .Where(rt => rt.TokenString == tokenString)
                .FirstOrDefaultAsync();
        }

        #endregion

        #region Functions

        /// <summary>
        /// Convenience function for creating a cryptographically secure, random refresh token string.
        /// </summary>
        /// <returns>a randomly generated, cryptographically secure refresh token string.</returns>
        private static string GenerateRefreshTokenString()
        {
            var bytes = new byte[32];

            using (var rng = RandomNumberGenerator.Create())
                rng.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }

        #endregion
    }
}

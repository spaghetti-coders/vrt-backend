﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VRT.Data.Entities;
using VRT.Core.Exceptions;

namespace VRT.Core.Services
{
    /// <summary>
    /// A service for managing events.
    /// </summary>
    public interface IEventService
    {
        /// <summary>
        /// Returns the event with the specified ID (if any) from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to return</param>
        /// <returns>The event with the specified ID, or <see langword="null"/> if no event with that ID exists</returns>
        Task<Event> FindEventAsync(int id);
        /// <summary>
        /// Returns the event with the specified ID from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to return</param>
        /// <returns>The event with the specified ID</returns>
        /// <exception cref="EntityNotFoundException">if the event with the specified ID doesn't exist</exception>
        Task<Event> GetEventAsync(int id);
        /// <summary>
        /// Returns the number of events that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of events matching the given filter string</returns>
        Task<int> CountEventsAsync(string filter);
        /// <summary>
        /// Returns a list of events that are matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="userId">The current user's ID</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of events that are matching the given filter (if any) from the underlying datastore</returns>
        Task<IEnumerable<Event>> FindEventsAsync(int userId, int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc");
        /// <summary>
        /// Returns the number of future/upcoming events matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <param name="activeOnly">If set to <see langword="true"/>, only events open for registration will be returned; otherwise all future events</param>
        /// <returns>Returns the number of future/upcoming events matching the given filter string.</returns>
        Task<int> CountFutureEventsAsync(string filter, bool activeOnly);
        /// <summary>
        /// Returns a list of future/upcoming events matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="userId">The current user's ID</param>
        /// <param name="activeOnly">If set to <see langword="true"/>, only events open for registration will be returned; otherwise all future events</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of future/upcoming events matching the given filter (if any) from the underlying datastore.</returns>
        Task<IEnumerable<Event>> FindFutureEventsAsync(int userId, bool activeOnly, int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc");
        /// <summary>
        /// Returns the number of events that already took place and that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of events that already took place and that are matching the given filter string.</returns>
        Task<int> CountPastEventsAsync(string filter);
        /// <summary>
        /// Returns a list of events that already took place and that are matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="userId">The current user's ID</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of events that already took place and that are matching the given filter (if any) from the underlying datastore</returns>
        Task<IEnumerable<Event>> FindPastEventsAsync(int userId, int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc");
        /// <summary>
        /// Creates a new event with the specified properties in the underlying datastore.
        /// </summary>
        /// <param name="venueId">The ID of the venue where tis event is taking place</param>
        /// <param name="name">The name of the event</param>
        /// <param name="description">An optional description</param>
        /// <param name="date">The date and time when this event is taking place</param>
        /// <param name="capacity">The maximum number of participants that can partake in that event</param>
        /// <param name="isOpenForRegistration">Indicates whether users are allowed to register for this event</param>
        /// <returns>Returns the newly created event</returns>
        /// <exception cref="ValidationException">if one of the required parameters is invalid</exception>
        /// <exception cref="EntityNotFoundException">If the venue with the specified ID doesn't exist</exception>
        Task<Event> CreateEventAsync(int venueId, string name, string description, DateTime date, int capacity, bool isOpenForRegistration);
        /// <summary>
        /// Updates the specified event with in the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to update</param>
        /// <param name="venueId">The ID of the venue where tis event is taking place</param>
        /// <param name="name">The name of the event</param>
        /// <param name="description">An optional description</param>
        /// <param name="date">The date and time when this event is taking place</param>
        /// <param name="capacity">The maximum number of participants that can partake in that event</param>
        /// <param name="isOpenForRegistration">Indicates whether users are allowed to register for this event</param>
        /// <returns>Returns the updated event</returns>
        /// <exception cref="ValidationException">if one of the required parameters is invalid</exception>
        /// <exception cref="EntityNotFoundException">If the event or venue with the specified ID doesn't exist</exception>
        Task<Event> UpdateEventAsync(int id, int venueId, string name, string description, DateTime date, int capacity, bool isOpenForRegistration);
        /// <summary>
        /// Updates the given event object in the underlying data store.
        /// </summary>
        /// <param name="evt">The updated event object</param>
        /// <returns>The updated event record</returns>
        /// <exception cref="ValidationException">if the given event is null</exception>
        Task<Event> UpdateEventAsync(Event evt);
        /// <summary>
        /// Deletes an event from the underlying datastore.
        /// </summary>
        /// <param name="evt">The event to delete</param>
        /// <returns>The event that was deleted</returns>
        Task<Event> DeleteEventAsync(Event evt);
        /// <summary>
        /// Deletes the event with the specified ID from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The event that was deleted</returns>
        /// <exception cref="EntityNotFoundException">If the event with the specified ID doesn't exist</exception>
        /// <exception cref="ValidationException">If the events has concluded but is still in retention</exception>
        Task<Event> DeleteEventAsync(int id, int retentionDays);
        /// <summary>
        /// Creates a text log for the event with the specified ID. 
        /// The log contains the event's general information and list of participants.
        /// </summary>
        /// <param name="evt">The event for which to create the log</param>
        /// <returns>The EventLog object that was created</returns>
        /// <exception cref="EntityNotFoundException">If an event with the specified ID doesn't exist</exception>
        /// <exception cref="ValidationException">If the event is still in the future (hasn't started yet)</exception>
        Task<string> CreateEventLogAsync(Event evt);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// A service used for manaing users' push subscriptions and for sending push notifications.
    /// </summary>
    public interface IPushService
    {
        /// <summary>
        /// Creates a new subscription for the specified user and returns it.
        /// </summary>
        /// <param name="userId">The ID of the user that is subscribing</param>
        /// <param name="endpoint">The user's push notification endpoint</param>
        /// <param name="pubKey">The user subscription's public key</param>
        /// <param name="secret">The user subscription's authentication secret</param>
        /// <returns>The newly created PushSubscription object</returns>
        /// <exception cref="ValidationException">if one of the specified values is null or invalid</exception>
        /// <exception cref="EntityNotFoundException">if the user with the specified ID doesn't exist</exception>
        Task<PushSubscription> SubscribeAsync(int userId, string endpoint, string pubKey, string secret);
        /// <summary>
        /// Deletes the user subscription to receive push notifications.
        /// </summary>
        /// <param name="userId">The ID of the user that is unsubscribing</param>
        /// <returns>The subscription that was deleted, or <see langword="null"/> if the user was not subscribed</returns>
        Task<PushSubscription> UnsubscribeAsync(int userId);
        /// <summary>
        /// Tries to send a push notification to the specified user.
        /// </summary>
        /// <param name="userId">The user's ID</param>
        /// <param name="title">The notification's title</param>
        /// <param name="message">The message to be sent</param>
        /// <returns>a Task to be awaited</returns>
        /// <exception cref="ValidationException">if one of the specified values is null or invalid</exception>
        /// <exception cref="EntityNotFoundException">if the specified user wasn't found, or no subscription for that user exists</exception>
        Task SendNotificationAsync(int userId, string title, string message);
        /// <summary>
        /// Sends a push notification to all active subscribers.
        /// </summary>
        /// <param name="title">The notification's title</param>
        /// <param name="message">The message to send</param>
        /// <returns>a Task to be awaited</returns>
        /// <exception cref="ValidationException">if message is null or invalid</exception>
        Task SendBroadcastNotificationAsync(string title, string message);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// A service for notifying users about certain events.
    /// </summary>
    public interface INotificationService
    {
        /// <summary>
        /// Informs all users that have opted-in that a new event is available for registration.
        /// </summary>
        /// <param name="evt">The event</param>
        /// <returns>a task to be awaited</returns>
        Task SendNewEventNotificationAsync(Event evt);
        /// <summary>
        /// Informs all users that have opted-in, that an event they have subscribed for was canceled.
        /// </summary>
        /// <param name="evt">The event</param>
        /// <returns>a task to be awaited</returns>
        Task SendEventCanceledNotificationAsync(Event evt);
        /// <summary>
        /// Tries to inform the application admins that a new user has registered.
        /// </summary>
        /// <param name="user">The user that registered</param>
        /// <returns>a Task to be awaited</returns>
        Task SendNewUserRegisteredNotificationAsync(User user);
        /// <summary>
        /// Informs the specified users that they haved moved from the event's list of active participants to the waiting list.
        /// </summary>
        /// <param name="evt">The event for that the users are registered</param>
        /// <param name="registration">The registrations of the users that are affected</param>
        /// <returns>a task to be awaited</returns>
        Task SendMovedToWaitingListNotificationAsync(Event evt, List<Registration> registrations);
        /// <summary>
        /// Informs the specified users that they haved moved from the event's waiting list to the list of active participants.
        /// </summary>
        /// <param name="evt">The event for that the users are registered</param>
        /// <param name="registration">The registrations of the users that are affected</param>
        /// <returns>a task to be awaited</returns>
        Task SendMovedToParticipantsNotificationAsync(Event evt, List<Registration> registrations);
        /// <summary>
        /// Informs the specified user that their registration to an event has been deleted by an administrator.
        /// </summary>
        /// <param name="evt">The event for that the user registered</param>
        /// <param name="registration">The affected user's registration</param>
        /// <returns>a task to be awaited</returns>
        Task SendRegistrationDeletedNotificationAsync(Event evt, Registration registration);
        /// <summary>
        /// Informs the user that their registration has been received.
        /// </summary>
        /// <param name="user">The user to inform</param>
        /// <returns>a Task to be awaited</returns>
        Task SendUserRegistrationConfirmationNotificationAsync(User user);
        /// <summary>
        /// Informs the user that their account has been activated.
        /// </summary>
        /// <param name="user">The user to inform</param>
        /// <returns>a Task to be awaited</returns>
        Task SendUserAccountActivatedNotificationAsync(User user);
        /// <summary>
        /// Sends the user an account recovery notification.
        /// </summary>
        /// <param name="user">The user to inform</param>
        /// <param name="recoveryToken">The recovery token that was generated for the user</param>
        /// <returns>a Task to be awaited</returns>
        Task SendAccountRecoveryNotificationAsync(User user, AccountRecoveryToken recoveryToken);
        /// <summary>
        /// Informs the user that their account and all related data has been deleted.
        /// </summary>
        /// <param name="user">The user to notify</param>
        /// <returns>a task to be awaited</returns>
        Task SendAccountDeletedNotificationAsync(User user);
        /// <summary>
        /// Informs the user that the deletion of their account and all related data has been scheduled.
        /// </summary>
        /// <param name="user">The user to notify</param>
        /// <returns>a task to be awaited</returns>
        Task SendAccountDeletionScheduledNotificationAync(User user);
        /// <summary>
        /// Sends a push notification to the user of the given subscription, informing them that they successfully subscribed to the push service.
        /// </summary>
        /// <param name="subscription">The user's subscription</param>
        void SendPushSubscriptionNotification(PushSubscription subscription);
        /// <summary>
        /// Sends a push notification to the user of the given subscription, informing them that they have succesfully unsubscribed from receiving further push notifications.
        /// </summary>
        /// <param name="subscription">The user's subscription</param>
        void SendPushUnsubscribeNotification(PushSubscription subscription);
        /// <summary>
        /// Sends a push message to the specified subscriber.
        /// </summary>
        /// <param name="title">The message's title</param>
        /// <param name="subscription">The subscription of the user that is to receive the message</param>
        /// <param name="message">The message that is to be send to the user</param>
        void SendPushNotification(PushSubscription subscription, string title, string message);
        /// <summary>
        /// Sends a push message to the specified subscriber.
        /// </summary>
        /// <param name="subscriptions">The subscriptions of the users that are to receive the message</param>
        /// <param name="title">The message's title</param>
        /// <param name="message">The message that is to be send to the users</param>
        void SendPushNotification(List<PushSubscription> subscriptions, string title, string message);
        /// <summary>
        /// Sends a custom email notification to the specified recipients.
        /// </summary>
        /// <param name="subject">The message's subject line</param>
        /// <param name="body">The message's body</param>
        /// <param name="recipients">The message's recipients</param>
        void SendEmailNotification(string subject, string body, List<string> recipients);
    }
}

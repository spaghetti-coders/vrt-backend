﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;
using VRT.Core.Exceptions;

namespace VRT.Core.Services
{
    /// <summary>
    /// Provides methods for managing venues.
    /// </summary>
    public interface IVenueService
    {
        /// <summary>
        /// Returns the venue with the specified ID (if any) from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the venue to return</param>
        /// <returns>The venue with the specified ID, or <see langword="null"/> if no venue with that ID exists</returns>
        Task<Venue> FindVenueAsync(int id);
        /// <summary>
        /// Returns the number of venues that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of venues matching the given filter string</returns>
        Task<int> CountVenuesAsync(string filter);
        /// <summary>
        /// Returns a list of venues that are matching the given filter (if any) from the underyling datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of users that are matching the given filter (if any) from the underyling datastore</returns>
        Task<IEnumerable<Venue>> FindVenuesAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc");
        /// <summary>
        /// Returns the number of active venues that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of active venues matching the given filter string</returns>
        Task<int> CountActiveVenuesAsync(string filter);
        /// <summary>
        /// Returns a list of active venues that are matching the given filter (if any) from the underyling datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of active venues that are matching the given filter (if any) from the underyling datastore</returns>
        Task<IEnumerable<Venue>> FindActiveVenuesAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc");
        /// <summary>
        /// Creates a new venue with the specified properties in the underlying datastore.
        /// </summary>
        /// <param name="name">The name of the venue</param>
        /// <param name="address">The addrress of the venue</param>
        /// <param name="capacity">The maximum number of participants that venue can hold</param>
        /// <param name="colorCode">A hex-code used to colorize events taking place at this venue with</param>
        /// <param name="isActive">Sets the venue in/active</param>
        /// <returns>Returns the newly created venue</returns>
        /// <exception cref="DuplicateValueException">if a venue with the given name already exists</exception>
        /// <exception cref="ValidationException">if one of the required parameters is invalid</exception>
        Task<Venue> CreateVenueAsync(string name, string address, int capacity, string colorCode, bool isActive);
        /// <summary>
        /// Updates the given venue object in the underlying data store.
        /// </summary
        /// <param name="venue">The updated venue object</param>
        /// <returns>The updated venue record</returns>
        /// <exception cref="ValidationException">if the given venue is null</exception>
        /// <exception cref="DuplicateValueException">If a venue with the specified name already exists</exception>
        Task<Venue> UpdateVenueAsync(Venue venue);
        /// <summary>
        /// Updates the venue with the specified ID.
        /// </summary>
        /// <param name="id">ID of venue to update</param>
        /// <param name="name">The venue's new name</param>
        /// <param name="address">The venue's new address</param>
        /// <param name="capacity">The venue's new capacity</param>
        /// <param name="colorCode">A hex-code used to colorize events taking place at this venue with</param>
        /// <param name="isActive">Sets the venue in/active</param>
        /// <returns>The updated venue object.</returns>
        /// <exception cref="ValidationException">If any of required arguments are null or empty</exception>
        /// <exception cref="EntityNotFoundException">If a venue with the specified ID doesn't exist</exception>
        /// <exception cref="DuplicateValueException">If a venue with the specified name already exists</exception>
        Task<Venue> UpdateVenueAsync(int id, string name, string address, int capacity, string colorCode, bool isActive);
        /// <summary>
        /// Deletes a venue from the underlying datastore.
        /// </summary>
        /// <param name="venue">The venue to delete</param>
        /// <returns>The venue that was deleted</returns>
        Task<Venue> DeleteVenueAsync(Venue venue);
        /// <summary>
        /// Deletes the venue with the specified ID from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the venue to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The venue that was deleted</returns>
        /// <exception cref="EntityNotFoundException">If the venue with the specified ID doesn't exist</exception>
        /// <exception cref="ValidationException">If the venue has events still in retention</exception>
        Task<Venue> DeleteVenueAsync(int id, int retentionDays);
    }
}

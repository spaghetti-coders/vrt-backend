﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.Models;
using VRT.Core.Util;
using VRT.Data;
using VRT.Data.Entities;
using VRT.Data.Extensions;

namespace VRT.Core.Services
{
    public class UserService : IUserService
    {
        #region Fields

        private INotificationService _notificationService;
        private ILogger<UserService> _logger;
        private VRTContext _context;

        #endregion


        /// <summary>
        /// Constructs a new UserService object.
        /// </summary>
        /// <param name="context">A VRTContext reference</param>
        /// <param name="notificationService">An INotificationService reference</param>
        /// <param name="logger">An ILogger reference</param>
        public UserService(VRTContext context, INotificationService notificationService, ILogger<UserService> logger)
        {
            _notificationService = notificationService;
            _context = context;
            _logger = logger;
        }

        #region Methods

        /// <summary>
        /// Returns the user with the specified username from the underyling datastore.
        /// </summary>
        /// <param name="username">The user's username</param>
        /// <returns>The user object with the specified <paramref name="username"/>, or null if no such user exists.</returns>
        /// <exception cref="ValidationException">if the specified username is null or empty</exception>
        public async Task<User> FindUserAsync(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(username)}' cannot be null or empty.");

            return await _context.Users
                .FirstOrDefaultAsync(u => u.Username == username);
        }
        /// <summary>
        /// Returns the user with the specified ID from the underyling datastore.
        /// </summary>
        /// <param name="id">The user's ID</param>
        /// <returns>The user object with the specified <paramref name="id"/>, or null if no such user exists.</returns>
        public async Task<User> FindUserAsync(int id)
        {
            return await _context.Users
                .FirstOrDefaultAsync(u => u.Id == id);
        }
        /// <summary>
        /// Returns the number of users whose username or display name contain the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <param name="status">Specifies what status filter to apply</param>
        /// <returns>Returns the number of users whose username or display name contain the given filter string.</returns>
        public async Task<int> CountUsersAsync(string filter, UserStatus status = UserStatus.Any)
        {
            IQueryable<User> query = _context.Users;
            int result;

            if (status != UserStatus.Any)
                query = query
                    .Where(u => u.Status == Convert.ToInt32(status));

            if (string.IsNullOrEmpty(filter))
            {
                result = await query.CountAsync();
            }
            else
            {
                filter = filter.ToLower();

                result =  await query
                    .Where(u => u.Username.ToLower().Contains(filter) || u.DisplayName.ToLower().Contains(filter) || 
                        (!string.IsNullOrEmpty(u.Email) && u.Email.ToLower().Contains(filter)))
                    .CountAsync();
            }

            return result;
        }
        /// <summary>
        /// Returns a list of users that are matching the given filter (if any) from the underyling datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <param name="status">Specifies what status filter to apply</param>
        /// <returns>a list of users that are matching the given filter (if any) from the underyling datastore</returns>
        public async Task<IEnumerable<User>> FindUsersAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc", UserStatus status = UserStatus.Any)
        {
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Id" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";
            IQueryable<User> query = _context.Users;

            if (status != UserStatus.Any)
                query = query
                    .Where(u => u.Status == Convert.ToInt32(status));

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();

                query = query.Where(u => u.Username.ToLower().Contains(filter) || u.DisplayName.ToLower().Contains(filter) ||
                    (!string.IsNullOrEmpty(u.Email) && u.Email.ToLower().Contains(filter)));
            }

            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Return paged results
            return await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
        /// <summary>
        /// Updates the user with the specified ID in the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the user to update</param>
        /// <param name="displayName">The user's new display name</param>
        /// <param name="email">The user's new email address</param>
        /// <param name="role">The user's new role</param>
        /// <param name="receiveNotifications">Boolean indicating whether the user wants to receive notifications from the application</param>
        /// <returns>The updated user object from the datastore</returns>
        /// <exception cref="ValidationException">if one of the specified values is invalid</exception>
        /// <exception cref="EntityNotFoundException">if a user with the specified ID doesn't exist</exception>
        /// <exception cref="DuplicateValueException">if <paramref name="displayName"/> or <paramref name="email"/> is already used by a different user</exception>
        public async Task<User> UpdateUserAsync(int id, string displayName, string email, string role, bool receiveNotifications)
        {
            // Validate inputs
            if (string.IsNullOrEmpty(displayName))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(displayName)}' cannot be null or empty.");
            if (!string.IsNullOrEmpty(email) && !RegexUtil.IsValidEmail(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(email)}' is not a valid email address.");
            if (receiveNotifications && string.IsNullOrEmpty(email))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(receiveNotifications)}' can only be true in combination with a valid email address.");
            if (!Roles.IsValidRole(role))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(role)}' is not a valid user role.");

            // Retrieve user with ID
            var src = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            
            if (src == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The user with the ID '{id}' does not exist.");

            // If the display name has changed, make sure that it is not already taken by another user
            if (src.DisplayName != displayName && await _context.Users.CountAsync(u => u.DisplayName == displayName && u.Id != id) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateDisplayName.Code, $"The display name '{displayName}' is already taken by another user.");

            // If the email address has changed, make sure that it is not already taken by a different user
            email = !string.IsNullOrEmpty(email) ? email.ToLower().Trim() : null;
            if (!string.IsNullOrEmpty(email) && src.Email != email && await _context.Users.CountAsync(u => u.Email == email && u.Id != id) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateEmailAddress.Code, Messages.Errors.DuplicateEmailAddress.Message);

            // Update values
            src.DisplayName = displayName;
            src.Email = email;
            src.ReceiveNotifications = receiveNotifications;
            src.Role = role;

            // Persist changes
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Updated the user with ID '{id}'.");

            return src;
        }
        /// <summary>
        /// Updates the given user object in the underlying data store.
        /// </summary>
        /// <param name="user">The updated user object</param>
        /// <returns>The updated user record</returns>
        /// <exception cref="ValidationException">if the given user is null</exception>
        /// <exception cref="DuplicateValueException">if the display name is already used by another user</exception>
        public async Task<User> UpdateUserAsync(User user)
        {
            if (user == null)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(user)}' cannot be null.");

            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Updated the user with ID '{user.Id}'.");

            return user;
        }
        /// <summary>
        /// Updates the status of the user with the specified ID in the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the user to update</param>
        /// <param name="status">The user's new status</param>
        /// <returns>The updated user object from the datastore</returns>
        /// <exception cref="ValidationException">if the user status cannot be updated to the given value</exception>
        /// <exception cref="EntityNotFoundException">if a user with the specified ID doesn't exist</exception>
        public async Task<User> UpdateUserStatusAsync(int id, UserStatus status)
        {
            // Validate inputs
            if (status != UserStatus.Active && status != UserStatus.Inactive)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Invalid user status. The status must be either 'Active' or 'Inactive'.");

            // Retrieve user with ID
            var src = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (src == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The user with the ID '{id}' does not exist.");

            // Notify user, if they were previously inactive and were set to active
            var notifyUser = (src.Status == (int)UserStatus.PendingRegistration || src.Status == (int)UserStatus.Inactive) && status == UserStatus.Active;

            // If the user was set to be deleted, clear the targeted deletion date
            if (src.Status == (int)UserStatus.PendingDeletion)
                src.DeletionDate = null;

            // Update user
            src.Status = (int)status;

            // Persist changes
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Updated the status of user (ID='{id}') to '{status}'.");

            if (notifyUser)
                await _notificationService.SendUserAccountActivatedNotificationAsync(src);

            return src;
        }
        /// <summary>
        /// Deletes a user from the underlying data store or marks them to be deleted in the future, if they had participated in an event 
        /// in the past <paramref name="retentionDays"/> days.<br/><br/>If a user is marked to be deleted, 
        /// the date of the deletion will be set to the date of the last event they had participated in + <paramref name="retentionDays"/> days.
        /// </summary>
        /// <param name="id">The ID of the user to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The user that was deleted, or is marked for deletion</returns>
        /// <exception cref="EntityNotFoundException">If the user with the specified ID doesn't exist</exception>
        public async Task<User> DeleteUserAsync(int id, int retentionDays)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The user with ID '{id}' doesn't exist.");

            await DeleteUserAsync(user, retentionDays);

            return user;
        }
        /// <summary>
        /// Deletes a user from the underlying data store or marks them to be deleted in the future, if they had participated in an event 
        /// in the past <paramref name="retentionDays"/> days.<br/><br/>If a user is marked to be deleted, 
        /// the date of the deletion will be set to the date of the last event they had participated in + <paramref name="retentionDays"/> days.
        /// </summary>
        /// <param name="user">The user to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The user that was deleted, or is marked for deletion</returns>
        public async Task<User> DeleteUserAsync(User user, int retentionDays)
        {
            // Retrieve the last event that had taken place and that the user participated in
            var lastEvent = await _context.Registrations
                .Where(r => r.UserId == user.Id && r.Status == Convert.ToInt32(RegistrationStatus.Registered))
                .Where(r => r.Event.Date < DateTime.UtcNow)
                .OrderByDescending(r => r.Event.Date)
                .Select(r => r.Event)
                .FirstOrDefaultAsync();

            // Evaluate if the user can be deleted (must not have participated in any events during the last X days)
            if (lastEvent == null || (DateTime.UtcNow - lastEvent.Date).Days > retentionDays)
            {
                // It's safe to delete the user
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
                
                _logger.LogInformation($"Deleted the user with ID '{user.Id}'.");
            }
            else
            {
                // Can't delete user right away, set the deletion date to the last event's date + X days
                user.DeletionDate = DateTime.SpecifyKind(lastEvent.Date.AddDays(retentionDays), DateTimeKind.Utc);
                user.Status = Convert.ToInt32(UserStatus.PendingDeletion);

                // Retrieve all upcoming event that the user has registered for
                var upcomingEventRegistrations = await _context.Registrations
                    .Where(r => r.Event.Date > DateTime.UtcNow)
                    .Where(r => r.UserId == user.Id)
                    .ToListAsync();

                // Remove upcoming registrations and persist changes
                _context.Registrations.RemoveRange(upcomingEventRegistrations);
                await _context.SaveChangesAsync();

                _logger.LogInformation($"The user with ID '{user.Id}' has been marked for deletion on {user.DeletionDate?.ToString("yyyy-MM-dd")}.");
            }

            return user;
        }
        
        #endregion
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VRT.Core.Configurations;
using VRT.Core.Models;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// Provides methods for managing users.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Returns the user with the specified ID from the underyling datastore.
        /// </summary>
        /// <param name="id">The user's ID</param>
        /// <returns>The user object with the specified <paramref name="id"/>, or null if no such user exists.</returns>
        Task<User> FindUserAsync(int id);
        /// <summary>
        /// Returns the user with the specified username from the underyling datastore.
        /// </summary>
        /// <param name="username">The user's username</param>
        /// <returns>The user object with the specified <paramref name="username"/>, or null if no such user exists.</returns>
        /// <exception cref="ValidationException">if the specified username is null or empty</exception>
        Task<User> FindUserAsync(string username);
        /// <summary>
        /// Returns the number of users whose username or display name contain the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <param name="status">Specifies what status filter to apply</param>
        /// <returns>Returns the number of users whose username or display name contain the given filter string.</returns>
        Task<int> CountUsersAsync(string filter, UserStatus status = UserStatus.Any);
        /// <summary>
        /// Returns a list of users that are matching the given filter (if any) from the underyling datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <param name="status">Specifies what status filter to apply</param>
        /// <returns>a list of users that are matching the given filter (if any) from the underyling datastore</returns>
        Task<IEnumerable<User>> FindUsersAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc", UserStatus status = UserStatus.Any);
        /// <summary>
        /// Updates the given user object in the underlying data store.
        /// </summary
        /// <param name="user">The updated user object</param>
        /// <returns>The number of affected records</returns>
        /// <exception cref="ValidationException">if the given user is null</exception>
        /// <exception cref="DuplicateValueException">if the display name is already used by another user</exception>
        Task<User> UpdateUserAsync(User user);
        /// <summary>
        /// Updates the user with the specified ID in the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the user to update</param>
        /// <param name="displayName">The user's new display name</param>
        /// <param name="email">The user's new email address</param>
        /// <param name="role">The user's new role</param>
        /// <param name="receiveNotifications">Boolean indicating whether the user wants to receive notifications from the application</param>
        /// <returns>The updated user object from the datastore</returns>
        /// <exception cref="ValidationException">if one of the specified values is invalid</exception>
        /// <exception cref="EntityNotFoundException">if a user with the specified ID doesn't exist</exception>
        /// <exception cref="DuplicateValueException">if <paramref name="displayName"/> or <paramref name="email"/> is already used by a different user</exception>
        Task<User> UpdateUserAsync(int id, string displayName, string email, string role, bool receiveNotifications);
        /// <summary>
        /// Updates the status of the user with the specified ID in the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the user to update</param>
        /// <param name="status">The user's new status</param>
        /// <returns>The updated user object from the datastore</returns>
        /// <exception cref="ValidationException">if the user status cannot be updated to the given value</exception>
        /// <exception cref="EntityNotFoundException">if a user with the specified ID doesn't exist</exception>
        Task<User> UpdateUserStatusAsync(int id, UserStatus status);
        /// <summary>
        /// Deletes a user from the underlying data store or marks them to be deleted in the future, if they had participated in an event 
        /// in the past <paramref name="retentionDays"/> days.<br/><br/>If a user is marked to be deleted, 
        /// the date of the deletion will be set to the date of the last event they had participated in + <paramref name="retentionDays"/> days.
        /// </summary>
        /// <param name="user">The user to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The user that was deleted, or is marked for deletion</returns>
        Task<User> DeleteUserAsync(User user, int retentionDays);
        /// <summary>
        /// Deletes a user from the underlying data store or marks them to be deleted in the future, if they had participated in an event 
        /// in the past <paramref name="retentionDays"/> days.<br/><br/>If a user is marked to be deleted, 
        /// the date of the deletion will be set to the date of the last event they had participated in + <paramref name="retentionDays"/> days.
        /// </summary>
        /// <param name="id">The ID of the user to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The user that was deleted</returns>
        /// <exception cref="EntityNotFoundException">If the user with the specified ID doesn't exist</exception>
        Task<User> DeleteUserAsync(int id, int retentionDays);
    }
}

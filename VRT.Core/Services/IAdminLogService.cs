﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// A service used for creating and retrieving log entries in the admin log.
    /// </summary>
    public interface IAdminLogService
    {
        /// <summary>
        /// Returns the number of log entries that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of log entries containing the given filter string.</returns>
        Task<int> CountEntriesAsync(string filter);
        /// <summary>
        /// Returns a list of log entries that are matching the given filter (if any) from the underyling datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of log entries that are matching the given filter (if any) from the underyling datastore</returns>
        Task<IEnumerable<AdminLogEntry>> FindEntriesAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc");
        /// <summary>
        /// Creates a new entry in the admin log and returns it.
        /// </summary>
        /// <param name="actor">The name of the actor that performed an action</param>
        /// <param name="action">The action that was performed, e.g., 'Deleted User'</param>
        /// <param name="indirectTarget">The indirect target of an action, e.g., the name of the user that was removed from an event (the target)</param>
        /// <param name="target">The target of the action, e.g. the name of an event/user/venue</param>
        /// <returns>The log entry that was created</returns>
        /// <exception cref="ArgumentException">if any of the required arguments are null or empty</exception>
        Task<AdminLogEntry> CreateEntryAsync(string actor, string action, string target, string indirectTarget = null);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;
using VRT.Core.Exceptions;
using VRT.Core.Models.Jwt;

namespace VRT.Core.Services
{
    /// <summary>
    /// Provides methods related to managing user accounts.
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Registers a new user with the application. 
        /// </summary>
        /// <param name="username">The user's username (required)</param>
        /// <param name="displayName">The user's display name (required)</param>
        /// <param name="password">The user's password (required)</param>
        /// <param name="email">The user's email address (optional)</param>
        /// <param name="receiveNotifications">Indicates whether the user wants to receive notifications from the application</param>
        /// <returns>The new user object from the application's database</returns>
        /// <returns>The newly registered user object.</returns>
        /// <exception cref="ValidationException">if any of the given parameters are invalid.</exception>
        /// <exception cref="DuplicateValueException">if a user with the specified <paramref name="username"/>, <paramref name="displayName"/>, or <paramref name="email"/> already exists.</exception>
        Task<User> RegisterUserAsync(string username, string displayName, string password, string email = null, bool receiveNotifications = false);
        /// <summary>
        /// Retrieves the user with the specified username from the underlying datastore.
        /// </summary>
        /// <param name="username">The username of the user to retrieve</param>
        /// <returns>The user object from the underlying datastore</returns>
        /// <exception cref="ValidationException">if the username is null or empty</exception>
        /// <exception cref="AccountServiceException">If an account with the specified username no longer exists, or the account has been set to inactive</exception>
        Task<User> GetUserAsync(string username);
        /// <summary>
        /// Tries to authenticate the user with the provided credentials.
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="password">The passwort</param>
        /// <returns>Returns the a JWT token pair for the authenticated user, if the authentication was successful</returns>
        /// <exception cref="ValidationException">If the supplied username or password is <see langword="null"/> or an empty string</exception>
        /// <exception cref="AccountServiceException">if the authentication failed, or the user was deleted/set to inactive</exception>
        Task<JwtAuthResult> AuthenticateUserAsync(string username, string password);
        /// <summary>
        /// De-auhtenticates (logs out) users by revoking their refresh tokens.
        /// </summary>
        /// <param name="userId">The ID of the user to de-authenticate</param>
        /// <param name="username">The username of the user to de-authenticate</param>
        /// <exception cref="ValidationException">if the specified username is null or empty</exception>
        Task DeauthenticateUserAsync(int userId, string username);
        /// <summary>
        /// Deletes the user with the specified name.
        /// </summary>
        /// <param name="username">The user's username</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>A task that can be awaited</returns>
        /// <exception cref="ValidationException">if the given username is null or empty.</exception>
        /// <exception cref="EntityNotFoundException">if a user with the specified username doesn't exist.</exception>
        Task DeleteAccountAsync(string username, int retentionDays);
        /// <summary>
        /// Upates the user with the given username without changing their password.
        /// </summary>
        /// <param name="username">Username of user to update</param>
        /// <param name="displayName">Updated display name</param>
        /// <param name="email">updated email address</param>
        /// <param name="receiveNotifications">Indicates whether the user wants to receive notifications from the application</param>
        /// <returns>The updated user object.</returns>
        /// <exception cref="AccountServiceException">If an account with the specified username no longer exists, or the account has been set to inactive</exception>
        /// <exception cref="ValidationException">if any of the specified arguments are null or empty</exception>
        /// <exception cref="DuplicateValueException">if the specified <paramref name="displayName"/> or <paramref name="email"/> is already used by another user.</exception>
        Task<User> UpdateUserAsync(string username, string displayName, string email = null, bool receiveNotifications = false);
        /// <summary>
        /// Updates the specified user's password.
        /// </summary>
        /// <param name="username">Username of user to update</param>
        /// <param name="password">The new password</param>
        /// <returns>a task to be awaited</returns>
        /// <exception cref = "AccountServiceException" > If an account with the specified ID no longer exists, or the account has been set to inactive</exception>
        /// <exception cref="ValidationException">if the password is null or doesn't meet the minimum length requirements</exception>
        Task UpdatePasswordAsync(string username, string password);
        /// <summary>
        /// Refreshes the JWT token pair for the user with the specified username.
        /// </summary>
        /// <param name="accessToken">The user's current access token</param>
        /// <param name="refreshToken">The user's refresh token</param>
        /// <returns>A new token pair for the user</returns>
        /// <exception cref="ValidationException">If any of the given arguments are null or empty</exception>
        /// <exception cref="AccountServiceException">If the user was disabled or deleted</exception>
        Task<JwtAuthResult> RefreshTokenAsync(string accessToken, string refreshToken);
        /// <summary>
        /// Generates an account recovery token for the user with the specified email address and sends it to that email address.
        /// </summary>
        /// <param name="email">The user's email address</param>
        /// <returns>An account recovery token for the user.</returns>
        /// <exception cref="ValidationException">If one of the given arguments is invalid</exception>
        /// <exception cref="EntityNotFoundException">If a user with the specified email address doesn't exist</exception>
        /// <exception cref="AccountServiceException">If the account has been disabled.</exception>
        Task<AccountRecoveryToken> GenerateRecoveryTokenAsync(string email);
        /// <summary>
        /// Generates an account recovery token for the user with the specified ID.
        /// </summary>
        /// <param name="userId">The ID of the user for whom to generate a new recovery token</param>
        /// <returns>An account recovery token for the user.</returns>
        /// <exception cref="EntityNotFoundException">If a user with the specified ID doesn't exist</exception>
        /// <exception cref="AccountServiceException">If the account has been disabled.</exception>
        Task<AccountRecoveryToken> GenerateRecoveryTokenAsync(int userId);
        /// <summary>
        /// Updates a user's password using a previously generated account recovery token.
        /// </summary>
        /// <param name="recoveryToken">A valid recovery token that identifies the user</param>
        /// <param name="password">The user's new password value</param>
        /// <returns>a Task that can be awaited</returns>
        /// <exception cref="ValidationException">if either of the given paramaters is <see langword="null"/> or empty, the token is invalid or if the user account has been disabled</exception>
        Task ResetPasswordAsync(string recoveryToken, string password);
    }
}

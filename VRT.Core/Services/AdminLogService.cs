﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Core.Exceptions;
using VRT.Core.Util;
using VRT.Data;
using VRT.Data.Entities;
using VRT.Data.Extensions;

namespace VRT.Core.Services
{
    /// <summary>
    /// The default implementation of the IAdminLogService interface.
    /// </summary>
    public class AdminLogService : IAdminLogService
    {
        #region Fields

        private VRTContext _context;

        #endregion


        /// <summary>
        /// Constructs a new AdminLogService object.
        /// </summary>
        /// <param name="context">A reference to the application's database context</param>
        public AdminLogService(VRTContext context)
        {
            _context = context;
        }


        #region Methods

        /// <summary>
        /// Returns the number of log entries that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of log entries containing the given filter string.</returns>
        public async Task<int> CountEntriesAsync(string filter)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return await _context.AdminLogEntries.CountAsync();
            }
            else
            {
                filter = filter.ToLower();

                return await _context.AdminLogEntries
                    .Where(e => e.Actor.ToLower().Contains(filter) || e.Action.ToLower().Contains(filter) ||
                        e.Target.ToLower().Contains(filter) || (!string.IsNullOrEmpty(e.IndirectTarget) && e.IndirectTarget.ToLower().Contains(filter)))
                    .CountAsync();
            }
        }
        /// <summary>
        /// Returns a list of log entries that are matching the given filter (if any) from the underyling datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of log entries that are matching the given filter (if any) from the underyling datastore</returns>
        /// <exception cref="ValidationException"></exception>
        public async Task<IEnumerable<AdminLogEntry>> FindEntriesAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc")
        {
            // Parse and validate the query information
            var qInfo = new QueryInfoBuilder()
                .SetPageIndex(page).SetPageSize(pageSize, 50).SetFilter(filter)
                .SetSortColumnOrDefault<AdminLogEntry>(sortColumn, "Id")
                .SetSortOrderOrDefault(sortOrder, SortOrder.Ascending)
                .Build();

            IQueryable<AdminLogEntry> query;

            if (string.IsNullOrEmpty(qInfo.Filter))
            { 
                query = _context.AdminLogEntries;
            }
            else
            {
                query = _context.AdminLogEntries.Where(e => e.Actor.ToLower().Contains(qInfo.Filter) || e.Action.ToLower().Contains(qInfo.Filter) ||
                    e.Target.ToLower().Contains(qInfo.Filter) || (!string.IsNullOrEmpty(e.IndirectTarget) && e.IndirectTarget.ToLower().Contains(qInfo.Filter)));
            }

            // Order result set
            query = qInfo.SortOrder == SortOrder.Ascending 
                ? query.OrderBy(qInfo.SortColumn) 
                : query.OrderByDescending(qInfo.SortColumn);

            // Return paged results
            return await query
                .Skip(qInfo.Page * qInfo.PageSize)
                .Take(qInfo.PageSize)
                .ToListAsync();
        }
        /// <summary>
        /// Creates a new entry in the admin log and returns it.
        /// </summary>
        /// <param name="actor">The name of the actor that performed an action</param>
        /// <param name="action">The action that was performed, e.g., 'Deleted User'</param>
        /// <param name="indirectTarget">The indirect target of an action, e.g., the name of the user that was removed from an event (the target)</param>
        /// <param name="target">The target of the action, e.g. the name of an event/user/venue</param>
        /// <returns>The log entry that was created</returns>
        /// <exception cref="ArgumentException">if any of the required arguments are null or empty</exception>
        public async Task<AdminLogEntry> CreateEntryAsync(string actor, string action, string target, string indirectTarget = null)
        {
            if (string.IsNullOrEmpty(actor))
                throw new ArgumentException("Argument must not be null or empty.", nameof(actor));
            if (string.IsNullOrEmpty(action))
                throw new ArgumentException("Argument must not be null or empty.", nameof(action));
            if (string.IsNullOrEmpty(target))
                throw new ArgumentException("Argument must not be null or empty.", nameof(target));

            // Construct a new log entry
            var entry = new AdminLogEntry
            {
                Actor = actor,
                Action = action,
                IndirectTarget = indirectTarget,
                Target = target,
                Timestamp = DateTime.UtcNow
            };

            await _context.AdminLogEntries.AddAsync(entry);
            await _context.SaveChangesAsync();

            return entry;
        }

        #endregion
    }
}

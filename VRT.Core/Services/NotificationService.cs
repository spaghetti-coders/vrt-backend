﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.HostedServices.Notifications;
using VRT.Core.Models;
using VRT.Data;
using VRT.Data.Entities;
using VRT.Mail.Models;
using VRT.Mail.Services;

namespace VRT.Core.Services
{
    /// <summary>
    /// The service is responsible for generating the application's various push- and email-notifications and handing them over to the delivery service. 
    /// </summary>
    public class NotificationService : INotificationService
    {
        #region Fields

        private readonly CultureInfo _defaultCulture;

        private readonly VRTContext _context;
        private readonly INotificationQueue _queue;
        private readonly IMailTemplateService _mtService;
        private readonly HostingConfiguration _hostingConfig;
        private readonly ILogger<NotificationService> _logger;

        #endregion


        /// <summary>
        /// Constructs a new NotificationService object.
        /// </summary>
        /// <param name="context">A VRTContext reference</param>
        /// <param name="hostingConfiguration /">A HostingConfiguration reference</param>
        /// <param name="mailTemplateService">An IMailTemplateService reference</param>
        /// <param name="queue">An INotificationQueue reference</param>
        /// <param name="logger">An ILogger reference</param>
        public NotificationService(VRTContext context, HostingConfiguration hostingConfiguration, IMailTemplateService mailTemplateService, INotificationQueue queue, ILogger<NotificationService> logger)
        {
            _context = context;
            _mtService = mailTemplateService;
            _hostingConfig = hostingConfiguration;
            _queue = queue;
            _logger = logger;

            try 
            {
                // Try to set configured culture
                _defaultCulture = CultureInfo.CreateSpecificCulture(_hostingConfig.DefaultLocale);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, $"Unable to set configured default locale to: {_hostingConfig.DefaultLocale}");
                _defaultCulture = new CultureInfo("de-DE");
            }
        }


        #region Event Notifications

        /// <summary>
        /// Informs all users that have opted-in that a new event is available for registration.
        /// </summary>
        /// <param name="evt">The event</param>
        /// <returns>a task to be awaited</returns>
        public async Task SendNewEventNotificationAsync(Event evt)
        {
            _logger.LogInformation($"Notifying users that a new event (ID={evt.Id}) is available for registration.");

            try
            {
                // Load venue if necessary
                if (evt.Venue == null)
                    await _context.Entry(evt).Reference(e => e.Venue).LoadAsync();

                // Retrieve recipients
                var recipients = await _context.Users
                    .Include(u => u.PushSubscription)
                    .Where(u => u.PushSubscription != null || (u.ReceiveNotifications == true && !string.IsNullOrEmpty(u.Email)))
                    .ToListAsync();

                // Retrieve push subscriptions
                var pushSubscriptions = recipients
                    .Where(r => r.PushSubscription != null)
                    .Select(r => r.PushSubscription)
                    .ToList();

                // Retrieve email recipients
                var emailRecipients = recipients
                    .Where(r => !string.IsNullOrEmpty(r.Email))
                    .Select(r => r.Email)
                    .ToList();

                // Build messages ...
                PushPayload pushPayload = null;
                string emailMessage = null;
                string emailSubject = null;

                // Build push message
                if (pushSubscriptions.Count > 0)
                {
                    pushPayload = new PushPayload(
                        Notifications.GenericPush_Title,
                        string.Format(Notifications.NewEvent_PushMessage, evt.Name, evt.Date.ToLocalTime().ToString("f", _defaultCulture)),
                        _hostingConfig.PushIconUri
                    );

                    pushPayload.Notification.Data.Add("action", "view-event");
                    pushPayload.Notification.Data.Add("targetId", evt.Id);
                    pushPayload.Notification.Data.Add("url", $"/events/detail/{evt.Id}");
                }

                // Load and format email body
                if (emailRecipients.Count > 0)
                {
                    emailSubject = Notifications.NewEvent_MailSubject;
                    
                    // Build the model to render the HTML template
                    var model = new EventMailModel
                    {
                        AppUrl = _hostingConfig.AppUrl,
                        Event = evt,
                        Venue = evt.Venue
                    };

                    emailMessage = await LoadMailTemplate(MailTemplates.Events.NewEvent, model);
                }

                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    PushSubscriptions = pushSubscriptions,
                    PushPayload = pushPayload,
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Informs all users that have opted-in, that an event they have subscribed for was canceled.
        /// </summary>
        /// <param name="evt">The event</param>
        /// <returns>a task to be awaited</returns>
        public async Task SendEventCanceledNotificationAsync(Event evt)
        {
            _logger.LogInformation($"Notifying users that the event (ID={evt.Id}) was canceled ...");

            try
            { 
                // Load venue if necessary
                if (evt.Venue == null)
                    await _context.Entry(evt).Reference(e => e.Venue).LoadAsync();

                // Retrieve push subscriptions
                var pushSubscriptions = await _context.Registrations
                    .Include(r => r.User)
                    .ThenInclude(u => u.PushSubscription)
                    .Where(r => r.EventId == evt.Id && r.User.PushSubscription != null)
                    .Select(r => r.User.PushSubscription)
                    .ToListAsync();

                // Retrieve email recipients
                var emailRecipients = await _context.Registrations
                    .Include(r => r.User)
                    .Where(r => r.EventId == evt.Id && !string.IsNullOrEmpty(r.User.Email))
                    .Select(r => r.User.Email)
                    .ToListAsync();


                // Build messages ...
                PushPayload pushPalyload = null;
                string emailMessage = null;
                string emailSubject = null;

                // Build push message
                if (pushSubscriptions.Count > 0)
                { 
                    pushPalyload = new PushPayload(
                        Notifications.GenericPush_Title, 
                        string.Format(Notifications.EventCanceled_PushMessage, evt.Name, evt.Date.ToLocalTime().ToString("f", _defaultCulture)),
                        _hostingConfig.PushIconUri
                    );
                }

                // Load and format email body
                if (emailRecipients.Count > 0)
                {
                    emailSubject = Notifications.Event_Canceled_MailSubject;

                    // Build the model used for rendering the template
                    var model = new EventMailModel
                    {
                        AppUrl = _hostingConfig.AppUrl,
                        Event = evt,
                        Venue = evt.Venue
                    };

                    emailMessage = await LoadMailTemplate(MailTemplates.Events.EventCanceled, model);
                }


                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    PushSubscriptions = pushSubscriptions,
                    PushPayload = pushPalyload,
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }

        #endregion

        #region User / Registration Notifications

        /// <summary>
        /// Tries to inform the application admins that a new user has registered.
        /// </summary>
        /// <param name="user">The user that registered</param>
        /// <returns>a Task to be awaited</returns>
        public async Task SendNewUserRegisteredNotificationAsync(User user)
        {
            _logger.LogInformation($"Informing admins that a new user (ID={user.Id}) has registered ...");

            try 
            { 
                // Retrieve recipients (all admins with active push subscription or email address)
                var recipients = await _context.Users
                    .Include(u => u.PushSubscription)
                    .Where(u => u.Role == Roles.Admin && ((u.ReceiveNotifications == true && !string.IsNullOrEmpty(u.Email)) || u.PushSubscription != null))
                    .ToListAsync();

                // Retrieve push subscriptions
                var pushSubscriptions = recipients
                    .Where(r => r.PushSubscription != null)
                    .Select(r => r.PushSubscription)
                    .ToList();

                // Retrieve email recipients
                var emailRecipients = recipients
                    .Where(r => !string.IsNullOrEmpty(r.Email))
                    .Select(r => r.Email)
                    .ToList();


                // Build messages ...
                PushPayload pushPayload = null;
                string emailMessage = null;
                string emailSubject = null;

                // Build push message
                if (pushSubscriptions.Count > 0)
                {
                    pushPayload = new PushPayload(
                        Notifications.GenericPush_Title, 
                        string.Format(Notifications.NewUserRegistered_PushMessage, user.DisplayName),
                        _hostingConfig.PushIconUri
                    );
                }

                // Load and format email body
                if (emailRecipients.Count > 0)
                {
                    emailSubject = Notifications.NewUser_MailSubject;

                    // Build the model used for rendering the template
                    var model = new UserMailModel
                    {
                        AppUrl = _hostingConfig.AppUrl,
                        User = user
                    };

                    emailMessage = await LoadMailTemplate(MailTemplates.Users.NewRegistration, model);
                }


                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    PushSubscriptions = pushSubscriptions,
                    PushPayload= pushPayload,
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Informs the user that their registration has been received.
        /// </summary>
        /// <param name="user">The user to inform</param>
        /// <returns>a Task to be awaited</returns>
        public async Task SendUserRegistrationConfirmationNotificationAsync(User user)
        {
            // Make sure the user is able to receive emails
            if (string.IsNullOrEmpty(user.Email))
                return;

            _logger.LogInformation($"Informing the user (ID={user.Id}) that their registeration has been received ...");

            try
            {
                // Determine recipients
                var emailRecipients = new List<string>();
                emailRecipients.Add(user.Email);

                // Set the mail subject
                var emailSubject = Notifications.UserRegistrationConfirmation_MailSubject;

                // Build the model and render the template
                var model = new UserMailModel
                {
                    AppUrl = _hostingConfig.AppUrl,
                    User = user
                };

                var emailMessage = await LoadMailTemplate(MailTemplates.Users.RegistrationConfirmation, model);


                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Informs the user that their account has been activated.
        /// </summary>
        /// <param name="user">The user to inform</param>
        /// <returns>a Task to be awaited</returns>
        public async Task SendUserAccountActivatedNotificationAsync(User user)
        {
            // Make sure the user is able to receive emails
            if (string.IsNullOrEmpty(user.Email))
                return;

            _logger.LogInformation($"Informing the user (ID={user.Id}) that their account has been activated ...");

            try
            {
                // Determine recipients
                var emailRecipients = new List<string>();
                emailRecipients.Add(user.Email);

                // Set the mail subject
                var emailSubject = Notifications.UserAccountActivated_MailSubject;

                // Build the model and render the template
                var model = new UserMailModel
                {
                    AppUrl = _hostingConfig.AppUrl,
                    User = user
                };

                var emailMessage = await LoadMailTemplate(MailTemplates.Users.AccountActivated, model);


                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Sends the user an account recovery notification.
        /// </summary>
        /// <param name="user">The user to inform</param>
        /// <param name="recoveryToken">The recovery token that was generated for the user</param>
        /// <returns>a Task to be awaited</returns>
        public async Task SendAccountRecoveryNotificationAsync(User user, AccountRecoveryToken recoveryToken)
        {
            // Make sure the user is able to receive emails
            if (string.IsNullOrEmpty(user.Email))
                return;

            _logger.LogInformation($"Sending an account recovery email to the user (ID={user.Id}) ...");

            try
            {
                // Determine email recipients
                var emailRecipients = new List<string>();
                emailRecipients.Add(user.Email);

                // Set the mail subject
                var emailSubject = Notifications.AccountRecovery_MailSubject;

                // Build the model and render the template
                var model = new AccountRecoveryMailModel
                {
                    AppUrl = _hostingConfig.AppUrl,
                    User = user,
                    RecoveryToken = recoveryToken
                };

                var emailMessage = await LoadMailTemplate(MailTemplates.Users.AccountRecovery, model);


                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Informs the user that their account and all related data has been deleted.
        /// </summary>
        /// <param name="user">The user to notify</param>
        /// <returns>a Task to be awaited</returns>
        public async Task SendAccountDeletedNotificationAsync(User user)
        {
            // Make sure the user is able to receive emails
            if (string.IsNullOrEmpty(user.Email))
                return;

            _logger.LogInformation($"Informing a user (ID={user.Id}) that their account was successfully deleted ...");

            try
            {
                // Determine email recipients
                var emailRecipients = new List<string>();
                emailRecipients.Add(user.Email);

                // Set the mail subject
                var emailSubject = Notifications.AccountDeleted_MailSubject;

                // Build the model and render the template
                var model = new UserMailModel
                {
                    AppUrl = _hostingConfig.AppUrl,
                    User = user
                };

                var emailMessage = await LoadMailTemplate(MailTemplates.Users.AccountDeleted, model);

                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Informs the user that the deletion of their account and all related data has been scheduled.
        /// </summary>
        /// <param name="user">The user to notify</param>
        /// <returns>a task to be awaited</returns>
        public async Task SendAccountDeletionScheduledNotificationAync(User user)
        {
            // Make sure the user is able to receive emails
            if (string.IsNullOrEmpty(user.Email))
                return;

            _logger.LogInformation($"Informing a user (ID={user.Id}) that their account is scheduled to be deleted ...");

            try
            {
                // Determine email recipients
                var emailRecipients = new List<string>();
                emailRecipients.Add(user.Email);

                // Set the mail subject
                var emailSubject = Notifications.AccountDeletionScheduled_MailSubject;

                // Build the model and render the template
                var model = new UserMailModel
                {
                    AppUrl = _hostingConfig.AppUrl,
                    User = user
                };

                var emailMessage = await LoadMailTemplate(MailTemplates.Users.AccountDeletionScheduled, model);

                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }

        #endregion

        #region Event Registration Notifications

        /// <summary>
        /// Informs the specified users that they haved moved from the event's list of active participants to the waiting list.
        /// </summary>
        /// <param name="evt">The event for that the users are registered</param>
        /// <param name="registration">The registrations of the users that are affected</param>
        /// <returns>a task to be awaited</returns>
        public async Task SendMovedToWaitingListNotificationAsync(Event evt, List<Registration> registrations)
        {
            if (registrations.Count == 0)
                return;

            _logger.LogInformation($"Informing {registrations.Count} users that they were moved to waiting list of the event (ID={evt.Id}) ...");

            try 
            { 
                // Load venue if necessary
                if (evt.Venue == null)
                    await _context.Entry(evt).Reference(e => e.Venue).LoadAsync();

                // Determine push subscriptions
                var pushSubscriptions = registrations
                    .Where(r => r.User?.PushSubscription != null)
                    .Select(r => r.User.PushSubscription)
                    .ToList();

                // Determine email recipients
                var emailRecipients = registrations
                    .Where(r => r.User?.ReceiveNotifications == true && !string.IsNullOrEmpty(r.User?.Email))
                    .Select(r => r.User.Email)
                    .ToList();

                // Build messages ...
                PushPayload pushPayload = null;
                string emailMessage = null;
                string emailSubject = null;

                // Build push message
                if (pushSubscriptions.Count > 0)
                {
                    pushPayload = new PushPayload(
                        Notifications.GenericPush_Title, 
                        string.Format(Notifications.MovedToWaitingList_PushMessage, evt.Name, evt.Date.ToLocalTime().ToString("f", _defaultCulture)),
                        _hostingConfig.PushIconUri
                    );
                }

                // Load and format email body
                if (emailRecipients.Count > 0)
                {
                    // Set the mail subject
                    emailSubject = Notifications.RegistrationStatusChange_MailSubject;

                    // Build the model and render the tempalte
                    var model = new EventMailModel
                    {
                        AppUrl = _hostingConfig.AppUrl,
                        Event = evt,
                        Venue = evt.Venue
                    };

                    emailMessage = await LoadMailTemplate(MailTemplates.EventRegistrations.Downgraded, model);
                }

                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    PushSubscriptions = pushSubscriptions,
                    PushPayload = pushPayload,
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Informs the specified users that they haved moved from the event's waiting list to the list of active participants.
        /// </summary>
        /// <param name="evt">The event for that the users are registered</param>
        /// <param name="registration">The registrations of the users that are affected</param>
        /// <returns>a task to be awaited</returns>
        public async Task SendMovedToParticipantsNotificationAsync(Event evt, List<Registration> registrations)
        {
            if (registrations.Count == 0)
                return;

            _logger.LogInformation($"Informing {registrations.Count} users that they were moved to the list of active participants for the event (ID={evt.Id}) ...");

            try 
            { 
                // Load venue if necessary
                if (evt.Venue == null)
                    await _context.Entry(evt).Reference(e => e.Venue).LoadAsync();

                // Determine push subscriptions
                var pushSubscriptions = registrations
                    .Where(r => r.User?.PushSubscription != null)
                    .Select(r => r.User.PushSubscription)
                    .ToList();

                // Determine email recipients
                var emailRecipients = registrations
                    .Where(r => r.User?.ReceiveNotifications == true && !string.IsNullOrEmpty(r.User?.Email))
                    .Select(r => r.User.Email)
                    .ToList();


                // Build messages ...
                PushPayload pushPayload = null;
                string emailMessage = null;
                string emailSubject = null;

                // Build push message
                if (pushSubscriptions.Count > 0)
                {
                    pushPayload = new PushPayload(
                        Notifications.GenericPush_Title, 
                        string.Format(Notifications.MovedToParticipants_PushMessage, evt.Name, evt.Date.ToLocalTime().ToString("f", _defaultCulture)),
                        _hostingConfig.PushIconUri
                    );
                }

                // Load and format email body
                if (emailRecipients.Count > 0)
                {
                    // Set the mail subject
                    emailSubject = Notifications.RegistrationStatusChange_MailSubject;

                    // Build the model and render the template
                    var model = new EventMailModel
                    {
                        AppUrl = _hostingConfig.AppUrl,
                        Event = evt,
                        Venue = evt.Venue
                    };

                    emailMessage = await LoadMailTemplate(MailTemplates.EventRegistrations.Upgraded, model);
                }


                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    PushSubscriptions = pushSubscriptions,
                    PushPayload = pushPayload,
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }
        /// <summary>
        /// Informs the specified user that their registration to an event has been deleted by an administrator.
        /// </summary>
        /// <param name="evt">The event for that the user registered</param>
        /// <param name="registration">The affected user's registration</param>
        /// <returns>a task to be awaited</returns>
        public async Task SendRegistrationDeletedNotificationAsync(Event evt, Registration registration)
        {
            _logger.LogInformation($"Informing a user (ID={registration.UserId}) that their registration for an event (ID={registration.EventId}) was deleted ...");

            try
            { 
                // Load references if necessary
                if (evt.Venue == null)
                    await _context.Entry(evt).Reference(e => e.Venue).LoadAsync();
                if (registration.User == null)
                    await _context.Entry(registration).Reference(r => r.User).LoadAsync();

                // Determine push subscriptions
                var pushSubscriptions = new List<PushSubscription>();
            
                if (registration.User.PushSubscription != null)
                    pushSubscriptions.Add(registration.User.PushSubscription);

                // Determine email recipients
                var emailRecipients = new List<string>();

                if (registration.User.ReceiveNotifications && !string.IsNullOrEmpty(registration.User.Email))
                    emailRecipients.Add(registration.User.Email);


                // Build messages ...
                PushPayload pushPayload = null;
                string emailMessage = null;
                string emailSubject = null;

                // Build push message
                if (pushSubscriptions.Count > 0)
                { 
                    pushPayload = new PushPayload(
                        Notifications.GenericPush_Title,
                        string.Format(Notifications.RegistrationDeleted_PushMessage, registration.Event.Name, registration.Event.Date.ToLocalTime().ToString("f",_defaultCulture)),
                        _hostingConfig.PushIconUri
                    );
                }

                // Load and format email body
                if (emailRecipients.Count > 0)
                {
                    // Set the mail subject
                    emailSubject = Notifications.RegistrationDelete_MailSubject;

                    // Build the model and render the template
                    var model = new EventRegistrationMailModel
                    {
                        AppUrl = _hostingConfig.AppUrl,
                        Event = evt,
                        Venue = evt.Venue,
                        User = registration.User
                    };

                    emailMessage = await LoadMailTemplate(MailTemplates.EventRegistrations.DeletedByAdmin, model);
                }


                // Queue notification for delivery
                QueueNotification(new Notification
                {
                    PushSubscriptions = pushSubscriptions,
                    PushPayload = pushPayload,
                    EmailRecipients = emailRecipients,
                    EmailSubject = emailSubject,
                    EmailBody = emailMessage,
                    EmailIsHtml = true
                });
            }
            catch (Exception ex)
            {
                if (ex is not NotificationServiceException)
                    _logger.LogError(ex, "An unexpected error prevented notifications from being sent.");
            }
        }

        #endregion

        #region Push-Subscription

        /// <summary>
        /// Sends a push notification to the user of the given subscription, informing them that they successfully subscribed to the push service.
        /// </summary>
        /// <param name="subscription">The user's subscription</param>
        public void SendPushSubscriptionNotification(PushSubscription subscription)
        {
            _logger.LogInformation($"Sending push subscription confirmation to user with ID={subscription.UserId} ...");

            var pushSubscriptions = new List<PushSubscription>();
            pushSubscriptions.Add(subscription);

            // Queue notification for delivery
            QueueNotification(new Notification
            {
                PushSubscriptions = pushSubscriptions,
                PushPayload = new PushPayload(Notifications.GenericPush_Title, Notifications.Subscribed_PushMessage, _hostingConfig.PushIconUri),
            });
        }
        /// <summary>
        /// Sends a push notification to the user of the given subscription, informing them that they have succesfully unsubscribed from receiving further push notifications.
        /// </summary>
        /// <param name="subscription">The user's subscription</param>
        public void SendPushUnsubscribeNotification(PushSubscription subscription)
        {
            _logger.LogInformation($"Sending a subscription cancelation confirmation to user with ID={subscription.UserId} ...");

            var pushSubscriptions = new List<PushSubscription>();
            pushSubscriptions.Add(subscription);

            // Queue notification for delivery
            QueueNotification(new Notification
            {
                PushSubscriptions = pushSubscriptions,
                PushPayload = new PushPayload(Notifications.GenericPush_Title, Notifications.Unsubscribed_PushMessage, _hostingConfig.PushIconUri),
            });
        }

        #endregion

        #region General Messaging 

        /// <summary>
        /// Sends a push message to the specified subscriber.
        /// </summary>
        /// <param name="title">The message's title</param>
        /// <param name="subscription">The subscription of the user that is to receive the message</param>
        /// <param name="message">The message that is to be send to the user</param>
        public void SendPushNotification(PushSubscription subscription, string title, string message)
        {
            if (string.IsNullOrEmpty(title))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(title)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(message))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(message)}' cannot be null or empty.");
            if (subscription == null)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(subscription)}' cannot be null or empty.");

            var pushSubscriptions = new List<PushSubscription>();
            pushSubscriptions.Add(subscription);

            QueueNotification(new Notification
            {
                PushSubscriptions = pushSubscriptions,
                PushPayload = new PushPayload(title, message)
            });
        }
        /// <summary>
        /// Sends a push message to the specified subscriber.
        /// </summary>
        /// <param name="subscriptions">The subscriptions of the users that are to receive the message</param>
        /// <param name="title">The message's title</param>
        /// <param name="message">The message that is to be send to the users</param>
        public void SendPushNotification(List<PushSubscription> subscriptions, string title, string message)
        {
            if (string.IsNullOrEmpty(title))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(title)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(message))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(message)}' cannot be null or empty.");
            if (subscriptions == null || subscriptions.Count == 0)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(subscriptions)}' cannot be null or empty.");

            QueueNotification(new Notification
            {
                PushSubscriptions = subscriptions,
                PushPayload = new PushPayload(title, message, _hostingConfig.PushIconUri)
            });
        }
        /// <summary>
        /// Sends a custom email notification to the specified recipients.
        /// </summary>
        /// <param name="subject">The message's subject line</param>
        /// <param name="body">The message's body</param>
        /// <param name="recipients">The message's recipients</param>
        public void SendEmailNotification(string subject, string body, List<string> recipients)
        {
            if (string.IsNullOrEmpty(subject))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(subject)}' cannot be null or empty.");
            if (string.IsNullOrEmpty(body))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(body)}' cannot be null or empty.");
            if (recipients == null || recipients.Count == 0)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(recipients)}' cannot be null or empty.");

            QueueNotification(new Notification
            {
                EmailRecipients = recipients,
                EmailSubject = subject,
                EmailBody = body,
                EmailIsHtml = false
            });
        }

        #endregion

        #region Helper / Utilitiy Methods

        /// <summary>
        /// If notifications are enabled, the given notification is queued and sent to the delivery service, otherwise a warning will be logged.
        /// </summary>
        /// <param name="notification">The notification thas is to be sent</param>
        private void QueueNotification(Notification notification)
        {
            if (_hostingConfig.EnableNotifications)
            {
                _queue.Add(notification);
            }
            else
            {
                _logger.LogWarning("Notifications are currently disabled. No emails or push notifications will be sent.");
            }
        }
        /// <summary>
        /// Convenience method for loading the email template with the specified filename.
        /// </summary>
        /// <param name="template">The templates filename</param>
        /// <param name="model">The model to render into the template</param>
        /// <returns>the rendered template as HTML string</returns>
        /// <exception cref="NotificationServiceException">if an error occurred loading the specified mail template</exception>
        private async Task<string> LoadMailTemplate<T>(string template, T model) where T: TemplateModel
        {
            try
            {
                _logger.LogDebug($"Loading email template '{template}' ...");

                return await _mtService.GetTemplateAsHtmlStringAsync(template, model, _defaultCulture);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred loading the mail template '{template}'.");
                throw new NotificationServiceException(Messages.Errors.FailedToLoadMailTemplate.Code, $"Unable to load the specified mail template '{template}'.");
            }
        }

        #endregion
    }
}

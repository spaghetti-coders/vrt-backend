﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using VRT.Core.Models.Jwt;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// Defines functions for authenticating users using a JWT Bearer token authentication scheme.
    /// </summary>
    public interface IJwtAuthManager
    {
        /// <summary>
        /// Returns the refresh token with the specified token string, or <see langword="null"/> if no such token exists.
        /// </summary>
        /// <paramref name="tokenString">The refresh token's string</paramref>
        /// <returns>the refresh token with the specified token string, or <see langword="null"/> if no such refresh token exists.</returns>
        Task<RefreshToken> FindIssuedRefreshTokensAsync(string tokenString);
        /// <summary>
        /// Generates a set of access and refresh tokens with specified claims and user.
        /// </summary>
        /// <param name="user">The user for whom the tokens will be issued</param>
        /// <param name="claims">The claims to encode in the tokens</param>
        /// <param name="now">The date and time this token will be issued at</param>
        /// <returns>A JwtAuthResult containing tokens for the user.</returns>
        Task<JwtAuthResult> GenerateTokensAsync(User user, Claim[] claims, DateTime now);
        /// <summary>
        /// Refreshes a user's token pair.
        /// </summary>
        /// <param name="refreshToken">The user's current refresh token</param>
        /// <param name="accessToken">The user's current access token</param>
        /// <param name="now"></param>
        /// <returns>A JwtAuthResult containing a new set of tokens for the user.</returns>
        Task<JwtAuthResult> RefreshAsync(string refreshToken, string accessToken, DateTime now);
        /// <summary>
        /// Removes all tokens that have expired compared to the given DateTime object.
        /// </summary>
        /// <param name="now">DateTime object to compare refresh tokens against</param>
        /// <returns>the number of refresh tokens that have been removed</returns>
        int RemoveExpiredRefreshTokens(DateTime now);
        /// <summary>
        /// Removes any refresh tokens that might be associated with the specified user.
        /// </summary>
        /// <param name="userId">The ID of the user whose token to discard</param>
        /// <returns>A task to be awaited</returns>
        Task RemoveRefreshTokenByUserAsync(int userId);
        /// <summary>
        /// Decodes the given access token.
        /// </summary>
        /// <param name="token">The access token to decode</param>
        /// <returns>The token's claims and security token.</returns>
        (ClaimsPrincipal, JwtSecurityToken) DecodeJwtToken(string token);
    }
}

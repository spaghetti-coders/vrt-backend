﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Core.Models;
using VRT.Data.Entities;

namespace VRT.Core.Services
{
    /// <summary>
    /// A service for managing event registrations.
    /// </summary>
    public interface IRegistrationService
    {
        /// <summary>
        /// Returns the number of registrations for the specified user, that are matching the given filter
        /// and registration status.
        /// </summary>
        /// <param name="userId">ID of user whose registrations are to be retrieved</param>
        /// <param name="status">The status of registrations to return (may be null)</param>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>The number of registrations for the specified user</returns>
        /// <exception cref="EntityNotFoundException">If the user with the given ID doesn't exist</exception>
        Task<int> CountRegistrationsByUserAsync(int userId, string filter = null, RegistrationStatus? status = null);
        /// <summary>
        /// Returns a list of event registrations for the specified user, that are matching the given filter and registration status.
        /// </summary>
        /// <param name="userId">ID of user whose registrations to retrieve</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="status">The status of registrations to return (may be null)</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>A list of event registrations for the specified user</returns>
        /// <exception cref="EntityNotFoundException">If the user with the given ID deosn't exist</exception>
        Task<IEnumerable<Registration>> FindRegistrationsByUserAsync(int userId, int page = 1, int pageSize = 10, string filter = null,
            RegistrationStatus? status = null, string sortColumn = "Registerd", string sortOrder = "desc");
        /// <summary>
        /// Returns the number of registrations for the specified event, that are matching the given filter string and status.
        /// </summary>
        /// <param name="eventId">ID of the event whose registration to retrieve</param>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <param name="status">The status of registrations to return (may be null)</param>
        /// <returns>Returns the number of registrations that are matching the given filter string.</returns>
        /// <exception cref="EntityNotFoundException">If the event with the specified ID doesn't exist</exception>
        Task<int> CountRegistrationsByEventAsync(int eventId, string filter = null, RegistrationStatus? status = null);
        /// <summary>
        /// Returns a list of registrations for the specified event and registration status that are matching the given filter (if any) from the underyling datastore.
        /// </summary>
        /// <param name="eventId">ID of the event whose registration to retrieve</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="status">The status of registrations to return (may be null)</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of registrations that are matching the given filter (if any) from the underyling datastore</returns>
        /// <exception cref="EntityNotFoundException">If the event with the specified ID doesn't exist</exception>
        Task<IEnumerable<Registration>> FindRegistrationsByEventAsync(int eventId, int page = 1, int pageSize = 10, string filter = null,
            RegistrationStatus? status = null, string sortColumn = "Registered", string sortOrder = "asc");
        /// <summary>
        /// Tries to find a registration for with the specified event and user IDs.
        /// May return <see langword="null"/>.
        /// </summary>
        /// <param name="eventId">The ID of the event</param>
        /// <param name="userId">The ID of the user</param>
        /// <returns>The registration with the specified IDs or <see langword="null"/> if no such registration exists</returns>
        Task<Registration> FindRegistrationAsync(int eventId, int userId);
        /// <summary>
        /// Creates a new registration for the specified user and event.
        /// </summary>
        /// <param name="eventId">The ID of the Event the user is registering to</param>
        /// <param name="userId">The ID of the user that is to be registered to the event</param>
        /// <returns>The newly created registration object</returns>
        /// <exception cref="EntityNotFoundException">If either the user or event with the specified IDs doesn't exit</exception>
        /// <exception cref="DuplicateValueException">If the user is already registerd to that event</exception>
        Task<Registration> RegisterAsync(int eventId, int userId);
        /// <summary>
        /// Unregisters the specifid user from the specified event.
        /// </summary>
        /// <param name="eventId">The ID of the event</param>
        /// <param name="userId">The ID of the user</param>
        /// <returns>The deleted Registration object</returns>
        /// <exception cref="EntityNotFoundException">if a registration for the specified event and user doesn't exist</exception>
        /// <exception cref="ValidationException">if the user tries to unregister from an event that has already started / concluded</exception>
        Task<Registration> UnregisterAsync(int eventId, int userId);
        /// <summary>
        /// Adds a user's registration to an event in the past. 
        /// </summary>
        /// <param name="eventId">ID of the event</param>
        /// <param name="userId">ID of the user</param>
        /// <returns>The newly created Registration object</returns>
        /// <exception cref="EntityNotFoundException">if the event or the user does not exist</exception>
        /// <exception cref="ValidationException">if the event is not a valid target (not in the past, not open for registration, already at max participants)</exception>
        /// <exception cref="ValidationException">if the user is already registered to this event</exception>
        Task<Registration> AddRegistrationAsync(int eventId, int userId);
        /// <summary>
        /// Deletes the registration with the specified event and user IDs regardless of the event's date and time.
        /// As this operation is usually performed by an administrator, the user whose registration is deleted will 
        /// be informed if the event has not yet concluded.
        /// </summary>
        /// <param name="eventId">ID of the event</param>
        /// <param name="userId">ID of the user</param>
        /// <returns>The deleted Registration object</returns>
        /// <exception cref="EntityNotFoundException">if a registration for the specified event and user doesn't exist</exception>
        Task<Registration> DeleteRegistrationAsync(int eventId, int userId);
        /// <summary>
        /// Recalculates the list of active participants/waiting list for the event of the specified ID,
        /// notifying users about their registration status change (if any).
        /// </summary>
        /// <param name="eventId">ID of the event whose particpant/waiting list to re-calculate</param>
        /// <returns>a task to be awaited.</returns>
        Task CalculateWaitingListAsync(int eventId);
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.Util;
using VRT.Data;
using VRT.Data.Entities;
using VRT.Data.Extensions;

namespace VRT.Core.Services
{
    /// <summary>
    /// A service for managing Venue objects.
    /// </summary>
    public class VenueService : IVenueService
    {
        #region Fields

        private readonly VRTContext _context;
        private readonly ILogger<VenueService> _logger;

        #endregion


        /// <summary>
        /// Constructs a new VenueService object.
        /// </summary>
        /// <param name="context">A VRTContext reference</param>
        /// <param name="logger">An ILogger reference</param>
        public VenueService(VRTContext context, ILogger<VenueService> logger)
        {
            _context = context;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Returns the venue with the specified ID (if any) from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the venue to return</param>
        /// <returns>The venue with the specified ID, or <see langword="null"/> if no venue with that ID exists</returns>
        public async Task<Venue> FindVenueAsync(int id)
        {
            return await _context.Venues
                .FirstOrDefaultAsync(v => v.Id == id);
        }
        /// <summary>
        /// Returns the number of venues that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of venues matching the given filter string</returns>
        public async Task<int> CountVenuesAsync(string filter)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return await _context.Venues.CountAsync();
            }
            else
            {
                filter = filter.ToLower();

                return await _context.Venues
                    .Where(v => v.Name.ToLower().Contains(filter) || v.Address.ToLower().Contains(filter))
                    .CountAsync();
            }
        }
        /// <summary>
        /// Returns a list of venues that are matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of venues that are matching the given filter (if any) from the underlying datastore</returns>
        public async Task<IEnumerable<Venue>> FindVenuesAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc")
        {
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Id" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";
            IQueryable<Venue> query;

            if (string.IsNullOrEmpty(filter))
            { 
                query = _context.Venues;
            }
            else
            {
                filter = filter.ToLower();

                query = _context.Venues.Where(v => v.Name.ToLower().Contains(filter) || v.Address.ToLower().Contains(filter));
            }

            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Return paged results
            return await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
        /// <summary>
        /// Returns the number of active venues that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of active venues matching the given filter string</returns>
        public async Task<int> CountActiveVenuesAsync(string filter)
        {
            if (string.IsNullOrEmpty(filter))
            { 
                return await _context.Venues
                    .Where(v => v.IsActive == true)
                    .CountAsync();
            }
            else
            {
                filter = filter.ToLower();

                return await _context.Venues
                    .Where(v => v.IsActive == true)
                    .Where(v => v.Name.ToLower().Contains(filter) || v.Address.ToLower().Contains(filter))
                    .CountAsync();
            }
        }
        /// <summary>
        /// Returns a list of active venues that are matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of active venues that are matching the given filter (if any) from the underlying datastore</returns>
        public async Task<IEnumerable<Venue>> FindActiveVenuesAsync(int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc")
        {
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Id" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";
            IQueryable<Venue> query;

            if (string.IsNullOrEmpty(filter))
            {
                query = _context.Venues
                    .Where(v => v.IsActive == true);
            }
            else
            {
                filter = filter.ToLower();

                query = _context.Venues
                    .Where(v => v.IsActive == true)
                    .Where(v => v.Name.ToLower().Contains(filter) || v.Address.ToLower().Contains(filter));
            }

            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Return paged results
            return await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
        /// <summary>
        /// Creates a new venue with the specified properties in the underlying datastore.
        /// </summary>
        /// <param name="name">The name of the venue</param>
        /// <param name="address">The address of the venue</param>
        /// <param name="capacity">The maximum number of participants that venue can hold</param>
        /// <param name="colorCode">A hex-code used to colorize events taking place at this venue with</param>
        /// <param name="isActive">Sets the venue in/active</param>
        /// <returns>Returns the newly created venue</returns>
        /// <exception cref="DuplicateValueException">if a venue with the given name already exists</exception>
        /// <exception cref="ValidationException">if one of the required parameters is invalid</exception>
        public async Task<Venue> CreateVenueAsync(string name, string address, int capacity, string colorCode, bool isActive)
        {
            if (string.IsNullOrEmpty(name))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(name)}' cannot be null or empty.");
            if (capacity <0 )
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(capacity)}' must be positive.");
            if (!RegexUtil.IsValidHexCode(colorCode))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(colorCode)}' is not a valid hex-code.");

            // Make sure the venue name is still available
            if (await _context.Venues.CountAsync(v => v.Name == name) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateVenueName.Code, Messages.Errors.DuplicateVenueName.Message);
            
            // Create new venue
            var venue = new Venue
            {
                Name = name,
                Address = address,
                Capacity = capacity,
                ColorCode = colorCode,
                IsActive = isActive
            };

            // Persist to datastore
            var result = await _context.Venues.AddAsync(venue);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Created a new venue [{name}]");

            return result.Entity;
        }
        /// <summary>
        /// Updates the given venue object in the underlying data store.
        /// </summary>
        /// <param name="venue">The updated venue object</param>
        /// <returns>The updated venue records</returns>
        /// <exception cref="ValidationException">if the given venue is null</exception>
        /// <exception cref="DuplicateValueException">if a venue with that name already exists</exception>
        public async Task<Venue> UpdateVenueAsync(Venue venue)
        {
            if (venue == null)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(venue)}' cannot be null.");

            _context.Entry(venue).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Updated the venue with ID '{venue.Id}'.");

            return venue;
        }
        /// <summary>
        /// Updates the venue with the specified ID.
        /// </summary>
        /// <param name="id">ID of venue to update</param>
        /// <param name="name">The venue's new name</param>
        /// <param name="address">The venue's new address</param>
        /// <param name="capacity">The venue's new capacity</param>
        /// <param name="colorCode">A hex-code used to colorize events taking place at this venue with</param>
        /// <param name="isActive">Sets the venue in/active</param>
        /// <returns>The updated venue object.</returns>
        /// <exception cref="ValidationException">If any of required arguments are null or empty</exception>
        /// <exception cref="EntityNotFoundException">If a venue with the specified ID doesn't exist</exception>
        /// <exception cref="DuplicateValueException">If a venue with the specified name already exists</exception>
        public async Task<Venue> UpdateVenueAsync(int id, string name, string address, int capacity, string colorCode, bool isActive)
        {
            if (string.IsNullOrEmpty(name))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(name)}' cannot be null or empty.");
            if (capacity < 0)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(capacity)}' must be positive.");
            if (!RegexUtil.IsValidHexCode(colorCode))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(colorCode)}' is not a valid hex-code.");

            // Retrieve venue from database
            var src = await _context.Venues.FirstOrDefaultAsync(v => v.Id == id);
            
            if (src == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The venue with the ID '{id}' does not exist.");
            
            // If the venue name has changed, make sure that it is still available
            if (src.Name != name && await _context.Venues.CountAsync(v => v.Name == name && v.Id != src.Id) > 0)
                throw new DuplicateValueException(Messages.Errors.DuplicateVenueName.Code, $"A venue with the name '{name}' already exists.");

            src.Name = name;
            src.Address = address;
            src.Capacity = capacity;
            src.ColorCode = colorCode;
            src.IsActive = isActive;

            await _context.SaveChangesAsync();

            _logger.LogInformation($"Updated the venue with ID '{id}'.");

            return src;
        }
        /// <summary>
        /// Deletes a venue from the underlying datastore.
        /// </summary>
        /// <param name="venue">The venue to delete</param>
        /// <returns>The venue that was deleted</returns>
        public async Task<Venue> DeleteVenueAsync(Venue venue)
        {
            _context.Venues.Remove(venue);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Deleted the venue with ID '{venue.Id}'.");

            return venue;
        }
        /// <summary>
        /// Deletes the venue with the specified ID from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the venue to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The venue that was deleted</returns>
        /// <exception cref="EntityNotFoundException">If the venue with the specified ID doesn't exist</exception>
        /// <exception cref="ValidationException">If the venue has events still in retention</exception>
        public async Task<Venue> DeleteVenueAsync(int id, int retentionDays)
        {
            var venue = await _context.Venues.FirstOrDefaultAsync(v => v.Id == id);

            if (venue == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The venue with ID '{id}' doesn't exist.");
            
            // Count the number of events that concluded at this venue, and that are still in retention
            var retentionDate = DateTime.UtcNow.Subtract(TimeSpan.FromDays(retentionDays));
            var eventCount = await _context.Events
                .Where(e => e.VenueId == id)
                .Where(e => e.Date < DateTime.UtcNow && e.Date > retentionDate)
                .CountAsync();

            if (eventCount > 0)
                throw new ValidationException(Messages.Errors.VenueHasEventsInRetention.Code,
                    Messages.Errors.VenueHasEventsInRetention.Message);

            // Go through with deleting the venue
            _context.Venues.Remove(venue);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Deleted the venue with ID '{id}'.");

            return venue;
        }

        #endregion
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Core.Constants;
using VRT.Core.Exceptions;
using VRT.Core.Models;
using VRT.Data;
using VRT.Data.Entities;
using VRT.Data.Extensions;

namespace VRT.Core.Services
{
    /// <summary>
    /// A service for managing events.
    /// </summary>
    public class EventService : IEventService
    {
        #region Fields

        private readonly VRTContext _context;
        private readonly IRegistrationService _registrationService;
        private readonly INotificationService _notificationService;
        private readonly ILogger<EventService> _logger;

        #endregion


        /// <summary>
        /// Constructs a new EventService object.
        /// </summary>
        /// <param name="context">A VRTContext reference</param>
        /// <param name="registrationService">An IRegistrationService reference</param>
        /// <param name="notificationService">An INotificationService reference</param>
        /// <param name="logger">An ILogger reference</param>
        public EventService(VRTContext context, IRegistrationService registrationService, INotificationService notificationService, ILogger<EventService> logger)
        {
            _registrationService = registrationService;
            _notificationService = notificationService;
            _context = context;
            _logger = logger;
        }


        #region Methods

        /// <summary>
        /// Returns the event with the specified ID (if any) from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to return</param>
        /// <returns>The event with the specified ID, or <see langword="null"/> if no event with that ID exists</returns>
        public async Task<Event> FindEventAsync(int id)
        {
            var evt = await _context.Events
                .Include(e => e.Venue)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            // Retrieve event's registrations
            if (evt != null)
            {
                var regCounts = await _context.Registrations
                    .Where(r => r.EventId == id)
                    .GroupBy(r => r.Status)
                    .Select(r => new { status = r.Key, count = r.Count() })
                    .ToListAsync();

                evt.RegistrationCount = regCounts.Sum(rc => rc.count);
                evt.WaitingListCount = regCounts
                    .Where(rc => rc.status == (int)RegistrationStatus.OnWaitingList)
                    .Select(rc => rc.count)
                    .FirstOrDefault();
            }

            return evt;
        }
        /// <summary>
        /// Returns the event with the specified ID from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to return</param>
        /// <returns>The event with the specified ID</returns>
        /// <exception cref="EntityNotFoundException">if the event with the specified ID doesn't exist</exception>
        public async Task<Event> GetEventAsync(int id)
        {
            var evt = await FindEventAsync(id);

            if (evt == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The event with the specified ID '{id}' does not exist.");

            return evt;
        }
        /// <summary>
        /// Returns the number of events that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of events matching the given filter string</returns>
        public async Task<int> CountEventsAsync(string filter)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return await _context.Events.CountAsync();
            }
            else
            {
                filter = filter.ToLower();

                return await _context.Events
                    .Where(e => e.Name.ToLower().Contains(filter) || e.Description.ToLower().Contains(filter) || 
                        e.Venue.Name.ToLower().Contains(filter) || e.Venue.Address.ToLower().Contains(filter))
                    .CountAsync();
            }
        }
        /// <summary>
        /// Returns a list of events that are matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="userId">The ID of the current user</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of events that are matching the given filter (if any) from the underlying datastore</returns>
        public async Task<IEnumerable<Event>> FindEventsAsync(int userId, int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc")
        {
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Id" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";
            IQueryable<Event> query;

            if (string.IsNullOrEmpty(filter))
            {
                query = _context.Events
                    .Include(e => e.Venue)
                    .Include(e => e.Registrations.Where(r => r.UserId == userId));
            }
            else
            {
                filter = filter.ToLower();

                query = _context.Events
                    .Include(e => e.Venue)
                    .Include(e => e.Registrations.Where(r => r.UserId == userId))
                    .Where(e => e.Name.ToLower().Contains(filter) || e.Venue.Name.ToLower().Contains(filter) || e.Venue.Address.ToLower().Contains(filter) ||
                        (!string.IsNullOrEmpty(e.Description) && e.Description.ToLower().Contains(filter)));
            }

            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Retrieve paged results
            var events = await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            // Retrieve and add the events' registration counts
            await RetrieveEventRegistrations(events);

            // Return results
            return events;
        }
        /// <summary>
        /// Returns the number of events that already took place and that are matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <returns>Returns the number of events that already took place and that are matching the given filter string.</returns>
        public async Task<int> CountPastEventsAsync(string filter)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return await _context.Events
                    .Where(e => e.Date < DateTime.UtcNow)
                    .CountAsync();
            }
            else
            {
                filter = filter.ToLower();

                return await _context.Events
                    .Where(e => e.Date < DateTime.UtcNow)
                    .Where(e => e.Name.ToLower().Contains(filter) || e.Venue.Name.ToLower().Contains(filter) || e.Venue.Address.ToLower().Contains(filter) ||
                        (!string.IsNullOrEmpty(e.Description) && e.Description.ToLower().Contains(filter)))
                    .CountAsync();
            }
        }
        /// <summary>
        /// Returns a list of events that already took place and that are matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="userId">The current user's ID</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of events that already took place and that are matching the given filter (if any) from the underlying datastore</returns>
        public async Task<IEnumerable<Event>> FindPastEventsAsync(int userId, int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc")
        {
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Id" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";
            IQueryable<Event> query;

            if (string.IsNullOrEmpty(filter))
            {
                query = _context.Events
                    .Include(e => e.Venue)
                    .Include(e => e.Registrations.Where(r => r.UserId == userId))
                    .Where(e => e.Date < DateTime.UtcNow);
            }
            else
            {
                filter = filter.ToLower();

                query = _context.Events
                    .Include(e => e.Venue)
                    .Include(e => e.Registrations.Where(r => r.UserId == userId))
                    .Where(e => e.Date < DateTime.UtcNow)
                    .Where(e => e.Name.ToLower().Contains(filter) || e.Venue.Name.ToLower().Contains(filter) || e.Venue.Address.ToLower().Contains(filter) ||
                        (!string.IsNullOrEmpty(e.Description) && e.Description.ToLower().Contains(filter)));
            }

            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Retrieve paged results
            var events = await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            // Retrieve and add the events' registration counts
            await RetrieveEventRegistrations(events);

            // Return results
            return events;
        }
        /// <summary>
        /// Returns the number of future/upcoming events matching the given filter string.
        /// </summary>
        /// <param name="filter">The filter string to be applied (may be null)</param>
        /// <param name="activeOnly">If set to <see langword="true"/>, only events open for registration will be returned; otherwise all future events</param>
        /// <returns>Returns the number of future/upcoming events matching the given filter string.</returns>
        public async Task<int> CountFutureEventsAsync(string filter, bool activeOnly)
        {
            var query = _context.Events.Where(e => e.Date > DateTime.UtcNow);

            // Include only events that are open for registration
            if (activeOnly)
                query = query.Where(e => e.IsOpenForRegistration == true);

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();

                query = query.Where(e => e.Name.ToLower().Contains(filter) || e.Venue.Name.ToLower().Contains(filter) || e.Venue.Address.ToLower().Contains(filter) ||
                        (!string.IsNullOrEmpty(e.Description) && e.Description.ToLower().Contains(filter)));
            }
   
            return await query.CountAsync();
        }
        /// <summary>
        /// Returns a list of future/upcoming events matching the given filter (if any) from the underlying datastore.
        /// </summary>
        /// <param name="userId">The current user's ID</param>
        /// <param name="activeOnly">If set to <see langword="true"/>, only events open for registration will be returned; otherwise all future events</param>
        /// <param name="page">The number of results to return</param>
        /// <param name="pageSize">The maximum number of results per page</param>
        /// <param name="filter">A filter to be applied to objects of the result set</param>
        /// <param name="sortColumn">The property name by which to sort result objects by</param>
        /// <param name="sortOrder">The sort order: 'asc' or 'desc'</param>
        /// <returns>a list of future/upcoming events matching the given filter (if any) from the underlying datastore.</returns>
        public async Task<IEnumerable<Event>> FindFutureEventsAsync(int userId, bool activeOnly, int page = 1, int pageSize = 10, string filter = null, string sortColumn = "Id", string sortOrder = "asc")
        {
            sortColumn = string.IsNullOrEmpty(sortColumn) ? "Id" : sortColumn;
            bool sortAscending = string.IsNullOrEmpty(sortOrder) || sortOrder == "asc";
            
            var query = _context.Events
                    .Include(e => e.Venue)
                    .Include(e => e.Registrations.Where(r => r.UserId == userId))
                    .Where(e => e.Date > DateTime.UtcNow);

            // Include only events that are open for registration
            if (activeOnly)
                query = query.Where(e => e.IsOpenForRegistration == true);

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();

                query = query.Where(e => e.Name.ToLower().Contains(filter) || e.Venue.Name.ToLower().Contains(filter) || e.Venue.Address.ToLower().Contains(filter) ||
                        (!string.IsNullOrEmpty(e.Description) && e.Description.ToLower().Contains(filter)));
            }

            // Order result set
            query = sortAscending ? query.OrderBy(sortColumn) : query.OrderByDescending(sortColumn);

            // Retrieve paged results
            var events = await query
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            // Retrieve and add the events' registration counts
            await RetrieveEventRegistrations(events);

            // Return results
            return events;
        }
        /// <summary>
        /// Creates a new event with the specified properties in the underlying datastore.
        /// </summary>
        /// <param name="venueId">The ID of the venue where tis event is taking place</param>
        /// <param name="name">The name of the event</param>
        /// <param name="description">An optional description</param>
        /// <param name="date">The date and time when this event is taking place</param>
        /// <param name="capacity">The maximum number of participants that can partake in that event</param>
        /// <param name="isOpenForRegistration">Indicates whether users are allowed to register for this event</param>
        /// <returns>Returns the newly created event</returns>
        /// <exception cref="ValidationException">if one of the required parameters is invalid</exception>
        /// <exception cref="EntityNotFoundException">If the venue with the specified ID doesn't exist</exception>
        public async Task<Event> CreateEventAsync(int venueId, string name, string description, DateTime date, int capacity, bool isOpenForRegistration)
        {
            if (string.IsNullOrEmpty(name))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(name)}' cannot be null or empty.");
            if (capacity < 0)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(capacity)}' must be positive.");
            if (date < DateTime.UtcNow)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(date)}' must be in the future.");

            // Make sure that the specified venue exists
            var venue = await _context.Venues.FirstOrDefaultAsync(v => v.Id == venueId);
            if (venue == null)
                throw new ValidationException(Messages.Errors.ReferenceNotFound.Code, $"The referenced venue with the ID '{venueId}' does not exist.");

            // Create new event
            var evt = new Event
            {
                Venue = venue,
                Name = name,
                Description = description,
                Date = date,
                Capacity = capacity,
                IsOpenForRegistration = isOpenForRegistration
            };

            // Persist to datastore
            var result = await _context.Events.AddAsync(evt);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"New event [{result.Entity.Id} - '{evt.Name}'] created successfully.");

            // Inform users
            if (evt.IsOpenForRegistration)
                await _notificationService.SendNewEventNotificationAsync(evt);

            return result.Entity;
        }
        /// <summary>
        /// Updates the specified event with in the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to update</param>
        /// <param name="venueId">The ID of the venue where tis event is taking place</param>
        /// <param name="name">The name of the event</param>
        /// <param name="description">An optional description</param>
        /// <param name="date">The date and time when this event is taking place</param>
        /// <param name="capacity">The maximum number of participants that can partake in that event</param>
        /// <param name="isOpenForRegistration">Indicates whether users are allowed to register for this event</param>
        /// <returns>Returns the updated event</returns>
        /// <exception cref="ValidationException">if one of the required parameters is invalid</exception>
        /// <exception cref="EntityNotFoundException">If the event or venue with the specified ID doesn't exist</exception>
        public async Task<Event> UpdateEventAsync(int id, int venueId, string name, string description, DateTime date, int capacity, bool isOpenForRegistration)
        {
            if (string.IsNullOrEmpty(name))
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(name)}' cannot be null or empty.");
            if (capacity < 0)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(capacity)}' must be positive.");
            if (date < DateTime.UtcNow)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(date)}' must be in the future.");

            // Retrieve the event
            var src = await FindEventAsync(id);

            // Event must exist and must not have started yet
            if (src == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The event with the specified ID '{id}' does not exist.");
            if (src.Date < DateTime.UtcNow)
                throw new ValidationException(Messages.Errors.EventAlreadyStarted.Code, Messages.Errors.EventAlreadyStarted.Message);

            // Make sure that the specified venue exists
            var venue = await _context.Venues.FirstOrDefaultAsync(v => v.Id == venueId);
            if (venue == null)
                throw new ValidationException(Messages.Errors.ReferenceNotFound.Code, $"The referenced venue with the ID '{venueId}' does not exist.");

            // The event was previously open for registrations and the event's capacity has changed
            // A recalculation of the participant/waiting list is required
            var recalcRequired = src.IsOpenForRegistration && src.Capacity != capacity;

            // The event was just made public
            var wasJustMadePublic = !src.IsOpenForRegistration & isOpenForRegistration;
            
            // TODO: Changing the venue / date / capacity should trigger a notification

            // Update event
            src.Venue = venue;
            src.Name = name;
            src.Description = description;
            src.Date = date;
            src.Capacity = capacity;
            src.IsOpenForRegistration = isOpenForRegistration;

            // Persist to datastore
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Updated the event with ID '{id}'.");

            // Notify users that a new event is available for registration
            if (wasJustMadePublic)
                await _notificationService.SendNewEventNotificationAsync(src);
            // Recalculate event registrations and send notifications if necessary
            else if (recalcRequired)
                await _registrationService.CalculateWaitingListAsync(id);

            return src;
        }
        /// <summary>
        /// Updates the given event object in the underlying data store.
        /// </summary>
        /// <param name="evt">The updated event object</param>
        /// <returns>The updated event record</returns>
        /// <exception cref="ValidationException">if the given event is null</exception>
        public async Task<Event> UpdateEventAsync(Event evt)
        {
            if (evt == null)
                throw new ValidationException(Messages.Errors.InputIsInvalid.Code, $"Argument '{nameof(evt)}' cannot be null.");

            _context.Entry(evt).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Updated the event with ID '{evt.Id}'.");

            return evt;
        }
        /// <summary>
        /// Deletes an event from the underlying datastore.
        /// </summary>
        /// <param name="evt">The event to delete</param>
        /// <returns>The event that was deleted</returns>
        public async Task<Event> DeleteEventAsync(Event evt)
        {
            _context.Events.Remove(evt);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"The event with the ID '{evt.Id}' was deleted.");

            // Notify users that the event was canceled
            if (evt.IsOpenForRegistration && evt.Date > DateTime.UtcNow)
                await _notificationService.SendEventCanceledNotificationAsync(evt);

            return evt;
        }
        /// <summary>
        /// Deletes the event with the specified ID from the underlying datastore.
        /// </summary>
        /// <param name="id">The ID of the event to delete</param>
        /// <param name="retentionDays">The number of days that user data must be kept</param>
        /// <returns>The event that was deleted</returns>
        /// <exception cref="EntityNotFoundException">If the event with the specified ID doesn't exist</exception>
        /// <exception cref="ValidationException">If the events has concluded but is still in retention</exception>
        public async Task<Event> DeleteEventAsync(int id, int retentionDays)
        {
            var evt = await _context.Events
                .Include(e => e.Venue)
                .Include(e => e.Registrations)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();

            if (evt == null)
                throw new EntityNotFoundException(Messages.Errors.EntityNotFound.Code, $"The event with ID '{id}' doesn't exist.");

            // Check if the event has already concluded, if there were active participants, and if it's safe to delete
            var dontDeleteBefore = evt.Date.AddDays(retentionDays);
            if (evt.Date < DateTime.UtcNow && evt.Registrations.Count > 0 && DateTime.UtcNow < dontDeleteBefore)
                throw new ValidationException(Messages.Errors.EventIsInRetention.Code,
                    $"The event (ID={evt.Id}) can not be deleted before {dontDeleteBefore:u}");
                
            _context.Events.Remove(evt);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"The event with the ID '{id}' was deleted.");

            // Notify users that the event was canceled
            if (evt.IsOpenForRegistration && evt.Date > DateTime.UtcNow)
                await _notificationService.SendEventCanceledNotificationAsync(evt);

            return evt;
        }
        /// <summary>
        /// Creates a text log for the event with the specified ID. 
        /// The log contains the event's general information and list of participants.
        /// </summary>
        /// <param name="evt">The event for which to create the log</param>
        /// <returns>The EventLog object that was created</returns>
        /// <exception cref="EntityNotFoundException">If an event with the specified ID doesn't exist</exception>
        /// <exception cref="ValidationException">If the event is still in the future (hasn't started yet)</exception>
        public async Task<string> CreateEventLogAsync(Event evt)
        {            
            // Load venue if necessary
            if (evt.Venue == null)
                await _context.Entry(evt).Reference(e => e.Venue).LoadAsync();

            // Retrieve event registrations
            var registrations = await _context.Registrations
                .Include(r => r.User)
                .Where(r => r.EventId == evt.Id)
                .ToListAsync();

            return CreateEventLog(evt, registrations);
        }

        #endregion

        #region Utility

        /// <summary>
        /// Convenience method for retrieving and adding the number of event registrations (total and on waiting list) to the given list of events.
        /// </summary>
        /// <param name="events">The list of events for which to retrieve the number of event registrations</param>
        private async Task RetrieveEventRegistrations(List<Event> events)
        {
            // Count the registrations for all events, by status
            var regCounts = await _context.Registrations
                .GroupBy(r => new { r.EventId, r.Status })
                .Select(r => new { eventId = r.Key.EventId, status = r.Key.Status, count = r.Count() })
                .ToListAsync();

            // Add registration counts to retrieved events
            foreach (var evt in events)
            {
                // Total registrations count
                evt.RegistrationCount = regCounts
                    .Where(rc => rc.eventId == evt.Id)
                    .ToList()
                    .Sum(rc => rc.count);

                // Registrations with status 'on waiting list'
                evt.WaitingListCount = regCounts
                    .Where(rc => rc.eventId == evt.Id)
                    .Where(rc => rc.status == (int)RegistrationStatus.OnWaitingList)
                    .Select(rc => rc.count)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// Creates a text log for the given event and associated registrations and returns it.
        /// </summary>
        /// <param name="evt">The event</param>
        /// <param name="registrations">The event's registrations</param>
        /// <returns>an array of bytes containing the event's text log</returns>
        private string CreateEventLog(Event evt, List<Registration> registrations)
        {
            // Build log file
            StringBuilder builder = new StringBuilder();

            // General event information
            builder.AppendLine($"{"ID:",-15} {evt.Id}");
            builder.AppendLine($"{"Name:",-15} {evt.Name}");
            builder.AppendLine($"{"Datum:",-15} {evt.Date.ToLocalTime().ToString("f")}");
            builder.AppendLine($"{"Sportstätte:",-15} {evt.Venue?.Name ?? "n/a"}");
            builder.AppendLine($"{"Adresse:",-15} {evt.Venue?.Address ?? "n/a"}");
            builder.AppendLine($"{"Beschreibung:",-15} {evt.Description}");
            builder.AppendLine($"{"Anmeldungen:",-15} {registrations.Count()}/{evt.Capacity}");

            // Active participants
            builder.AppendLine();
            builder.AppendLine("Aktive Teilnehmer:");

            foreach (var registration in registrations.Where(p => p.Status == (int)RegistrationStatus.Registered))
                builder.AppendLine($"  {registration.User.DisplayName} ({registration.Registered.ToLocalTime().ToString("f")})");

            // Participants on waiting list
            builder.AppendLine();
            builder.AppendLine("Auf Warteliste:");

            foreach (var registration in registrations.Where(p => p.Status == (int)RegistrationStatus.OnWaitingList))
                builder.AppendLine($"  {registration.User.DisplayName} ({registration.Registered.ToLocalTime().ToString("f")})");

            // Return log file as bytes
            return builder.ToString();
        }

        #endregion
    }
}

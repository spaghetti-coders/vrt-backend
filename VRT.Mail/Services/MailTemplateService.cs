﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Mail.Models;

namespace VRT.Mail.Services
{
    /// <summary>
    /// The default implementation of the <see cref="IMailTemplateService"/> interface.
    /// </summary>
    public class MailTemplateService : IMailTemplateService
    {
        #region Fields

        private IRazorViewEngine _razorViewEngine;
        private ITempDataProvider _tempDataProvider;
        private IServiceProvider _serviceProvider;

        #endregion


        /// <summary>
        /// Constructs a new MailTemplateService object.
        /// </summary>
        /// <param name="razorViewEngine">An IRazorViewEngine reference</param>
        /// <param name="tempDataProvider">An ITempDataProvider reference</param>
        /// <param name="serviceProvider">An IServiceProvider reference</param>
        public MailTemplateService(IRazorViewEngine razorViewEngine, ITempDataProvider tempDataProvider, IServiceProvider serviceProvider)
        {
            _razorViewEngine = razorViewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
        }


        #region Methods

        /// <summary>
        /// Loads the specified template and renders it using the provided model.
        /// </summary>
        /// <typeparam name="T">The type of model used for rendering the specified template</typeparam>
        /// <param name="template">The name of the template to load and render</param>
        /// <param name="model">The model providing the values to be rendered</param>
        /// <param name="culture">An optional culture info to render culture specific information such as dates</param>
        /// <returns>The rendered template as HTML string</returns>
        /// <exception cref="FileNotFoundException">if the specified template doesn't exist</exception>
        public async Task<string> GetTemplateAsHtmlStringAsync<T>(string template, T model, CultureInfo culture = null) where T: TemplateModel
        {
            var actionContext = GetContext();
            var view = FindTemplate("/Areas/MyFeature/Views/", template);

            using (var writer = new StringWriter())
            {
                var viewContext = new ViewContext(
                    actionContext,
                    view,
                    new ViewDataDictionary<T>(new EmptyModelMetadataProvider(), new ModelStateDictionary()) { Model = model },
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    writer,
                    new HtmlHelperOptions()
                );

                // Set culture
                if (culture != null)
                {
                    CultureInfo.CurrentUICulture = culture;
                    CultureInfo.CurrentCulture = culture;
                }

                await view.RenderAsync(viewContext);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Convenience method for loading the specified view.
        /// </summary>
        /// <param name="templatesPath">The path that contains the templates</param>
        /// <param name="template">The templates's filename</param>
        /// <returns>the specified view</returns>
        /// <exception cref="FileNotFoundException">if the specified view doesn't exist</exception>
        private IView FindTemplate(string templatesPath, string template)
        {
            var viewResult = _razorViewEngine.GetView(templatesPath, template, true);

            if (!viewResult.Success)
                throw new FileNotFoundException("The specified template could not be found.", template);

            return viewResult.View;
        }
        /// <summary>
        /// Convenience method for creating and returning a basic ActionContext instance.
        /// </summary>
        /// <returns>a basic ActionContext instance</returns>
        private ActionContext GetContext()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.RequestServices = _serviceProvider;

            return new ActionContext(httpContext, new RouteData(), new ActionDescriptor());
        }

        #endregion
    }
}

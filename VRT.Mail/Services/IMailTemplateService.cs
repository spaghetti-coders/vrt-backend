﻿using System.Globalization;
using System.Threading.Tasks;
using VRT.Mail.Models;

namespace VRT.Mail.Services
{
    /// <summary>
    /// A helper service for loading and rendering HTML Mail Templates using the ASP.NET Razor engine.
    /// </summary>
    public interface IMailTemplateService
    {
        /// <summary>
        /// Loads the specified template and renders it using the provided model.
        /// </summary>
        /// <typeparam name="T">The type of model used for rendering the specified template</typeparam>
        /// <param name="template">The name of the template to load and render</param>
        /// <param name="model">The model providing the values to be rendered</param>
        /// <param name="culture">An optional culture info to render culture specific information such as dates</param>
        /// <returns>The rendered template as HTML string</returns>
        Task<string> GetTemplateAsHtmlStringAsync<T>(string template, T model, CultureInfo culture) where T: TemplateModel;
    }
}

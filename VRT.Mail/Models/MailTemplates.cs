﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Mail.Models
{
    /// <summary>
    /// The class defines the available mail templates.
    /// </summary>
    public static class MailTemplates
    {
        /// <summary>
        /// Defines the mail templates related to events.
        /// </summary>
        public static class Events
        {
            /// <summary>
            /// Mail template for informing users that a new event is available.
            /// </summary>
            public const string NewEvent = "NewEvent.cshtml";
            /// <summary>
            /// Mail template for informing users that an event was canceled.
            /// </summary>
            public const string EventCanceled = "EventCanceled.cshtml";
        }
        /// <summary>
        /// Defines the mail templates related to users.
        /// </summary>
        public static class Users
        {
            /// <summary>
            /// Mail template for informing administrators about a new user that needs to be activated.
            /// </summary>
            public const string NewRegistration = "NewUserRegistration.cshtml";
            /// <summary>
            /// Mail template for confirming a user's registration with the app.
            /// </summary>
            public const string RegistrationConfirmation = "UserRegistrationConfirmation.cshtml";
            /// <summary>
            /// Mail template for notifying a user that their account has been activated.
            /// </summary>
            public const string AccountActivated = "UserRegistrationCompleted.cshtml";
            /// <summary>
            /// Mail template for confirming to the user that their account has been deleted.
            /// </summary>
            public const string AccountDeleted = "UserAccountDeletionConfirmation.cshtml";
            /// <summary>
            /// Mail template for confirming to the user that their account has been deleted.
            /// </summary>
            public const string AccountDeletionScheduled = "UserAccountDeletionScheduledConfirmation.cshtml";
            /// <summary>
            /// Mail template for sending a user an account recovery token/link.
            /// </summary>
            public const string AccountRecovery = "UserAccountRecovery.cshtml";
        }
        /// <summary>
        /// Defines the mail templates related to 
        /// </summary>
        public static class EventRegistrations
        {
            /// <summary>
            /// Mail template for informing a user that they moved from an event's waiting list to the active participants.
            /// </summary>
            public const string Upgraded = "EventRegistrationUpgraded.cshtml";
            /// <summary>
            /// Mail template for informing a user that they moved from an event's active participants to the waiting list.
            /// </summary>
            public const string Downgraded = "EventRegistrationDowngraded.cshtml";
            /// <summary>
            /// Mail template for informing a user that their registration for an upcoming event was deleted by an administrator.
            /// </summary>
            public const string DeletedByAdmin = "EventRegistrationDeleted.cshtml";
        }
    }
}

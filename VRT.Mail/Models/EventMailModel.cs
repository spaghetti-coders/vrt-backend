﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Mail.Models
{
    /// <summary>
    /// The <see cref="TemplateModel"/> for all event related mail notifications. 
    /// </summary>
    public class EventMailModel : TemplateModel
    {
        /// <summary>
        /// The event that is the subject of the mail notification.
        /// </summary>
        public Event Event { get; set; }
        /// <summary>
        /// The venue at which the event is taking place.
        /// </summary>
        public Venue Venue { get; set; }
    }
}

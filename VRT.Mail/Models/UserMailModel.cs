﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Mail.Models
{
    /// <summary>
    /// The <see cref="TemplateModel"/> used for all kinds of user related mails.
    /// </summary>
    public class UserMailModel : TemplateModel
    {
        /// <summary>
        /// The user that is the mail's subject.
        /// </summary>
        public User User { get; set; }
    }
}

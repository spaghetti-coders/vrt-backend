﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Mail.Models
{
    /// <summary>
    /// The <see cref="TemplateModel"/> used for account recovery mails.
    /// </summary>
    public class AccountRecoveryMailModel : TemplateModel
    {
        /// <summary>
        /// The user that is the mail's subject.
        /// </summary>
        public User User { get; set; }
        /// <summary>
        /// The user's recovery token.
        /// </summary>
        public AccountRecoveryToken RecoveryToken { get; set; }
    }
}

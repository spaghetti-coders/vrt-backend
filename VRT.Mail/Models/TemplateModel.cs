﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Mail.Models
{
    /// <summary>
    /// The abstract base class for all mail template models.
    /// </summary>
    public abstract class TemplateModel
    {
        /// <summary>
        /// The application's URL. (e.g., 'https://vrt.example.com') 
        /// </summary>
        public string AppUrl { get; set; }
    }
}

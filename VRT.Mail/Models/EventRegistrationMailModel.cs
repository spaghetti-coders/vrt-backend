﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Mail.Models
{
    /// <summary>
    /// The <see cref="TemplateModel"/> used for all kinds of event-registration related mails.
    /// </summary>
    public class EventRegistrationMailModel : TemplateModel
    {
        /// <summary>
        /// The user that is the mail's subject.
        /// </summary>
        public User User { get; set; }
        /// <summary>
        /// The event to which the user is/was registered.
        /// </summary>
        public Event Event { get; set; }
        /// <summary>
        /// The venue at which the event is taking place.
        /// </summary>
        public Venue Venue { get; set; }
    }
}

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Push-notifications now include actions

### Fixed / Changed
- Fixed missing email address format validation when requesting account recovery tokens

## [1.1.1] - 2022-05-22

### Changed
- Updated to .NET 6
- Updated 3rd-party libraries to latest stable release

## [1.1.0] - 2022-01-16

### Added
- Added a new endpoint for retrieving a specified user's event registrations **(admins only)**: 
  - `GET /api/EventRegistrations/user/{userId}`
- Added integration tests for testing the new endpoint 

### Fixed / Changed
- Added the `EventName` and `EventDate` properties to the `AppRegistration` class
- Changed query parameters for `/api/EventRegistrations/event/{eventId}`
  - The `status` parameter now takes an enum value of type `RegistrationStatus` 
  - The `status` parameter is now optional (omitting it, or setting it to `null` will return all results)
  - The `EventName` and `EventDate` values can now be used with the `sortBy` parameter
- Refactoring of the `RegistrationService` class (removed duplicate code and functions)
- Updated existing integration tests to check for the new `AppRegistration` properties

### Removed
- The `RegStatusFilter` enum used for querying event registrations by status

## [1.0.5] - 2022-01-15

### Added
- Added a new property `sortProperties` to the `metadata` object returned in paged responses. 
The property contains a list of properties that can be used with the `sortBy` query parameter.
- Added a new property `requestParams` to the `metadata` object returned in paged responses.
The property contains the query parameters that were used to produce the paged response.
- Added new `AppLogEntry` response model
- Added the `EventsQueryParams` and `PastEventsQueryParams` classes for validating events query parameters

### Fixed / Changed
- **[Breaking Change]** The `/api/AdminLog` endpoint(s) now return `AppLogEntry` instead of `AdminLogEntry` objects
- **[Breaking Change]** The query parameter `sortColumn` has been renamed to `sortBy`
- **[Breaking Change]** The following values for sorting (`&sortBy=...`) event registrations have been renamed to match their `AppRegistration` property names:
  - `User.DisplayName` has been renamed to `UserDisplayName`
  - `Registered` has been renamed to `RegistrationDate`
- Updated integration test
- Changed the default sorting for `FindEvents`, `FindAvailableEvents`, and `FindFutureEvents` to `sortBy=Date&sortOrder=asc`
- Changed the default sorting for `FindPastEvents` to `sortBy=Date&sortOrder=desc`
- Changed the PagedResult's metadata property from `metaData` to `metadata`


## [1.0.4] - 2022-01-09

### Added
- Query and path parameter validation to `AdminLogController` and `UsersController`
- Added the `AdminLogQueryParams` and `UserQueryParams` classes
- Integration tests for the `UsersController` class
- Added integration tests for sorting paged results to all controllers

### Fixed / Changed
- Changed the HTTP Status returned when trying to generate account recovery tokens for inactive users, from `401 Unauthorized` to `400 Bad Request`. Affected endpoints are:
  - `POST /api/Account/recover/request-token` 
  - `POST /api/Users/recover/<user_id>`
- Added missing role value validation to the `UpdateUserModel` class
- Added missing email settings validation when updating user settings 


## [1.0.3] - 2022-01-01

### Added
- Added new controller for managing event registrations (`EventRegistrationsController`)
    - `GET /api/EventRegistrations/event/{eventId}` _(Lists the event's registered users)_
    - `POST /api/EventRegistrations/register/{eventId}` _(Signs-up the current user for an event)_
    - `DELETE /api/EventRegistrations/unregister/{eventId}` _(Delete the current user's registration for an event)_
    - `POST /api/EventRegistrations` _(Adds a user to an event in the past; **requires admin role**)_
    - `DELETE /api/EventRegistrations` _(Deletes a user's registration for an event; **requires admin role**)_
- Added the `EventRegistrationModel` for adding/removing a user's event registration as admin
- Added the `EventRegQueryParams`model for validating query parameters in the `EventRegistrationsContoller`
- Added the error message `E005` to indicate a **bad request** due to invalid **query** parameters
- Added the error message `E006` to indicate a **bad request** due to invalid **path** parameters
- Added the error message `E007` to indicate a **forbidden request** due to invalid user role permissions

### Fixed / Changed
- Added missing check for duplicate user sign-up to an event
- Added missing validation of all query and path parameters in the `EventsController`
- Added missing check for user's role when accessing event registrations of past/closed events 
- Re-purposed the error `E202` _(used for indicating that event log for upcoming events cannot be exported; changed in v1.0.1)_ 
  to indicate that a past event cannot be deleted yet, as it still falls under the configured retention period
- Many event registration methods will now correctly return a `404 Not Found` instead of a `400 Bad Request` response, 
  when a specified user or event does not exist in the database  
- General Refactoring

### Removed
- Event registration methods and endpoints from `EventsController` 


## [1.0.2] - 2021-12-30

### Added
- Integration tests for the venues controller (`VenuesControllerTests`) 
- Added a model for `PagedQueryParameters`
- Added the new error message `E601` returned when a venue is created/updated with a name that already exists 
- Added the new error message `E602` returned when a venue that has events in retention, was tried to be deleted

### Fixed / Changed
- Added missing check for duplicate venue name in the VenueService's `UpdateVenueAsync` and `CreateVenueAsync` methods.
- Added missing validation of all query and path parameters in the `VenuesController`
- Some general refactoring


## [1.0.1] - 2021-12-25

### Fixed / Changed
- The CORS configuration is now being loaded in all environments  
- Event logs can now also be downloaded for upcoming events

### Removed
- Support for SPA and static files
- The `Client/Angular` template folder


## [1.0.0] - 2021-10-18

### Added
- Added a POSTGRES database creation script `Scripts\createdb.sql` to the `VRT.Data` project
- Dockerized the VRT backend

### Fixed / Changed
- Moved `VRT.API/appsettings.json` and `VRT.API/nlog.config` to the `VRT.API/Settings` directory
- Updated logfiles in `nlog.config` to use UNIX paths
- Updated documentation (`README.MD`) for using VRT with Docker

## [0.9.9] - 2021-08-08

### Added
- Added the property `waitingListCount` to the `AppEvent` model

## [0.9.8] - 2021-07-19

### Added
- Added the endpoint `api/Events/{eventId}/registrations/{userId}` (`POST`) for adding users to past events (admins only)
- Added a project license (MIT)
- Added project's 3rd-party library licenses

## [0.9.7] - 2021-07-06

### Added
- Added a unique constraint to the `User` table's `Email` address column
- Added the user's **username** to the account recovery email

### Fixed / Changed
- User Registration: It is now no longer possible to register with an email address that is already being used
- Account Management: Added email address validation, preventing the user to set an email address that is already being used by a different user
- User Management (Admin): Added email address validation, preventing the admin from setting an email address that is being used by a different user
- Account Recovery: Now only requires the user to specify their email address instead of username and email address
- Account Recovery: Now returns `400 Bad Request` instead of `401 Unauthorized` when the specified token is invalid, or the associated user is inactive

## [0.9.6] - 2021-06-28

### Added
- Added the property `EnableNotifications` (**boolean**) to the `Hosting` section of the application configuration (`appsettings.json`). 

### Fixed / Change
- User Registration: Usernames are now trimmed before being stored to the database
- DateTime objects are now converted .ToLocalTime() when used in Push-Notifications

## [0.9.5] - 2021-06-18

### Added
- Added a new endpoint `api/Events/future` for querying all future events (admins and managers only)

### Fixed / Change
- The `api/Account/recover/request-token` endpoint now no longer returns the generated recovery token in the response
- Email addresses will now be stored as and compared to lower-case
- Added display name validation (backend will now return HTTP 400 instead of 500, if the display name is already taken)

## [0.9.4] - 2021-06-17

### Added
- Added a new endpoint `api/Events/past` for querying events that already took place

### Fixed / Change
- Filtered queries against all endpoints will now compoare in lowercase


## [0.9.3] - 2021-06-10

### Added
- Added a new email notification, notifying a users that the deletion of their account was postponed until a specific date
- Added the properties `Status` _(int; values: [0, 1, 2, 3]; see UserStatus)_ and `DeletionDate` _(DateTime)_ to the `AppUser` model
- Added an endpoint for changing a user's status (admins only): `/api/Users/{userId}/status`
- Added the `UpdateUserStatus` model for updating a users status
- Added code for preventing possible DOS attacks, whereby the application's defautl resource `/` is spammed with non-GET requests 
- Added a service for periodically (twice a day) checking whether user accounts are due to be deleted

### Removed
- Removed the `IsActive` property from the `AppUser` model
- Removed the `IsActive` property from the `UpdateUserModel`

### Fixed / Change
- Updated the UserStatus enumeration (0 = `Pending Registration`, 1 = `Active`, 2 = `Inactive`, 3 = `Pending Deletion`, 4 = `Any`)
- When deleting user accounts, the deletion may be postoned for the configured number of days (`DataRetentionPeriod`) after the last event that the user has participated in


## [0.8.5] - 2021-06-05

### Added
- New endpoint for downloading event logs: `/api/Events/{id}/log`
- Added Resource file for translating the actions written to the AdminLog
- Added the properties `DataRetentionPeriod` and `PushIconUri` to the `Hosting`-section of the `appsettings.json`
- Push-Notifications now show the app's icon

### Removed

### Fixed / Change
- User's JWT refresh tokens are now stored in the database instead of the server's memory
- Timestamp comparions are now performed using UTC+0 instead of the current server's timezone


## [0.7.´0] - 2021-05-17

### Added
- Added a new Razor Library for rendering HTML mail templates 
- Added new mail notifications: registration confirmation, account activated, account recovery
- Added a /MailTemplates endpoint for testing the rendering of the notifications

### Removed

### Fixed / Change
- Changed old email notification templates and for new HTML ones


## [0.6.1] - 2021-05-02

### Added
- Added the 'UpdatePasswordModel' and the corresponding /update-password endpoint (PUT) to the account controller

### Removed

### Fixed / Change
- Removed the 'Password' property from the 'UpdateAccountModel'
- Changed the HTTP method of the account controller's /update endpoint from POST to PUT
- Added the optional parameter `status` to /Events/{eventId}/registrations endpoint, for filtering registrations by their status


## [0.6.0] - 2021-04-20

### Added
- Added a hosted service for periodically deleting expired account recovery tokens
- Added support for sending web push notifications (+ corresponding section in appsetings.json)
- Added the capability to send mail notifications

### Removed
- Removed the old notification model and associated endpoints

### Fixed / Changed
- Fixed the response status code of the `/api/{eventId}/registrations` (POST) ressource from `201` to `200`

## [0.5.1] - 2021-04-10

### Added
- Added an optional *status* query parameter to the `/api/Users` endpoint, that allows for filtering based on user account activeness
- Added the `registrationCount` (the number of user registrations for the event) property to the `AppEvent` model

### Fixed / Changed
- Fixed a bug that allowed users to register to past events

## [0.5.0] - 2021-04-06

### Added
- Added controller for retrieving user notifications
- Added methods for managing event registrations to the events controller
- Added support for user notifications to different services

### Removed
- Removed the `ReceiveNotifications` property from the registration model

### Fixed / Changed
- Added a (hex-) color-code property to the venues model
- Added a boolean to the user model for indicating whether they wish to receive notifications
- Added the status and registration date to the registration model
- Changed default sort order of events to 'by date, descending'
- Changed default sort order of the admin log to 'by date, descending'


## [0.4.1] - 2021-03-27

### Fixed / Changed
- Refresh token requests can now be made anonymously, but require the client's last access token as additional POST data
- Refresh tokens can now only be used once to successfully refresh a JWT token pair (a new refresh token will be issued, and the old one be deleted from the server)

## [0.4.0] - 2021-03-26

### Added
- Added controller for managing events

### Removed

### Fixed / Changed


## [0.3.0] - 2021-03-25

### Added
- Added controller for managing venues

### Removed

### Fixed / Changed
- Fixed the return types in the swagger documentation

## [0.2.0] - 2021-03-24

### Added
- Users can recover their account using recovery tokens
- Admins can generate recovery tokens for users
- Added an error message model (code, message, type) returned all in 4xx responses
- Actions performed by administrators that change data within the application are now logged; the log can be viewed by administrators

### Removed

### Fixed / Changed
- Extended the user role model. Available user roles are: 'admin', 'manager', and 'user'
- The BCrypt section in the appsettings.json file is now called `Account`, some property names have changed. 


## [0.1.0] - 2021-03-21

### Added
- Added support for user account management and authentication
- Added basic (administrative) user management

### Removed

### Fixed / Changed

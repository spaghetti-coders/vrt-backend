﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VRT.Test.Fixtures;
using Xunit;
using Flurl;
using Newtonsoft.Json;
using VRT.API.Models;
using FluentAssertions;
using System.Net;
using VRT.API.Models.AdminLog;
using VRT.Core.Constants;

namespace VRT.Test
{
    [Collection("Collection1")]
    public class AdminLogControllerTest : IntegrationTest
    {
        public AdminLogControllerTest(ApiWebApplicationFactory fixture, DatabaseResetFixture dbResetFixture) : base(fixture, dbResetFixture) { }


        [Theory]
        [InlineData(1, 10, null, "Timestamp", "desc")]
        [InlineData(5, 50, "Joe", "Id", "asc")]
        [InlineData(1, 10, null, null, null)]
        public async Task RetrieveLogs_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize, string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = "/api/AdminLog"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            
            var result = JsonConvert.DeserializeObject<PagedResult<AppLogEntry>>(
               await response.Content.ReadAsStringAsync());
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.Should().NotBeNull("because metadata should always be returned in paged responses.");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in response.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should()
                .NotBeEmpty("because there should be at least some sort properties.");
        }
        [Theory]
        [InlineData(0, 10, null, null, null)]
        [InlineData(2, -20, null, null, null)]
        [InlineData(1, 5, null, "InvalidProperty", null)]
        [InlineData(1, 5, null, null, "randomly")]
        public async Task RetrieveLogs_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize, string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = "/api/AdminLog"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });

            // Act
            var response = await _client.GetAsync(requestUri);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData("Actor", "asc")]
        [InlineData("Target", "desc")]
        [InlineData("Timestamp", "desc")]
        [InlineData("Timestamp", "asc")]
        public async Task RetrieveLogs_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/AdminLog"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppLogEntry>>(content);

            var propertyInfo = typeof(AppLogEntry).GetProperty(sortBy);
            var comparer = Comparer<AppLogEntry>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task RetrieveLogs_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);

            // Act
            var response = await _client.GetAsync("/api/AdminLog");

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
    }
}

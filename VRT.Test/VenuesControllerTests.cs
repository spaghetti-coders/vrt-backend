using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Flurl;
using Newtonsoft.Json;
using VRT.API.Models;
using VRT.API.Models.Venues;
using VRT.Core.Constants;
using VRT.Test.Fixtures;
using Xunit;

namespace VRT.Test
{
    [Collection("Collection1")]
    public class VenuesControllerTests : IntegrationTest
    {
        private static readonly Random _random = new();
        
        public VenuesControllerTests(ApiWebApplicationFactory fixture, DatabaseResetFixture dbResetFixture) : base(fixture, dbResetFixture) { }


        #region Find Venues
        
        [Theory]
        [InlineData(1, 5, null, "Id", "asc")]
        [InlineData(1, 10, "Wiese", "Address", "desc")]
        [InlineData(1, 200, null, null, null)]
        public async Task FindVenues_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize, 
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = "/api/Venues"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            
            var result = JsonConvert.DeserializeObject<PagedResult<AppVenue>>(
                await response.Content.ReadAsStringAsync());
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "beacause that's what we've requested");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Name", "asc")]
        [InlineData("Capacity", "desc")]
        [InlineData("Capacity", "asc")]
        [InlineData("Address", "desc")]
        public async Task FindVenues_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppVenue>>(content);

            var propertyInfo = typeof(AppVenue).GetProperty(sortBy);
            var comparer = Comparer<AppVenue>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(0, 5, null, "Id", "asc")]
        [InlineData(1, -10, "Wiese", "Address", "desc")]
        [InlineData(1, 200, null, "MaxSpeed", null)]
        [InlineData(1, 3, null, null, "random")]
        public async Task FindVenues_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize, 
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = "/api/Venues"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task FindVenues_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Venues";

            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion

        #region Find Available Venues

        [Theory]
        [InlineData(1, 5, null, "Id", "asc")]
        [InlineData(1, 10, "Wiese", "Address", "desc")]
        [InlineData(1, 200, null, null, null)]
        public async Task FindAvailableVenues_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize, 
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = "/api/Venues/available"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            
            var result = JsonConvert.DeserializeObject<PagedResult<AppVenue>>(
                await response.Content.ReadAsStringAsync());
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "beacause that's what we've requested");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Name", "asc")]
        [InlineData("Capacity", "desc")]
        [InlineData("Capacity", "asc")]
        [InlineData("Address", "desc")]
        public async Task FindAvailableVenues_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues/available"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppVenue>>(content);

            var propertyInfo = typeof(AppVenue).GetProperty(sortBy);
            var comparer = Comparer<AppVenue>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.OK)]
        public async Task FindAvailableVenues_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Venues/available";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);

            var result = JsonConvert.DeserializeObject<PagedResult<AppVenue>>(
                await response.Content.ReadAsStringAsync());

            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
        }

        [Fact]
        public async Task FindAvailableVenues_ShouldNotReturnInactiveVenues()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues/available"
                .SetQueryParam("filter", "Außenanlage"); // Venue is set to inactive

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<PagedResult<AppVenue>>(
                await response.Content.ReadAsStringAsync());

            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Items.Should().BeEmpty("because we filtered for a venue that is set to inactive");
        }

        [Theory]
        [InlineData(0, 5, null, "Id", "asc")]
        [InlineData(1, -10, "Wiese", "Address", "desc")]
        [InlineData(1, 200, null, "MaxSpeed", null)]
        [InlineData(1, 3, null, null, "random")]
        public async Task FindAvailableVenues_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsUser();

            var requestUri = "/api/Venues/available"
                .SetQueryParams(new
                {
                    page,
                    pageSize,
                    filter,
                    sortBy,
                    sortOrder
                });

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        #endregion

        #region Get Venues

        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task GetVenue_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Venues/1";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        [Fact]
        public async Task GetVenue_ShouldReturnInactiveVenues()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues/3"; // Venue is set to inactive

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppVenue>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull();
            result.IsActive.Should().BeFalse("because Venue w/ ID 3 should be set to inactive");
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task GetVenue_WithInvalidId_ShouldReturnBadRequest(int venueId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Venues/{venueId}";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Theory]
        [InlineData(99)]
        [InlineData(1234)]
        public async Task GetVenue_WithUnknownId_ShouldReturnNotFound(int venueId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Venues/{venueId}";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeNull();
        }

        #endregion

        #region Create Venue

        [Fact]
        public async Task CreateVenue_WithValidInputs_ShouldReturnAppVenue()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues";

            var model = new VenueModel
            {
                Name = "Wrigley Field",
                Address = "1060 West Addison Street, Chicago, Illinois",
                Capacity = 41649,
                ColorCode = "#ff2000",
                IsActive = false
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);

            var result = JsonConvert.DeserializeObject<AppVenue>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because it should be the venue we've just created");
            result.Id.Should().BePositive();
            result.Name.Should().Be(model.Name);
            result.Address.Should().Be(model.Address);
            result.Capacity.Should().Be(model.Capacity);
            result.ColorCode.Should().Be(model.ColorCode);
            result.IsActive.Should().Be(model.IsActive);
        }
        [Theory]
        [InlineData(null, "Somwhere in the vast nothingness, of space", 22, "#ffeb22", false)]
        [InlineData("There is no cow level", null, -1, "#d2d2d2", true)]
        [InlineData("Somewhere over the rainbow", null, 12, "rainbow", false)]
        [InlineData("Meh", "I don't know", 33, null, true)]
        public async Task CreateVenueWithInvalidInputs_ShouldReturnBadRequest(string name, string address, int capacity,
            string colorCode, bool isActive)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues";

            var model = new VenueModel
            {
                Name = name,
                Address = address,
                Capacity = capacity,
                ColorCode = colorCode,
                IsActive = isActive
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Fact]
        public async Task CreateVenue_WithDuplicateName_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues";

            var model = new VenueModel
            {
                Name = "Nußriede",
                Address = "Somewhere I don't care",
                Capacity = 555,
                ColorCode = "#ff5501",
                IsActive = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.DuplicateVenueName.Code);
            errorMessage.Message.Should().Be(Messages.Errors.DuplicateVenueName.Message);
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.Created)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Created)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task CreateVenue_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Venues";

            var model = new VenueModel
            {
                Name = $"New Venue {_random.Next(1, 5000)}",
                Address = "Somewhere I don't care",
                Capacity = 555,
                ColorCode = "#ff5501",
                IsActive = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion

        #region Update Venue

        [Fact]
        public async Task UpdateVenue_WithValidInputs_ShouldReturnAppVenue()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var venueId = 4;
            var requestUri = $"/api/Venues/{venueId}";

            var model = new VenueModel
            {
                Name = "Marlins Park",
                Address = "501 Marlins Way, Miami, Florida",
                Capacity = 36742,
                ColorCode = "#0056f3",
                IsActive = false
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppVenue>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because the controller should return the updated venue");
            result.Id.Should().Be(venueId, "The ID shouldn't have changed");
            result.Name.Should().Be(model.Name);
            result.Address.Should().Be(model.Address);
            result.Capacity.Should().Be(model.Capacity);
            result.ColorCode.Should().Be(model.ColorCode);
            result.IsActive.Should().Be(model.IsActive);
        }
        [Theory]
        [InlineData(null, "Somwhere in the vast nothingness, of space", 22, "#ffeb22", false)]
        [InlineData("There is no cow level", null, -1, "#d2d2d2", true)]
        [InlineData("Somewhere over the rainbow", null, 12, "rainbow", false)]
        [InlineData("Meh", "I don't know", 33, null, true)]
        public async Task UpdateVenue_WithInvalidInputs_ShouldReturnBadRequest(string name, string address,
            int capacity, string colorCode, bool isActive)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Venues/4";

            var model = new VenueModel
            {
                Name = name,
                Address = address,
                Capacity = capacity,
                ColorCode = colorCode,
                IsActive = isActive
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task UpdateVenue_WithInvalidId_ShouldReturnBadRequest(int venueId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Venues/{venueId}";

            var model = new VenueModel
            {
                Name = "Marlins Park",
                Address = "501 Marlins Way, Miami, Florida",
                Capacity = 36742,
                ColorCode = "#0056f3",
                IsActive = false
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task UpdateVenue_WithUnknownId_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues/4444";

            var model = new VenueModel
            {
                Name = "Marlins Park",
                Address = "501 Marlins Way, Miami, Florida",
                Capacity = 36742,
                ColorCode = "#0056f3",
                IsActive = false
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
        }
        [Fact]
        public async Task UpdateVenue_WithDuplicateName_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues/4";

            var model = new VenueModel
            {
                Name = "Tannenbergallee", // Name already taken by Venue w/ ID 2
                Address = "501 Marlins Way, Miami, Florida",
                Capacity = 36742,
                ColorCode = "#0056f3",
                IsActive = false
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.DuplicateVenueName.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task UpdateVenue_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var venueId = 4;
            var requestUri = $"/api/Venues/{venueId}";

            var model = new VenueModel
            {
                Name = $"New Venue {_random.Next(1, 5000)}",
                Address = "Somewhere I don't care",
                Capacity = 555,
                ColorCode = "#ff5501",
                IsActive = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion

        #region Delete Venue

        [Fact]
        public async Task DeleteVenue_WithValidId_ShouldReturnOk()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Venues/5";

            // Act
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task DeleteVenue_AsUser_ShouldReturnForbidden()
        {
            // Arrange
            await AuthenticateAsUser();
            var requestUri = "/api/Venues/5";

            // Act 
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }

        [Fact]
        public async Task DeleteVenue_WithUnknownId_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsManager();
            var requestUri = "/api/Venues/54321";

            // Act 
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task DeleteVenue_WithInvalidId_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsManager();
            var requestUri = "/api/Venues/0";

            // Act 
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        [Fact]
        public async Task DeleteVenue_WithEventsInRetention_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsManager();
            var requestUri = "/api/Venues/6";

            // Act 
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.VenueHasEventsInRetention.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        #endregion
    }
}
﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using Respawn;
using System;
using System.IO;


namespace VRT.Test.Fixtures
{
    /// <summary>
    /// A fixture used for resetting the integration test database.
    /// </summary>
    public class DatabaseResetFixture : IDisposable
    {
        #region Fields

        // Checkpoint to reset the integration database
        private readonly Checkpoint _checkpoint = new Checkpoint
        {
            SchemasToInclude = new[]
            {
                "VRT"
            },
            DbAdapter = DbAdapter.Postgres
        };

        #endregion


        /// <summary>
        /// Constructs a new DatabaseResetFixture and resets the database configured through the 'IntegrationSettings.json' file.
        /// </summary>
        public DatabaseResetFixture()
        {
            Configuration = new ConfigurationBuilder()
                    .AddJsonFile("IntegrationSettings.json")
                    .AddEnvironmentVariables()
                    .Build();

            ResetDatabase();
        }


        public IConfiguration Configuration { get; private set; }

        #region Methods

        /// <summary>
        /// Resets and reseeds the database.
        /// </summary>
        public void ResetDatabase()
        {
            string connString = Configuration["DB_CONNECTION_STRING"] ?? Configuration.GetConnectionString("VRT");
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();

                // Reset DB
                _checkpoint.Reset(conn).Wait();

                // Re-seed DB
                var script = File.ReadAllText("Scripts/seed_test_db.sql");
                var cmd = new NpgsqlCommand(script, conn);
                cmd.ExecuteNonQuery();
            }
        }

        public void Dispose() { }

        #endregion
    }
}

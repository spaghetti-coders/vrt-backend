﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VRT.API.Models.Account;
using VRT.Test.Fixtures;
using Xunit;
using Xunit.Abstractions;

namespace VRT.Test
{
    public abstract class IntegrationTest : IClassFixture<ApiWebApplicationFactory>, IClassFixture<DatabaseResetFixture>
    {
        #region Fields

        private LoginResult _adminToken;
        private LoginResult _managerToken;
        private LoginResult _userToken;

        protected readonly DatabaseResetFixture _dbResetFixture;
        protected readonly ApiWebApplicationFactory _factory;
        protected readonly HttpClient _client;

        #endregion


        public IntegrationTest(ApiWebApplicationFactory fixture, DatabaseResetFixture dbResetFixture)
        {
            _factory = fixture;
            _client = _factory.CreateClient();
            _dbResetFixture = dbResetFixture;
        }


        #region Authentication Helpers

        /// <summary>
        /// Ensures that subsequent requests will be unauthenticated.
        /// </summary>
        public void BeAnonymous()
        {
            _client.DefaultRequestHeaders.Authorization = null;
        }
        /// <summary>
        /// Ensures that subsequent requests will be authenticated as user in the 'admin' role.
        /// </summary>
        /// <returns>the user's current access and refresh token.</returns>
        public async Task<LoginResult> AuthenticateAsAdmin()
        {
            // Perform authentication if necessary
            if (_adminToken == null)
            {
                var model = new LoginModel
                {
                    Username = "admin",
                    Password = "passw0rd"
                };

                _adminToken = await Authenticate(model);
            }

            // Set authentication header
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _adminToken.AccessToken);

            return _adminToken;
        }
        /// <summary>
        /// Ensures that subsequent requests will be authenticated as user in the 'manager' role.
        /// </summary>
        /// <returns>the user's current access and refresh token.</returns>
        public async Task<LoginResult> AuthenticateAsManager()
        {
            // Perform authentication if necessary
            if (_managerToken == null)
            {
                var model = new LoginModel
                {
                    Username = "manager",
                    Password = "passw0rd"
                };

                _managerToken = await Authenticate(model);
            }

            // Set authentication header
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _managerToken.AccessToken);

            return _managerToken;
        }
        /// <summary>
        /// Ensures that subsequent requests will be authenticated as user in the 'user' role.
        /// </summary>
        /// <returns>the user's current access and refresh token.</returns>
        public async Task<LoginResult> AuthenticateAsUser()
        {
            // Perform authentication if necessary
            if (_userToken == null)
            {
                var model = new LoginModel
                {
                    Username = "user",
                    Password = "passw0rd"
                };

                _userToken = await Authenticate(model);
            }

            // Set authentication header
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _userToken.AccessToken);

            return _userToken;
        }
        /// <summary>
        /// Ensures that subsequent requests will be authenticated as the user with the specified credentials.
        /// </summary>
        /// <param name="username">The username to submit</param>
        /// <param name="password">The password to submit</param>
        /// <returns>the LoginResult containing the user's access and refresh token.</returns>
        public async Task<LoginResult> Authenticate(string username, string password)
        {
            // Perform authentication
            var model = new LoginModel
            {
                Username = username,
                Password = password
            };

            var loginResult = await Authenticate(model);

            // Set authentication header
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginResult.AccessToken);

            return loginResult;
        }
        /// <summary>
        /// Convenience method for authenticating using the given LoginModel.
        /// </summary>
        /// <param name="model">The credentials to use for authentication</param>
        /// <returns>The LoginResult containing both teh access and refresh token</returns>
        private async Task<LoginResult> Authenticate(LoginModel model)
        {
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("/api/Account/login", content);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<LoginResult>(
                await response.Content.ReadAsStringAsync());
        }

        #endregion
    }
}

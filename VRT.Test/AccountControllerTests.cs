﻿using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.Account;
using VRT.Core.Constants;
using VRT.Test.Fixtures;
using Xunit;
using Xunit.Abstractions;

namespace VRT.Test
{
    [Collection("Collection1")]
    public class AccountControllerTests : IntegrationTest
    {
        public AccountControllerTests(ApiWebApplicationFactory fixture, DatabaseResetFixture dbResetFixture) : base(fixture, dbResetFixture) { }

        #region Registration

        [Fact]
        public async Task Register_WithValidUserDetails_ShouldReturnOk()
        {
            // Arrange
            BeAnonymous();

            var model = new RegistrationModel
            {
                DisplayName = "John Doe",
                Username = "jdoe",
                Password = "passw0rd"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/register", content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Theory]
        [InlineData("jdoe", "passw0rd", "John Doe", null, true)]
        [InlineData("jdoe", "passw0rd", "John Doe", "jdoe.@acme.com", true)]
        [InlineData("", "passw0rd", "John Doe", "jdoe@acme.com", true)]
        [InlineData("jdoe", null, "John Doe", "jdoe@acme.com", true)]
        [InlineData("jdoe", "2short", "John Doe", "jdoe@acme.com", true)]
        public async Task Register_WithInvalidUserDetails_ShouldReturnBadRequest(string username, string password, string displayName, string email, bool receiveNotifications)
        {
            // Arrange
            BeAnonymous();

            var model = new RegistrationModel
            {
                DisplayName = displayName,
                Username = username,
                Password = password,
                Email = email,
                ReceiveNotifications = receiveNotifications
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/register", content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Fact]
        public async Task Register_WithDuplicateUsername_ShouldReturnBadRequest()
        {
            // Arrange
            BeAnonymous();

            var model = new RegistrationModel
            {
                DisplayName = "Ash Ketchum",
                Username = "user",
                Password = "passw0rd",
                Email = null,
                ReceiveNotifications = false,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/register", content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.DuplicateUsername.Code);
        }
        [Fact]
        public async Task Register_WithDuplicateDisplayName_ShouldReturnBadRequest()
        {
            // Arrange
            BeAnonymous();

            var model = new RegistrationModel
            {
                DisplayName = "Joe Smith",
                Username = "newuser",
                Password = "passw0rd",
                Email = null,
                ReceiveNotifications = false,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/register", content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.DuplicateDisplayName.Code);
        }
        [Fact]
        public async Task Register_WithDuplicateEmail_ShouldReturnBadRequest()
        {
            // Arrange
            BeAnonymous();

            var model = new RegistrationModel
            {
                DisplayName = "Ash Ketchum",
                Username = "newuser",
                Password = "passw0rd",
                Email = "user@example.com",
                ReceiveNotifications = false,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/register", content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.DuplicateEmailAddress.Code);
        }

        #endregion

        #region Authentication

        [Fact]
        public async Task Authenticate_WithValidCredentials_ShouldReturnLoginResult()
        {
            // Arrange
            BeAnonymous();

            var model = new LoginModel
            {
                Username = "admin",
                Password = "passw0rd"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/login", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<LoginResult>(
                await response.Content.ReadAsStringAsync());

            result.AccessToken.Should().NotBeNullOrEmpty("because we need the access token to authenticate future requests.");
            result.RefreshToken.Should().NotBeNullOrEmpty("because we need the refresh token to retrieve new access tokens.");
        }
        [Theory]
        [InlineData("admin", "wR0nGp4$$w0rd")]
        [InlineData("unknown", "passw0rd")]
        public async Task Authenticate_WithInvalidCredentials_ShouldReturnUnauthorized(string username, string password)
        {
            // Arrange
            BeAnonymous();

            var model = new LoginModel
            {
                Username = username,
                Password = password
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/login", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.AuthenticationFailed.Code);
        }
        [Theory]
        [InlineData("admin", null)]
        [InlineData(null, "passw0rd")]
        [InlineData("", "")]
        public async Task Authenticate_WithMissingInformation_ShouldReturnBadRequest(string username, string password)
        {
            // Arrange
            BeAnonymous();

            var model = new LoginModel
            {
                Username = username,
                Password = password
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/login", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Fact]
        public async Task RefreshTokens_WithValidRequest_ShouldReturnLoginResult()
        {
            // Arrange
            var tokens = await AuthenticateAsUser();
            BeAnonymous();

            var model = new RefreshTokenRequest
            {
                AccessToken = tokens.AccessToken,
                RefreshToken = tokens.RefreshToken
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/refresh", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<LoginResult>(
                await response.Content.ReadAsStringAsync());

            result.AccessToken.Should().NotBeNullOrEmpty("because we need the access token to authenticate future requests.");
            result.RefreshToken.Should().NotBeNullOrEmpty("because we need the refresh token to retrieve new access tokens.");
        }
        [Fact]
        public async Task RefreshTokens_WithUnknownRefreshToken_ShouldReturnUnauthorized()
        {
            // Arrange
            var tokens = await AuthenticateAsUser();
            BeAnonymous();

            var model = new RefreshTokenRequest
            {
                AccessToken = tokens.AccessToken,
                RefreshToken = "unknown_refresh_token"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/refresh", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.RefreshTokenInvalidOrExpired.Code);
        }
        [Theory]
        [InlineData("not_a_valid_access_token", "")]
        [InlineData("", "not_a_valid_refresh_token")]
        [InlineData(null, null)]
        public async Task RefreshTokens_WithInvalidRequest_ShouldReturnBadRequest(string accessToken, string refreshToken)
        {
            // Arrange
            BeAnonymous();

            var model = new RefreshTokenRequest
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/refresh", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
        [Fact]
        public async Task Deauthenticate_AsValidUser_ShouldReturnOk()
        {
            // Arrange
            await Authenticate("logmeout", "passw0rd");

            // Act
            var response = await _client.PostAsync("/api/Account/logout", null);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Theory]
        [InlineData("GET", "/api/Account")]
        [InlineData("POST", "/api/Account/logout")]
        [InlineData("POST", "/api/Account/delete")]
        [InlineData("PUT", "/api/Account/update")]
        [InlineData("PUT", "/api/Account/update-password")]
        public async Task CallingProtectedResources_WhileAnonymous_ShouldReturnUnauthorized(string method, string requestUri)
        {
            // Arrange
            BeAnonymous();

            var request = new HttpRequestMessage(new HttpMethod(method), requestUri);

            // Act
            var response = await _client.SendAsync(request);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        #endregion

        #region Account Management

        [Fact]
        public async Task RetrieveUser_WhileAuthenticated_ShouldReturnAppUser()
        {
            // Arrange
            await AuthenticateAsUser();

            // Act
            var response = await _client.GetAsync("/api/Account");

            var result = JsonConvert.DeserializeObject<AppUser>(
               await response.Content.ReadAsStringAsync());

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            result.Should().NotBeNull();
            result.Role.Should().Be(Roles.User);
        }
        [Fact]
        public async Task UpdateAccount_WithValidRequest_ShouldReturnUpdatedUser()
        {
            // Arrange
            await Authenticate("changeme", "passw0rd");

            var model = new UpdateAccountModel
            {
                DisplayName = "Fred Flintstone",
                Email = "fred.flintstone@bedrock.gov",
                ReceiveNotifications = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync("/api/Account/update", content);

            var result = JsonConvert.DeserializeObject<AppUser>(
               await response.Content.ReadAsStringAsync());

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            result.ReceiveNotifications.Should().BeTrue();
            result.DisplayName.Should().Be(model.DisplayName);
            result.Email.Should().Be(model.Email);
        }
        [Theory]
        [InlineData("John Doe", null, true)]
        [InlineData("John Doe", "", true)]
        [InlineData("John Doe", "jdoe.@acme.com", true)]
        [InlineData("", "jdoe@acme.com", false)]
        public async Task UpdateAccount_WithInvalidDetails_ShouldReturnBadRequest(string displayName, string email, bool receiveNotifications)
        {
            // Arrange
            await AuthenticateAsUser();

            var model = new UpdateAccountModel
            {
                DisplayName = displayName,
                Email = email,
                ReceiveNotifications = receiveNotifications,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync("/api/Account/update", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Fact]
        public async Task UpdateAccount_WithDuplicateDisplayName_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsUser();

            var model = new UpdateAccountModel
            {
                DisplayName = "Alex Steele",
                Email = null,
                ReceiveNotifications = false,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync("/api/Account/update", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.DuplicateDisplayName.Code);
        }
        [Fact]
        public async Task UpdateAccount_WithDuplicateEmail_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsUser();

            var model = new UpdateAccountModel
            {
                DisplayName = "Updated User",
                Email = "admin@example.com",
                ReceiveNotifications = false,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync("/api/Account/update", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.DuplicateEmailAddress.Code);
        }
        [Fact]
        public async Task UpdatePassword_WithValidInput_ShouldReturnOk()
        {
            // Arrange
            await AuthenticateAsManager();

            var model = new UpdatePasswordModel
            {
                Password = "_p4Ssw0rD!"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync("/api/Account/update-password", content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Theory]
        [InlineData("2short")]
        [InlineData("")]
        [InlineData(null)]
        public async Task UpdatePassword_WithInvalidInput_ShouldReturnBadRequest(string password)
        {
            // Arrange
            await AuthenticateAsUser();

            var model = new UpdatePasswordModel
            {
                Password = password
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            
            // Act
            var response = await _client.PutAsync("/api/Account/update-password", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Fact]
        public async Task DeleteAccount_WithValidUser_ShouldReturnOk()
        {
            // Arrange
            await Authenticate("deleteme", "passw0rd");

            // Act
            var response = await _client.PostAsync("/api/Account/delete", null);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        #endregion

        #region Account Recovery

        [Fact]
        public async Task RequestRecoveryToken_ForValidUser_ShouldReturnOk()
        {
            // Arrange
            BeAnonymous();

            var model = new RecoveryTokenRequest
            {
                Email = "user@example.com"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/recover/request-token", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Fact]
        public async Task RequestRecoveryToken_ForInactiveUser_ShouldReturnBadRequest()
        {
            // Arrange
            BeAnonymous();

            var model = new RecoveryTokenRequest
            {
                Email = "disabled@example.com"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/recover/request-token", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.AccountDisabledOrDeleted.Code);
        }
        [Fact]
        public async Task RequestRecoveryToken_ForUnknownUser_ShouldReturnNotFound()
        {
            // Arrange
            BeAnonymous();

            var model = new RecoveryTokenRequest
            {
                Email = "unknown@example.com"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/recover/request-token", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
        }
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("notAnEmail.address")]
        [InlineData("user.@example.com")]
        public async Task RequestRecoveryToken_WithInvalidInput_ShouldReturnBadRequest(string email)
        {
            // Arrange
            BeAnonymous();

            var model = new RecoveryTokenRequest
            {
                Email = email
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/recover/request-token", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Fact]
        public async Task RecoverAccount_UsingValidToken_ShouldReturnOk()
        {
            // Arrange
            BeAnonymous();

            var model = new PasswordResetModel
            {
                RecoveryToken = "kmbtSal5b0kA",
                Password = "aN3wP4s$w0rd!"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/recover/reset-password", content);

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Theory]
        [InlineData("unknownToken", "passw0rd")]
        [InlineData("kmbtSal5b0kB", "passw0rd")]    // Expired in 1995
        public async Task RecoverAccount_UsingExpiredOrUnknownToken_ShouldReturnBadRequest(string token, string password)
        {
            // Arrange
            BeAnonymous();

            var model = new PasswordResetModel
            {
                RecoveryToken = token,
                Password = password
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/recover/reset-password", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.RecoveryTokenInvalidOrExpired.Code);
        }
        [Theory]
        [InlineData("unknownToken", "")]
        [InlineData("", "passw0rd")]
        [InlineData(null, null)]
        [InlineData("unknownToken", "2short")]
        public async Task RecoverAccount_UsingInvalidData_ShouldReturnBadRequest(string token, string password)
        {
            // Arrange
            BeAnonymous();

            var model = new PasswordResetModel
            {
                RecoveryToken = token,
                Password = password
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/Account/recover/reset-password", content);
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            // Assert 
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }

        #endregion
    }
}

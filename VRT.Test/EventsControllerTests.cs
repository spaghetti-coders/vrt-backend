using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Flurl;
using Newtonsoft.Json;
using VRT.API.Models;
using VRT.API.Models.Events;
using VRT.Core.Constants;
using VRT.Test.Fixtures;
using Xunit;

namespace VRT.Test
{
    [Collection("Collection1")]
    public class EventsControllerTest : IntegrationTest
    {
        public EventsControllerTest(ApiWebApplicationFactory fixture, DatabaseResetFixture dbResetFixture) : base(fixture, dbResetFixture) { }

        
        #region Find Events

        [Theory]
        [InlineData(1, 5, null, "Id", "asc")]
        [InlineData(1, 10, "Montagstraining", "Date", "desc")]
        [InlineData(1, 200, null, null, null)]
        public async Task FindEvents_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(content);
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "because that's what we've requested");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Name", "asc")]
        [InlineData("Date", "desc")]
        [InlineData("Date", "asc")]
        [InlineData("VenueId", "desc")]
        public async Task FindEvents_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(content);

            var propertyInfo = typeof(AppEvent).GetProperty(sortBy);
            var comparer = Comparer<AppEvent>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(0, 5, null, "Id", "asc")]
        [InlineData(1, -10, "Montagstraining", "Address", "desc")]
        [InlineData(1, 200, null, "CombatRating", null)]
        [InlineData(1, 3, null, null, "random")]
        public async Task FindEvents_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("Validation info is missing");
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task FindEvents_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
    
        #endregion
        
        #region Find Available Events
        
        [Theory]
        [InlineData(1, 5, null, "Id", "asc")]
        [InlineData(1, 10, "Mittwochstraining", "Date", "desc")]
        [InlineData(1, 200, null, null, null)]
        public async Task FindAvailableEvents_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/available"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(
                await response.Content.ReadAsStringAsync());
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "because that's what we've requested");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Name", "asc")]
        [InlineData("Date", "desc")]
        [InlineData("Date", "asc")]
        [InlineData("VenueId", "desc")]
        public async Task FindAvailableEvents_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/available"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(content);

            var propertyInfo = typeof(AppEvent).GetProperty(sortBy);
            var comparer = Comparer<AppEvent>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(0, 5, null, "Id", "asc")]
        [InlineData(1, -10, "Montagstraining", "Address", "desc")]
        [InlineData(1, 200, null, "CombatRating", null)]
        [InlineData(1, 3, null, null, "random")]
        public async Task FindAvailableEvents_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/available"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("Validation info is missing");
        }
        [Fact]
        public async Task FindAvailableEvents_ShouldNotReturnPastOrInactiveEvents()
        {
            // Arrange
            await AuthenticateAsUser();
            var requestUri = "/api/Events/available";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(
                await response.Content.ReadAsStringAsync());

            result.Items.Should().NotBeEmpty("because we need to test what this endpoint returned");
            foreach (var item in result.Items)
            {
                item.IsOpenForRegistration.Should().BeTrue("because available events must be open for registration");
                item.Date.Should().BeAfter(DateTime.UtcNow, "because available events must be in the future");
            }
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.OK)]
        public async Task FindAvailableEvents_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events/available";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Find Past Events
        
        [Theory]
        [InlineData(1, 5, null, "VenueId", "asc")]
        [InlineData(1, 10, "Montagstraining", "Date", "desc")]
        [InlineData(1, 200, null, null, null)]
        public async Task FindPastEvents_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/past"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(
                await response.Content.ReadAsStringAsync());
            
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "because that's what we've requested");
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Name", "asc")]
        [InlineData("Date", "desc")]
        [InlineData("Date", "asc")]
        [InlineData("VenueId", "desc")]
        public async Task FindPastEvents_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/past"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(content);

            var propertyInfo = typeof(AppEvent).GetProperty(sortBy);
            var comparer = Comparer<AppEvent>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(0, 5, null, "Id", "asc")]
        [InlineData(1, -10, "Montagstraining", "Address", "desc")]
        [InlineData(1, 200, null, "CombatRating", null)]
        [InlineData(1, 3, null, null, "random")]
        public async Task FindPastEvents_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/past"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("Validation info is missing");
        }
        [Fact]
        public async Task FindPastEvents_ShouldNotReturnFutureEvents()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/past";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(
                await response.Content.ReadAsStringAsync());

            result.Items.Should().NotBeEmpty("because we need to test what this endpoint returned");
            foreach (var item in result.Items)
                item.Date.Should().BeBefore(DateTime.UtcNow, "because past events must not be in the future");
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task FindPastEvents_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events/past";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Find Future Events
        
        [Theory]
        [InlineData(1, 5, null, "VenueId", "asc")]
        [InlineData(1, 10, "Montagstraining", "Date", "desc")]
        [InlineData(1, 200, null, null, null)]
        public async Task FindFutureEvents_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/future"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(
                await response.Content.ReadAsStringAsync());
            
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "because that's what we've requested");
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Name", "asc")]
        [InlineData("Date", "desc")]
        [InlineData("Date", "asc")]
        [InlineData("VenueId", "desc")]
        public async Task FindFutureEvents_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/future"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(content);

            var propertyInfo = typeof(AppEvent).GetProperty(sortBy);
            var comparer = Comparer<AppEvent>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(0, 5, null, "Id", "asc")]
        [InlineData(1, -10, "Montagstraining", "Address", "desc")]
        [InlineData(1, 200, null, "CombatRating", null)]
        [InlineData(1, 3, null, null, "random")]
        public async Task FindFutureEvents_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize,
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/future"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("Validation info is missing");
        }
        [Fact]
        public async Task FindFutureEvents_ShouldNotReturnPastEvents()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/future";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<PagedResult<AppEvent>>(
                await response.Content.ReadAsStringAsync());

            result.Items.Should().NotBeEmpty("because we need to test what this endpoint returned");
            foreach (var item in result.Items)
                item.Date.Should().BeAfter(DateTime.UtcNow, "because future events must not be in the past");
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task FindFutureEvents_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events/past";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Get Event
        
        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        public async Task GetEvent_WithValidId_ShouldReturnAppEvent(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Events/{eventId}";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppEvent>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull();
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task GetEvent_WithInvalidId_ShouldReturnBadRequest(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Events/{eventId}";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Theory]
        [InlineData(99)]
        [InlineData(1234)]
        public async Task GetEvent_WithUnknownId_ShouldReturnNotFound(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Events/{eventId}";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task GetEvent_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events/1";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Create Event
        
        [Fact]
        public async Task CreateEvent_WithValidInputs_ShouldReturnAppEvent()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events";

            var model = new EventModel
            {
                VenueId = 1,
                Name = "Farming Cows",
                Description = "Let's farm cows together!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddMonths(2),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);

            var result = JsonConvert.DeserializeObject<AppEvent>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because it should be the event we've just created");
            result.Id.Should().BePositive();
            result.Name.Should().Be(model.Name);
            result.Description.Should().Be(model.Description);
            result.Capacity.Should().Be(model.Capacity);
            result.Date.Should().Be(model.Date);
            result.IsOpenForRegistration.Should().Be(model.IsOpenForRegistration);
        }
        [Theory]
        [InlineData(null, "Somewhere in the vast nothingness of space", 22, "2028-01-01 12:45:00", false)]
        [InlineData("Farming Cows", null, -1, "2027-01-01 12:45:00", true)]
        [InlineData("Pre-COVID FUN", "Before it all began", 12, "2019-10-30 19:30:00", false)]
        public async Task CreateEvent_WithInvalidInputs_ShouldReturnBadRequest(string name, string description,
            int capacity, string timestamp, bool isOpenForRegistration)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events";

            var model = new EventModel
            {
                VenueId = 2,
                Name = name,
                Description = description,
                Capacity = capacity,
                Date = DateTime.Parse(timestamp).ToUniversalTime(),
                IsOpenForRegistration = isOpenForRegistration
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Fact]
        public async Task CreateEvent_WithUnknownVenueId_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events";

            var model = new EventModel
            {
                VenueId = 9991,
                Name = "Farming Cows",
                Description = "Let's farm cows together!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddMonths(2),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.ReferenceNotFound.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.Created)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Created)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task CreateEvent_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events";

            var model = new EventModel
            {
                VenueId = 1,
                Name = "Farming Cows",
                Description = "Let's farm cows together!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddMonths(2),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Update Event
        
         [Fact]
        public async Task UpdateEvent_WithValidInputs_ShouldReturnAppEvent()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var eventId = 6;
            var requestUri = $"/api/Events/{eventId}";

            var model = new EventModel
            {
                VenueId = 1,
                Name = "Better, Faster, Harder, Scooter",
                Description = "What she said!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddMonths(2),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppEvent>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because the controller should return the updated event");
            result.Id.Should().Be(eventId, "The ID shouldn't have changed");
            result.Name.Should().Be(model.Name);
            result.Description.Should().Be(model.Description);
            result.Capacity.Should().Be(model.Capacity);
            result.Date.Should().Be(model.Date);
            result.IsOpenForRegistration.Should().Be(model.IsOpenForRegistration);
        }
        [Theory]
        [InlineData(null, "Somewhere in the vast nothingness of space", 22, "2028-01-01 12:45:00", false)]
        [InlineData("Farming Cows", null, -1, "2027-01-01 12:45:00", true)]
        [InlineData("Pre-COVID FUN", "Before it all began", 12, "2019-10-30 19:30:00", false)]
        public async Task UpdateEvent_WithInvalidInputs_ShouldReturnBadRequest(string name, string description,
            int capacity, string timestamp, bool isOpenForRegistration)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var eventId = 6;
            var requestUri = $"/api/Events/{eventId}";

            var model = new EventModel
            {
                VenueId = 2,
                Name = name,
                Description = description,
                Capacity = capacity,
                Date = DateTime.Parse(timestamp).ToUniversalTime(),
                IsOpenForRegistration = isOpenForRegistration
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task UpdateEvent_WithInvalidId_ShouldReturnBadRequest(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Events/{eventId}";

            var model = new EventModel
            {
                VenueId = 1,
                Name = "Better, Faster, Harder, Scooter",
                Description = "What she said!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddMonths(2),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task UpdateEvent_WithUnknownVenueId_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/6";

            var model = new EventModel
            {
                VenueId = 404,
                Name = "Better, Faster, Harder, Scooter",
                Description = "What she said!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddMonths(2),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.ReferenceNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task UpdateEvent_WithIdOfPastEvent_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/3";

            var model = new EventModel
            {
                VenueId = 2,
                Name = "Better, Faster, Harder, Scooter",
                Description = "What she said!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddMonths(2),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EventAlreadyStarted.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task UpdateEvent_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events/6";

            var model = new EventModel
            {
                VenueId = 2,
                Name = "Better, Faster, Harder, Scooter",
                Description = "What she said!",
                Capacity = 8,
                Date = DateTime.UtcNow.AddDays(21),
                IsOpenForRegistration = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Delete Event

        [Theory]
        [InlineData("admin", "passw0rd", 7, HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", 8, HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", 6, HttpStatusCode.Forbidden)]
        public async Task DeleteEvent_TestPermissions(string username, string password, int eventId,
            HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/Events/{eventId}";

            // Act
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        [Fact]
        public async Task DeleteEvent_WithUnknownId_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Events/54321";

            // Act 
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task DeleteEvent_WithInvalidId_ShouldReturnBadRequest(int eventId)
        {
            // Arrange
            await AuthenticateAsManager();
            var requestUri = $"/api/Events/{eventId}";

            // Act 
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        [Fact]
        public async Task DeleteEvent_InRetention_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsManager();
            var requestUri = "/api/Events/4";

            // Act 
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.EventIsInRetention.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        
        #endregion
        
        #region Event Logs
        
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task RetrieveEventLog_OfExitingEvent_ShouldReturnLogFile(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Events/{eventId}/log";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.Headers.ContentLength.Should().BePositive();
            response.Content.Headers.ContentDisposition.Should().NotBeNull();
            response.Content.Headers.ContentDisposition?.FileName.Should().NotBeNullOrEmpty();
        }
        [Fact]
        public async Task RetrieveEventLog_WithUnknownId_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Events/999/log";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task RetrieveEventLog_WithInvalidId_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Events/999/log";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData("manager", "passw0rd")]
        [InlineData("user", "passw0rd")]
        public async Task RetrieveEventLog_AsNonAdmin_ShouldReturnForbidden(string username, string password)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Events/1/log";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }
        
        #endregion
    }
}
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Flurl;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using VRT.API.Models;
using VRT.API.Models.Account;
using VRT.API.Models.Users;
using VRT.Core.Constants;
using VRT.Core.Models;
using VRT.Test.Fixtures;
using Xunit;

namespace VRT.Test
{
    [Collection("Collection1")]
    public class UserControllerTests : IntegrationTest
    {
        public UserControllerTests(ApiWebApplicationFactory fixture, DatabaseResetFixture dbResetFixture) : base(fixture, dbResetFixture) { }
        
        
        #region Find Users
        
        [Theory]
        [InlineData(1, 5, null, "Id", "asc", UserStatus.Active)]
        [InlineData(1, 10, "example.com", "DisplayName", "desc", UserStatus.Any)]
        [InlineData(1, 200, null, null, null, null)]
        public async Task FindUsers_WithValidQueryParams_ShouldReturnPagedResults(int page, int pageSize, 
            string filter, string sortBy, string sortOrder, int? status)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = "/api/Users"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder,
                    status = status
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            
            var result = JsonConvert.DeserializeObject<PagedResult<AppUser>>(
                await response.Content.ReadAsStringAsync());
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "because that's what we've requested");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("DisplayName", "asc")]
        [InlineData("Created", "desc")]
        [InlineData("Created", "asc")]
        [InlineData("Role", "desc")]
        public async Task FindUsers_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = "/api/Users"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppUser>>(content);

            var propertyInfo = typeof(AppUser).GetProperty(sortBy);
            var comparer = Comparer<AppUser>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(0, 5, null, "Id", "asc")]
        [InlineData(1, -10, "Destin", "Role", "desc")]
        [InlineData(1, 200, null, "IQ", null)]
        [InlineData(1, 3, null, null, "random")]
        public async Task FindUsers_WithInvalidQueryParams_ShouldReturnBadRequest(int page, int pageSize, 
            string filter, string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = "/api/Users"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            
            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task FindUsers_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Users";

            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion

        #region Get User

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetUser_WithValidId_ShouldReturnAppUser(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/{userId}";
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppUser>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull();
            result.Id.Should().Be(userId, "because that's the user we've requested.");
            result.Username.Should().NotBeNullOrEmpty("because the username is required.");
            result.DisplayName.Should().NotBeNullOrEmpty("because the display name is required.");
            result.Status.Should().BePositive();
            result.Role.Should().ContainAny(new[] {"user", "manager", "admin"}, 
                "because the user must have a valid role");
        }
        [Theory]
        [InlineData(99)]
        [InlineData(404)]
        public async Task GetUser_WithUnknownId_ShouldReturnNotFound(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/{userId}";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task GetUser_WithInvalidId_ShouldReturnBadRequest(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/{userId}";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<MessageResult>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeNull();
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task GetUser_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = "/api/Users/1";

            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion

        #region Update User

        [Fact]
        public async Task UpdateUser_WithValidInputs_ShouldReturnAppUser()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 5;
            var requestUri = $"/api/Users/{userId}";

            var model = new UpdateUserModel()
            {
                DisplayName = "Stephen Fry",
                Role = Roles.Manager,
                Email = "s.fry@example.com",
                ReceiveNotifications = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppUser>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because the controller should return the updated user");
            result.Id.Should().Be(userId, "The ID shouldn't have changed");
            result.DisplayName.Should().Be(model.DisplayName);
            result.Email.Should().Be(model.Email);
            result.ReceiveNotifications.Should().Be(model.ReceiveNotifications);
        }
        [Theory]
        [InlineData(null, "user", "cat.fish@starwars.com", false)]          // Missing display name
        [InlineData("Peter Singer", null, null, false)]                     // Missing role
        [InlineData("Frank Sinatra", "artist", null, false)]                // Invalid role
        [InlineData("Sneeky Pete", "admin", "invalid.@mail.com", false)]    // Invalid email
        [InlineData("Sneeky Pete", "admin", "invalid.email.com", false)]    // Invalid email
        [InlineData("Sneeky Pete", "admin", "@mail.com", false)]            // Invalid email
        [InlineData("Michael Eisener", "manager", null, true)]              // Receive notifications w/o email address
        public async Task UpdateUser_WithInvalidInputs_ShouldReturnBadRequest(string displayName, string role,
            string email, bool receiveNotifications)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/5";

            var model = new UpdateUserModel()
            {
                DisplayName = displayName,
                Role = role,
                Email = email,
                ReceiveNotifications = receiveNotifications
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
        }
        [Fact]
        public async Task UpdateUser_WithDuplicateDisplayName_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/5";

            var model = new UpdateUserModel()
            {
                DisplayName = "Alex Steele",
                Role = Roles.User,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.DuplicateDisplayName.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task UpdateUser_WithDuplicateEmail_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/5";

            var model = new UpdateUserModel()
            {
                DisplayName = "Horst Seehofer",
                Role = Roles.User,
                Email = "user@example.com"
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.DuplicateEmailAddress.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData(-34)]
        [InlineData(0)]
        public async Task UpdateUser_WithInvalidId_ShouldReturnBadRequest(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/{userId}";

            var model = new UpdateUserModel()
            {
                DisplayName = "Micky Mouse",
                Role = Roles.Admin,
                Email = "mr.mouse@disney.com",
                ReceiveNotifications = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task UpdateUser_WithUnknownId_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/404";

            var model = new UpdateUserModel()
            {
                DisplayName = "Horst Seehofer",
                Role = Roles.User,
                Email = "horst@schlemmerladen.de",
                ReceiveNotifications = false
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task UpdateUser_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/Users/5";

            var model = new UpdateUserModel()
            {
                DisplayName = "Deckard Cain",
                Role = Roles.Manager,
                Email = "dcain@tristram.d2",
                ReceiveNotifications = true
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        #endregion
        
        #region Update User Status

        [Theory]
        [InlineData((int)UserStatus.Active)]
        [InlineData((int)UserStatus.Inactive)]
        public async Task UpdateUserStatus_WithValidStatus_ShouldReturnAppUser(int status)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 5;
            var requestUri = $"/api/Users/{userId}/status";

            var model = new UpdateUserStatusModel()
            {
                Status = status
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppUser>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because the controller should return the updated user");
            result.Id.Should().Be(userId, "The ID shouldn't have changed");
            result.Status.Should().Be(model.Status, "because that's the status we set");
        }
        [Theory]
        [InlineData((int)UserStatus.Any)]
        [InlineData((int)UserStatus.PendingDeletion)]
        [InlineData((int)UserStatus.PendingRegistration)]
        [InlineData(-99)]
        public async Task UpdateUserStatus_WithInvalidStatus_ShouldReturnBadRequest(int status)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 5;
            var requestUri = $"/api/Users/{userId}/status";

            var model = new UpdateUserStatusModel()
            {
                Status = status
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData(-62)]
        [InlineData(0)]
        public async Task UpdateUserStatus_WithInvalidUserId_ShouldReturnBadRequest(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/{userId}/status";

            var model = new UpdateUserStatusModel()
            {
                Status = (int)UserStatus.Active
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task UpdateUserStatus_WithUnknownUserId_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 404;
            var requestUri = $"/api/Users/{userId}/status";

            var model = new UpdateUserStatusModel()
            {
                Status = (int)UserStatus.Active
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task UpdateUserStatus_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var userId = 5;
            var requestUri = $"/api/Users/{userId}/status";

            var model = new UpdateUserStatusModel()
            {
                Status = (int)UserStatus.Active
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PutAsync(requestUri, content);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion

        #region Delete User

        [Fact]
        public async Task DeleteUser_WithValidId_ShouldReturnDeletedAppUser()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 9;
            var requestUri = $"/api/Users/{userId}";
            
            // Act
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppUser>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because the deleted user should be returned");
            result.Id.Should().Be(userId, "because that's the user we've deleted");
            result.Status.Should().NotBe((int)UserStatus.PendingDeletion);
            result.DeletionDate.Should().BeNull();
        }
        [Fact]
        public async Task DeleteUser_InRetention_ShouldReturnAppUserWithDeletionDate()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 8;     // User participated in event 4 (in retention)
            var requestUri = $"/api/Users/{userId}";
            
            // Act
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppUser>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because the deleted user should be returned");
            result.Id.Should().Be(userId, "because that's the user we've deleted");
            result.Status.Should().Be((int)UserStatus.PendingDeletion);
            result.DeletionDate.Should().BeAfter(DateTime.UtcNow);
        }
        [Theory]
        [InlineData(-56)]
        [InlineData(0)]
        public async Task DeleteUser_WithInvalidId_ShouldReturnBadRequest(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/{userId}";

            // Act
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task DeleteUser_WithUnknownId_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 404;
            var requestUri = $"/api/Users/{userId}";

            // Act
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        [Theory]
        [InlineData(11, "user", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData(11, "manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData(10, "admin", "passw0rd", HttpStatusCode.OK)]
        public async Task DeleteUser_TestPermissions(int userId, string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/Users/{userId}";

            // Act
            var response = await _client.DeleteAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion

        #region Generate Account Recovery Token

        [Fact]
        public async Task GenerateRecoveryToken_WithValidUser_ShouldReturnRecoveryTokenResult()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 7;
            var requestUri = $"/api/Users/recover/{userId}";

            var tokenLifetime = (int)_factory.Configuration
                .GetSection("Account")
                .GetValue(typeof(int), "RecoveryTokenLifetime");
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<RecoveryTokenResult>(
                await response.Content.ReadAsStringAsync());

            result.RecoveryToken.Should().NotBeEmpty();
            result.ExpiresAt.Should().BeAfter(DateTime.UtcNow);
            result.ExpiresAt.Should().NotBeAfter(DateTime.UtcNow.AddMinutes(tokenLifetime), 
                "Tokens expiration date must not be greater then configured lifetime");
        }
        [Fact]
        public async Task GenerateRecoveryToken_WithInactiveUser_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 4;     // User is disabled
            var requestUri = $"/api/Users/recover/{userId}";

            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.AccountDisabledOrDeleted.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData(-72)]
        [InlineData(0)]
        public async Task GenerateRecoveryToken_WithIvalidUser_ShouldReturnBadRequest(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/Users/recover/{userId}";

            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task GenerateRecoveryToken_WithUnknownUser_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var userId = 404;
            var requestUri = $"/api/Users/recover/{userId}";
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task GenerateRecoveryToken_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/Users/recover/7";
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion
    }
}
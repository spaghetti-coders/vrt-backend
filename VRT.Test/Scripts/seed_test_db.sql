START TRANSACTION;

--
-- Seed Users
--
INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(1, 'admin', 'Alex Steele', 'admin@example.com', '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'admin', false, '2021-01-01 00:00:01', 1);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(2, 'manager', 'Manfred Anger', 'manager@example.com', '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'manager', false, '2021-01-05 08:12:01', 1);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(3, 'user', 'Joe Smith', 'user@example.com', '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '2021-05-28 14:36:12', 1);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(4, 'disabled', 'Destin Abledguy', 'disabled@example.com', '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '2021-08-03 16:21:57', 0);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(5, 'changeme', 'Shane Gemy', null, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '2021-01-02 03:45:28', 1);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(6, 'deleteme', 'Sebastian Simpson', null, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '2021-07-12 11:31:48', 1);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(7, 'logmeout', 'Irmgard Briant', null, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '2022-01-01 00:00:01', 1);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(8, 'cannotdelete', 'Derick Boys', null, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '2021-10-15 17:38:41', 1);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(9, 'safetodelete', 'Richard Pryor', null, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '2022-01-05 13:37:00', 0);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(10, 'safe2delete', 'Julia Child', null, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '1912-08-15 10:46:23', 0);

INSERT INTO "VRT"."Users" ("Id", "Username", "DisplayName", "Email", "Password", "Role", "ReceiveNotifications", "Created", "Status")
VALUES(11, 'sadbro', 'Andrew Bourdain', null, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', 'user', false, '1956-06-25 08:32:56', 0);

-- Update sequences
SELECT pg_catalog.setval(pg_get_serial_sequence('"VRT"."Users"', 'Id'), MAX("Id"))
FROM "VRT"."Users";

--
-- Seed Recovery Tokens
--
INSERT INTO "VRT"."AccountRecoveryTokens" ("UserId", "Token", "ExpiresAt")
VALUES(5, 'kmbtSal5b0kA', '2045-01-01 00:00:01');

INSERT INTO "VRT"."AccountRecoveryTokens" ("UserId","Token", "ExpiresAt")
VALUES(2, 'kmbtSal5b0kB', '1995-01-01 00:00:01');

INSERT INTO "VRT"."AccountRecoveryTokens" ("UserId", "Token", "ExpiresAt")
VALUES(4, 'kmbtSal5b0kC', '2045-01-01 00:00:01');


--
-- Seed Admin Log entries
--
INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(1, 'Administrator', 'Veranstaltung erstellt', null, 'Veranstaltung 1', '2021-08-21 00:00:01');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(2, 'Administrator', 'Veranstaltung erstellt', null, 'Veranstaltung 2', '2021-08-21 00:00:02');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(3, 'Administrator', 'Veranstaltung erstellt', null, 'Veranstaltung 3', '2021-08-21 00:00:03');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(4, 'Manager', 'Veranstaltung bearbeitet', null, 'Veranstaltung 1', '2021-08-21 00:00:04');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(5, 'Manager', 'Veranstaltung bearbeitet', null, 'Veranstaltung 2', '2021-08-21 00:00:05');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(6, 'Manager', 'Veranstaltung bearbeitet', null, 'Veranstaltung 3', '2021-08-21 00:00:06');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(7, 'Administrator', 'Benutzer bearbeitet', null, 'Benutzer 1', '2021-08-21 00:00:07');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(8, 'Administrator', 'Benutzer bearbeitet', null, 'Benutzer 2', '2021-08-21 00:00:08');

INSERT INTO "VRT"."LogEntries" ("Id", "Actor", "Action", "IndirectTarget", "Target", "Timestamp")
VALUES(9, 'Administrator', 'Benutzer bearbeitet', null, 'Benutzer 3', '2021-08-21 00:00:09');

-- Update sequences
SELECT pg_catalog.setval(pg_get_serial_sequence('"VRT"."LogEntries"', 'Id'), MAX("Id"))
FROM "VRT"."LogEntries";


--
-- Seed Venues
--
INSERT INTO "VRT"."Venues" ("Id", "Name", "Address", "Capacity", "IsActive", "ColorCode")
VALUES (1, 'Nußriede', 'Nußriede 4B, 30627 Hannover', 21, TRUE, '#c4a000');

INSERT INTO "VRT"."Venues" ("Id", "Name", "Address", "Capacity", "IsActive", "ColorCode")
VALUES (2, 'Tannenbergallee', 'Tannenbergallee 11, 30163 Hannover', 36, TRUE, '#ffa500');

INSERT INTO "VRT"."Venues" ("Id", "Name", "Address", "Capacity", "IsActive", "ColorCode")
VALUES (3, 'Außenanlage', 'An der breiten Wiese 70, 30625 Hannover', 100, FALSE , '#24ff24');

INSERT INTO "VRT"."Venues" ("Id", "Name", "Address", "Capacity", "IsActive", "ColorCode")
VALUES (4, 'Update Center', 'Weg der Neuerungen 1a, 30171 Hannover', 200, TRUE , '#fffabc');

INSERT INTO "VRT"."Venues" ("Id", "Name", "Address", "Capacity", "IsActive", "ColorCode")
VALUES (5, 'Ihmecourt', 'Ihmeplatz, 30449 Hannover', 200, FALSE , '#000000');

INSERT INTO "VRT"."Venues" ("Id", "Name", "Address", "Capacity", "IsActive", "ColorCode")
VALUES (6, 'Olympiastadion', 'Spiridon-Louis-Ring, 80809 München', 69250, True , '#f0f0f0');

-- Update sequences
SELECT pg_catalog.setval(pg_get_serial_sequence('"VRT"."Venues"', 'Id'), MAX("Id"))
FROM "VRT"."Venues";


--
-- Seed Events
--
INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (1, 2, ('NOW()'::timestamp + '5 year'::interval), 'Mittwochstraining', 'Reguläres Training unter 1G+++ Bedingungen. Bitte kommt pünktlich und im ABC-Anzug!', 24, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (2, 1, '2021-11-01 19:45:00.000000', 'Montagstraining', 'Einlass ab 19:45 Uhr; 2G+ Beschränkung.', 21, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (3, 3, '2017-11-08 19:45:00.000000', 'Montagstraining', 'No one showed up. :-(', 3, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (4, 6, ('NOW()'::timestamp - '5 day'::interval), 'Die Olympischen Spiele', 'Cannot be deleted!', 69250, FALSE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (5, 6, '2163-05-27 19:15:00.000000', 'Coronation of Angela IV', 'All hail the emperor of Terra!', 69250, FALSE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (6, 4, ('NOW()'::timestamp + '5 day'::interval), 'Update me, senpai!', 'Cuttlefish and Asparagus', 2, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (7, 4, '2021-11-01 19:45:00.000000', 'Delete me, for I am unworthy', 'Will be deleted', 2, FALSE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (8, 2, '2020-11-01 19:45:00.000000', 'Make me go away', 'You brought great shame on our house', 13, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (9, 2, ('NOW()'::timestamp + '15 day'::interval), 'Please do not leave me', 'People will unregister', 13, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (10, 1, ('NOW()'::timestamp + '3 day'::interval), 'Go away, I hate you', 'People will unregister', 13, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (11, 1, ('2017-05-05 00:01:00.000000'), 'Fyre Festival 2017', 'Its gonna be soo amazing, you guys!', 5000, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (12, 1, ('2017-05-06 00:01:00.000000'), 'Fyre Festival 2017', 'Its gonna be soo amazing, you guys!', 5000, FALSE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (13, 1, ('NOW()'::timestamp + '5 year'::interval), 'Fyre Festival Revamped', 'This time its for realz!', 5000, TRUE);

INSERT INTO "VRT"."Events" ("Id", "VenueId", "Date", "Name", "Description", "Capacity", "IsOpenForRegistration")
VALUES (14, 1, ('NOW()'::timestamp + '3 hours'::interval), 'Unregistering from me', 'should not change my date!', 20, TRUE);

-- Update sequences
SELECT pg_catalog.setval(pg_get_serial_sequence('"VRT"."Events"', 'Id'), MAX("Id"))
FROM "VRT"."Events";


--
-- Seed Event Registrations
--

-- Event 1
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 1,  1, Now());

-- Event 2
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(1, 2,  1, '2021-10-18 19:23:45.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 2,  1, '2021-10-23 08:43:15.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(3, 2,  2, '2021-11-01 19:23:11.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(4, 2,  2, '2021-10-29 16:12:33.000000');

-- Event 3
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 3,  2, '2017-11-03 08:43:15.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(3, 3,  2, '2017-11-01 15:48:11.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(4, 3,  1, '2017-10-29 16:12:33.000000');

-- Event 4
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(1, 4,  1, '2021-10-18 19:23:45.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 4,  1, '2021-10-23 08:43:15.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(3, 4,  1, '2021-11-01 19:23:11.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(4, 4,  2, '2021-12-06 17:54:23.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(8, 4,  1, '2021-12-16 15:43:29.000000');

-- Event 9 
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(1, 9,  1, '2021-10-18 19:23:45.000000');
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 9,  1, '2021-10-23 08:43:15.000000');

-- Event 10
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(1, 10,  1, 'NOW()'::timestamp);
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 10,  1, 'NOW()'::timestamp);
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(3, 10,  1, 'NOW()'::timestamp);

-- Event 12 (Test: Delete event registration)
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(3, 12,  1, '2016-11-01 15:48:11.000000');

-- Event 13 (Tests: Delete event registrations, Delete event registration permissions)
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(1, 13,  1, 'NOW()'::timestamp);
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 13,  1, 'NOW()'::timestamp);
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(3, 13,  1, 'NOW()'::timestamp);
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(7, 13,  1, 'NOW()'::timestamp);

-- Event 14 (Tests: Unregister should not change event date)
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(1, 14,  1, Now());
INSERT INTO "VRT"."Registrations" ("UserId", "EventId", "Status", "Registered")
VALUES(2, 14,  1, Now());

COMMIT;
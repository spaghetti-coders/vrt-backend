using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Flurl;
using Newtonsoft.Json;
using Respawn.Postgres;
using VRT.API.Models;
using VRT.API.Models.Events;
using VRT.API.Models.Registrations;
using VRT.Core.Constants;
using VRT.Core.Models;
using VRT.Test.Fixtures;
using Xunit;

namespace VRT.Test
{
    [Collection("Collection1")]
    public class EventRegistrationsControllerTests : IntegrationTest
    {
        public EventRegistrationsControllerTests(ApiWebApplicationFactory fixture, DatabaseResetFixture dbResetFixture) : base(fixture, dbResetFixture) { }


        #region Find Registrations by User

        [Theory]
        [InlineData(2, 1, 5, null, "RegistrationDate", "desc", null)]
        [InlineData(3, 1, 5, null, "EventId", "asc", null)]
        [InlineData(1, 1, 20, null, null, null, null)]
        public async Task FindUserEventRegistrations_WithValidQueryparams_ShouldReturnPagedResults(int userId, int page,
            int pageSize, string filter, string sortBy, string sortOrder, RegistrationStatus? status)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/user/{userId}"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder,
                    status = status
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = JsonConvert.DeserializeObject<PagedResult<AppRegistration>>(
                await response.Content.ReadAsStringAsync());
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");

            foreach (var reg in result.Items)
            {
                reg.UserDisplayName.Should().NotBe("n/a", "because the user should be included");
                reg.EventName.Should().NotBe("n/a", "because the event should be included");
                reg.EventDate.Should().NotBe(DateTime.MinValue, "because the event's date should be included.");
                
                if (status.HasValue)
                    reg.Status.Should().Be(status, "Registration status doesn't match requested status");
            }
            
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "because that's what we've requested");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Status", "asc")]
        [InlineData("RegistrationDate", "desc")]
        [InlineData("EventName", "asc")]
        [InlineData("EventDate", "desc")]
        public async Task FindUserEventRegistrations_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = $"/api/EventRegistrations/user/2"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppRegistration>>(content);

            var propertyInfo = typeof(AppRegistration).GetProperty(sortBy);
            var comparer = Comparer<AppRegistration>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(1, 0, 5, null, "Id", "asc", null)]
        [InlineData(2, 1, -10, "Montagstraining", "EventDate", "desc", null)]
        [InlineData(2, 1, 200, null, "CombatRating", null, null)]
        [InlineData(3, 1, 3, null, null, "random", null)]
        [InlineData(1, 1, 3, null, null, null, 88)]
        public async Task FindUserEventRegistrations_WithInvalidQueryParams_ShouldReturnBadRequest(int userId, int page, 
            int pageSize, string filter, string sortBy, string sortOrder, int? status)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/user/{userId}"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder,
                    status = status
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var content = await response.Content.ReadAsStringAsync();
            
            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task FindUserEventRegistrations_WithInvalidUser_ShouldReturnBadRequest(int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/user/{userId}";

            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var content = await response.Content.ReadAsStringAsync();
            
            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Fact]
        public async Task FindUserEventRegistrations_WithUnknownUser_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/user/404";

            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            var content = await response.Content.ReadAsStringAsync();
            
            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task FindUserEventRegistrations_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/EventRegistrations/user/2";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion
        
        #region Find Registrations by Event

        [Theory]
        [InlineData(2, 1, 5, null, "Status", "asc", null)]
        [InlineData(2, 1, 10, "Joe", "EventId", "desc", (int)RegistrationStatus.Registered)]
        [InlineData(3, 1, 10, "Tannenberg", "RegistrationDate", "desc", (int)RegistrationStatus.OnWaitingList)]
        [InlineData(4, 1, 200, null, null, null, null)]
        public async Task FindEventRegistrations_WithValidQueryParams_ShouldReturnPagedResults(int eventId, int page, 
            int pageSize, string filter, string sortBy, string sortOrder, int? status)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/{eventId}"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder,
                    status = status
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = JsonConvert.DeserializeObject<PagedResult<AppRegistration>>(
                await response.Content.ReadAsStringAsync());
            
            result.Items.Should().NotBeNull("because items may be empty, but should never be null");

            foreach (var reg in result.Items)
            {
                reg.UserDisplayName.Should().NotBe("n/a", "because the user should be included");
                reg.EventName.Should().NotBe("n/a", "because the event should be included");
                reg.EventDate.Should().NotBe(DateTime.MinValue, "because the event's date should be included.");
                
                if (status.HasValue)
                    reg.Status.Should().Be((RegistrationStatus)status, "Registration status doesn't match requested status");
            }
            
            result.Metadata.Should().NotBeNull("because it holds information about available log entries");
            result.Metadata.PageSize.Should().Be(pageSize, "because that's what we've requested");
            result.Metadata.RequestParams.Should()
                .NotBeNull("because request parameters should be included in paged responses.");
            result.Metadata.RequestParams.SortBy.Should().NotBeEmpty("Backend should always set a default sortBy");
            result.Metadata.SortProperties.Should().NotBeEmpty("because some sort properties should be available");
        }
        [Theory]
        [InlineData("Status", "asc")]
        [InlineData("RegistrationDate", "desc")]
        [InlineData("RegistrationDate", "asc")]
        [InlineData("UserDisplayName", "desc")]
        public async Task FindEventRegistrations_Sorted_ShouldReturnSortedResults(string sortBy, string sortOrder)
        {
            // Arrange
            await AuthenticateAsAdmin();

            var requestUri = $"/api/EventRegistrations/event/2"
                .SetQueryParams(new
                {
                    sortBy = sortBy,
                    sortOrder = sortOrder
                });
    
            // Act
            var response = await _client.GetAsync(requestUri);
    
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResult<AppRegistration>>(content);

            var propertyInfo = typeof(AppRegistration).GetProperty(sortBy);
            var comparer = Comparer<AppRegistration>.Create(
                (a1, a2) =>
                    ((IComparable) propertyInfo.GetValue(a1)).CompareTo((IComparable) propertyInfo.GetValue(a2)));
    
            if (sortOrder == "asc")
                result.Items.Should().BeInAscendingOrder(comparer);
            else
                result.Items.Should().BeInDescendingOrder(comparer);
        }
        [Theory]
        [InlineData(1, 0, 5, null, "Id", "asc", null)]
        [InlineData(2, 1, -10, "Montagstraining", "Address", "desc", null)]
        [InlineData(3, 1, 200, null, "CombatRating", null, null)]
        [InlineData(4, 1, 3, null, null, "random", null)]
        [InlineData(4, 1, 3, null, null, null, 88)]
        public async Task FindEventRegistrations_WithInvalidQueryParams_ShouldReturnBadRequest(int eventId, int page, 
            int pageSize, string filter, string sortBy, string sortOrder, int? status)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/{eventId}"
                .SetQueryParams(new
                {
                    page = page,
                    pageSize = pageSize,
                    filter = filter,
                    sortBy = sortBy,
                    sortOrder = sortOrder,
                    status = status
                });
            
            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var content = await response.Content.ReadAsStringAsync();
            
            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.QueryIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task FindEventRegistrations_WithInvalidEvent_ShouldReturnBadRequest(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/{eventId}";

            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var content = await response.Content.ReadAsStringAsync();
            
            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Fact]
        public async Task FindEventRegistrations_WithUnknownEvent_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/404";

            // Act
            var response = await _client.GetAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            var content = await response.Content.ReadAsStringAsync();
            
            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());
            
            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData(1, "admin", "passw0rd", HttpStatusCode.OK)]     // 1 = Future event
        [InlineData(3, "admin", "passw0rd", HttpStatusCode.OK)]     // 3 = Past event
        [InlineData(5, "admin", "passw0rd", HttpStatusCode.OK)]     // 5 = Future event, but not open for registrations
        [InlineData(1, "manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData(3, "manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData(5, "manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData(1, "user", "passw0rd", HttpStatusCode.OK)]
        [InlineData(3, "user", "passw0rd", HttpStatusCode.Forbidden)]
        [InlineData(5, "user", "passw0rd", HttpStatusCode.Forbidden)]
        public async Task FindEventRegistrations_TestPermissions(int eventId, string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/EventRegistrations/event/{eventId}";
            
            // Act
            var response = await _client.GetAsync(requestUri);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion

        #region Register

        [Fact]
        public async Task RegisterToEvent_ShouldReturnAppRegistration()
        {
            // Arrange
            await AuthenticateAsUser();
            var eventId = 1;
            var requestUri = $"/api/EventRegistrations/event/{eventId}";
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppRegistration>(
                await response.Content.ReadAsStringAsync());

            result.EventId.Should().Be(eventId);
            result.UserId.Should().BePositive();
            result.UserDisplayName.Should().NotBe("n/a", "because the user should be included");
            result.EventName.Should().NotBe("n/a", "because the event should be included");
            ((int) result.Status).Should().BePositive();
            result.EventDate.Should().NotBe(DateTime.MinValue, "because the event's date should be included.");
            result.RegistrationDate.Should().BeBefore(DateTime.UtcNow);
        }
        [Fact]
        public async Task RegisterToEvent_Twice_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsManager();  // Manager (user 2) is already registered to event 1 
            var requestUri = $"/api/EventRegistrations/event/1";
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.AlreadyRegistered.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Fact]
        public async Task RegisterToEvent_ThatHasAlreadyStarted_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/3";    // Event 3 has already concluded
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.UnRegisterFromStartedEvent.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task RegisterToEvent_WithInvalidEvent_ShouldReturnBadRequest(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/{eventId}";
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Fact]
        public async Task RegisterToEvent_WithUnknownEvent_ShouldReturnNotFound()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/404";
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.OK)]
        public async Task RegisterToEvent_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/EventRegistrations/event/6";
            
            // Act
            var response = await _client.PostAsync(requestUri, null);
            
            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Unregister

        [Fact]
        public async Task UnregisterFromEvent_ShouldReturnOk()
        {
            // Arrange
            await AuthenticateAsManager();  // Manager (user 2) is registered to event 9
            var requestUri = $"/api/EventRegistrations/event/9";    // 9 = Future event, open for registrations
            
            // Act
            var response = await _client.DeleteAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        [Theory]
        [InlineData(404, "admin", "passw0rd")]  // Event doesn't exist
        [InlineData(9, "user", "passw0rd")]     // Event exists, but user (ID=3) is not registered
        public async Task UnregisterFromEvent_WithUnknownUserOrEvent_ShouldReturnNotFound(int eventId, string username, string password)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/EventRegistrations/event/{eventId}";
            
            // Act
            var response = await _client.DeleteAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            
            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task UnregisterFromEvent_WithInvalidEvent_ShouldReturnBadRequest(int eventId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations/event/{eventId}";
            
            // Act
            var response = await _client.DeleteAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.PathIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Fact]
        public async Task UnregisterFromEvent_ThatAlreadyConcluded_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsUser(); // User (ID=3) is registered to event 3
            var requestUri = $"/api/EventRegistrations/event/3";    // Event 3 already concluded
            
            // Act
            var response = await _client.DeleteAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.UnRegisterFromStartedEvent.Code);
            errorMessage.Message.Should().NotBeEmpty("because the message should include validation info");
        }
        [Theory]
        [InlineData("admin", "passw0rd", HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", HttpStatusCode.OK)]
        [InlineData("user", "passw0rd", HttpStatusCode.OK)]
        public async Task UnregisterFromEvent_TestPermissions(string username, string password, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/EventRegistrations/event/10";
            
            // Act
            var response = await _client.DeleteAsync(requestUri);
            
            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }
        
        #endregion
        
        #region Add Registration

        [Fact]
        public async Task AddEventRegistration_ToPastEventWithCapacity_ShouldReturnAppRegistration()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = 2,    // Event is in the past and open for registrations
                UserId = 7      // User is not registered to the event
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppRegistration>(
                await response.Content.ReadAsStringAsync());
            

            result.EventId.Should().Be(model.EventId);
            result.UserId.Should().Be(model.UserId);
            result.UserDisplayName.Should().NotBe("n/a", "because the user should be included");
            result.EventName.Should().NotBe("n/a", "because the event should be included");
            ((int) result.Status).Should().BePositive();
            result.EventDate.Should().NotBe(DateTime.MinValue, "because the event's date should be included.");
            result.RegistrationDate.Should().BeBefore(DateTime.UtcNow);
        }
        [Theory]
        [InlineData(3, 7)]  // 3 = Past event at max capacity
        [InlineData(4, 7)]  // 4 = Past event, closed for registrations
        [InlineData(6, 7)]  // 5 = Future event
        public async Task AddEventRegistration_ToFutureOrClosedEvent_ShouldReturnBadRequest(int eventId, int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.InvalidEventForAdminRegistration.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Fact]
        public async Task AddEventRegistration_WithAlreadyRegisteredUser_ShouldReturnBadRequest()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = 2,    // Past event with capacity and open for registration
                UserId = 3      // User 3 already registered to event 2
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.AlreadyRegistered.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData(2, -44)]  // Invalid User ID
        [InlineData(2, 0)]
        [InlineData(-23, 5)]  // Invalid Event ID
        [InlineData(0, 5)]
        public async Task AddEventRegistration_WithInvalidPostData_ShouldReturnBadRequst(int eventId, int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData(2, 404)]  // Unknown user
        [InlineData(404, 5)]  // Unknown event
        public async Task AddEventRegistration_WithUnknownUserOrEvent_ShouldReturnNotFound(int eventId, int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData("admin", "passw0rd", 11, 1, HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", 11, 2, HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", 11, 3, HttpStatusCode.Forbidden)]
        public async Task AddEventRegistration_TestPermissions(string username, string password, int eventId, int userId,
            HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(requestUri, content);
            
            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion
        
        #region Delete Registration

        [Theory]
        [InlineData(12, 3)]     // Past event, closed
        [InlineData(13, 7)]     // Future event, open
        public async Task DeleteEventRegistration_WithExistingRegistration_ShouldReturnAppRegistration(int eventId, int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var request = new HttpRequestMessage(HttpMethod.Delete, requestUri)
            {
                Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
            };
            
            // Act
            var response = await _client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = JsonConvert.DeserializeObject<AppRegistration>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull("because the deleted event registration should be returned");
            result.EventId.Should().Be(model.EventId, "because eventId must match the request");
            result.UserId.Should().Be(model.UserId, "because userId must match the request");
            result.UserDisplayName.Should().NotBe("n/a", "because the user should be included");
            result.EventName.Should().NotBe("n/a", "because the event should be included");
            result.EventDate.Should().NotBe(DateTime.MinValue, "because the event's date should be included.");
        }
        [Theory]
        [InlineData(2, -44)]  // Invalid User ID
        [InlineData(2, 0)]
        [InlineData(-23, 5)]  // Invalid Event ID
        [InlineData(0, 5)]
        public async Task DeleteEventRegistration_WithInvalidPostData_ShouldReturnBadRequst(int eventId, int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var request = new HttpRequestMessage(HttpMethod.Delete, requestUri)
            {
                Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
            };
            
            // Act
            var response = await _client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.InputIsInvalid.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }
        [Theory]
        [InlineData(2, 404)]  // Unknown user
        [InlineData(404, 5)]  // Unknown event
        public async Task DeleteEventRegistration_WithUnknownUserOrEvent_ShouldReturnNotFound(int eventId, int userId)
        {
            // Arrange
            await AuthenticateAsAdmin();
            var requestUri = $"/api/EventRegistrations";
            
            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var request = new HttpRequestMessage(HttpMethod.Delete, requestUri)
            {
                Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
            };
            
            // Act
            var response = await _client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);

            var errorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                await response.Content.ReadAsStringAsync());

            errorMessage.Should().NotBeNull();
            errorMessage.Code.Should().Be(Messages.Errors.EntityNotFound.Code);
            errorMessage.Message.Should().NotBeEmpty();
        }

        [Theory]
        [InlineData("admin", "passw0rd", 13, 1, HttpStatusCode.OK)]
        [InlineData("manager", "passw0rd", 13, 2, HttpStatusCode.Forbidden)]
        [InlineData("user", "passw0rd", 13, 3, HttpStatusCode.Forbidden)]
        public async Task DeleteEventRegistration_TestPermissions(string username, string password, int eventId,
            int userId, HttpStatusCode expectedStatus)
        {
            // Arrange
            await Authenticate(username, password);
            var requestUri = $"/api/EventRegistrations";

            var model = new EventRegistrationModel
            {
                EventId = eventId,
                UserId = userId
            };

            var request = new HttpRequestMessage(HttpMethod.Delete, requestUri)
            {
                Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
            };

            // Act
            var response = await _client.SendAsync(request);

            // Assert
            response.StatusCode.Should().Be(expectedStatus);
        }

        #endregion

        #region Bugs

        [Fact]
        public async Task DeleteEventRegistration_ShouldNotModifyEventsDate()
        {
            // Arrange
            await AuthenticateAsAdmin();
            var eventId = 14;
            var eventUri = $"/api/Events/{eventId}";
            var registrationUri = $"/api/EventRegistrations/event/{eventId}";

            // Act
            // Retrieve event before user unregistered
            var response = await _client.GetAsync(eventUri);
            var result = JsonConvert.DeserializeObject<AppEvent>(
                await response.Content.ReadAsStringAsync());

            result.Should().NotBeNull();
            var dateBeforeUnregister = result.Date;
            
            // Unregister from event
            await _client.DeleteAsync(registrationUri);
            
            // Retrieve event after user unregistered
            response = await _client.GetAsync(eventUri);
            result = JsonConvert.DeserializeObject<AppEvent>(
                await response.Content.ReadAsStringAsync());
            
            // Assert
            result.Date.Should().BeSameDateAs(dateBeforeUnregister,
                "Users unregistering from an event should never change that event's date!");
        }

        #endregion
    }
}
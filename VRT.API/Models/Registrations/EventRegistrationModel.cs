using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace VRT.API.Models.Registrations
{
    /// <summary>
    /// The model used to for adding or deleting a user's event registration to an event.
    /// </summary>
    public class EventRegistrationModel
    {
        /// <summary>
        /// The event's ID.
        /// </summary>
        [Required]
        [Range(1, Int32.MaxValue)]
        [JsonPropertyName("eventId")]
        public int EventId { get; set; }
        /// <summary>
        /// The user's ID.
        /// </summary>
        [Range(1, Int32.MaxValue)]
        [JsonPropertyName("userId")]
        public int UserId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VRT.Core.Models;
using VRT.Data.Entities;

namespace VRT.API.Models.Registrations
{
    /// <summary>
    /// Represents a user's registration to an event.
    /// </summary>
    public class AppRegistration
    {
        /// <summary>
        /// This map defines the properties by which paged requests returning AppRegistration objects can be sorted by.
        /// The dictionaries keys represent the sortable properties and their corresponding value, the actual value
        /// used in queries. 
        /// </summary>
        public static readonly Dictionary<string, string> SortProperties = new Dictionary<string, string>()
        {
            {"EventId", "EventId"},
            {"UserId", "UserId"},
            {"UserDisplayName", "User.DisplayName"},
            {"EventName", "Event.Name"},
            {"EventDate", "Event.Date"},
            {"RegistrationDate", "Registered"},
            {"Status", "Status"},
        };
        
        /// <summary>
        /// The event's ID.
        /// </summary>
        [JsonPropertyName("eventId")]
        public int EventId { get; set; }
        /// <summary>
        /// The ID of the user that has registered for the event.
        /// </summary>
        [JsonPropertyName("userId")]
        public int UserId { get; set; }
        /// <summary>
        /// The user's display name.
        /// </summary>
        [JsonPropertyName("userDisplayName")]
        public string UserDisplayName { get; set; }
        /// <summary>
        /// The event's name.
        /// </summary>
        [JsonPropertyName("eventName")]
        public string EventName { get; set; }
        /// <summary>
        /// The date and time at which the event will take / has taken place.
        /// </summary>
        [JsonPropertyName("eventDate")]
        public DateTime EventDate { get; set; }
        /// <summary>
        /// The date and time at which the user registered for the event.
        /// </summary>
        [JsonPropertyName("registrationDate")]
        public DateTime RegistrationDate { get; set; }
        /// <summary>
        /// The status of the user's registration.
        /// </summary>
        [JsonPropertyName("status")]
        public RegistrationStatus Status { get; set; }


        /// <summary>
        /// Constructs a new AppRegistration object from the given Registration entity.
        /// </summary>
        /// <param name="registration">The registration to parse</param>
        /// <returns>a new AppRegistration object with the properties from the given Registration entity</returns>
        public static AppRegistration FromEntity(Registration registration)
        {
            return new AppRegistration
            {
                EventId = registration.EventId,
                UserId = registration.UserId,
                UserDisplayName = registration.User?.DisplayName ?? "n/a",
                EventName = registration.Event?.Name ?? "n/a",
                EventDate = registration.Event?.Date ?? DateTime.MinValue,
                RegistrationDate = registration.Registered,
                Status = (RegistrationStatus)registration.Status
            };
        }
    }
}

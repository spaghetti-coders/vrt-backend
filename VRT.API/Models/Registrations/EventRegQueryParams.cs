using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using VRT.Core.Models;

namespace VRT.API.Models.Registrations
{
    /// <summary>
    /// A model for validating the query part of a paged request to an event registration request URI.
    /// </summary>
    public class EventRegQueryParams : PagedQueryParameters
    {
        /// <summary>
        /// Constructs a new EventRegQueryParams object using default values.
        /// </summary>
        public EventRegQueryParams() : base()
        {
            SortBy = "RegistrationDate";
        }
        
        
        /// <summary>
        /// The column/property by which the results will be sorted by.
        /// </summary>
        [DefaultValue("RegistrationDate")]
        [MinLength(2)]
        public new string SortBy { get; set; }
        /// <summary>
        /// A filter applied to the registrations' status.
        /// </summary>
        public RegistrationStatus? Status { get; set; }
    }
}
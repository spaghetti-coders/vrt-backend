using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VRT.API.Models
{
    /// <summary>
    /// A model for validating the query part of a paged resource's request URI.
    /// </summary>
    public class PagedQueryParameters
    {
        /// <summary>
        /// Constructs a new PagedQueryParameters object using default values.
        /// </summary>
        public PagedQueryParameters()
        {
            Page = 1;
            PageSize = 10;
            SortBy = "Id";
            SortOrder = "asc";
        }

        /// <summary>
        /// The page that is being requested.
        /// </summary>
        [DefaultValue(1)]
        [Range(1, Int32.MaxValue)]
        public int Page { get; set; }
        /// <summary>
        /// The maximum number of entries per page.
        /// </summary>
        [DefaultValue(10)]
        [Range(1, Int32.MaxValue)]
        public int PageSize { get; set; }
        /// <summary>
        /// A filter/search string by which the result is being filtered.
        /// </summary>
        [MinLength(3), MaxLength(32)]
        public string Filter { get; set; }
        /// <summary>
        /// The column/property by which the results will be sorted by.
        /// </summary>
        [DefaultValue("Id")]
        [MinLength(2)]
        public string SortBy { get; set; }
        /// <summary>
        /// The direction in which results will be sorted by ('asc' or 'desc')
        /// </summary>
        [DefaultValue("asc")]
        [RegularExpression(@"^(asc)|(desc)$", ErrorMessage = "Sort direction must be 'asc' or 'desc'.")]
        public string SortOrder { get; set; }
    }
}
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VRT.API.Models.AdminLog
{
    /// <summary>
    /// A model for validating the query part of a paged AdminLog requests.
    /// </summary>
    public class AdminLogQueryParams : PagedQueryParameters
    {
        /// <summary>
        /// Constructs a new AdminLogQueryParams object using default values.
        /// </summary>
        public AdminLogQueryParams() : base()
        {
            SortBy = "Timestamp";
            SortOrder = "desc";
        }
        
        
        /// <summary>
        /// The column/property by which the results will be sorted by.
        /// </summary>
        [DefaultValue("Timestamp")]
        [MinLength(2)]
        public new string SortBy { get; set; }
        /// <summary>
        /// The direction in which results will be sorted by ('asc' or 'desc')
        /// </summary>
        [DefaultValue("desc")]
        [RegularExpression(@"^(asc)|(desc)$", ErrorMessage = "Sort direction must be 'asc' or 'desc'.")]
        public new string SortOrder { get; set; }
    }
}
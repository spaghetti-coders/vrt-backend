using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using VRT.Data.Entities;

namespace VRT.API.Models.AdminLog
{
    /// <summary>
    /// Represents an admin log entry that take place at a specific date, time and venue.
    /// </summary>
    public class AppLogEntry
    {
        /// <summary>
        /// This map defines the properties by which paged requests returning AppEvent objects can be sorted by.
        /// The dictionaries keys represent the sortable properties and their corresponding value, the actual value
        /// used in queries. 
        /// </summary>
        public static readonly Dictionary<string, string> SortProperties = new Dictionary<string, string>()
        {
            {"Id", "Id"},
            {"Actor", "Actor"},
            {"Action", "Action"},
            {"IndirectTarget", "IndirectTarget"},
            {"Target", "Target"},
            {"Timestamp", "Timestamp"}
        };
        
        /// <summary>
        /// The ID of the log entry.
        /// </summary>
        [JsonPropertyName("id")]
        public int Id { get; set; }
        /// <summary>
        /// The username of the actor.
        /// </summary>
        [JsonPropertyName("actor")]
        public string Actor { get; set; }
        /// <summary>
        /// The action that was performed by the actor.
        /// </summary>
        [JsonPropertyName("action")]
        public string Action { get; set; }
        /// <summary>
        /// The indirect target of an action, e.g., the name of a user that was removed from an even (the target).
        /// </summary>
        [JsonPropertyName("indirectTarget")]
        public string IndirectTarget { get; set; }
        /// <summary>
        /// The target of an action, e.g., the of an event/user/venue
        /// </summary>
        [JsonPropertyName("target")]
        public string Target { get; set; }
        /// <summary>
        /// The date and time the action was performed.
        /// </summary>
        [JsonPropertyName("timestamp")]
        public DateTime Timestamp { get; set; }


        /// <summary>
        /// Constructs a new AppLogEntry object from the given AdminLogEntry entity.
        /// </summary>
        /// <param name="ale">The log entry to parse</param>
        /// <returns>a new AppLogEntry object with the properties from the given AdminLogEntry entity</returns>
        public static AppLogEntry FromEntity(AdminLogEntry ale)
        {
            return new AppLogEntry
            {
                Id = ale.Id,
                Action = ale.Action,
                Actor = ale.Actor,
                Target = ale.Target,
                IndirectTarget = ale.IndirectTarget,
                Timestamp = ale.Timestamp
            };
        }
    }
}
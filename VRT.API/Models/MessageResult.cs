﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VRT.Core;
using VRT.Core.Constants;
using VRT.Core.Exceptions;

namespace VRT.API.Models
{
    /// <summary>
    /// Represents a message that may be returned by the API as the result of an action.
    /// </summary>
    public class MessageResult
    {
        /// <summary>
        /// A code that uniquely identifies the message.
        /// </summary>
        [JsonPropertyName("code")]
        public string Code { get; set; }
        /// <summary>
        /// The message string.
        /// </summary>
        [JsonPropertyName("message")]
        public string Message { get; set; }
        /// <summary>
        /// The type of this message.
        /// </summary>
        [JsonPropertyName("type")]
        public MessageType Type { get; set; }
    }

    /// <summary>
    /// Defines the different types of message.
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// Represents an informational message.
        /// </summary>
        Information = 0,
        /// <summary>
        /// Represents a warning.
        /// </summary>
        Warning = 1,
        /// <summary>
        /// Represents an error.
        /// </summary>
        Error = 2
    }

    /// <summary>
    /// Defines extensions for MessageResult objects.
    /// </summary>
    public static class MessageResultExtensions
    {
        /// <summary>
        /// Returns this ErrorMessage as <see cref="MessageResult"/> object.
        /// </summary>
        /// <param name="errorMessage">This message</param>
        /// <param name="message">A custom error message to override the default message</param>
        /// <returns>a corresponding MessageResult object</returns>
        public static MessageResult AsMessageResult(this ErrorMessage errorMessage, string message = null)
        {
            // Create new message result object
            var result = new MessageResult
            {
                Code = errorMessage.Code,
                Message = string.IsNullOrEmpty(message) ? errorMessage.Message : message
            };

            // Determine the type of message
            if (result.Code.StartsWith('E'))
                result.Type = MessageType.Error;
            else if (result.Code.StartsWith('W'))
                result.Type = MessageType.Warning;
            else
                result.Type = MessageType.Information;

            // Return MessageResult
            return result;
        }
        /// <summary>
        /// Returns this VRTExcpetions as <see cref="MessageResult"/> object.
        /// </summary>
        /// <param name="exception">This exception</param>
        /// <param name="message">A custom error message to override the default message</param>
        /// <returns>a corresponding MessageResult object</returns>
        public static MessageResult AsMessageResult(this VRTException exception, string message = null)
        {
            // Create new message result object
            var result = new MessageResult
            {
                Code = exception.Code,
                Message = string.IsNullOrEmpty(message) ? exception.Message : message
            };

            // Determine the type of message
            if (result.Code.StartsWith('E'))
                result.Type = MessageType.Error;
            else if (result.Code.StartsWith('W'))
                result.Type = MessageType.Warning;
            else
                result.Type = MessageType.Information;

            // Return MessageResult
            return result;
        }
    }
}

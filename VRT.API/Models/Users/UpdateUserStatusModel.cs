﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Users
{
    /// <summary>
    /// The model used for updating an application user's account status.
    /// </summary>
    public class UpdateUserStatusModel
    {
        /// <summary>
        /// The user's new account status (0 = 'Pending Registration', 1 = 'Active', 2 = 'Inactive', 3 = 'Pending Deletion').
        /// </summary>
        [Required]
        [JsonPropertyName("status")]
        public int Status { get; set; }
    }
}


using System.ComponentModel;
using VRT.Core.Models;

namespace VRT.API.Models.Users
{
    /// <summary>
    /// A model for validating the query part of a paged request to a users request URI.
    /// </summary>
    public class UserQueryParams : PagedQueryParameters
    {
        /// <summary>
        /// Constructs a new UserQueryParams object using default values.
        /// </summary>
        public UserQueryParams() : base()
        {
            Status = UserStatus.Any;
        }

        /// <summary>
        /// A filter applied to the user's status.
        /// </summary>
        [DefaultValue(UserStatus.Any)]
        public UserStatus Status { get; set; }
    }
}
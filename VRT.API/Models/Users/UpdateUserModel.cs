﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.API.Models.Users
{
    /// <summary>
    /// The model used for updating application users (admin).
    /// </summary>
    public class UpdateUserModel
    {
        #region Properties

        /// <summary>
        /// The user's display name.
        /// </summary>
        [Required]
        [MaxLength(64)]
        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }
        /// <summary>
        /// The user's email address.
        /// </summary>
        [JsonPropertyName("email")]
        public string Email { get; set; }
        /// <summary>
        /// The user's role.
        /// </summary>
        [Required]
        [RegularExpression(@"^(user)|(manager)|(admin)$", ErrorMessage = "Role must be 'user', 'manager' or 'admin'.")]
        [JsonPropertyName("role")]
        public string Role { get; set; }
        /// <summary>
        /// Boolean indicating whether the user wants to receive notifications from the application.
        /// </summary>
        [JsonPropertyName("receiveNotifications")]
        public bool ReceiveNotifications { get; set; }

        #endregion
    }
}

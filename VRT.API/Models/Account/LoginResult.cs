﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// The class represents the result of a successful user authentication.
    /// </summary>
    public class LoginResult
    {
        /// <summary>
        /// The access token that was issued to the user.
        /// </summary>
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; set; }
        /// <summary>
        /// The refresh token that was issued to the user.
        /// </summary>
        [JsonPropertyName("refreshToken")]
        public string RefreshToken { get; set; }
    }
}

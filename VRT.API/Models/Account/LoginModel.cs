﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// The model used in authentication requests.
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// The username part of the user's credentials.
        /// </summary>
        [Required]
        [JsonPropertyName("username")]
        public string Username { get; set; }
        /// <summary>
        /// The password part of the user's credentials.
        /// </summary>
        [Required]
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}

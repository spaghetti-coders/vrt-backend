﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// The model used when a new user registers an account in the application.
    /// </summary>
    public class RegistrationModel
    {
        /// <summary>
        /// The user's username.
        /// </summary>
        [Required]
        [MaxLength(32)]
        [JsonPropertyName("username")]
        public string Username { get; set; }
        /// <summary>
        /// The user's password.
        /// </summary>
        [Required]
        [MinLength(8)]
        [JsonPropertyName("password")]
        public string Password { get; set; }
        /// <summary>
        /// The user's display name.
        /// </summary>
        [Required]
        [MaxLength(64)]
        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }
        /// <summary>
        /// The user's email address (optional)
        /// </summary>
        [EmailAddress]
        [JsonPropertyName("email")]
        public string Email { get; set; }
        /// <summary>
        /// Boolean indicating whether the user wants to receive notifications from the application.
        /// </summary>
        [JsonPropertyName("receiveNotifications")]
        public bool ReceiveNotifications { get; set; }
    }
}

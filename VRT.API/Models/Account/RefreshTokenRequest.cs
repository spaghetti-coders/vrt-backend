﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// This class represents the post data when refreshing a JWT token.
    /// </summary>
    public class RefreshTokenRequest
    {
        /// <summary>
        /// The last access token that was issued to the user.
        /// </summary>
        [Required]
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; set; }
        /// <summary>
        /// The refresh token to use.
        /// </summary>
        [Required]
        [JsonPropertyName("refreshToken")]
        public string RefreshToken { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// The model used for updating a user's account.
    /// </summary>
    public class UpdateAccountModel
    {
        /// <summary>
        /// The user's display name.
        /// </summary>
        [Required]
        [MaxLength(64)]
        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }
        /// <summary>
        /// The user's email address (optional)
        /// </summary>
        [EmailAddress]
        [JsonPropertyName("email")]
        public string Email { get; set; }
        /// <summary>
        /// Boolean indicating whether the user wants to receive notifications from the application.
        /// </summary>
        [JsonPropertyName("receiveNotifications")]
        public bool ReceiveNotifications { get; set; }
    }
}

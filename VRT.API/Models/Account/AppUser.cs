﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// An application user.
    /// </summary>
    public class AppUser
    {
        /// <summary>
        /// This map defines the properties by which paged requests returning AppUser objects can be sorted by.
        /// The dictionaries keys represent the sortable properties and their corresponding value, the actual value
        /// used in queries. 
        /// </summary>
        public static readonly Dictionary<string, string> SortProperties = new Dictionary<string, string>()
        {
            {"Id", "Id"},
            {"Username", "Username"},
            {"DisplayName", "DisplayName"},
            {"Email", "Email"},
            {"ReceiveNotifications", "ReceiveNotifications"},
            {"Role", "Role"},
            {"Created", "Created"},
            {"DeletionDate", "DeletionDate"},
            {"Status", "Status"}
        };
        
        #region Properties

        /// <summary>
        /// The user's ID.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The user's username.
        /// </summary>
        [Required]
        [JsonPropertyName("username")]
        public string Username { get; set; }
        /// <summary>
        /// The user's display name.
        /// </summary>
        [Required]
        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }
        /// <summary>
        /// The user's email address.
        /// </summary>
        [JsonPropertyName("email")]
        public string Email { get; set; }
        /// <summary>
        /// Boolean indicating whether the user wants to receive notifications from the application.
        /// </summary>
        [JsonPropertyName("receiveNotifications")]
        public bool ReceiveNotifications { get; set; }
        /// <summary>
        /// The user's role.
        /// </summary>
        [JsonPropertyName("role")]
        public string Role { get; set; }
        /// <summary>
        /// The date and time when this user was created.
        /// </summary>
        [JsonPropertyName("created")]
        public DateTime Created { get; set; }
        /// <summary>
        /// The date and time when the user's account is scheduled for deletion.
        /// </summary>
        [JsonPropertyName("deletionDate")]
        public DateTime? DeletionDate { get; set; }
        /// <summary>
        /// The user's account status (0 = 'Pending Registration', 1 = 'Active', 2 = 'Inactive', 3 = 'Pending Deletion').
        /// </summary>
        [JsonPropertyName("status")]
        public int Status { get; set; }

        #endregion

        /// <summary>
        /// Convenience method for creating an AppUser object from the given <see cref="User"/> entity.
        /// </summary>
        /// <param name="user">The User entity whose values to copy</param>
        /// <returns>an AppUser object with the values from the given User entitiy.</returns>
        public static AppUser FromUser(User user)
        {
            return new AppUser
            {
                Id = user.Id,
                Username = user.Username,
                DisplayName = user.DisplayName,
                Email = user.Email,
                Role = user.Role,
                ReceiveNotifications = user.ReceiveNotifications,
                Created = user.Created,
                DeletionDate = user.DeletionDate,
                Status = user.Status
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// The model used for updating a user's password.
    /// </summary>
    public class UpdatePasswordModel
    {
        /// <summary>
        /// The new password value.
        /// </summary>
        [Required]
        [MinLength(8)]
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}

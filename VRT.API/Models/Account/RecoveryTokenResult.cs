﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// Represents the result of a user's request for an account recovery token.
    /// </summary>
    public class RecoveryTokenResult
    {
        /// <summary>
        /// The account recovery token that was issued to the user.
        /// </summary>
        [JsonPropertyName("recoveryToken")]
        public string RecoveryToken { get; set; }
        /// <summary>
        /// The expiration date and time of the token.
        /// </summary>
        [JsonPropertyName("expires")]
        public DateTime ExpiresAt { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Account
{
    /// <summary>
    /// The model used when resetting a user's password with an account recovery token.
    /// </summary>
    public class PasswordResetModel
    {
        /// <summary>
        /// The account recovery token to use.
        /// </summary>
        [Required]
        [StringLength(12)]
        [JsonPropertyName("recoveryToken")]
        public string RecoveryToken { get; set; }
        /// <summary>
        /// The user's new password.
        /// </summary>
        [Required]
        [MinLength(8)]
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VRT.API.Models.Events
{
    /// <summary>
    /// A model for validating the query part of a paged request to an event registration request URI.
    /// </summary>
    public class PastEventsQueryParams : EventsQueryParams
    {
        /// <summary>
        /// Constructs a new PastEventsQueryParams object using default values.
        /// </summary>
        public PastEventsQueryParams() : base()
        {
            SortOrder = "desc";
        }
        
        
        /// <summary>
        /// The direction in which results will be sorted by ('asc' or 'desc')
        /// </summary>
        [DefaultValue("desc")]
        [RegularExpression(@"^(asc)|(desc)$", ErrorMessage = "Sort direction must be 'asc' or 'desc'.")]
        public new string SortOrder { get; set; }
    }
}
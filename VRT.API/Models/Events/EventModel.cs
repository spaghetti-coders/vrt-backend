﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Events
{
    /// <summary>
    /// Model used for creating and updating events.
    /// </summary>
    public class EventModel
    {
        /// <summary>
        /// The ID of the venue where the event is being hosted at.
        /// </summary>
        [Required]
        [Range(1, Int32.MaxValue)]
        [JsonPropertyName("venueId")]
        public int VenueId { get; set; }
        /// <summary>
        /// The date and time this event is scheduled to be held at.
        /// </summary>
        [Required]
        [JsonPropertyName("date")]
        public DateTime Date { get; set; }
        /// <summary>
        /// The event's name (e.g., 'Fun and Games 2021').
        /// </summary>
        [Required]
        [StringLength(128)]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        /// <summary>
        /// An optional, short description about this event (e.g., 'Bring your own snacks!'). 
        /// </summary>
        [StringLength(256)]
        [JsonPropertyName("description")]
        public string Description { get; set; }
        /// <summary>
        /// The maximum number of participants that can attend the event. 
        /// </summary>
        [Required]
        [JsonPropertyName("capacity")]
        public int Capacity { get; set; }
        /// <summary>
        /// Indicates whether users can register to take part in this event.
        /// </summary>
        [Required]
        [JsonPropertyName("isOpenForRegistration")]
        public bool IsOpenForRegistration { get; set; }
    }
}

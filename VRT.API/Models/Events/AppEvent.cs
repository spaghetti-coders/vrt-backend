﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VRT.Core.Models;
using VRT.Data.Entities;

namespace VRT.API.Models.Events
{
    /// <summary>
    /// Represents an event that take place at a specific date, time and venue.
    /// </summary>
    public class AppEvent
    {
        /// <summary>
        /// This map defines the properties by which paged requests returning AppEvent objects can be sorted by.
        /// The dictionaries keys represent the sortable properties and their corresponding value, the actual value
        /// used in queries. 
        /// </summary>
        public static readonly Dictionary<string, string> SortProperties = new Dictionary<string, string>()
        {
            {"Id", "Id"},
            {"VenueId", "VenueId"},
            {"Date", "Date"},
            {"Name", "Name"},
            {"Description", "Name"},
        };

        /// <summary>
        /// The event's ID.
        /// </summary>
        [JsonPropertyName("id")]
        public int Id { get; set; }
        /// <summary>
        /// The ID of the venue where the event is being hosted at.
        /// </summary>
        [JsonPropertyName("venueId")]
        public int VenueId { get; set; }
        /// <summary>
        /// The date and time this event is scheduled to be held at.
        /// </summary>
        [Required]
        [JsonPropertyName("date")]
        public DateTime Date { get; set; }
        /// <summary>
        /// The event's name (e.g., 'Fun and Games 2021').
        /// </summary>
        [Required]
        [StringLength(128)]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        /// <summary>
        /// An optional, short description about this event (e.g., 'Bring your own snacks!'). 
        /// </summary>
        [StringLength(256)]
        [JsonPropertyName("description")]
        public string Description { get; set; }
        /// <summary>
        /// The maximum number of participants that can attend the event. 
        /// </summary>
        [JsonPropertyName("capacity")]
        public int Capacity { get; set; }
        /// <summary>
        /// The total number of users that are already registered for this event.
        /// </summary>
        [JsonPropertyName("registrationCount")]
        public int RegistrationCount { get; set; }
        /// <summary>
        /// The total number of users that are currently on the event's wating list.
        /// </summary>
        [JsonPropertyName("waitingListCount")]
        public int WaitingListCount { get; set; }
        /// <summary>
        /// Indicates whether users can register to take part in this event.
        /// </summary>
        [JsonPropertyName("isOpenForRegistration")]
        public bool IsOpenForRegistration { get; set; }

        /// <summary>
        /// The name of the venue at which this event is being hosted.
        /// </summary>
        [JsonPropertyName("venueName")]
        public string VenueName { get; set; }
        /// <summary>
        /// The address of the venue at which this event is being hosted.
        /// </summary>
        [JsonPropertyName("venueAddress")]
        public string VenueAddress { get; set; }
        
        /// <summary>
        /// The current user's registration status for this event.
        /// </summary>
        public RegistrationStatus RegistrationStatus { get; set; }


        /// <summary>
        /// Constructs a new AppEvent object from the given Event entity.
        /// </summary>
        /// <param name="evt">The event to parse</param>
        /// <returns>a new AppEvent object with the properties from the given Event entity</returns>
        public static AppEvent FromEntity(Event evt)
        {
            var registration = evt.Registrations?.FirstOrDefault();

            return new AppEvent
            {
                Id = evt.Id,
                VenueId = evt.VenueId,
                Name = evt.Name,
                Description = evt.Description,
                Date = evt.Date,
                Capacity = evt.Capacity,
                RegistrationCount = evt.RegistrationCount,
                WaitingListCount = evt.WaitingListCount,
                IsOpenForRegistration = evt.IsOpenForRegistration,
                VenueName = evt.Venue?.Name ?? "n/a",
                VenueAddress = evt.Venue?.Address ?? "n/a",
                RegistrationStatus = registration != null ? (RegistrationStatus)registration.Status : RegistrationStatus.NotRegistered
            };                
        }
    }
}

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VRT.API.Models.Events
{
    /// <summary>
    /// A model for validating the query part of a paged request to an event registration request URI.
    /// </summary>
    public class EventsQueryParams : PagedQueryParameters
    {
        /// <summary>
        /// Constructs a new EventsQueryParams object using default values.
        /// </summary>
        public EventsQueryParams() : base()
        {
            SortBy = "Date";
        }
        
        
        /// <summary>
        /// The column/property by which the results will be sorted by.
        /// </summary>
        [DefaultValue("Date")]
        [MinLength(2)]
        public new string SortBy { get; set; }
    }
}
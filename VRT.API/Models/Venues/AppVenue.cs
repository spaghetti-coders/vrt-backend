﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.API.Models.Venues
{
    /// <summary>
    /// Represents a venue in the VRT application.
    /// </summary>
    public class AppVenue : VenueModel
    {
        /// <summary>
        /// This map defines the properties by which paged requests returning AppVenue objects can be sorted by.
        /// The dictionaries keys represent the sortable properties and their corresponding value, the actual value
        /// used in queries. 
        /// </summary>
        public static readonly Dictionary<string, string> SortProperties = new Dictionary<string, string>()
        {
            {"Id", "Id"},
            {"Name", "Name"},
            {"Address", "Address"},
            {"Capacity", "Capacity"},
            {"ColorCode", "ColorCode"},
            {"IsActive", "IsActive"}
        };
        
        /// <summary>
        /// The ID of the venue.
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Creates a VenueModel object from the given Venue object.
        /// </summary>
        /// <param name="venue">The Venue object whose values to copy</param>
        /// <returns>a VenueModel with the values from the provided Venue object</returns>
        public static AppVenue FromEntity(Venue venue)
        {
            return new AppVenue
            {
                Id = venue.Id,
                Name = venue.Name,
                Address = venue.Address,
                Capacity = venue.Capacity,
                ColorCode = venue.ColorCode,
                IsActive = venue.IsActive
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.API.Models.Venues
{
    /// <summary>
    /// The model used for creating or updating venues.
    /// </summary>
    public class VenueModel
    {
        /// <summary>
        /// The venue's name (e.g., 'Sports Gym 2, Hannover').
        /// </summary>
        [Required]
        [MaxLength(64)]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        /// <summary>
        /// The address of the venue (e.g., 'Tannenbergallee 11, 30163 Hannover').
        /// </summary>
        [Required]
        [MaxLength(255)]
        [JsonPropertyName("address")]
        public string Address { get; set; }
        /// <summary>
        /// The venue's capacity in terms of participants/people it can hold.
        /// </summary>
        [Required]
        [JsonPropertyName("capacity")]
        public int Capacity { get; set; }
        /// <summary>
        /// The hex-code (e.g., '#ff8500') used to color-code events at this venue in.
        /// </summary>
        [Required]
        [StringLength(7)]
        [JsonPropertyName("colorCode")]
        public string ColorCode { get; set; }
        /// <summary>
        /// Indicates whether events or sessions can be scheduled at this venue.
        /// </summary>
        [Required]
        [JsonPropertyName("isActive")]
        public bool IsActive { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Notifications
{
    /// <summary>
    /// The model used to subscribe a user to receive push notificiations.
    /// </summary>
    public class PushSubscriptionModel
    {
        /// <summary>
        /// The endpoint to which push notifications for this user must be sent.
        /// </summary>
        [Required]
        [JsonPropertyName("endpoint")]
        public string Endpoint { get; set; }
        /// <summary>
        /// The subscription's public key (P-256 ECDH Diffie-Hellman).
        /// </summary>
        [Required]
        [JsonPropertyName("p256dh")]
        public string P256DH { get; set; }
        /// <summary>
        /// The subscription's authentication secret.
        /// </summary>
        [Required]
        [JsonPropertyName("auth")]
        public string Auth { get; set; }
    }
}

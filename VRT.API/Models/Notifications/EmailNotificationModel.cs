﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Notifications
{
    /// <summary>
    /// The model used to send push notifications to users.
    /// </summary>
    public class EmailNotificationModel
    {
        /// <summary>
        /// A list of recipient email addresses.
        /// </summary>
        [Required]
        [JsonPropertyName("recipients")]
        public List<string> Recipients { get; set; }
        /// <summary>
        /// The notification message's subject.
        /// </summary>
        [Required]
        [MaxLength(78)]
        [JsonPropertyName("subject")]
        public string Subject { get; set; }
        /// <summary>
        /// The notification message's body.
        /// </summary>
        [Required]
        [JsonPropertyName("body")]
        public string Body { get; set; }
    }
}

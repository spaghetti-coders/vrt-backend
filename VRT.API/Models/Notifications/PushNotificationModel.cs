﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace VRT.API.Models.Notifications
{
    /// <summary>
    /// The model used to send push notifications to users.
    /// </summary>
    public class PushNotificationModel
    {
        /// <summary>
        /// The ID of the user that is receiving the notification.
        /// Do not specify if you want to send a notification to all users.
        /// </summary>
        [JsonPropertyName("userId")]
        public int? UserId { get; set; }
        /// <summary>
        /// The notification's title (max. 96 characters).
        /// </summary>
        [Required]
        [StringLength(96)]
        [JsonPropertyName("title")]
        public string Title { get; set; }
        /// <summary>
        /// The message to be sent (max. 255 characters).
        /// </summary>
        [Required]
        [StringLength(255)]
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}

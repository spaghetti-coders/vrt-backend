﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.Macs;

namespace VRT.API.Models
{
    /// <summary>
    /// Represents a paged result of an API query.
    /// </summary>
    /// <typeparam name="T">The type of objects contained in this result</typeparam>
    public class PagedResult<T>
    {
        /// <summary>
        /// Constructs a new PagedResult object using default values.
        /// </summary>
        public PagedResult()
        {
            Metadata = new Metadata();
            Items = new List<T>();
        }
        /// <summary>
        /// Constructs a new PagedResult object.
        /// </summary>
        /// <param name="items">The items to be included in the result</param>
        /// <param name="count">The maximum number of items that the query returned</param>
        /// <param name="queryParams">The query parameters of the original request</param>
        /// <param name="sortProperties">The list of properties by which results may be sorted by</param>
        public PagedResult(List<T> items, int count, PagedQueryParameters queryParams, IEnumerable<string> sortProperties = null)
        {
            Metadata = new Metadata(count, queryParams, sortProperties);
            Items = items;
        }


        #region Properties

        /// <summary>
        /// The items contained in this result.
        /// </summary>
        [JsonPropertyName("items")]
        public IEnumerable<T> Items { get; set; }
        /// <summary>
        /// The metadata object describing this result.
        /// </summary>
        [JsonPropertyName("metadata")]
        public Metadata Metadata { get; set; }

        #endregion
    }

    /// <summary>
    /// A metadata object used for describing the paged results of an API query.
    /// </summary>
    public class Metadata
    {
        /// <summary>
        /// Constructs a new MetaData object using default values.
        /// </summary>
        public Metadata()
        {
            TotalCount = 0;
            SortProperties = new List<string>();
            RequestParams = null;
            CurrentPage = 0;
            PageSize = 10;
            TotalPages = 0;
        }
        /// <summary>
        /// Constructs a new MetaData object.
        /// </summary>
        /// <param name="count">The total number of results</param>
        /// <param name="queryParams">The query parameters of the original request</param>
        /// <param name="sortProperties">The list of properties by which results may be sorted by</param>
        public Metadata(int count, PagedQueryParameters queryParams, IEnumerable<string> sortProperties = null)
        {
            TotalCount = count;
            SortProperties = sortProperties ?? new List<string>();
            RequestParams = queryParams;
            
            CurrentPage = queryParams.Page;
            PageSize = queryParams.PageSize;
            TotalPages = (int)Math.Ceiling(count / (double)queryParams.PageSize);
        }


        #region Properties

        /// <summary>
        /// A list of properties by which results may be sorted by.
        /// </summary>
        [JsonPropertyName("sortProperties")]
        public IEnumerable<string> SortProperties { get; set; }
        /// <summary>
        /// The original request parameters.
        /// </summary>
        [JsonPropertyName("requestParams")]
        public PagedQueryParameters RequestParams { get; set; }
        /// <summary>
        /// The current page's number.
        /// </summary>
        [JsonPropertyName("currentPage")]
        public int CurrentPage { get; set; }
        /// <summary>
        /// The total number of available pages.
        /// </summary>
        [JsonPropertyName("totalPages")]
        public int TotalPages { get; set; }
        /// <summary>
        /// The page's maximum number of results.
        /// </summary>
        [JsonPropertyName("pageSize")]
        public int PageSize { get; set; }
        /// <summary>
        /// The total number of results.
        /// </summary>
        [JsonPropertyName("totalCount")]
        public int TotalCount { get; set; }
        /// <summary>
        /// Indicates whether there are previous result pages.
        /// </summary>
        [JsonPropertyName("hasPrevious")]
        public bool HasPrevious => CurrentPage > 1;
        /// <summary>
        /// Indicates whether there are further result pages.
        /// </summary>
        [JsonPropertyName("hasNext")]
        public bool HasNext => CurrentPage < TotalPages;

        #endregion
    }
}

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VRT.API
{
    /// <summary>
    /// This class is responsible for bootstrapping the web application.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The web application's main entry point.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            NLog.Logger logger = NLogBuilder.ConfigureNLog("Settings/nlog.config").GetCurrentClassLogger();

            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Application stopped due to unhandled exception.");
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        /// <summary>
        /// Creates and configures the IHostBuilder used for bootstrapping the web application.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>a pre-configured IHostBuilder instance.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.ConfigureLogging(logging =>
                    {
                        logging.ClearProviders();
                        logging.SetMinimumLevel(LogLevel.Trace);
                    });
                    webBuilder.UseNLog();
                })
                .ConfigureAppConfiguration(config => 
                {
                    config.AddJsonFile("Settings/appsettings.json");
                    config.AddJsonFile("Settings/appsettings.Development.json", true);
                    config.AddEnvironmentVariables();
                });
    }
}

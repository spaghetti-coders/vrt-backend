﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.Core;
using VRT.Core.Constants;
using VRT.Core.Exceptions;

namespace VRT.API.Middleware
{
    /// <summary>
    /// A custom middleware for handling exceptions and returning corresponding results.
    /// </summary>
    public class ExceptionHandlingMiddleware
    {
        #region Fields

        private readonly ILogger<ExceptionHandlingMiddleware> _logger;
        private readonly RequestDelegate _next;

        #endregion


        /// <summary>
        /// Constructs a new ExceptionHandlingMiddleware object.
        /// <paramref name="next">The next handler in the request chain</paramref>
        /// <paramref name="logger">The logger to use</paramref>
        /// </summary>
        public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }


        #region Methods

        /// <summary>
        /// Invokes the next handler in the request chain
        /// </summary>
        /// <param name="context">The request's context</param>
        /// <returns>A task to be awaited</returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {

                await HandleExceptionAsync(context, ex);
            }
        }
        /// <summary>
        /// Handles any previously unhandled exceptions by returning known status codes for them.
        /// </summary>
        /// <param name="httpContext">The current request's HTTP context</param>
        /// <param name="ex">The exception that occurred</param>
        /// <returns>a task to be awaited</returns>
        private Task HandleExceptionAsync(HttpContext httpContext, Exception ex)
        {

            MessageResult msgResult = null;
            HttpStatusCode statusCode;

            switch (ex)
            {
                case EntityNotFoundException appEx:
                    statusCode = HttpStatusCode.NotFound;
                    msgResult = appEx.AsMessageResult();
                    break;

                case DuplicateValueException appEx:
                    statusCode = HttpStatusCode.BadRequest;
                    msgResult = appEx.AsMessageResult();
                    break;

                case ValidationException appEx:
                    statusCode = HttpStatusCode.BadRequest;
                    msgResult = appEx.AsMessageResult();
                    break;

                case AccountServiceException appEx:
                    statusCode = HttpStatusCode.Unauthorized;
                    msgResult = appEx.AsMessageResult();
                    break;

                default:
                    _logger.LogError(ex, "An unexpected error occurred.");

                    statusCode = HttpStatusCode.InternalServerError;
                    msgResult = Messages.Errors.UnexpectedError.AsMessageResult();
                    break;
            }

            string result = JsonConvert.SerializeObject(msgResult);

            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)statusCode;

            return httpContext.Response.WriteAsync(result);
        }

        #endregion
    }
}

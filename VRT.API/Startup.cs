using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using VRT.API.Extensions;
using VRT.API.Extensions.Swagger;
using VRT.API.Middleware;
using VRT.Core.Configurations;
using VRT.Core.HostedServices;
using VRT.Core.HostedServices.Notifications;
using VRT.Core.Services;
using VRT.Data;
using VRT.Mail.Services;

namespace VRT.API
{
    /// <summary>
    /// Configures the VRT API.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Constructs a new Startup object.
        /// </summary>
        /// <param name="configuration">The application's configuration</param>
        /// <param name="environment">The hosting environment for the application</param>
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            HostingEnvironment = environment;
        }

        #region Properties

        /// <summary>
        /// The application's configuration.
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// The application's hosting environment.
        /// </summary>
        public IWebHostEnvironment HostingEnvironment { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Configures the various services that are available to the application.
        /// </summary>
        /// <param name="services">The application's IServiceCollection reference</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add JSON support
            services.AddControllers()
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    opt.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                    opt.SerializerSettings.Formatting = Formatting.None;
                    opt.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
                    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });

            // Add db context for DI
            services.AddDbContext<VRTContext>(options =>
            {
                string connString = Configuration["DB_CONNECTION_STRING"] ?? Configuration.GetConnectionString("VRT");

                options.UseNpgsql(connString);
                //options.EnableSensitiveDataLogging();
            });


            // Disable automatic model validation
            services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

            // Add CORS support & configuration
            var allowedOrigins = Configuration
                .GetSection("AllowedHosts")
                .GetChildren()
                .Select(x => x.Value)
                .ToArray();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    corsPolicyBuilder => corsPolicyBuilder
                        .WithOrigins(allowedOrigins)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .Build());
            });

            // Add authentication and configure JWT as scheme
            var jwtConfig = Configuration.GetSection("JWT").Get<JwtConfiguration>();
            services.AddSingleton(jwtConfig);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = jwtConfig.Issuer,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtConfig.Secret)),
                    ValidateAudience = true,
                    ValidAudience = jwtConfig.Audience,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(1)
                };
            });

            // Add Razor Engine (used for rendering mail templates)
            services.AddRazorPages();

            // Add JWT auth manager
            services.AddScoped<IJwtAuthManager, JwtAuthManager>();

            // Add account service
            var bcryptConfig = Configuration.GetSection("Account").Get<AccountServiceConfiguration>();
            services.AddSingleton(bcryptConfig);
            services.AddScoped<IAccountService, AccountService>();

            // Add web push configuration
            var wpConfig = Configuration.GetSection("WebPush").Get<WebPushConfiguration>();
            services.AddSingleton(wpConfig);

            // Add email configuration
            var emailConfig = Configuration.GetSection("Email").Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);

            // Add hosting configuration
            var hostingConfig = Configuration.GetSection("Hosting").Get<HostingConfiguration>();
            services.AddSingleton(hostingConfig);

            // Add Notification service event queue
            services.AddSingleton<INotificationQueue, NotificationQueue>();

            // Add custom services
            services.AddScoped<IAdminLogService, AdminLogService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IVenueService, VenueService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<IRegistrationService, RegistrationService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IPushService, PushService>();
            services.AddScoped<IMailTemplateService, MailTemplateService>();
            services.AddScoped<INotificationService, NotificationService>();

            // Hosted services
            services.AddHostedService<DeliveryService>();
            services.AddHostedService<JwtTokenCache>();
            services.AddHostedService<RecoveryTokenCache>();
            services.AddHostedService<AccountDeletionService>();

            if (HostingEnvironment.IsDevelopment())
            {
                // Register the Swagger generator, defining 1 or more Swagger documents
                services.AddSwaggerDocumentation();
            }
        }
        /// <summary>
        /// Configures the application.
        /// </summary>
        /// <param name="app">IApplicationBuilder reference</param>
        /// <param name="env">IWebHostingEnvironment reference</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseCors("CorsPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerDocumentation();
            }

            app.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    var error = context.Features.Get<IExceptionHandlerFeature>();

                    if (error != null)
                    {
                        context.Response.AddApplicationError(error.Error.Message);
                        await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(true);
                    }
                });
            });

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseRootRewrite();
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }

    #endregion
}

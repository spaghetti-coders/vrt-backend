﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VRT.API.Extensions
{
    /// <summary>
    /// This class defines extensions related to exception handling.
    /// </summary>
    public static class ExceptionHandlingExtensions
    { 
        /// <summary>
        /// Adds an 'Application-Error' header with the specified <paramref name="message"/> as value to the response object. 
        /// </summary>
        /// <param name="response">The response object</param>
        /// <param name="message">The error message to attach to the response</param>
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
            response.Headers.Add("Access-Control-Allow.Origin", "*");
        }  
    }
}

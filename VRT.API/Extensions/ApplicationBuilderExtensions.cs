﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VRT.API.Extensions
{
    /// <summary>
    /// A Collection of extesions for the IApplicationBuilder.
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// This extension method should prevent a possible DOS attack vector, whereby a SPA's default resource (i.e., '/') 
        /// is flodded with non-GET requests (HEAD, OPTIONS, POST, PUT, ...), causing a System.InvalidOperationException
        /// with the message 'The SPA default page middleware could not return the default page '/index.html' because it was not found, and no other middleware handled the request.'
        /// </summary>
        /// <param name="builder">An IApplicationBuilder reference</param>
        /// <returns>Returns the IApplicationBuilder that was passed to this method</returns>
        public static IApplicationBuilder UseRootRewrite(this IApplicationBuilder builder)
        {
            builder.Use((context, next) =>
            {
                if (context.Request.Path == "/" && !HttpMethods.IsGet(context.Request.Method))
                {
                    context.Request.Method = "GET";
                }

                return next();
            });

            return builder;
        }
    }
}

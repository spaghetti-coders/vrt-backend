﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VRT.API.Extensions.Swagger
{
    /// <summary>
    /// This class implements a Swagger document filter for adding information about enumerations to the generated documentation.
    /// </summary>
    public class EnumDocumentFilter : IDocumentFilter
    {
        /// <summary>
        /// Apply this filter to the given swagger document.
        /// </summary>
        /// <param name="swaggerDoc">The swagger document to "filter"</param>
        /// <param name="context">The current DocumentFilterContext</param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            // add enum descriptions to result models
            foreach (var property in swaggerDoc.Components.Schemas.Where(x => x.Value?.Enum?.Count > 0))
            {
                IList<IOpenApiAny> propertyEnums = property.Value.Enum;
                if (propertyEnums != null && propertyEnums.Count > 0)
                {
                    property.Value.Description += DescribeEnum(propertyEnums, property.Key);
                }
            }

            // add enum descriptions to input parameters
            foreach (var pathItem in swaggerDoc.Paths.Values)
            {
                DescribeEnumParameters(pathItem.Operations, swaggerDoc);
            }
        }

        /// <summary>
        /// Convenience method for identifying enumeration parameters and describing them using reflection.
        /// </summary>
        /// <param name="operations">The API opertaions described by swagger</param>
        /// <param name="swaggerDoc">The Swagger document</param>
        private void DescribeEnumParameters(IDictionary<OperationType, OpenApiOperation> operations, OpenApiDocument swaggerDoc)
        {
            if (operations != null)
            {
                foreach (var oper in operations)
                {
                    foreach (var param in oper.Value.Parameters)
                    {
                        var paramEnum = swaggerDoc.Components.Schemas.FirstOrDefault(x => x.Key == param.Name);
                        if (paramEnum.Value != null)
                        {
                            param.Description = param.Description + DescribeEnum(paramEnum.Value.Enum, paramEnum.Key);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Convenience method for returning a given enumeration type by name. 
        /// </summary>
        /// <param name="enumTypeName">The name of the enum type</param>
        /// <returns>The type of the specified enumeration.</returns>
        private Type GetEnumTypeByName(string enumTypeName)
        {
            return AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .FirstOrDefault(x => x.Name == enumTypeName);
        }
        /// <summary>
        /// Convenience method for returning a description for the specified enumeration value.
        /// </summary>
        /// <param name="enums">The enumeration from the swagger document</param>
        /// <param name="proprtyTypeName">The name of the enumeration type</param>
        /// <returns>a description for the specified enumeration value.</returns>
        private string DescribeEnum(IList<IOpenApiAny> enums, string proprtyTypeName)
        {
            var enumType = GetEnumTypeByName(proprtyTypeName);
            if (enumType == null)
                return null;

            return string.Join(", ", (
                from OpenApiInteger enumOption in enums
                    select enumOption.Value into enumInt
                    select $"{enumInt} = {Enum.GetName(enumType, enumInt)}")
                .ToArray());
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using System.Threading.Tasks;
using VRT.Core.Util;
using VRT.Data;
using VRT.Data.Entities;
using VRT.Mail.Models;
using VRT.Mail.Services;

namespace VRT.API.Controllers
{
    /// <summary>
    /// A controller for testing the various mail templates.
    /// </summary>
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class MailTemplatesController : Controller
    {
        #region Fields

        private readonly CultureInfo _defaultCulture = new CultureInfo("de-DE");

        private IMailTemplateService _mtService;
        private VRTContext _db;

        #endregion


        /// <summary>
        /// Constructs a new MailTemplatesController object.
        /// </summary>
        /// <param name="mailTemplateService">An IMailTemplateService reference.</param>
        /// <param name="context">A VRTContext reference.</param>
        public MailTemplatesController(IMailTemplateService mailTemplateService, VRTContext context)
        {
            _mtService = mailTemplateService;
            _db = context;
        }


        /// <summary>
        /// The currently authenticated application user.
        /// </summary>
        public ClaimsPrincipalWrapper CurrentUser
        {
            get
            {
                if (User.Identity != null && User.Identity.IsAuthenticated)
                    return new ClaimsPrincipalWrapper(User);

                return null;
            }
        }


        #region User / Account related

        /// <summary>
        /// Returns a "new-user-registration" mail-body for the current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("user/new-registration")]
        public async Task<IActionResult> NewUserRegistration()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);

            var model = new UserMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                User = user
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.Users.NewRegistration, model, _defaultCulture);

            return Ok(mailBody);
        }
        /// <summary>
        /// Returns a "registration-confirmation" mail-body for the current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("user/registration-confirmation")]
        public async Task<IActionResult> UserRegistrationConfirmation()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);

            var model = new UserMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                User = user
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.Users.RegistrationConfirmation, model, _defaultCulture);

            return Ok(mailBody);
        }
        /// <summary>
        /// Returns a "registration-completed" mail-body for the current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("user/registration-completed")]
        public async Task<IActionResult> UserRegistrationCompleted()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);

            var model = new UserMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                User = user
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.Users.AccountActivated, model, _defaultCulture);

            return Ok(mailBody);
        }
        /// <summary>
        /// Returns an "account deleted confirmation" mail-body for the current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("user/account-deleted")]
        public async Task<IActionResult> UserAccountDeleted()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);

            var model = new UserMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                User = user
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.Users.AccountDeleted, model, _defaultCulture);

            return Ok(mailBody);
        }
        /// <summary>
        /// Returns an "account recovery" mail-body for the current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("user/account-recovery")]
        public async Task<IActionResult> UserAccountRecovery()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);
            var token = new AccountRecoveryToken
            {
                UserId = user.Id,
                User = user,
                ExpiresAt = DateTime.UtcNow.AddHours(2),
                Token = KeyGen.GenerateUniqueKey(12)
            };

            var model = new AccountRecoveryMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                User = user,
                RecoveryToken = token
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.Users.AccountRecovery, model, _defaultCulture);

            return Ok(mailBody);
        }

        #endregion

        #region Event related

        /// <summary>
        /// Returns a "new event" mail-body.
        /// </summary>
        /// <returns></returns>
        [HttpGet("event/new-event")]
        public async Task<IActionResult> NewEvent()
        {
            var evt = await _db.Events
                .Include(e => e.Venue)
                .FirstOrDefaultAsync();
        
            var model = new EventMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                Event = evt,
                Venue = evt.Venue
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.Events.NewEvent, model, _defaultCulture);

            return Ok(mailBody);
        }
        /// <summary>
        /// Returns an "event canceled" mail-body.
        /// </summary>
        /// <returns></returns>
        [HttpGet("event/event-canceled")]
        public async Task<IActionResult> EventCanceled()
        {
            var evt = await _db.Events
                .Include(e => e.Venue)
                .FirstOrDefaultAsync();

            var model = new EventMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                Event = evt,
                Venue = evt.Venue
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.Events.EventCanceled, model, _defaultCulture);

            return Ok(mailBody);
        }

        #endregion

        #region Event Registrations

        /// <summary>
        /// Returns an "event registration upgraded" mail-body.
        /// </summary>
        /// <returns></returns>
        [HttpGet("eventreg/upgraded")]
        public async Task<IActionResult> EventRegistrationUpgraded()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);

            var evt = await _db.Events
                .Include(e => e.Venue)
                .FirstOrDefaultAsync();

            var model = new EventRegistrationMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                Event = evt,
                Venue = evt.Venue,
                User = user
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.EventRegistrations.Upgraded, model, _defaultCulture);

            return Ok(mailBody);
        }
        /// <summary>
        /// Returns an "event registration downgraded" mail-body.
        /// </summary>
        /// <returns></returns>
        [HttpGet("eventreg/downgraded")]
        public async Task<IActionResult> EventRegistrationDowngraded()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);

            var evt = await _db.Events
                .Include(e => e.Venue)
                .FirstOrDefaultAsync();

            var model = new EventRegistrationMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                Event = evt,
                Venue = evt.Venue,
                User = user
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.EventRegistrations.Downgraded, model, _defaultCulture);

            return Ok(mailBody);
        }
        /// <summary>
        /// Returns an "event registration deleted" mail-body.
        /// </summary>
        /// <returns></returns>
        [HttpGet("eventreg/deleted")]
        public async Task<IActionResult> EventRegistrationDeleted()
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Id == CurrentUser.Id);

            var evt = await _db.Events
                .Include(e => e.Venue)
                .FirstOrDefaultAsync();

            var model = new EventRegistrationMailModel
            {
                AppUrl = "https://fb.holistic-net.de",
                Event = evt,
                Venue = evt.Venue,
                User = user
            };

            string mailBody = await _mtService.GetTemplateAsHtmlStringAsync(MailTemplates.EventRegistrations.DeletedByAdmin, model, _defaultCulture);

            return Ok(mailBody);
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VRT.API.Models;
using VRT.API.Models.Registrations;
using VRT.API.Resources.AdminLog;
using VRT.Core.Constants;
using VRT.Core.Models;
using VRT.Core.Services;

namespace VRT.API.Controllers
{
    /// <summary>
    /// Provides functions for managing event registrations.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class EventRegistrationsController : VRTController
    {
        #region Fields
        
        private readonly IRegistrationService _regService;
        private readonly IEventService _eventService;
        private readonly IAdminLogService _adminLog;

        #endregion
        
        /// <summary>
        /// Constructs a new EventRegistrationsController object. 
        /// </summary>
        /// <param name="registrationService">An IRegistrationService reference</param>
        /// <param name="eventService">An IEventService reference</param>
        /// <param name="adminLog">An IAdminLogService reference</param>
        public EventRegistrationsController(IRegistrationService registrationService, IEventService eventService,
            IAdminLogService adminLog)
        {
            _regService = registrationService;
            _eventService = eventService;
            _adminLog = adminLog;
        }


        /// <summary>
        /// Returns a list of registrations for the user with the specified ID.
        /// </summary>
        /// <param name="userId">The ID of the user</param>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of events matching the specified filter</returns>
        [Authorize(Roles = Roles.Admin)]
        [HttpGet("user/{userId}")]
        [ProducesResponseType(typeof(PagedResult<AppRegistration>), 200)]
        public async Task<ActionResult<PagedResult<AppRegistration>>> FindRegistrationsByUser(int userId, [FromQuery] EventRegQueryParams query)
        {
            // Validate query parameters
            if (userId <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'userId' must be >= 1"));
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppRegistration.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppRegistration)}'."));
            
            // Make sure results will be sorted
            query.SortBy ??= "RegistrationDate";
            var sortColumn = AppRegistration.SortProperties[query.SortBy];

            var count = await _regService.CountRegistrationsByUserAsync(userId, query.Filter, query.Status);
            var items = (await _regService.FindRegistrationsByUserAsync(userId, query.Page, query.PageSize,
                    query.Filter, query.Status, sortColumn, query.SortOrder))
                .Select(AppRegistration.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppRegistration>(items, count, query, AppRegistration.SortProperties.Keys));
        }
        /// <summary>
        /// Returns a list of registrations for the event with the specified ID.
        /// </summary>
        /// <param name="eventId">The ID of the event</param>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of events matching the specified filter</returns>
        [HttpGet("event/{eventId}")]
        [ProducesResponseType(typeof(PagedResult<AppRegistration>), 200)]
        public async Task<ActionResult<PagedResult<AppRegistration>>> FindRegistrationsByEvent(int eventId, [FromQuery]EventRegQueryParams query)
        {
            // Validate query parameters
            if (eventId <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'eventId' must be >= 1"));
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppRegistration.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppRegistration)}'."));
            
            // Make sure results will be sorted
            query.SortBy ??= "RegistrationDate";
            var sortColumn = AppRegistration.SortProperties[query.SortBy];
            
            // User must be a manager or an admin to access past events / events that are not open for registration
            var evt = await _eventService.GetEventAsync(eventId);
            if ((!evt.IsOpenForRegistration || evt.Date < DateTime.UtcNow) && !Roles.ManagerOrAdmin.Contains(CurrentUser.Role))
                return new ObjectResult(Messages.Errors.AccessDenied.AsMessageResult(
                    $"You don't have the required user role to access the event (ID={eventId})")) 
                    { StatusCode = (int)HttpStatusCode.Forbidden };
            
            var count = await _regService.CountRegistrationsByEventAsync(eventId, query.Filter, query.Status);
            var items = (await _regService.FindRegistrationsByEventAsync(eventId, query.Page, query.PageSize,
                    query.Filter, query.Status, sortColumn, query.SortOrder))
                .Select(AppRegistration.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppRegistration>(items, count, query, AppRegistration.SortProperties.Keys));
        }
        /// <summary>
        /// Registers the current user to the Event with the specified ID.
        /// </summary>
        /// <param name="eventId">ID of the event for which to sign-up</param>
        /// <returns>The newly created registration</returns>
        [HttpPost("event/{eventId}")]
        [ProducesResponseType(typeof(AppRegistration), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult<AppRegistration>> RegisterToEvent(int eventId)
        {
            // Validate parameters
            if (eventId <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'eventId' must be >= 1"));
            
            var registration = await _regService.RegisterAsync(eventId, CurrentUser.Id);

            return Ok(AppRegistration.FromEntity(registration));
        }
        /// <summary>
        /// Unregister the current user from the event with the specified ID.
        /// </summary>
        /// <param name="eventId">ID of the event from which to sign-off</param>
        /// <returns>An OkObjectResult</returns>
        [HttpDelete("event/{eventId}")]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> UnregisterFromEvent(int eventId)
        {
            // Validate parameters
            if (eventId <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'eventId' must be >= 1"));
            
            await _regService.UnregisterAsync(eventId, CurrentUser.Id);

            return Ok();
        }
        /// <summary>
        /// Adds a user to an event in the past (requires admin role).
        /// </summary>
        /// <param name="model">The registration model</param>
        /// <returns>The registration that was deleted</returns>
        [HttpPost]
        [Authorize(Roles = Roles.Admin)]
        [ProducesResponseType(typeof(AppRegistration), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> AddRegistration([FromBody]EventRegistrationModel model)
        {
            // Validate model
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult(ValidationMessage));
            
            var registration = await _regService.AddRegistrationAsync(model.EventId, model.UserId);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.EventRegistration_Added, registration.User.DisplayName, registration.Event.Name);

            return Ok(AppRegistration.FromEntity(registration));
        }
        /// <summary>
        /// Deletes the registration for the specified user and event (requires admin role).
        /// </summary>
        /// <param name="model">The registration model</param>
        /// <returns>The registration that was deleted</returns>
        [HttpDelete]
        [Authorize(Roles = Roles.Admin)]
        [ProducesResponseType(typeof(AppRegistration),200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> DeleteRegistration([FromBody]EventRegistrationModel model)
        {
            // Validate model
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult(ValidationMessage));
            
            var registration = await _regService.DeleteRegistrationAsync(model.EventId, model.UserId);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.EventRegistration_Deleted, registration.User.DisplayName, registration.Event.Name);

            return Ok(AppRegistration.FromEntity(registration));
        }
    }
}
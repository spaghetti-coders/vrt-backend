﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.Venues;
using VRT.API.Resources.AdminLog;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.Services;

namespace VRT.API.Controllers
{
    /// <summary>
    /// Provides functions for managing venues.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class VenuesController : VRTController
    {
        #region Fields

        private readonly IAdminLogService _adminLog;
        private readonly IVenueService _venueService;

        #endregion


        /// <summary>
        /// Constructs a new VenuesController
        /// </summary>
        /// <param name="venueService">An IVenueService reference</param>
        /// <param name="adminLog">An IAdminLogService reference</param>
        public VenuesController(IVenueService venueService, IAdminLogService adminLog)
        {
            _venueService = venueService;
            _adminLog = adminLog;
        }


        #region Methods

        /// <summary>
        /// Returns a list of venues matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of venues matching the specified filter</returns>
        [HttpGet]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(PagedResult<AppVenue>), 200)]
        public async Task<ActionResult<PagedResult<AppVenue>>> FindVenues([FromQuery]PagedQueryParameters query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppVenue.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppVenue)}'."));

            // Make sure results will be sorted
            query.SortBy ??= "Id";
            var sortColumn = AppVenue.SortProperties[query.SortBy];
            
            var count = await _venueService.CountVenuesAsync(query.Filter);
            var items = (await _venueService.FindVenuesAsync(query.Page, query.PageSize, 
                    query.Filter, sortColumn, query.SortOrder))
                .Select(AppVenue.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppVenue>(items, count, query, AppVenue.SortProperties.Keys));
        }
        /// <summary>
        /// Returns a list of venues that are available for scheduling events at
        /// and that are matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of available venues that are matching the specified filter</returns>
        [Authorize(Roles = Roles.AllUsers)]
        [HttpGet("available")]
        [ProducesResponseType(typeof(PagedResult<AppVenue>), 200)]
        public async Task<ActionResult<PagedResult<AppVenue>>> FindAvailableVenues([FromQuery]PagedQueryParameters query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppVenue.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppVenue)}'."));

            // Make sure results will be sorted
            query.SortBy ??= "Id";
            var sortColumn = AppVenue.SortProperties[query.SortBy];
            
            var count = await _venueService.CountActiveVenuesAsync(query.Filter);
            var items = (await _venueService.FindActiveVenuesAsync(query.Page, query.PageSize, query.Filter, 
                    sortColumn, query.SortOrder))
                .Select(AppVenue.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppVenue>(items, count, query, AppVenue.SortProperties.Keys));
        }
        /// <summary>
        /// Returns the venue with the specified ID.
        /// </summary>
        /// <param name="id">ID of the venue to return</param>
        /// <returns>The venue with the specified ID, or a <see cref="NotFoundResult"/> if no such venue exists</returns>
        [HttpGet("{id}")]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(AppVenue), 200), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> GetVenue(int id)
        {
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            
            var venue = await _venueService.FindVenueAsync(id);

            if (venue == null)
                return NotFound(Messages.Errors.EntityNotFound.AsMessageResult($"The venue with the ID '{id}' doesn't exist."));

            return Ok(AppVenue.FromEntity(venue));
        }
        /// <summary>
        /// Creates a new venue.
        /// </summary>
        /// <param name="model">The venue's properties</param>
        /// <returns>The newly created venue</returns>
        [HttpPost]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(AppVenue), 201), ProducesResponseType(typeof(MessageResult), 400)]
        public async Task<ActionResult> CreateVenue([FromBody] VenueModel model)
        {
            // Validate model
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult(ValidationMessage));

            var venue = await _venueService.CreateVenueAsync(model.Name, model.Address, model.Capacity, model.ColorCode, model.IsActive);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.Venue_Created, venue.Name);

            return CreatedAtAction(nameof(GetVenue), new { id = venue.Id }, AppVenue.FromEntity(venue));
        }
        /// <summary>
        /// Updates the venue with the specified ID.
        /// </summary>
        /// <param name="id">ID of the venue to update</param>
        /// <param name="model">The updated venue properties</param>
        /// <returns>The updated venue</returns>
        [HttpPut("{id}")]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(AppVenue), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> UpdateVenue(int id, [FromBody] VenueModel model)
        {
            // Validate model
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            var venue = await _venueService.UpdateVenueAsync(id, model.Name, model.Address, model.Capacity, model.ColorCode, model.IsActive);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.Venue_Updated, model.Name);

            return Ok(AppVenue.FromEntity(venue));
        }
        /// <summary>
        /// Deletes the venue with the specified ID from the application.
        /// </summary>
        /// <param name="id">ID of the venue to delete</param>
        /// <param name="hostingConfiguration">A reference to the hosting configuration sections</param>
        /// <returns>An <see cref="OkObjectResult"/> result if the venue was deleted successfully, or a <see cref="NotFoundResult"/> if not such venue exists</returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> DeleteVenue(int id, [FromServices] HostingConfiguration hostingConfiguration)
        {
            // Validate model
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            
            var venue = await _venueService.DeleteVenueAsync(id, hostingConfiguration.DataRetentionPeriod);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.Venue_Deleted, venue.Name);

            return Ok();
        }

        #endregion
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.Account;
using VRT.Core.Constants;

namespace VRT.API.Controllers
{
    /// <summary>
    /// An controller base that provides access to the currently authenticated user.
    /// </summary>
    public abstract class VRTController : ControllerBase
    {
        /// <summary>
        /// The currently authenticated application user.
        /// </summary>
        protected ClaimsPrincipalWrapper CurrentUser
        {
            get
            {
                if (User.Identity != null && User.Identity.IsAuthenticated)
                    return new ClaimsPrincipalWrapper(User);

                return null;
            }
        }

        /// <summary>
        /// A pipe-separated (` | `) list of validation error messages,
        /// or `string.Empty` if the current model state is valid.
        /// </summary>
        protected string ValidationMessage
        {
            get
            {
                string message = string.Empty;
                
                if (!ModelState.IsValid)
                {
                    message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                }

                return message;
            }
        }
    }

    /// <summary>
    /// A wrapper for the ClaimsPrincipal class that provides information about the user requesting a resource.
    /// </summary>
    public class ClaimsPrincipalWrapper 
    {
        private ClaimsPrincipal _principal;

        /// <summary>
        /// Constructs a new ClaimsPrincipalWrapper for the given claims principal.
        /// </summary>
        /// <param name="principal">The ClaimsPrincipal object to wrap</param>
        public ClaimsPrincipalWrapper(ClaimsPrincipal principal)
        {
            _principal = principal;
        }

        /// <summary>
        /// The user's ID in the databse.
        /// </summary>
        public int Id { get { return Int32.Parse(_principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid)?.Value ?? "-1"); } }
        /// <summary>
        /// The user's username.
        /// </summary>
        public string Username { get { return _principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value; } }
        /// <summary>
        /// The user's display name.
        /// </summary>
        public string DisplayName { get { return _principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value; } }
        /// <summary>
        /// The user's role.
        /// </summary>
        public string Role { get { return _principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value; } }
    }
}

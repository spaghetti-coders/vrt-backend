﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.Account;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.HostedServices;
using VRT.Core.Services;

namespace VRT.API.Controllers
{
    /// <summary>
    /// This controller provides functions for de-/authenticating users and refreshing tokens.
    /// </summary>
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class AccountController : VRTController
    {
        #region Fields

        private readonly IAccountService _service;

        #endregion


        /// <summary>
        /// Constructs a new AccountController object.
        /// </summary>
        /// <param name="service">A reference to an IAccountService</param>
        public AccountController(IAccountService service) : base()
        {
            _service = service;
        }


        #region General Account Management

        /// <summary>
        /// Tries to register a new user with the specified attributes.
        /// </summary>
        /// <param name="model">The user registration model</param>
        /// <returns>An empty OkObjectResult</returns>
        [AllowAnonymous]
        [HttpPost("register")]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 400)]
        public async Task<ActionResult> Register([FromBody] RegistrationModel model)
        {
            // Validate model
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            // Try to register the new user
            var user = await _service.RegisterUserAsync(model.Username, model.DisplayName, model.Password, model.Email, model.ReceiveNotifications);

            return Ok();
        }
        /// <summary>
        /// Returns the account of the currently authenticated user.
        /// </summary>
        /// <returns>the account of the currently authenticated user</returns>
        [HttpGet]
        [ProducesResponseType(typeof(AppUser), 200), ProducesResponseType(typeof(MessageResult), 401)]
        public async Task<ActionResult> GetCurrentUser()
        {
            try
            {
                var user = await _service.GetUserAsync(CurrentUser?.Username);

                return Ok(AppUser.FromUser(user));
            }
            catch (Exception ex)
            {
                return Unauthorized(Messages.Errors.AccountDisabledOrDeleted.AsMessageResult(ex.Message));
            }
        }
        /// <summary>
        /// Updates the user's account with the provided properties.
        /// </summary>
        /// <param name="model">The user's updated properties</param>
        /// <returns>The updated user object</returns>
        [HttpPut("update")]
        [ProducesResponseType(typeof(AppUser), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 401)]
        public async Task<ActionResult> UpateAccount([FromBody] UpdateAccountModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            var user = await _service.UpdateUserAsync(CurrentUser?.Username, model.DisplayName, model.Email, model.ReceiveNotifications);

            return Ok(AppUser.FromUser(user));
        }
        /// <summary>
        /// Updates the user's password.
        /// </summary>
        /// <param name="model">The user's updated password</param>
        /// <returns>An empty OkObjectResult.</returns>
        [HttpPut("update-password")]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 401)]
        public async Task<ActionResult> UpatePassword([FromBody] UpdatePasswordModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            await _service.UpdatePasswordAsync(CurrentUser?.Username, model.Password);

            return Ok();
        }
        /// <summary>
        /// Tries to authenticate the user with the specified credentials.
        /// </summary>
        /// <param name="model">The user login model</param>
        /// <returns>a LoginResult containing the authenticated user and JWT tokens.</returns>
        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(typeof(LoginResult), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 401)]
        public async Task<ActionResult> Login([FromBody] LoginModel model)
        {
            // Validate model
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            var jwtResult = await _service.AuthenticateUserAsync(model.Username, model.Password);

            return Ok(new LoginResult
            {
                AccessToken = jwtResult.AccessToken,
                RefreshToken = jwtResult.RefreshToken.TokenString
            });
        }
        /// <summary>
        /// De-authenticate the currently logged in user.
        /// </summary>
        /// <returns>An empty OkObjectResult.</returns>
        [HttpPost("logout")]
        [ProducesResponseType(200), ProducesResponseType(401)]
        public ActionResult Logout()
        { 
            _service.DeauthenticateUserAsync(CurrentUser.Id, CurrentUser?.Username);
                
            return Ok();
        }
        /// <summary>
        /// Deletes and de-authenticates the currently logged in user's account.
        /// </summary>
        /// <param name="hostingConfiguration">A reference to the HostingConfiguration</param>
        /// <returns>An empty OkObjectResult.</returns>
        [HttpPost("delete")]
        [ProducesResponseType(200), ProducesResponseType(401)]
        public async Task<ActionResult> DeleteAccount([FromServices] HostingConfiguration hostingConfiguration)
        {
            await _service.DeleteAccountAsync(CurrentUser?.Username, hostingConfiguration.DataRetentionPeriod);

            return Ok();
        }
        /// <summary>
        /// Refreshes the user's JWT token pair.
        /// </summary>
        /// <param name="model">The refresh token model</param>
        /// <returns>a LoginResult containing the authenticated user and a new JWT tokens pair</returns>
        [AllowAnonymous]
        [HttpPost("refresh")]
        [ProducesResponseType(typeof(LoginResult), 200), ProducesResponseType(typeof(MessageResult), 401)]
        public async Task<ActionResult> RefreshToken([FromBody] RefreshTokenRequest model)
        {
            try
            {
                var jwtResult = await _service.RefreshTokenAsync(model.AccessToken, model.RefreshToken);
                
                return Ok(new LoginResult
                {    
                    AccessToken = jwtResult.AccessToken,
                    RefreshToken = jwtResult.RefreshToken.TokenString
                });
            }
            catch (SecurityTokenException ex)
            {
                return Unauthorized(Messages.Errors.RefreshTokenInvalidOrExpired.AsMessageResult(ex.Message));
            }
        }

        #endregion

        #region Account Recovery

        /// <summary>
        /// Generates an account recovery token that can be used to reset the specified user's password and notifies the user by mail.
        /// </summary>
        /// <param name="model">The recovery token request model</param>
        /// <returns>an OkObjectResult</returns>
        [HttpPost("recover/request-token")]
        [AllowAnonymous]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 401), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> RequestRecoveryToken([FromBody] RecoveryTokenRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            var token = await _service.GenerateRecoveryTokenAsync(model.Email);

            return Ok();
        }
        /// <summary>
        /// Reset the password of a user, using the provided account recovery token.
        /// </summary>
        /// <param name="model">The recovery token and new password</param>
        /// <returns>An empty OkObjectRespone</returns>
        [HttpPost("recover/reset-password")]
        [AllowAnonymous]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 400)]
        public async Task<ActionResult> ResetPassword([FromBody] PasswordResetModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            await _service.ResetPasswordAsync(model.RecoveryToken, model.Password);

            return Ok();
        }

        #endregion
    }
}

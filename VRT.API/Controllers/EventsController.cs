﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.Events;
using VRT.API.Resources.AdminLog;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.Services;

namespace VRT.API.Controllers
{
    /// <summary>
    /// Provides functions for managing events.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class EventsController : VRTController
    {
        #region Fields

        private readonly IEventService _eventService;
        private readonly IAdminLogService _adminLog;

        #endregion


        /// <summary>
        /// Constructs a new EventsController object. 
        /// </summary>
        /// <param name="eventService">An IEventService reference</param>
        /// <param name="adminLog">An IAdminLogService reference</param>
        public EventsController(IEventService eventService, IAdminLogService adminLog)
        {
            _eventService = eventService;
            _adminLog = adminLog;
        }
        

        /// <summary>
        /// Returns a list of events matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of events matching the specified filter</returns>
        [HttpGet]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(PagedResult<AppEvent>), 200)]
        public async Task<ActionResult<PagedResult<AppEvent>>> FindEvents([FromQuery]EventsQueryParams query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppEvent.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppEvent)}'."));
            
            // Make sure results will be sorted
            query.SortBy ??= "Id";
            var sortColumn = AppEvent.SortProperties[query.SortBy];
            
            var count = await _eventService.CountEventsAsync(query.Filter);
            var items = (await _eventService.FindEventsAsync(CurrentUser.Id, query.Page, query.PageSize, 
                    query.Filter, sortColumn, query.SortOrder))
                .Select(AppEvent.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppEvent>(items, count, query, AppEvent.SortProperties.Keys));
        }
        /// <summary>
        /// Returns a list of events that are open for registration and that are matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of events that are open for registration and that are matching the specified filter</returns>
        [Authorize(Roles = Roles.AllUsers)]
        [HttpGet("available")]
        [ProducesResponseType(typeof(PagedResult<AppEvent>), 200)]
        public async Task<ActionResult<PagedResult<AppEvent>>> FindAvailableEvents([FromQuery]EventsQueryParams query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppEvent.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppEvent)}'."));
            
            // Make sure results will be sorted
            query.SortBy ??= "Id";
            var sortColumn = AppEvent.SortProperties[query.SortBy];
            
            var count = await _eventService.CountFutureEventsAsync(query.Filter, true);
            var items = (await _eventService.FindFutureEventsAsync(CurrentUser.Id, true, 
                    query.Page, query.PageSize, query.Filter, sortColumn, query.SortOrder))
                .Select(AppEvent.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppEvent>(items, count, query, AppEvent.SortProperties.Keys));
        }
        /// <summary>
        /// Returns a list of events that have already taken place and that are matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of events that have already taken place and that are matching the specified filter</returns>
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [HttpGet("past")]
        [ProducesResponseType(typeof(PagedResult<AppEvent>), 200)]
        public async Task<ActionResult<PagedResult<AppEvent>>> FindPastEvents([FromQuery]PastEventsQueryParams query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppEvent.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppEvent)}'."));
            
            // Make sure results will be sorted
            query.SortBy ??= "Id";
            var sortColumn = AppEvent.SortProperties[query.SortBy];
            
            var count = await _eventService.CountPastEventsAsync(query.Filter);
            var items = (await _eventService.FindPastEventsAsync(CurrentUser.Id, query.Page, query.PageSize, 
                    query.Filter, sortColumn, query.SortOrder))
                .Select(AppEvent.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppEvent>(items, count, query, AppEvent.SortProperties.Keys));
        }
        /// <summary>
        /// Returns a list of future events that are matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of future events that are matching the specified filter.</returns>
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [HttpGet("future")]
        [ProducesResponseType(typeof(PagedResult<AppEvent>), 200)]
        public async Task<ActionResult<PagedResult<AppEvent>>> FindFutureEvents([FromQuery]EventsQueryParams query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppEvent.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppEvent)}'."));
            
            // Make sure results will be sorted
            query.SortBy ??= "Id";
            var sortColumn = AppEvent.SortProperties[query.SortBy];
            
            var count = await _eventService.CountFutureEventsAsync(query.Filter, false);
            var items = (await _eventService.FindFutureEventsAsync(CurrentUser.Id, false, 
                    query.Page, query.PageSize, query.Filter, sortColumn, query.SortOrder))
                .Select(AppEvent.FromEntity)
                .ToList();

            return Ok(new PagedResult<AppEvent>(items, count, query, AppEvent.SortProperties.Keys));
        }
        /// <summary>
        /// Returns the event with the specified ID.
        /// </summary>
        /// <param name="id">ID of the event to return</param>
        /// <returns>The event with the specified ID, or a <see cref="NotFoundResult"/> if no such event exists</returns>
        [HttpGet("{id}")]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(AppEvent), 200), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> GetEvent(int id)
        {
            // Validate parameters
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            
            var evt = await _eventService.FindEventAsync(id);

            if (evt == null)
                return NotFound(Messages.Errors.EntityNotFound.AsMessageResult($"The event with the ID '{id}' doesn't exist."));

            return Ok(AppEvent.FromEntity(evt));
        }
        /// <summary>
        /// Creates a new event.
        /// </summary>
        /// <param name="model">The event's properties</param>
        /// <returns>The newly created event</returns>
        [HttpPost]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(AppEvent), 201), ProducesResponseType(typeof(MessageResult), 400)]
        public async Task<ActionResult> CreateEvent([FromBody] EventModel model)
        {
            // Validate model
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult(ValidationMessage));

            var evt = await _eventService.CreateEventAsync(model.VenueId, model.Name, model.Description, model.Date, model.Capacity, model.IsOpenForRegistration);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.Event_Created, evt.Name);

            return CreatedAtAction(nameof(GetEvent), new { id = evt.Id }, AppEvent.FromEntity(evt));
        }
        /// <summary>
        /// Updates the event with the specified ID.
        /// </summary>
        /// <param name="id">ID of the event to update</param>
        /// <param name="model">The updated event properties</param>
        /// <returns>The updated event</returns>
        [HttpPut("{id}")]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(typeof(AppEvent), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> UpdateEvent(int id, [FromBody] EventModel model)
        {
            // Validate parameters and model
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult(ValidationMessage));

            var evt = await _eventService.UpdateEventAsync(id, model.VenueId, model.Name, model.Description, model.Date, model.Capacity, model.IsOpenForRegistration);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.Event_Updated, model.Name);

            return Ok(AppEvent.FromEntity(evt));
        }
        /// <summary>
        /// Deletes the event with the specified ID from the application.
        /// </summary>
        /// <param name="id">ID of the event to delete</param>
        /// <param name="hostingConfig">A reference to the hosting configuration sections</param>
        /// <returns>An <see cref="OkObjectResult"/> result if the event was deleted successfully, or a <see cref="NotFoundResult"/> if not such event exists</returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.ManagerOrAdmin)]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> DeleteEvent(int id, [FromServices]HostingConfiguration hostingConfig)
        {
            // Validate parameters
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            
            var evt = await _eventService.DeleteEventAsync(id, hostingConfig.DataRetentionPeriod);

            // Log event deletion
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.Event_Deleted, evt.Name);

            return Ok();
        }
        /// <summary>
        /// Exports the specified event's details (name, description, location, and participants) as text file.
        /// </summary>
        /// <param name="id">ID of the event to export</param>
        /// <returns>A FileStreamResult of type text/plain that contains the details of the specified event. </returns>
        [HttpGet("{id}/log")]
        [Authorize(Roles = Roles.Admin)]
        [ProducesResponseType(typeof(FileStreamResult), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<FileStreamResult> ExportEventDetails(int id)
        {
            // Retrieve the event
            var evt = await _eventService.GetEventAsync(id);

            // Create the log
            var eventLog = await _eventService.CreateEventLogAsync(evt);

            // Log event download
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.Event_Exported, evt.Name);

            // Convert log to memory stream
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(eventLog));

            // Return the stream as file
            return new FileStreamResult(stream, "text/plain; charset=utf-8")
            {
                // Filename: 2021-05-06 - Event 23 - Log.txt
                FileDownloadName = $"{evt.Date:yyyy-MM-dd} - Event {evt.Id} - Log.txt"
            };
        }
    }
}

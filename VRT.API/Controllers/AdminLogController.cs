﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.AdminLog;
using VRT.Core.Constants;
using VRT.Core.Services;
using VRT.Data.Entities;

namespace VRT.API.Controllers
{
    /// <summary>
    /// This controller provides functions viewing and filtering the admin event log.
    /// </summary>
    [ApiController]
    [Authorize(Roles = Roles.Admin)]
    [Route("api/[controller]")]
    public class AdminLogController : VRTController
    {
        #region Fields

        private IAdminLogService _service;

        #endregion

        /// <summary>
        /// Constructs a new AdminLogController object.
        /// </summary>
        /// <param name="service">An IAdminLogService reference</param>
        public AdminLogController(IAdminLogService service)
        {
            _service = service;
        }

        #region Methods

        /// <summary>
        /// Returns a list of log entries matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of log entries matching the specified filter</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagedResult<AppLogEntry>), 200), ProducesResponseType(typeof(MessageResult), 400)]
        public async Task<ActionResult<PagedResult<AppLogEntry>>> FindLogEntries([FromQuery] AdminLogQueryParams query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppLogEntry.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AdminLogEntry)}'."));
        
            // Make sure results will be sorted
            query.SortBy ??= "Timestamp";
            var sortColumn = AppLogEntry.SortProperties[query.SortBy];
            
            var count = await _service.CountEntriesAsync(query.Filter);
            var items = (await _service.FindEntriesAsync(query.Page, query.PageSize, query.Filter,
                    sortColumn, query.SortOrder))
                .Select(AppLogEntry.FromEntity)
                .ToList();
                
            return Ok(new PagedResult<AppLogEntry>(items, count, query, AppLogEntry.SortProperties.Keys));
        }

        #endregion
    }
}

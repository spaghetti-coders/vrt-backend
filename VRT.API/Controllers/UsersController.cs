﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.Account;
using VRT.API.Models.Users;
using VRT.API.Resources.AdminLog;
using VRT.Core.Configurations;
using VRT.Core.Constants;
using VRT.Core.Models;
using VRT.Core.Services;

namespace VRT.API.Controllers
{
    /// <summary>
    /// This controller provides functions related to managing user objects.
    /// </summary>
    [Authorize(Roles = Roles.Admin)]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : VRTController
    {
        #region Fields

        private readonly IAdminLogService _adminLog;
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;

        #endregion

        /// <summary>
        /// Constructs a new UserController object.
        /// </summary>
        /// <param name="userService">An IUserService reference</param>
        /// <param name="accountService">An IAccountService reference</param>
        /// <param name="adminLogService">An IAdminLogService reference</param>
        public UsersController(IUserService userService, IAccountService accountService, IAdminLogService adminLogService) : base()
        {
            _accountService = accountService;
            _userService = userService;
            _adminLog = adminLogService;
        }


        #region Methods

        /// <summary>
        /// Returns a list of users matching the specified filter.
        /// </summary>
        /// <param name="query">Parameters for paging, filtering, and sorting results</param>
        /// <returns>a list of users matching the specified filter</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagedResult<AppUser>), 200)]
        public async Task<ActionResult<PagedResult<AppUser>>> FindUsers([FromQuery] UserQueryParams query)
        {
            // Validate query parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(ValidationMessage));
            if (!string.IsNullOrEmpty(query.SortBy) && !AppUser.SortProperties.ContainsKey(query.SortBy))
                return BadRequest(Messages.Errors.QueryIsInvalid.AsMessageResult(
                    $"The property '{query.SortBy}' is not a valid sort property for type '{nameof(AppUser)}'."));
            
            // Make sure results will be sorted
            query.SortBy ??= "Id";
            var sortColumn = AppUser.SortProperties[query.SortBy];
            
            var count = await _userService.CountUsersAsync(query.Filter, query.Status);
            var items = (await _userService.FindUsersAsync(query.Page, query.PageSize, query.Filter, 
                    sortColumn, query.SortOrder, query.Status))
                .Select(AppUser.FromUser)
                .ToList();

            return Ok(new PagedResult<AppUser>(items, count, query, AppUser.SortProperties.Keys));
        }
        /// <summary>
        /// Returns the user with the specified ID.
        /// </summary>
        /// <param name="id">ID of the user to return</param>
        /// <returns>The user with the specified ID, or a <see cref="NotFoundResult"/> if no such user exists</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AppUser), 200), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> GetUser(int id)
        {
            // Validate parameters
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            
            var user = await _userService.FindUserAsync(id);

            if (user == null)
                return NotFound(Messages.Errors.EntityNotFound.AsMessageResult($"The user with the ID '{id}' doesn't exist."));

            return Ok(AppUser.FromUser(user));
        }
        /// <summary>
        /// Updates the user with the specified ID.
        /// </summary>
        /// <param name="id">ID of the user to update</param>
        /// <param name="model">The updated user properties</param>
        /// <returns>The updated user model</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(AppUser), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> UpdateUser(int id, [FromBody] UpdateUserModel model)
        {
            // Validate model and parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));

            var user = await _userService.UpdateUserAsync(id, model.DisplayName, model.Email, model.Role, model.ReceiveNotifications);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.User_Updated, model.DisplayName);

            return Ok(AppUser.FromUser(user));
        }
        /// <summary>
        /// Updates the account status of the user with the specified ID.
        /// </summary>
        /// <param name="id">ID of the user to update</param>
        /// <param name="model">The updated user properties</param>
        /// <returns>The updated user model</returns>
        [HttpPut("{id}/status")]
        [ProducesResponseType(typeof(AppUser), 200), ProducesResponseType(typeof(MessageResult), 400), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> UpdateUserStatus(int id, [FromBody] UpdateUserStatusModel model)
        {
            // Validate model and parameters
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));

            var user = await _userService.UpdateUserStatusAsync(id, (UserStatus)model.Status);

            // Log event
            await _adminLog.CreateEntryAsync(
                CurrentUser?.DisplayName, 
                model.Status == (int)UserStatus.Active ? AdminActions.User_Activated : AdminActions.User_Deactivated,
                user.DisplayName);

            return Ok(AppUser.FromUser(user));
        }
        /// <summary>
        /// Deletes the user with the specified ID from the application.
        /// </summary>
        /// <param name="id">ID of the user to delete</param>
        /// <param name="hostingConfiguration">A reference to the HostingConfiguration</param>
        /// <returns>The user model of the deleted user, or a <see cref="NotFoundResult"/> if not such user exists</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(AppUser), 200), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> DeleteUser(int id, [FromServices] HostingConfiguration hostingConfiguration)
        {
            // Validate parameters
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            
            var user = await _userService.DeleteUserAsync(id, hostingConfiguration.DataRetentionPeriod);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, AdminActions.User_Deleted, user.DisplayName);

            return Ok(AppUser.FromUser(user));
        }
        /// <summary>
        /// Generates an account recovery token for the user with the specified ID.
        /// </summary>
        /// <param name="id">ID of the user for whom to generate the token</param>
        /// <returns>The recovery token that was generated for the user</returns>
        [HttpPost("recover/{id}")]
        [ProducesResponseType(typeof(RecoveryTokenResult), 200), ProducesResponseType(typeof(MessageResult), 404)]
        public async Task<ActionResult> GenerateRecoveryToken(int id)
        {
            // Validate parameters
            if (id <= 0)
                return BadRequest(Messages.Errors.PathIsInvalid.AsMessageResult("Parameter 'id' must be >= 1"));
            
            var token = await _accountService.GenerateRecoveryTokenAsync(id);

            // Log event
            await _adminLog.CreateEntryAsync(CurrentUser?.DisplayName, "Generated a recovery token", $"User with ID={id}");

            return Ok(new RecoveryTokenResult
            {
                RecoveryToken = token.Token,
                ExpiresAt = token.ExpiresAt
            });
        }

        #endregion
    }
}

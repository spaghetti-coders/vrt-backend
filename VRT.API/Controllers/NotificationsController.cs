﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VRT.API.Models;
using VRT.API.Models.Notifications;
using VRT.Core.Constants;
using VRT.Core.Services;

namespace VRT.API.Controllers
{
    /// <summary>
    /// This controller provides functions for subscribing to the application's push service, as well as sending notifications to users.
    /// </summary>
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class NotificationsController : VRTController
    {
        #region Fields

        private readonly IPushService _pushService;
        private readonly INotificationService _notificationService;

        #endregion


        /// <summary>
        /// Constructs a new NotificationController object.
        /// </summary>
        /// <param name="pushService">An IPushService reference</param>
        /// <param name="notificationService">An INotificationService reference</param>
        public NotificationsController(IPushService pushService, INotificationService notificationService)
        {
            _pushService = pushService;
            _notificationService = notificationService;
        }


        #region Methods

        /// <summary>
        /// Subscribes the user to receive push notifications.
        /// </summary>
        /// <param name="model">The push subscription model</param>
        /// <returns>An empty OkObjectRespone</returns>
        [HttpPost("push/subscribe")]
        [ProducesResponseType(200), ProducesResponseType(typeof(MessageResult), 400)]
        public async Task<ActionResult> SubscribeToPush([FromBody] PushSubscriptionModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            await _pushService.SubscribeAsync(CurrentUser.Id, model.Endpoint, model.P256DH, model.Auth);

            return Ok();
        }
        /// <summary>
        /// Unsubscribes the user from receiving push notifications.
        /// </summary>
        /// <returns>An empty OkObjectRespone</returns>
        [HttpDelete("push/unsubscribe")]
        [ProducesResponseType(200)]
        public async Task<ActionResult> UnsubscribeFromPush()
        {
            var subscription = await _pushService.UnsubscribeAsync(CurrentUser.Id);

            return Ok();
        }
        /// <summary>
        /// Sends a push-notification direct message/broadcast.
        /// </summary>
        /// <param name="model">The message and optionally a target user ID</param>
        /// <returns>An empty OkObjectRespone</returns>
        [HttpPost("push/send")]
        [ProducesResponseType(200), ProducesResponseType(400), ProducesResponseType(404)]
        [Authorize(Roles = Roles.Admin)]
        public ActionResult SendPushNotification([FromBody]PushNotificationModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            if (model.UserId.HasValue)
            {
                if (model.UserId.Value < 0)
                    return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

                _pushService.SendNotificationAsync(model.UserId.Value, model.Title, model.Message);
            }
            else
            {
                _pushService.SendBroadcastNotificationAsync(model.Title, model.Message);
            }

            return Ok();
        }
        /// <summary>
        /// Sends an email notification to the specified recipients.
        /// </summary>
        /// <param name="model">The subject, body and recipients of the message</param>
        /// <returns>An empty OkObjectRespone</returns>
        [HttpPost("email/send")]
        [ProducesResponseType(200)]
        [Authorize(Roles = Roles.Admin)]
        public ActionResult SendEmailNotification([FromBody]EmailNotificationModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(Messages.Errors.InputIsInvalid.AsMessageResult());

            _notificationService.SendEmailNotification(model.Subject, model.Body, model.Recipients);

            return Ok();
        }

        #endregion
    }
}

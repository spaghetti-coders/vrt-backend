﻿START TRANSACTION;

ALTER TABLE "VRT"."Users" ALTER COLUMN "DeletionDate" TYPE timestamp with time zone;

ALTER TABLE "VRT"."Users" ALTER COLUMN "Created" TYPE timestamp with time zone;

ALTER TABLE "VRT"."Registrations" ALTER COLUMN "Registered" TYPE timestamp with time zone;

ALTER TABLE "VRT"."RefreshTokens" ALTER COLUMN "ExpiresAt" TYPE timestamp with time zone;

ALTER TABLE "VRT"."LogEntries" ALTER COLUMN "Timestamp" TYPE timestamp with time zone;

ALTER TABLE "VRT"."Events" ALTER COLUMN "Date" TYPE timestamp with time zone;

ALTER TABLE "VRT"."AccountRecoveryTokens" ALTER COLUMN "ExpiresAt" TYPE timestamp with time zone;

INSERT INTO "VRT"."Users" ("Id", "Created", "DeletionDate", "DisplayName", "Email", "Password", "ReceiveNotifications", "Role", "Status", "Username")
VALUES (1, TIMESTAMPTZ '2022-06-07 16:27:13.300626Z', NULL, 'Administrator', NULL, '$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26', FALSE, 'admin', 1, 'admin');

INSERT INTO "VRT"."AccountRecoveryTokens" ("UserId", "ExpiresAt", "Token")
VALUES (1, TIMESTAMPTZ '2032-06-07 16:27:13.300636Z', 'ResetAdminPW');

SELECT setval(
    pg_get_serial_sequence('"VRT"."Users"', 'Id'),
    GREATEST(
        (SELECT MAX("Id") FROM "VRT"."Users") + 1,
        nextval(pg_get_serial_sequence('"VRT"."Users"', 'Id'))),
    false);

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20220607162713_NPGSQL6', '6.0.5');

COMMIT;


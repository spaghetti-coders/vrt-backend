﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class EventConfig : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            // Table and schema
            builder.ToTable("Events");

            // Primary key
            builder.HasKey(e => e.Id);

            // Relationships
            builder.HasMany<Registration>(e => e.Registrations)
                .WithOne(r => r.Event)
                .HasForeignKey(r => r.EventId)
                .OnDelete(DeleteBehavior.Cascade);

            // Properties
            builder.Property(e => e.Name)
                .HasMaxLength(128)
                .IsUnicode()
                .IsRequired();

            builder.Property(e => e.Description)
                .HasMaxLength(256)
                .IsUnicode();

            builder.Property(e => e.Date)
                .IsRequired();

            // Transient properties
            builder.Ignore(e => e.RegistrationCount);
            builder.Ignore(e => e.WaitingListCount);
        }
    }
}

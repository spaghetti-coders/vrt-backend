﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class RefreshTokenConfig : IEntityTypeConfiguration<RefreshToken>
    { 
        public void Configure(EntityTypeBuilder<RefreshToken> builder)
        {
            // Table & Schema
            builder.ToTable("RefreshTokens");

            // Primary Key
            builder.HasKey(t => t.UserId);
            builder.Property(t => t.UserId)
                .ValueGeneratedNever();

            // Constraints
            builder.HasIndex(t => t.TokenString)
                .IsUnique();

            // Properties
            builder.Property(t => t.TokenString)
                .HasMaxLength(44)
                .IsFixedLength()
                .IsRequired();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class AdminLogEntryConfig : IEntityTypeConfiguration<AdminLogEntry>
    {
        public void Configure(EntityTypeBuilder<AdminLogEntry> builder)
        {
            // Table and schema
            builder.ToTable("LogEntries");

            // Primary key
            builder.HasKey(e => e.Id);

            // Properties
            builder.Property(e => e.Actor)
                .HasMaxLength(32)
                .IsRequired();

            builder.Property(e => e.Action)
                .HasMaxLength(48)
                .IsRequired();

            builder.Property(e => e.Target)
                .HasMaxLength(128)
                .IsRequired();

            builder.Property(e => e.IndirectTarget)
                .HasMaxLength(128);

            builder.Property(e => e.Timestamp)
                .IsRequired();
        }
    }
}

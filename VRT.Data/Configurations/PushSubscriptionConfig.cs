﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class PushSubscriptionConfig : IEntityTypeConfiguration<PushSubscription>
    {
        public void Configure(EntityTypeBuilder<PushSubscription> builder)
        {
            // Table and schema
            builder.ToTable("PushSubscriptions");

            // Primary key
            builder.HasKey(s => s.UserId);

            // Properties
            builder.Property(s => s.Endpoint)
                .IsRequired();

            builder.Property(s => s.P256DH)
                .IsRequired();

            builder.Property(s => s.Auth)
                .IsRequired();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class VenueConfig : IEntityTypeConfiguration<Venue>
    {
        public void Configure(EntityTypeBuilder<Venue> builder)
        {
            // Table and schema
            builder.ToTable("Venues");

            // Primary key
            builder.HasKey(v => v.Id);

            // Constraints
            builder.HasIndex(v => v.Name)
                .IsUnique();

            // Relationships
            builder.HasMany<Event>(v => v.Events)
                .WithOne(e => e.Venue)
                .HasForeignKey(e => e.VenueId)
                .OnDelete(DeleteBehavior.Cascade);

            // Properties
            builder.Property(v => v.Name)
                .HasMaxLength(64)
                .IsUnicode()
                .IsRequired();

            builder.Property(v => v.Address)
                .HasMaxLength(255)
                .IsUnicode()
                .IsRequired();

            builder.Property(v => v.ColorCode)
                .HasMaxLength(7)
                .IsFixedLength()
                .IsRequired();
        }
    }
}

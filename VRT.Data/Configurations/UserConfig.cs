﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            // Table and Schema
            builder.ToTable("Users");


            // Primary Key
            builder.HasKey(u => u.Id);


            // Constraints
            builder.HasIndex(u => u.Username)
                .IsUnique();

            builder.HasIndex(u => u.DisplayName)
                .IsUnique();

            builder.HasIndex(u => u.Email)
                .IsUnique();

            
            // Relationships
            builder.HasMany<Registration>(u => u.Registrations)
                .WithOne(r => r.User)
                .HasForeignKey(r => r.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne<AccountRecoveryToken>(u => u.RecoveryToken)
                .WithOne(t => t.User)
                .HasForeignKey<AccountRecoveryToken>(t => t.UserId);

            builder.HasOne<RefreshToken>(u => u.RefreshToken)
                .WithOne(t => t.User)
                .HasForeignKey<RefreshToken>(t => t.UserId);

            builder.HasOne<PushSubscription>(u => u.PushSubscription)
                .WithOne(s => s.User)
                .HasForeignKey<PushSubscription>(s => s.UserId);

            // Properties
            builder.Property(u => u.Username)
                .HasMaxLength(32)
                .IsRequired();

            builder.Property(u => u.Password)
                .HasMaxLength(60)
                .IsFixedLength()
                .IsRequired();

            builder.Property(u => u.DisplayName)
                .HasMaxLength(64)
                .IsUnicode()
                .IsRequired();

            builder.Property(u => u.Role)
                .HasMaxLength(8)
                .IsRequired();

            builder.Property(u => u.Created)
                .IsRequired();
        }
    }
}

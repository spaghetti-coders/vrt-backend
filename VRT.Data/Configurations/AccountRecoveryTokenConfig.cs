﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class AccountRecoveryTokenConfig : IEntityTypeConfiguration<AccountRecoveryToken>
    {
        public void Configure(EntityTypeBuilder<AccountRecoveryToken> builder)
        {
            // Table & Schema
            builder.ToTable("AccountRecoveryTokens");

            // Primary Key
            builder.HasKey(t => t.UserId);
            builder.Property(t => t.UserId)
                .ValueGeneratedNever();

            // Constraints
            builder.HasIndex(t => t.Token)
                .IsUnique();
      
            // Properties
            builder.Property(t => t.Token)
                .HasMaxLength(12)
                .IsFixedLength()
                .IsRequired();
        }
    }
}

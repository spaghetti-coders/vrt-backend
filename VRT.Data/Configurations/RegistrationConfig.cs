﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VRT.Data.Entities;

namespace VRT.Data.Configurations
{
    public class RegistrationConfig : IEntityTypeConfiguration<Registration>
    {
        public void Configure(EntityTypeBuilder<Registration> builder)
        {
            // Table and schema
            builder.ToTable("Registrations");

            // Primary key
            builder.HasKey(r => new { r.UserId, r.EventId });
        }
    }
}

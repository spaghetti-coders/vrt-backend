﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;


namespace VRT.Data
{
    /// <summary>
    /// This factory class is used to instantiate the VRT database context during design time; e.g., when creating migrations.
    /// </summary>
    public class VRTContextFactory : IDesignTimeDbContextFactory<VRTContext>
    {
        public VRTContext CreateDbContext(string[] args)
        {
            // Configure DB
            var optionsBuilder = new DbContextOptionsBuilder<VRTContext>();

            // Build an IConfigurationRoot using environment variables
            var configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .Build();
            
            // Retrieve connection string from $DB_CONNECTION_STRING 
            optionsBuilder.UseNpgsql(configuration["DB_CONNECTION_STRING"]);

            return new VRTContext(optionsBuilder.Options);
        }
    }
}

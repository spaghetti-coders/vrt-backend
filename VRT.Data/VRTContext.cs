﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using VRT.Data.Configurations;
using VRT.Data.Entities;

namespace VRT.Data
{
    public class VRTContext : DbContext
    {
        public VRTContext(DbContextOptions<VRTContext> options) : base(options) { }

        #region Properties

        public DbSet<User> Users { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<AdminLogEntry> AdminLogEntries { get; set; }
        public DbSet<AccountRecoveryToken> RecoveryTokens { get; set; }
        public DbSet<PushSubscription> PushSubscriptions { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        #endregion

        #region Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("VRT");

            modelBuilder.ApplyConfiguration<User>(new UserConfig());
            modelBuilder.ApplyConfiguration<Venue>(new VenueConfig());
            modelBuilder.ApplyConfiguration<Event>(new EventConfig());
            modelBuilder.ApplyConfiguration<Registration>(new RegistrationConfig());
            modelBuilder.ApplyConfiguration<AdminLogEntry>(new AdminLogEntryConfig());
            modelBuilder.ApplyConfiguration<AccountRecoveryToken>(new AccountRecoveryTokenConfig());
            modelBuilder.ApplyConfiguration<PushSubscription>(new PushSubscriptionConfig());
            modelBuilder.ApplyConfiguration<RefreshToken>(new RefreshTokenConfig());
 
            SeedData(modelBuilder);
        }
        protected void SeedData(ModelBuilder modelBuilder)
        {
            // Create an admin user
            modelBuilder.Entity<User>().HasData(
                new User 
                {
                    Id = 1,
                    Username = "admin",
                    Password = "$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26",
                    DisplayName = "Administrator",
                    Created = DateTime.UtcNow,
                    Role = "admin",
                    Status = 1,
                    ReceiveNotifications = false
                }  
            );

            // Create a recovery token for 'admin', so that the account password can be reset
            modelBuilder.Entity<AccountRecoveryToken>().HasData(
                new AccountRecoveryToken
                {
                    UserId = 1,
                    Token = "ResetAdminPW",
                    ExpiresAt = DateTime.UtcNow.AddYears(10)
                } 
            );
        }

        #endregion
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VRT.Data.Migrations
{
    public partial class AddedEmailUniqueConstraint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                schema: "VRT",
                table: "Users",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_Email",
                schema: "VRT",
                table: "Users");
        }
    }
}

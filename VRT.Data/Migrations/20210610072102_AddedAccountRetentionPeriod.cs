﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VRT.Data.Migrations
{
    public partial class AddedAccountRetentionPeriod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "VRT",
                table: "Users");

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionDate",
                schema: "VRT",
                table: "Users",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                schema: "VRT",
                table: "Users",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletionDate",
                schema: "VRT",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Status",
                schema: "VRT",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "VRT",
                table: "Users",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VRT.Data.Migrations
{
    public partial class NPGSQL6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "VRT",
                table: "Venues",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "VRT",
                table: "Venues",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeletionDate",
                schema: "VRT",
                table: "Users",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Created",
                schema: "VRT",
                table: "Users",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Registered",
                schema: "VRT",
                table: "Registrations",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpiresAt",
                schema: "VRT",
                table: "RefreshTokens",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Timestamp",
                schema: "VRT",
                table: "LogEntries",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                schema: "VRT",
                table: "Events",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpiresAt",
                schema: "VRT",
                table: "AccountRecoveryTokens",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.InsertData(
                schema: "VRT",
                table: "Users",
                columns: new[] { "Id", "Created", "DeletionDate", "DisplayName", "Email", "Password", "ReceiveNotifications", "Role", "Status", "Username" },
                values: new object[] { 1, new DateTime(2022, 6, 7, 16, 27, 13, 300, DateTimeKind.Utc).AddTicks(6265), null, "Administrator", null, "$2a$04$d7xq03sjhQb0YmLllZr1/OuDAuU33j0BNAARlC73YljCf7sSfck26", false, "admin", 1, "admin" });

            migrationBuilder.InsertData(
                schema: "VRT",
                table: "AccountRecoveryTokens",
                columns: new[] { "UserId", "ExpiresAt", "Token" },
                values: new object[] { 1, new DateTime(2032, 6, 7, 16, 27, 13, 300, DateTimeKind.Utc).AddTicks(6361), "ResetAdminPW" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "VRT",
                table: "AccountRecoveryTokens",
                keyColumn: "UserId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "VRT",
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeletionDate",
                schema: "VRT",
                table: "Users",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Created",
                schema: "VRT",
                table: "Users",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Registered",
                schema: "VRT",
                table: "Registrations",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpiresAt",
                schema: "VRT",
                table: "RefreshTokens",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Timestamp",
                schema: "VRT",
                table: "LogEntries",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                schema: "VRT",
                table: "Events",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpiresAt",
                schema: "VRT",
                table: "AccountRecoveryTokens",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.InsertData(
                schema: "VRT",
                table: "Venues",
                columns: new[] { "Id", "Address", "Capacity", "ColorCode", "IsActive", "Name" },
                values: new object[,]
                {
                    { 1, "Tannenbergallee 11, 30163 Hannover", 36, "#63afae", true, "Tannenbergallee" },
                    { 2, "Nußriede 4B,30627 Hannover", 16, "#800080", true, "Nußriede" }
                });
        }
    }
}

﻿using System;

namespace VRT.Data.Entities
{
    /// <summary>
    /// Represents a user's registration to attend an event.
    /// </summary>
    public class Registration
    {
        #region Properties

        /// <summary>
        /// The ID of the user that is registering to attend an event.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// The ID of the event that the user is registering to attend.
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// Represents the status of the registration (1 = active participant, 2 = on waiting list)
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// The creation date and time of this registration.
        /// </summary>
        public DateTime Registered { get; set; }

        #endregion

        #region Navigation Properties

        /// <summary>
        /// The user that is registering to attend an event.
        /// </summary>
        public virtual User User { get; set; }
        /// <summary>
        /// The event that the user is registering to attend.
        /// </summary>
        public virtual Event Event { get; set; }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;

namespace VRT.Data.Entities
{
    /// <summary>
    /// Represents an event/seesion that is being hosted/held at a specific date time and venue.
    /// </summary>
    public class Event
    {
        /// <summary>
        /// Constructs a new Event object.
        /// </summary>
        public Event()
        {
            Registrations = new List<Registration>();
        }


        #region Properties

        /// <summary>
        /// The event's ID.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The ID of the venue where this event is being hosted.
        /// </summary>
        public int VenueId { get; set; }
        /// <summary>
        /// The date and time this event is scheduled to be held at.
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// The event's name (e.g., 'Fun and Games 2021').
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// An optional, short description about this event (e.g., 'Bring your own snacks!'). 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The maximum number of participants that can attend the event. 
        /// </summary>
        public int Capacity { get; set; }
        /// <summary>
        /// The number of users that are currently registered to this event.
        /// </summary>
        public int RegistrationCount { get; set; }
        /// <summary>
        /// The number of users that are currently on this event's waiting list.
        /// </summary>
        public int WaitingListCount { get; set; }
        /// <summary>
        /// Indicates whether users can register to take part in this event.
        /// </summary>
        public bool IsOpenForRegistration { get; set; }

        #endregion

        #region Navigation Properties

        /// <summary>
        /// The venue at which this event is being held at.
        /// </summary>
        public virtual Venue Venue { get; set; }
        /// <summary>
        /// A collection of user-registrations for this event (past & future).
        /// </summary>
        public virtual ICollection<Registration> Registrations { get; set; }

        #endregion
    }
}

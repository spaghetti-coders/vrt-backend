﻿using System.Collections.Generic;

namespace VRT.Data.Entities
{
    /// <summary>
    /// Represents a venue at which sessions and events can be held.
    /// </summary>
    public class Venue
    {
        /// <summary>
        /// Constructs a new Venue object.
        /// </summary>
        public Venue()
        {
            Events = new List<Event>();
        }

        #region Properties

        /// <summary>
        /// The venue's ID.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The venue's name (e.g., 'Sports Gym 2, Hannover').
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The address of the venue.
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// The venue's capacity in terms of participants/people it can hold.
        /// </summary>
        public int Capacity { get; set; }
        /// <summary>
        /// Indicates whether events or sessions can be scheduled at this venue.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// The hex-code used to color-code events hosted at this venue in. 
        /// </summary>
        public string ColorCode { get; set; }

        #endregion

        #region Navigation Properties

        /// <summary>
        /// A collection of events that are going to be, or have been held at this venue.
        /// </summary>
        public virtual ICollection<Event> Events { get; set; }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Data.Entities
{
    /// <summary>
    /// Represents a user's subscription to receive push notifications.
    /// </summary>
    public class PushSubscription
    {
        /// <summary>
        /// The subscribed user's ID.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// The endpoint to which push notifications for this user must be sent.
        /// </summary>
        public string Endpoint { get; set; }
        /// <summary>
        /// The subscription's public key (P-256 ECDH Diffie-Hellman).
        /// </summary>
        public string P256DH { get; set; }
        /// <summary>
        /// The subscription's authentication secret.
        /// </summary>
        public string Auth { get; set; }

        /// <summary>
        /// The subscribed user.
        /// </summary>
        public virtual User User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Data.Entities
{
    /// <summary>
    /// Represents a log entry of an action performed by an administrator. 
    /// </summary>
    public class AdminLogEntry
    {
        /// <summary>
        /// The ID of the log entry.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The username of the actor.
        /// </summary>
        public string Actor { get; set; }
        /// <summary>
        /// The action that was performed by the actor.
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// The indirect target of an action, e.g., the name of a user that was removed from an even (the target).
        /// </summary>
        public string IndirectTarget { get; set; }
        /// <summary>
        /// The target of an action, e.g., the of an event/user/venue
        /// </summary>
        public string Target { get; set; }
        /// <summary>
        /// The date and time the action was performed.
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}

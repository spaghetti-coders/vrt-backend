﻿using System;
using System.Collections.Generic;


namespace VRT.Data.Entities
{
    /// <summary>
    /// Represents a user of the VRT application.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Constructs a new User object.
        /// </summary>
        public User()
        {
            Registrations = new List<Registration>();
        }


        #region Properties

        /// <summary>
        /// The user's ID.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The user's username (e.g., 'jdoe93').
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// The user's display name (e.g., 'Jane Doe').
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// The user's email address (e.g,, 'jdoe@gmail.com'; may be null).
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// The user's salted password hash.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// The user's account status (0 = 'Pending Registration', 1 = 'Active', 2 = 'Inactive', 3 = 'Pending Deletion').
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// The user's role; e.g. ('user').
        /// </summary>
        /// <see cref="Roles"/>
        public string Role { get; set; }
        /// <summary>
        /// Boolean indicating whether the user wants to receive notifications from the application.
        /// </summary>
        public bool ReceiveNotifications { get; set; }
        /// <summary>
        /// The date and time when this user account was created.
        /// </summary>
        public DateTime Created { get; set; }
        /// <summary>
        /// The date and time that this user account is scheduled to be deleted.
        /// </summary>
        public DateTime? DeletionDate { get; set; }

        #endregion

        #region Navigation Properties

        /// <summary>
        /// The user's account recovery token, if any.
        /// </summary>
        public virtual AccountRecoveryToken RecoveryToken { get; set; }
        /// <summary>
        /// The user's current refresh token, if any.
        /// </summary>
        public virtual RefreshToken RefreshToken { get; set; }
        /// <summary>
        /// The user' push subscription, if any.
        /// </summary>
        public virtual PushSubscription PushSubscription { get; set; }
        /// <summary>
        /// A collection of the user's event-registrations (past & future).
        /// </summary>
        public virtual ICollection<Registration> Registrations { get; set; }

        #endregion
    }
}

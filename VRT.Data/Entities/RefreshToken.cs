﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Data.Entities
{
    /// <summary>
    /// Implements a JWT refresh token issued to a user.
    /// </summary>
    public class RefreshToken
    {
        /// <summary>
        /// The ID of the user to whom this token was issued.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// The actual refresh token.
        /// </summary>
        public string TokenString { get; set; }
        /// <summary>
        /// The refresh token's expiration date and time.
        /// </summary>
        public DateTime ExpiresAt { get; set; }

        /// <summary>
        /// The user for whom this token was generated.
        /// </summary>
        public virtual User User { get; set; }
    }
}

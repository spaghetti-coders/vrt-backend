﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Data.Entities
{
    /// <summary>
    /// The entity represents a token that can be used to recover a user's account (i.e., will allow them to reset their password).
    /// </summary>
    public class AccountRecoveryToken
    {
        /// <summary>
        /// The ID of user for whom this token was generated.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// The token string.
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// The date and time until this token is valid.
        /// </summary>
        public DateTime ExpiresAt { get; set; }

        /// <summary>
        /// The user for whom this token was generated.
        /// </summary>
        public virtual User User { get; set; }
    }
}

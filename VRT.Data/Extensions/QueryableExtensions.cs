﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace VRT.Data.Extensions
{
    /// <summary>
    /// A Collection of extensions for the <see cref="IQueryable"/> interface.
    /// </summary>
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Orders the query's result set by the the specified property of the queried objects in ascending order.
        /// </summary>
        /// <typeparam name="T">The type of queried objects</typeparam>
        /// <param name="query">This query</param>
        /// <param name="propertyName">The property by which to order the results set</param>
        /// <param name="comparer">An optional comparer instance</param>
        /// <returns>This query with objects ordered ascendingly by the specified property.</returns>
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "OrderBy", propertyName, comparer);
        }
        /// <summary>
        /// Orders the query's result set by the the specified property of the queried objects in descending order.
        /// </summary>
        /// <typeparam name="T">The type of queried objects</typeparam>
        /// <param name="query">This query</param>
        /// <param name="propertyName">The property by which to order the results set</param>
        /// <param name="comparer">An optional comparer instance</param>
        /// <returns>This query with objects ordered descendingly by the specified property.</returns>
        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "OrderByDescending", propertyName, comparer);
        }
        /// <summary>
        /// Orders the query's result set a second time, by the the specified property of the queried objects in ascending order.
        /// </summary>
        /// <typeparam name="T">The type of queried objects</typeparam>
        /// <param name="query">This query</param>
        /// <param name="propertyName">The property by which to order the results set</param>
        /// <param name="comparer">An optional comparer instance</param>
        /// <returns>This query with objects ordered ascendingly by the specified property.</returns>
        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "ThenBy", propertyName, comparer);
        }
        /// <summary>
        /// Orders the query's result set a second time, by the the specified property of the queried objects in descending order.
        /// </summary>
        /// <typeparam name="T">The type of queried objects</typeparam>
        /// <param name="query">This query</param>
        /// <param name="propertyName">The property by which to order the results set</param>
        /// <param name="comparer">An optional comparer instance</param>
        /// <returns>This query with objects ordered descendingly by the specified property.</returns>
        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> query, string propertyName, IComparer<object> comparer = null)
        {
            return CallOrderedQueryable(query, "ThenByDescending", propertyName, comparer);
        }

        /// <summary>
        /// Builds the Queryable functions using a TSource property name.
        /// </summary>
        public static IOrderedQueryable<T> CallOrderedQueryable<T>(this IQueryable<T> query, string methodName, string propertyName,
                IComparer<object> comparer = null)
        {
            var param = Expression.Parameter(typeof(T), "x");

            var body = propertyName.Split('.').Aggregate<string, Expression>(param, Expression.PropertyOrField);

            return comparer != null
                ? (IOrderedQueryable<T>)query.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        new[] { typeof(T), body.Type },
                        query.Expression,
                        Expression.Lambda(body, param),
                        Expression.Constant(comparer)
                    )
                )
                : (IOrderedQueryable<T>)query.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        new[] { typeof(T), body.Type },
                        query.Expression,
                        Expression.Lambda(body, param)
                    )
                );
        }
    }
}
